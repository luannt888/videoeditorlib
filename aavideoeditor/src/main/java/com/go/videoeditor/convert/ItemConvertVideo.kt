package com.go.videoeditor.convert

import com.go.videoeditor.transcoder.TranscoderListener
import java.util.concurrent.Future

class ItemConvertVideo(var id: Int, var filePath: String?){
    var title: String? = null
     var name: String? = null
    var content: String? = null
    var task: Future<Void>? = null
    var listener: TranscoderListener? = null
    var thumbnail: String? = null
}
