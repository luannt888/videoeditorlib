package com.go.videoeditor.convert

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.go.videoeditor.R
import com.go.videoeditor.app.Constant
import com.go.videoeditor.downloadVideo.DownloadManager
import com.go.videoeditor.downloadVideo.DownloadVideoAdapter
import com.go.videoeditor.downloadVideo.ItemDownloadVideo
import com.go.videoeditor.listener.OnNotifyDataChangeListener
import com.go.videoeditor.service.ServiceUtil
import com.go.videoeditor.upload.ItemUploadVideo
import com.go.videoeditor.upload.UploadManager
import com.go.videoeditor.upload.UploadVideoAdapter
import kotlinx.android.synthetic.main.fragment_task_manager_ve.*
import java.util.*

class TaskBackgroundFragment : Fragment() {
    var isTabSelected: Boolean = false
    var isViewCreated: Boolean = false
    private var filePath: String? = null
    var url: String? = null
    lateinit var uploadAdapter: UploadVideoAdapter
    lateinit var downloadAdapter: DownloadVideoAdapter
    lateinit var convertAdapter: ConvertVideoAdapter
    var itemUploadList: ArrayList<ItemUploadVideo> = ArrayList()
    var itemDownList: ArrayList<ItemDownloadVideo> = ArrayList()
    var itemConvertList: ArrayList<ItemConvertVideo> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_task_manager_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /*dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog.behavior.skipCollapsed = true
            bottomSheetDialog.behavior.isDraggable = true
            bottomSheetDialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }*/
        isViewCreated = true
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 1000)
            return
        }
        /*if (isTabSelected) {
            initData()
            return
        }
        isTabSelected = true*/
        initView()
        initData()
        initControl()
    }

    private fun initView() {
//        textHeaderTitle.text = getString(R.string.task_backgroud)
//        buttonDone.visibility = View.GONE
        initAdapter()
    }

    private fun initControl() {
    }

    fun initData() {
        itemDownList.clear()
        itemConvertList.clear()
        itemUploadList.clear()
        itemDownList.addAll(DownloadManager.getDownloadingVideoList()!!)
        itemConvertList.addAll(ConvertManager.getInstance()!!.getListItemUpload()!!)
        itemUploadList.addAll(UploadManager.getInstance()!!.getListItemUpload()!!)
        if (itemDownList.size == 0 &&
            itemConvertList.size == 0 &&
            itemUploadList.size == 0
        ) {
            textNotify.visibility = View.VISIBLE
        } else {
            textNotify.visibility = View.GONE
        }
        notifyDataChange()
    }

    private fun uploadVideo(item: ItemUploadVideo) {
        ServiceUtil.uploadVideo(item, activity!!)
//        val imageFile = FileUltis.getThumbnailFile(activity, item.filePath)
//        item.thumbnail = imageFile?.absolutePath
//        var myHttpRequest = MyHttpRequest(activity)
//        item.myHttpRequest = myHttpRequest
//        Loggers.e("Upload", "onStart ")
//        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
//        var handler = Handler(Looper.getMainLooper()!!)
//        myHttpRequest.uploadVideo(
//                ServiceUtil.validAPI(Constant.API_UPLOAD_VIDEO),
//                imageFile.absolutePath,
//                item.filePath, item.title, item.name, item.content,
//                object : MyHttpRequest.ResponseListener {
//                    override fun onFailure(statusCode: Int) {
//                        Log.e("Upload", "onFailure " + statusCode)
//                        handler.post {
//                            UploadManager.getInstance()?.getItemUpload(item.id)?.listener?.onFailure(statusCode)
//                        }
//                    }
//
//                    override fun onSuccess(statusCode: Int, responseString: String?) {
//
//                        handler.post {
//                            UploadManager.getInstance()?.getItemUpload(item.id)?.listener?.onSuccess(statusCode, responseString)
//                        }
//
//                    }
//                }, object : FileRequestBody.FileRequestBodyHttpListener {
//            override fun onProgress(readSize: Long, totalSixe: Long) {
//                handler.post {
//                    UploadManager.getInstance()?.getItemUpload(item.id)?.progressListener?.onProgress(readSize, totalSixe)
//                }
//            }
//        })
    }

    fun initAdapter() {
        val style = Constant.STYLE_HORIZONTAL
        uploadAdapter = UploadVideoAdapter(activity!!, itemUploadList, style, 1)
        val layoutManager = StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL)
        uploadRecyclerView.layoutManager = layoutManager
        uploadAdapter.onNotifyDataChangeListener = object : OnNotifyDataChangeListener {
            override fun onChange() {
                Handler(Looper.getMainLooper()).post {
                    initData()
                }
            }
        }
        uploadRecyclerView.adapter = uploadAdapter

        downloadAdapter = DownloadVideoAdapter(activity!!, itemDownList, style, 1)
        val layoutManager2 = StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL)
        downloadRecyclerView.layoutManager = layoutManager2
        downloadRecyclerView.adapter = downloadAdapter
        downloadAdapter.onNotifyDataChangeListener = object : OnNotifyDataChangeListener {
            override fun onChange() {
                Handler(Looper.getMainLooper()).post {
                    initData()
                }
            }
        }

        convertAdapter = ConvertVideoAdapter(activity!!, itemConvertList, style, 1)
        val layoutManager3 = StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL)
        convertRecyclerView.layoutManager = layoutManager3
        convertRecyclerView.adapter = convertAdapter
        convertAdapter.onNotifyDataChangeListener = object : OnNotifyDataChangeListener {
            override fun onChange() {
                Handler(Looper.getMainLooper()).post {
                    initData()
                }
            }
        }
    }

    fun notifyDataChange() {
        if (itemConvertList.size > 0) {
            layoutConvert.visibility = View.VISIBLE
        } else {
            layoutConvert.visibility = View.GONE
        }

        if (itemDownList.size > 0) {
            layoutDownload.visibility = View.VISIBLE
        } else {
            layoutDownload.visibility = View.GONE
        }

        if (itemUploadList.size > 0) {
            layoutUpload.visibility = View.VISIBLE
        } else {
            layoutUpload.visibility = View.GONE
        }
        uploadAdapter?.notifyDataSetChanged()
        convertAdapter?.notifyDataSetChanged()
        downloadAdapter?.notifyDataSetChanged()
    }


}