package com.go.videoeditor.convert

import com.go.videoeditor.utils.FileUltis
import okhttp3.internal.and
import java.io.UnsupportedEncodingException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.concurrent.ConcurrentHashMap

class ConvertManager {

    var currentRequestMap: ConcurrentHashMap<Int, ItemConvertVideo>? = null

    companion object {
        private var instance: ConvertManager? = null
        fun getInstance(): ConvertManager? {
            if (instance == null) {
                instance = ConvertManager()
            }
            return instance
        }
    }

    init {
        currentRequestMap = ConcurrentHashMap()
    }

    fun putItemConvertVideo(itemUpload: ItemConvertVideo?) {
        if (itemUpload == null) {
            return
        }
        cancel(itemUpload.id)
        currentRequestMap?.put(itemUpload.id, itemUpload)
    }

    fun getItemConvertVideo(id: Int): ItemConvertVideo? {
        if (currentRequestMap != null && currentRequestMap!!.containsKey(id)) {
            return currentRequestMap?.get(id)
        } else return null
    }

    fun getUniqueId(filePath: String): Int {
        val string = filePath
        val hash: ByteArray
        hash = try {
            MessageDigest.getInstance("MD5").digest(string.toByteArray(charset("UTF-8")))
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException("NoSuchAlgorithmException", e)
        } catch (e: UnsupportedEncodingException) {
            throw RuntimeException("UnsupportedEncodingException", e)
        }
        val hex = StringBuilder(hash.size * 2)
        for (b in hash) {
            if (b and 0xFF < 0x10) hex.append("0")
            hex.append(Integer.toHexString(b and 0xFF))
        }
        return hex.toString().hashCode()
    }

    fun getListItemUpload(): ArrayList<ItemConvertVideo>? {
        var list: ArrayList<ItemConvertVideo> = ArrayList()
        for ((_, item) in currentRequestMap!!) {
            list.add(item)
        }
        return list
    }

    fun cancelAll() {
        for ((id, item) in currentRequestMap!!) {
            cancel(id)
        }
    }

    fun cancel(uploadId: Int) {
        if (currentRequestMap!!.containsKey(uploadId) && currentRequestMap!!.get(uploadId) != null) {
            var item = currentRequestMap!!.get(uploadId)
            if (item != null && item.task != null && !item.task!!.isDone) {
                FileUltis.removeFilePath(item.filePath)
            }
            item?.task?.cancel(true)
        }
        currentRequestMap?.remove(uploadId)
    }
}