package com.go.videoeditor.convert

import android.content.Context
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.downloader.request.DownloadRequest
import com.go.videoeditor.R
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.listener.OnNotifyDataChangeListener
import com.go.videoeditor.transcoder.TranscoderListener
import kotlinx.android.synthetic.main.item_upload_video_ve.view.*
import java.util.*
import java.util.concurrent.ConcurrentHashMap

class ConvertVideoAdapter(
    val activity: Context, val itemList: ArrayList<ItemConvertVideo>, val style: Int,
    val
    numberColumn: Int,
) : RecyclerView.Adapter<ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0
    var onNotifyDataChangeListener: OnNotifyDataChangeListener? = null
    companion object {
        var currentRequestMap: Map<Int, DownloadRequest>? = null
            get() = if (field == null) ConcurrentHashMap<Int, DownloadRequest>() else field
    }

    override fun getItemViewType(position: Int): Int {
        val viewType = style
        return viewType
    }

    fun initViewSize(viewType: Int) {
//        val screenSize = ScreenSize(activity)
//        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
//            screenSize.width
        val height: Int = activity.resources.getDimensionPixelSize(R.dimen.dimen_150)
        when (viewType) {
            Constant.STYLE_HORIZONTAL -> {
                imageWidth = height * 2 / 3
                imageHeight = height
            }
        }
//        Loggers.e("initViewSize: $viewType", "$imageWidth x $imageHeight")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initViewSize(viewType)
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_upload_video_ve,
            parent, false
        )
        return VideoViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemObj: ItemConvertVideo = itemList.get(position)
        if (holder is VideoViewHolder) {
            bindDataFrameView(holder, itemObj, position)
        }
    }

    fun bindDataFrameView(holder: VideoViewHolder, itemObj: ItemConvertVideo, position: Int) {
        if (itemObj.thumbnail != null) {
            MyApplication.getInstance()?.loadImage(activity, holder.imageThumbnail, itemObj.thumbnail!!)
        } else {
            MyApplication.getInstance()?.loadImage(activity, holder.imageThumbnail, R.drawable.img_loading)
        }
        holder.textTitle.text = itemObj.title
//        holder.textDuration.text = VideoEditUtils.convertMiliSecondsToTime(itemObj.duration!!)
        holder.layoutRoot.setOnClickListener {
//            onItemClickListener?.onClick(itemObj, position, holder)
        }
        holder.buttonCancel.setOnClickListener {
            resetConvert(itemObj)
            onNotifyDataChangeListener?.onChange()
            onItemClickListener?.onCancelClick()
        }
        setListenerUpload(itemObj, holder)
    }


    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class VideoViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imageThumbnail = itemView.imageThumbnail
        val layoutImage = itemView.layoutImage
        val textTitle = itemView.textTitle
        val textDuration = itemView.textDuration
        val buttonPause = itemView.buttonPause
        val buttonCancel = itemView.buttonDownload
        val layoutDowload = itemView.layoutDowload
        val buttonFolder = itemView.buttonFolder
        val tvProgress = itemView.tvProgress
        val progress = itemView.progressBar
        val layoutDownloaded = itemView.layoutDownloaded
        val tvMsg = itemView.tvMsg

        init {
            buttonCancel.setColorFilter(ContextCompat.getColor(activity, R.color.color_primary), PorterDuff.Mode.SRC_ATOP)
        }
        /*init {
            val lp: LinearLayout.LayoutParams = layoutImage.layoutParams as LinearLayout
            .LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
//            Log.e("FrameViewHolder", "$imageWidth x $imageHeight")
            layoutImage.layoutParams = lp
        }*/
    }

    interface OnItemClickListener {
//        fun onClick(itemObject: ItemVideoServer, position: Int, holder: ViewHolder)
        fun onCancelClick()
//        fun onPlayClick(itemObject: ItemVideoServer, position: Int, holder: ViewHolder)
    }

    private fun setListenerUpload(itemObj: ItemConvertVideo, holder: VideoViewHolder) {

        itemObj.listener = object : TranscoderListener {
            override fun onTranscodeProgress(progress: Double) {
                holder.tvMsg.text = "Converting"
                holder.tvProgress.text = ((progress * 10000).toInt()/100F).toString() + " %"
                val progress = (progress * 100).toInt()
                holder.progress.progress = progress
            }

            override fun onTranscodeCompleted(successCode: Int) {
                onNotifyDataChangeListener?.onChange()
                resetConvert(itemObj)
            }

            override fun onTranscodeCanceled() {
                onNotifyDataChangeListener?.onChange()
                resetConvert(itemObj)
            }

            override fun onTranscodeFailed(exception: Throwable) {
                resetConvert(itemObj)
                onNotifyDataChangeListener?.onChange()
            }
        }
    }

    private fun resetConvert(itemObj: ItemConvertVideo) {
      itemObj?.task?.cancel(true)
        ConvertManager.getInstance()?.cancel(itemObj.id)
        itemList.clear()
        itemList.addAll(ConvertManager.getInstance()?.getListItemUpload()!!)
        notifyDataSetChanged()
    }
}