package com.go.videoeditor.fragment

import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.go.videoeditor.R
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.MyApplication
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_image_zoom_ve.*

class ImageZoomFragment : BottomSheetDialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_image_zoom_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setOnShowListener {
            val bottomSheetInternal: View? =
                (it as BottomSheetDialog).findViewById(R.id.design_bottom_sheet)
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetInternal!!)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            bottomSheetBehavior.isDraggable = false
            bottomSheetBehavior.skipCollapsed = true
//            setFullScreen(true)
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                initData()
            }, Constant.DELAY_HANDLE)
        }
    }

    fun initData() {
        val filePath = arguments?.getString(Constant.DATA) ?: return
        MyApplication.getInstance()?.loadImage(getActivity(), imageView, filePath)
        buttonClose.setOnClickListener {
            dismiss()
        }
    }

    fun setFullScreen(isFullScreen: Boolean) {
        if (isFullScreen) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                requireActivity().window.setFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN
                )
            } else {
            }
        } else {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            } else {
            }
        }
    }

    override fun onCancel(dialog: DialogInterface) {
        dismissDialog()
        super.onCancel(dialog)
    }

    override fun onDismiss(dialog: DialogInterface) {
        dismissDialog()
        super.onDismiss(dialog)
    }

    fun dismissDialog() {
//        setFullScreen(false)
    }

}