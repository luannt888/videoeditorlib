package com.go.videoeditor.fragment

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.*
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.adapter.MediaStorageAdapter
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.model.ItemFile
import com.go.videoeditor.utils.*
import com.skydoves.powermenu.*
import kotlinx.android.synthetic.main.fragment_media_storage_ve.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class MediaStorageFragment : Fragment() {
    var isTabSelected: Boolean = false
    var isViewCreated: Boolean = false
    var isPlaying: Boolean = false
    var itemList: ArrayList<ItemFile> = ArrayList()
    var adapter: MediaStorageAdapter? = null
    val DATE_FORMAT = "yyyy/MM/dd HH:mm"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_media_storage_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isViewCreated = true
        checkRefresh()
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 1000)
            return
        }
        if (isTabSelected) {
            return
        }
        isTabSelected = true
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
        buttonDone.visibility = View.GONE
        textHeaderTitle.text = getString(R.string.item_main_media_storage)
        textHeaderTitle.setTextColor(ContextCompat.getColor(requireActivity(), R.color.dark_text))
        buttonClose.setImageResource(R.mipmap.ic_back_ve)
        buttonClose.setColorFilter(
            ContextCompat.getColor(requireActivity(), R.color.dark_text),
            PorterDuff.Mode.SRC_ATOP
        )
        layoutRefresh.isEnabled = false
    }

    fun initData() {
        initAdapter()
        getFileList()
        progressBar.visibility = View.GONE
        if (itemList.size == 0) {
            textNotify.visibility = View.VISIBLE
            return
        }
        adapter?.notifyDataSetChanged()
        recyclerView.scheduleLayoutAnimation()
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            if (isDetached) {
                return@Runnable
            }
            getVideoDuration()
        }, 900)
    }

    fun initAdapter() {
        val spacePx: Int = resources.getDimensionPixelSize(R.dimen.space_item)
        val itemDecoration: RecyclerView.ItemDecoration = SpacesItemDecoration(spacePx, 1)
        val layoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        recyclerView.setHasFixedSize(true)
        recyclerView.itemAnimator = DefaultItemAnimator()
        adapter = MediaStorageAdapter(requireActivity(), itemList)
        recyclerView.addItemDecoration(itemDecoration)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
        adapter!!.onItemClickListener = object : MediaStorageAdapter.OnItemClickListener {
            override fun onClick(itemObject: ItemFile, position: Int, view: View) {
                when (itemObject.type) {
                    Constant.FILE_TYPE_VIDEO, Constant.FILE_TYPE_AUDIO -> {
                        if (!isPlaying) {
                            isPlaying = true
                            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                                playPreview(itemObject.filePath)
                            }, Constant.DELAY_HANDLE)
                        }
                    }
                    Constant.FILE_TYPE_IMAGE -> {
                        val fragment = ImageZoomFragment()
                        val bundle = Bundle()
                        bundle.putString(Constant.DATA, itemObject.filePath)
                        (activity as BaseActivity).showBottomSheetDialog(fragment, bundle)
                    }
                }
            }

            override fun onLongClick(itemObject: ItemFile, position: Int, view: View) {
            }

            override fun onMoreMenuClick(itemObject: ItemFile, position: Int, view: View) {
                val lastVisible = layoutManager.findLastVisibleItemPositions(null)[0]
                showMoreMenuPopup(itemObject, position, view, lastVisible)
            }
        }
    }

    @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    fun getFileList() {
        val childFolder = requireActivity().getString(R.string.app_name).trim()
            .replace(" ", "-") + "/" + AppUtils.FOLDER_STORAGE

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            var folderPath = Environment.DIRECTORY_MOVIES + "/" + childFolder
            val itemListTemp1 = AppUtils.getFileListFromFolder(requireActivity(), folderPath)
            itemList.addAll(itemListTemp1)

            folderPath = Environment.DIRECTORY_PICTURES + "/" + childFolder
            val itemListTemp2 = AppUtils.getFileListFromFolder(requireActivity(), folderPath)
            itemList.addAll(itemListTemp2)
        } else {
            val directory = Environment.getExternalStorageDirectory()
                .getAbsolutePath()
            var folderPath = directory + "/" + Environment.DIRECTORY_MOVIES + "/" + childFolder
            val itemListTemp1 = AppUtils.getFileListFromFolder(requireActivity(), folderPath)
            itemList.addAll(itemListTemp1)

            folderPath = directory + "/" + Environment.DIRECTORY_PICTURES + "/" + childFolder
            val itemListTemp2 = AppUtils.getFileListFromFolder(requireActivity(), folderPath)
            itemList.addAll(itemListTemp2)
        }
        Collections.sort(itemList, object : Comparator<ItemFile> {
            override fun compare(obj1: ItemFile?, obj2: ItemFile?): Int {
                if (obj1 == null || obj2 == null) {
                    return -1
                }
                return obj2.dateLong.compareTo(obj1.dateLong)
            }
        })
    }

    val idBackgroundThreadVideoInfoList = ArrayList<String>()
    fun getVideoDuration() {
        var count = 0
        for (itemObj in itemList) {
            if (itemObj.type != Constant.FILE_TYPE_AUDIO && itemObj.type != Constant.FILE_TYPE_VIDEO) {
                continue
            }
            count++
            val idName = "id_ $count ${Calendar.getInstance().timeInMillis}"
            idBackgroundThreadVideoInfoList.add(idName)
            BackgroundExecutor.execute(object : BackgroundExecutor.Task(idName, 0L, idName) {
                override fun execute() {
                    try {
                        val file = File(itemObj.filePath)
                        if (!file.exists()) {
                            return
                        }
                        if (isDetached) {
                            cancelAllGetVideoInfo()
                            return
                        }
//                        Loggers.e("A_getVideoDuration_ $count", "${itemObj.filePath}")
                        val mediaMetadataRetriever =
                            VideoEditUtils.getMediaMetadataRetriever(context, itemObj.filePath)
                        val timeInMillis = VideoEditUtils.getDurationMsVideo(mediaMetadataRetriever)
                        mediaMetadataRetriever.release()
                        Loggers.e(
                            "getVideoInfo_",
                            "execute done: ${itemObj.title}, timeInMillis = $timeInMillis"
                        )

                        itemObj.duration = timeInMillis
                        if (isDetached) {
                            cancelAllGetVideoInfo()
                            return
                        }
                        UiThreadExecutor.runTask("", {
                            adapter!!.notifyDataSetChanged()
                        }, 0L)
                    } catch (e: Throwable) {
                        e.printStackTrace()
                    }
                }
            })
        }
    }

    fun cancelAllGetVideoInfo() {
        for (i in idBackgroundThreadVideoInfoList.size - 1 downTo 0) {
            BackgroundExecutor.cancelAll(idBackgroundThreadVideoInfoList.get(i), true)
        }
        idBackgroundThreadVideoInfoList.clear()
        UiThreadExecutor.cancelAll("")
    }

    var popupMoreMenu: PowerMenu? = null
    fun showMoreMenuPopup(itemObject: ItemFile, position: Int, view: View, lastVisible: Int) {
        val itemList: ArrayList<PowerMenuItem> = ArrayList()
        if (itemObject.type == Constant.FILE_TYPE_VIDEO) {
            itemList.add(PowerMenuItem(getString(R.string.item_edit)))
        }
        itemList.add(PowerMenuItem(getString(R.string.item_delete)))
        val screenSize = ScreenSize(requireActivity())
        val popupWidth = screenSize.width * 2 / 3
//        val popupHeight = screenSize.height / 2
        popupMoreMenu = PowerMenu.Builder(requireActivity())
            .addItemList(itemList)
            .setAnimation(MenuAnimation.SHOWUP_TOP_RIGHT)
            .setAutoDismiss(true)
            .setShowBackground(true)
            .setWidth(popupWidth)
//                .setHeight(popupHeight)
            .setMenuRadius(10f)
            .setMenuShadow(10f)
            .setDividerHeight(1)
            .setDivider(ColorDrawable(ContextCompat.getColor(requireActivity(), R.color.dark_text)))
            .setCircularEffect(CircularEffect.BODY)
            .setTextSize(16)
            .setTextGravity(Gravity.START)
            .setTextColor(ContextCompat.getColor(requireActivity(), R.color.dark_text))
            .setTextTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL))
            .setSelectedTextColor(ContextCompat.getColor(requireActivity(), R.color.dark_text))
            .setMenuColor(Color.parseColor("#f8f8f8"))
            .setSelectedMenuColor(Color.parseColor("#f8f8f8"))
            .setOnMenuItemClickListener(object : OnMenuItemClickListener<PowerMenuItem> {
                override fun onItemClick(position: Int, item: PowerMenuItem?) {
                    popupMoreMenu!!.dismiss()
                    if (item == null) {
                        return
                    }
                    val title = item.title
                    if (title.equals(getString(R.string.item_edit))) {
                        if (activity is BaseActivity) {
                            (activity as BaseActivity).goVideoEditorActivity(itemObject.filePath)
                        }
                        return
                    }
                    if (title.equals(getString(R.string.item_delete))) {
                        deleteFile(itemObject)
                    }
                }

            })
            .build()
        if (position == lastVisible) {
            val menuHeight = resources.getDimensionPixelSize(R.dimen.dimen_40) * itemList.size
            popupMoreMenu!!.showAsAnchorRightTop(view, 0, -menuHeight)
        } else {
            popupMoreMenu!!.showAsDropDown(view)
        }
    }

    fun deleteFile(itemObject: ItemFile) {
        itemList.remove(itemObject)
        adapter!!.notifyDataSetChanged()
        AppUtils.deleteFile(requireActivity(), itemObject)
    }

    fun playPreview(filePath: String) {
        isPlaying = true
        val playPreviewFragment = PlayPreviewFragment()
        val bundle = Bundle()
        bundle.putString(Constant.FILE_PATH, filePath)
        playPreviewFragment.arguments = bundle

        playPreviewFragment.show(requireActivity().supportFragmentManager, playPreviewFragment.tag)
        playPreviewFragment.setButtonName("")
        playPreviewFragment.onClickButtonActionListenner = object : View.OnClickListener {
            override fun onClick(v: View?) {
                playPreviewFragment.dismiss()
            }
        }
        playPreviewFragment.onDiaLogOnDismissedListener = object : DialogInterface {
            override fun cancel() {
                isPlaying = false
            }

            override fun dismiss() {
                isPlaying = false
            }
        }
    }

    fun initControl() {
        buttonClose.setOnClickListener {
            requireActivity().finish()
        }
    }

    override fun onDetach() {
        cancelAllGetVideoInfo()
        super.onDetach()
    }

}