package com.go.videoeditor.fragment

import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.activity.VideoEditorActivity
import com.go.videoeditor.adapter.DraftVideoAdapter
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.listener.OnDismissListener
import com.go.videoeditor.merge.ItemVideo
import com.go.videoeditor.utils.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_draft_saved.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*
import org.json.JSONArray
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class DraftSavedFragment : BottomSheetDialogFragment() {
    lateinit var adapter: DraftVideoAdapter
    var itemList: ArrayList<ItemVideo> = ArrayList()
    var onDismissListener: OnDismissListener? = null
    var isSuccess = false
    var videoPath: String? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    val idBackgroundThreadList: ArrayList<String> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_merge_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = true
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        initData()
        initControl()
    }

    fun initData() {
        buttonDone.visibility = View.GONE
        textHeaderTitle.text = getString(R.string.draft_saved)
        getVideoList()
    }

    fun getVideoList() {
        val videoList = SharedPreferencesManager.getVideoList(requireActivity())
        if (videoList == null || videoList.size == 0) {
            (activity as? BaseActivity)?.showToast(getString(R.string.msg_not_video_draft))
            dismiss()
            return
        }
        for (i in 0 until videoList.size) {
            val item = videoList.get(i)
            val arr = item.split(Constant.KEY_SPLIT_DRAFT_VIDEO_ITEM_CONTENT)
            val editData = EncryptUtil.base64Decode(arr[1])
            try {
                val dataArr = JSONArray(editData)
                val itemVideo = ItemVideo(arr[0], editData, null, "Video $i", null, 0)
                for (j in 0 until dataArr.length()) {
                    val itemObj = dataArr.getJSONObject(j)
                    val keys = itemObj.keys()
                    while (keys.hasNext()) {
                        val key = keys.next()
                        val value = itemObj.get(key) as String
                        when(key) {
                            Constant.PROJECT_NAME -> {
                                itemVideo.title = value
                            }
                            Constant.FILE_PATH -> {
                                itemVideo.filePath = value
                            }
                        }
                    }
                }
                itemList.add(itemVideo)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        initAdapter()
        getVideoInfo()
    }

    fun initAdapter() {
        val style = Constant.STYLE_HORIZONTAL
        adapter = DraftVideoAdapter(requireActivity(), itemList, style, 1)
        val layoutManager = StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
        adapter.onItemClickListener = object : DraftVideoAdapter.OnItemClickListener {
            override fun onClick(itemObject: ItemVideo, position: Int, holder: RecyclerView.ViewHolder) {
                Handler(Looper.getMainLooper()).postDelayed(Runnable {
                    val intent = Intent(requireActivity(), VideoEditorActivity::class.java)
                    val bundle = Bundle()
                    bundle.putString(Constant.FILE_PATH, itemObject.filePath)
                    bundle.putString(Constant.KEY, itemObject.key)
                    bundle.putString(Constant.DATA, itemObject.editData)
                    intent.putExtras(bundle)
                    startActivity(intent)
                    dismiss()
                }, 150)
            }
        }
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val firstPos = layoutManager.findFirstCompletelyVisibleItemPositions(null)[0]
                bottomSheetDialog?.behavior?.isDraggable = if (firstPos < 0 || firstPos == 0) true else false
            }
        })
    }

    fun getVideoInfo() {
        for (i in 0 until itemList.size) {
            val itemObj = itemList.get(i)
            val idName = "id${Calendar.getInstance().timeInMillis}"
            idBackgroundThreadList.add(idName)
            BackgroundExecutor.execute(object : BackgroundExecutor.Task(idName, 0L, idName) {
                override fun execute() {
                    Loggers.e("getVideoInfo_$i", "execute done")
                    if (isDetached) {
                        return
                    }
                    try {
                        val mediaMetadataRetriever = MediaMetadataRetriever()
                        val videoUri = Uri.parse(itemObj.filePath)
                        mediaMetadataRetriever.setDataSource(activity, videoUri)
                        var bitmap: Bitmap? =
                            mediaMetadataRetriever.getFrameAtTime(
                                1000,
                                MediaMetadataRetriever.OPTION_CLOSEST_SYNC
                            )
                        bitmap = Bitmap.createScaledBitmap(
                            bitmap!!,
                            VideoEditUtils.THUMB_WIDTH,
                            VideoEditUtils.THUMB_HEIGHT,
                            false
                        )
                        itemObj.bitmap = bitmap
                        val videoFile = File(itemObj.filePath!!)
                        val timeInMillis = OptiCommonMethods.getVideoDuration(requireActivity(), videoFile)
                        val second = TimeUnit.MILLISECONDS.toSeconds(timeInMillis)
                        itemObj.duration = second
//                        itemObj.title = videoFile.nameWithoutExtension
                        mediaMetadataRetriever.release()
                        UiThreadExecutor.runTask(idName, Runnable {
                            adapter.notifyDataSetChanged()
                        }, 0L)
                    } catch (e: Throwable) {
                        e.printStackTrace()
                    }
                }
            })
        }
    }

    fun initControl() {
        buttonClose.setOnClickListener {
            isSuccess = false
            dismiss()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        for (i in 0 until idBackgroundThreadList.size) {
            BackgroundExecutor.cancelAll(idBackgroundThreadList.get(i), true)
        }
        onDismissListener?.onDismiss(isSuccess, videoPath, null)
        super.onDismiss(dialog)
    }
}