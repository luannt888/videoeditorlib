package com.go.videoeditor.fragment

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.go.videoeditor.R
import com.go.videoeditor.app.Constant
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_play_preview_ve.*

class PlayPreviewFragment : BottomSheetDialogFragment() {
    var url: String? = null
    var playerFragment: PlayerFragment? = null
    var autoPlay: Boolean? = true
    private var actionName: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_play_preview_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()
        initControl()
    }

    var onClickButtonActionListenner: View.OnClickListener? = null
    private fun initControl() {
        buttonDownload.setOnClickListener {
            onClickButtonActionListenner?.onClick(buttonDownload)
        }
    }

    fun initData() {
        url = arguments?.getString(Constant.FILE_PATH, null)
        autoPlay = arguments?.getBoolean(Constant.AUTO_PLAY, true)
        if (url.isNullOrEmpty()) {
            dismiss()
            return
        }
        setButtonName(actionName)
        initPlayerFragment()
    }

    fun setButtonName(name: String?) {
        actionName = name
        if (name.isNullOrEmpty()) {
            buttonDownload?.visibility = View.GONE
        } else {
            buttonDownload?.visibility = View.VISIBLE
            buttonDownload?.text = name
        }
    }

    private fun initPlayerFragment() {
//        initFrameLayout()
        playerFragment = PlayerFragment()
        val bundle = Bundle()
        bundle.putString(Constant.FILE_PATH, url)
        bundle.putInt(Constant.SCREEN_TYPE, Constant.TYPE_PLAY_URL)
        playerFragment!!.arguments = bundle
        playerFragment?.setPlayerView(true, true)
        playerFragment?.autoPlay = autoPlay!!
        val fragmentTransaction = childFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayout, playerFragment!!)
        fragmentTransaction.commit()
    }

    private fun initFrameLayout() {
        val layoutParamm: ViewGroup.LayoutParams = frameLayout.layoutParams
        layoutParamm.height = layoutParamm.width * 6 / 19
        frameLayout.layoutParams = layoutParamm
    }

    var onDiaLogOnDismissedListener: DialogInterface? = null
    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onDiaLogOnDismissedListener?.dismiss()
    }
}