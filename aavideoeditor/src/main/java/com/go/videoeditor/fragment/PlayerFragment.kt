package com.go.videoeditor.fragment

import android.app.Dialog
import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.add.cropVideo.widget.GestureEPlayerView
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.listener.OnPlayerStateChangeListener
import com.go.videoeditor.model.ItemVideoTimeLine
import com.go.videoeditor.transcoder.mp4Compose.filter.GlFilter
import com.go.videoeditor.utils.OptiCommonMethods
import com.go.videoeditor.utils.ProgressTracker
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.extractor.ts.DefaultTsPayloadReaderFactory
import com.google.android.exoplayer2.source.*
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource
import com.google.android.exoplayer2.source.hls.DefaultHlsExtractorFactory
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.PlayerControlView
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.exo_player_control_view.*
import kotlinx.android.synthetic.main.fragment_player_ve.*
import java.net.CookieHandler
import java.net.CookieManager
import java.net.CookiePolicy
import java.util.*
import kotlin.collections.ArrayList


class PlayerFragment : Fragment(), Player.Listener, PlayerControlView.VisibilityListener {
    var mediaDataSourceFactory: DataSource.Factory? = null
    var player: ExoPlayer? = null
    var trackSelector: DefaultTrackSelector? = null
    var fileUri: Uri? = null
    lateinit var rootView: View;
    var onPlayerStateChangeListener: OnPlayerStateChangeListener? = null
    var onPlayerStateListenerForTimeLine: OnPlayerStateChangeListener? = null
    var startPosition = 0L
    var endPosition = 0L
    var autoPlay = false
    var screenType: Int = Constant.TYPE_PLAY_FILE


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_player_ve, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initData()
        initControl()
    }

    private fun initControl() {
        buttonFullScreen.setOnClickListener(View.OnClickListener {
            if (!mExoPlayerFullscreen) {
                requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                openFullscreenDialog()
            } else {
                requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                closeFullscreenDialog()
            }
            if (onFullScreenButtonListener == null) {
                return@OnClickListener
            }
            if (mExoPlayerFullscreen) {
                onFullScreenButtonListener!!.onExpand()
            } else {
                onFullScreenButtonListener!!.onMinimize()
            }
        })
    }

    private fun initView() {
        ePlayerView = rootView.findViewById(R.id.ePlayerView)
    }

    var filePath: String? = null
    fun initData() {
        filePath = arguments?.getString(Constant.FILE_PATH, null)
        screenType = arguments?.getInt(Constant.SCREEN_TYPE, Constant.TYPE_PLAY_FILE)!!
        if (filePath.isNullOrEmpty()) {
            return
        }
//        val file = File(filePath)
//        fileUri = Uri.fromFile(file)
        fileUri = Uri.parse(filePath)
        playLink()
    }

    fun playLink() {
        if (player != null) {
            releasePlayer()
        }
        initializePlayer()
        mediaDataSourceFactory = buildDataSourceFactory()
        /*if (videoUriList.size > 0) {
            val concatenatingMediaSource = ConcatenatingMediaSource()
            for (i in 0 until videoUriList.size) {
                val uri = videoUriList.get(i)
                val ms = buildMediaSource(uri, "")
                concatenatingMediaSource.addMediaSource(ms!!)
            }
            player!!.setMediaSource(concatenatingMediaSource, startPosition)
        } else {
            val mediaSource = buildMediaSource(fileUri!!, "")
//            player!!.setMediaSource(mediaSource!!, startPosition)
            player!!.setMediaSource(mediaSource!!)
        }*/

//        startPosition = 15000000L
//        endPosition = 25000000L
//        playerView.setShowMultiWindowTimeBar(false)

        if (screenType == Constant.TYPE_PLAY_URL) {
            val mediaSource = buildMediaSource(fileUri!!, "")
            player!!.setMediaSource(mediaSource)
        } else {
            player!!.setMediaSource(concateVideoSource()!!, false)
//            player!!.setMediaSources(concateVideoSource()!!)
        }
        initSubPlayer()
        player!!.prepare()
        playAllSubPlayer()
    }

    fun playAllSubPlayer() {
        for (player in subPlayerList) {
            player.prepare()
        }
    }

    fun stopAllSubPlayer() {
        for (player in subPlayerList) {
            player.release()
        }
    }

    fun removeAllItemVideoList() {
        stopAllSubPlayer()
        subPlayerList.clear()
        itemVideoTimeLineList.clear()
    }

    fun addItemVideoTimeLine(itemVideoTimeLine: ItemVideoTimeLine) {
        itemVideoTimeLineList.add(itemVideoTimeLine)
    }

    fun removeItemVideoTimeLine(key: String) {
        for (itemVideoTimeLine in itemVideoTimeLineList) {
            if (key.equals(itemVideoTimeLine.key)) {
                itemVideoTimeLineList.remove(itemVideoTimeLine)
                return
            }
        }
    }

    /**
     * Update item video time line
     *
     * @param itemVideoTimeLine
     * after call this method, if want player changer, need call playLink() method
     */
    fun updateItemVideoTimeLine(itemVideoTimeLine: ItemVideoTimeLine) {
        for (obj in itemVideoTimeLineList) {
            if (obj.key.equals(itemVideoTimeLine.key)) {
                obj.filePath = itemVideoTimeLine.filePath
                obj.startPosition = itemVideoTimeLine.startPosition
                obj.endPosition = itemVideoTimeLine.endPosition
                obj.originStartPosition = itemVideoTimeLine.originStartPosition
                obj.originEndPosition = itemVideoTimeLine.originEndPosition
                obj.duration = itemVideoTimeLine.duration
                obj.maxRange = itemVideoTimeLine.maxRange
                return
            }
        }
    }

    fun seekTo(timeInMillisecond: Long) {
//        player?.seekTo(timeInMillisecond)
//        seekToSubPlayerTimeLine(timeInMillisecond)
        seekToPlayer(player!!, timeInMillisecond)
        seekToSubPlayer(timeInMillisecond)
    }

    private fun seekToSubPlayer(position: Long) {
        Loggers.e("seekToSubPlayer_", "------------START--------")
        for (i in 0 until subPlayerList.size) {
            val player = subPlayerList.get(i)
            val timeLine = player.currentTimeline

            val window = Timeline.Window()
            var windowIndex = 0
            var needSeekTo = position
            val totalWindowIndex = timeLine.windowCount
            Loggers.e("seekToSubPlayer_", "totalWindowIndex =  $totalWindowIndex")
            if (totalWindowIndex > 1) {
                for (j in 0 until totalWindowIndex) {
                    var durationWindowIndex = timeLine.getWindow(j, window).durationMs
                    Loggers.e("seekToSubPlayer_A_timeline_ $j", "$durationWindowIndex")
                    if (durationWindowIndex < 0) {// check
                        val itemVideoTimeLine = itemVideoTimeLineList.get(i)
                        durationWindowIndex = itemVideoTimeLine.startPosition
                        Loggers.e("seekToSubPlayer_B_timeline_ $j", "$durationWindowIndex")
                    }
                    if (position <= durationWindowIndex) {
                        windowIndex = 0
                        break
                    }
                    windowIndex = 1
                    needSeekTo = position - durationWindowIndex
                    break
                }
            }
            Loggers.e(
                "seekToSubPlayer_$i",
                "position = $position, windowIndex = $windowIndex, needSeekTo = $needSeekTo"
            )
            if (needSeekTo < 0) {
                return
            }
            player.seekTo(windowIndex, needSeekTo)
        }
        Loggers.e("seekToSubPlayer_", "------------END--------")
        /*for (player in subPlayerList) {
            val currentPos = player.currentPosition
            Loggers.e("seekToSubPlayer_", "currentPos = $currentPos, position = $position")
            player.seekTo(0, position)
        }*/
    }


    private fun seekToSubPlayerTimeLine(position: Long) {
        for (itemVideoTimeline in itemVideoTimeLineList) {
            if (itemVideoTimeline.isPlayAtTime(position)) {
                itemVideoTimeline.player?.seekTo(0, position)
                itemVideoTimeline.player?.playWhenReady = player?.playWhenReady!!
            } else {
                itemVideoTimeline.player?.playWhenReady = false
            }
        }
    }

    private fun initSubPlayer() {
        subPlayerList.clear()
        for (itemVideoTimeLine in itemVideoTimeLineList) {
            addSubPlayer(itemVideoTimeLine)
        }
    }

    private fun addSubPlayer(itemVideoTimeLine: ItemVideoTimeLine) {
        if (itemVideoTimeLine.type != Constant.TYPE_AUDIO) {
            return
        }
        val soundUri = Uri.parse(itemVideoTimeLine.filePath)
        val soundSource = buildMediaSource(soundUri!!, "")
        val subPlayer = createSubPlayer()
        itemVideoTimeLine.player = subPlayer
        subPlayerList.add(subPlayer)
        val mediaSource: MediaSource?
        if (itemVideoTimeLine.startPosition == 0L) {
//            mediaSource = soundSource
            mediaSource =
                ClippingMediaSource(
                    soundSource,
                    itemVideoTimeLine.originStartPosition * 1000,
                    itemVideoTimeLine.endPosition * 1000 + itemVideoTimeLine.originStartPosition * 1000
                )
        } else {
            val duration = itemVideoTimeLine.endPosition - itemVideoTimeLine.startPosition
            mediaSource = ConcatenatingMediaSource(
                SilenceMediaSource(itemVideoTimeLine.startPosition * 1000),
                ClippingMediaSource(
                    soundSource,
                    itemVideoTimeLine.originStartPosition * 1000,
                    duration * 1000 + itemVideoTimeLine.originStartPosition * 1000
                )
            )
        }
        subPlayer.setMediaSource(mediaSource)
    }

    val subPlayerList: ArrayList<Player> = ArrayList()
    val itemVideoTimeLineList: ArrayList<ItemVideoTimeLine> = ArrayList()
    private fun createSubPlayer(): ExoPlayer {
        val adaptiveTrackSelectionFactory = AdaptiveTrackSelection.Factory()
        val trackSelector = DefaultTrackSelector(requireActivity(), adaptiveTrackSelectionFactory)
        val renderersFactory = DefaultRenderersFactory(requireActivity())
        val player =
            ExoPlayer.Builder(requireActivity(), renderersFactory).setTrackSelector(trackSelector)
                .build()
//        player.playWhenReady = true
        return player
    }

    private fun initializePlayer() {
        val adaptiveTrackSelectionFactory = AdaptiveTrackSelection.Factory()
        trackSelector = DefaultTrackSelector(requireActivity(), adaptiveTrackSelectionFactory)
        val renderersFactory = DefaultRenderersFactory(requireActivity())
        player =
            ExoPlayer.Builder(requireActivity(), renderersFactory).setTrackSelector(trackSelector!!)
                .build()
        player!!.addListener(this)
//        playerView.useController = true
//        playerView.player = player
//        playerView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIT
////        player.setPlayWhenReady(true);
//        //        player.setPlayWhenReady(true);
        player!!.playWhenReady = autoPlay
        player!!.addListener(this)
        if (usePlayerView) {
            setPlayerView(true, true)
        } else {
            setPlayerView(false, false)
//            ePlayerView?.setExoPlayer(player)
            ePlayerView?.onResume()
        }
    }

    private fun buildMediaSource(uri: Uri, overrideExtension: String): MediaSource {
        @C.ContentType val type = Util.inferContentType(uri, overrideExtension)
        val mediaItem = MediaItem.Builder().setUri(uri).build()
        return when (type) {
            C.TYPE_SS -> SsMediaSource.Factory(
                DefaultSsChunkSource.Factory(mediaDataSourceFactory!!),
                buildDataSourceFactory()
            )
                .createMediaSource(mediaItem)
            C.TYPE_DASH -> DashMediaSource.Factory(
                DefaultDashChunkSource.Factory(mediaDataSourceFactory!!),
                buildDataSourceFactory()
            ).createMediaSource(mediaItem)
            C.TYPE_HLS -> {
                val defaultHlsExtractorFactory =
                    DefaultHlsExtractorFactory(
                        DefaultTsPayloadReaderFactory.FLAG_IGNORE_H264_STREAM,
                        true
                    )
                HlsMediaSource.Factory(mediaDataSourceFactory!!)
                    .setExtractorFactory(defaultHlsExtractorFactory)
                    .createMediaSource(mediaItem)
            }
            C.TYPE_OTHER -> {
                ProgressiveMediaSource.Factory(mediaDataSourceFactory!!)
                    .createMediaSource(mediaItem)
            }
            else -> {
                HlsMediaSource.Factory(mediaDataSourceFactory!!).createMediaSource(mediaItem)
            }
        }
    }

    private fun buildDataSourceFactory(): DataSource.Factory {
        val dataSourceFactory =
            DefaultDataSource.Factory(
                requireContext(), getHttpDataSourceFactory()
            )
        return dataSourceFactory
    }

    @Synchronized
    fun getHttpDataSourceFactory(): HttpDataSource.Factory {
        val cookieManager = CookieManager()
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER)
        CookieHandler.setDefault(cookieManager)
        val dataSourceFactory = DefaultHttpDataSource.Factory()
        dataSourceFactory.setUserAgent(Constant.WEB_USER_AGENT)
        return dataSourceFactory
    }

    fun handleProgressTracker() {
        if (player == null) {
            stopProgressTracker()
            return
        }
//        val position = player!!.currentPosition
        val position = getCurrentRealPosition()
//        Loggers.e("runnablePos", "run: position = $position")
        Loggers.e("onPlaybackStateChanged_A", "position = $position , endPosition = $endPosition")
        if (position!! >= endPosition) {
            stopProgressTracker()
            player!!.playWhenReady = false
            onPlayerStateChangeListener?.endVideo()
//            player!!.seekTo(startPosition)
            seekTo(startPosition)
        } else {
            startProgressTracker()
        }
    }

    override fun onVisibilityChange(visibility: Int) {
    }

    var progressTracker: ProgressTracker? = null

    var handlerPos: Handler? = null
    var runnablePos: Runnable = Runnable {
        run {
            handleProgressTracker()
        }
    }

    fun startProgressTracker() {
        handlerPos?.postDelayed(runnablePos, 300L)
    }

    fun stopProgressTracker() {
        handlerPos?.removeCallbacks(runnablePos)
    }

    override fun onIsPlayingChanged(isPlaying: Boolean) {
        onPlayerStateChangeListener?.playerState(isPlaying)
        if (handlerPos == null) {
            handlerPos = Handler(Looper.getMainLooper())
        }
        if (isPlaying) {
            startProgressTracker()
        } else {
            stopProgressTracker()
        }
        super.onIsPlayingChanged(isPlaying)
    }

    override fun onPlaybackStateChanged(state: Int) {
        super.onPlaybackStateChanged(state)
        when (state) {
            Player.STATE_READY -> {
                if (endPosition == 0L) {
                    endPosition = player!!.duration
                }
                if (isResumeCurrentPos) {
                    isResumeCurrentPos = false
                    seekTo(currentPos)
                }

                Loggers.e("onPlaybackStateChanged_C", "endPos = $endPosition")
                progressBar.visibility = View.GONE
                onPlayerStateListenerForTimeLine?.onStateReady(player!!)
                onPlayerStateChangeListener?.onStateReady(player!!)
            }
            Player.STATE_BUFFERING -> {
                progressBar.visibility = View.VISIBLE
                onPlayerStateListenerForTimeLine?.onBuffering()
                onPlayerStateChangeListener?.onBuffering()
            }
            Player.STATE_ENDED -> {
                onPlayerStateChangeListener?.endVideo()
                player!!.playWhenReady = false
//                player!!.seekTo(startPosition)
                pause()
                seekTo(startPosition)
//                seekToSubPlayer(0)
            }
        }
    }

    /*override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        when (playbackState) {
            Player.STATE_READY -> {
                progressBar.visibility = View.GONE
            }
            Player.STATE_BUFFERING -> {
                progressBar.visibility = View.VISIBLE
            }
        }
    }*/

    override fun onPlayerError(error: PlaybackException) {
        super.onPlayerError(error)
        error.printStackTrace()
        (activity as? BaseActivity)?.showToast(R.string.msg_player_file_error)
    }

    override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {
    }

    override fun onIsLoadingChanged(isLoading: Boolean) {
    }

    override fun onRepeatModeChanged(repeatMode: Int) {
    }

    fun releasePlayer() {
        ePlayerView?.onPause()
        setFilter(null)
        player?.release()
        player = null
        progressTracker?.cancel()
        stopAllSubPlayer()
    }


    override fun onDetach() {
        releasePlayer()
        super.onDetach()
    }

    fun pause() {
        player?.playWhenReady = false
        for (player in subPlayerList) {
            player.playWhenReady = false
        }
    }

    fun play() {
        player?.playWhenReady = true
        for (player in subPlayerList) {
            player.playWhenReady = true
        }
    }

    fun isMute(): Boolean {
        if (player == null) {
            return true
        }
        return player!!.volume <= 0f
    }

    fun mute(isMute: Boolean) {
        if (player == null) {
            return
        }
        val volume = if (isMute) 0f else 1.0f
        player?.volume = volume
    }

    fun toggleMute() {
        val volume = if (isMute()) 1.0f else 0f
        player?.volume = volume
    }

    override fun onPause() {
        pause()
        super.onPause()
    }

    var ePlayerView: GestureEPlayerView? = null;
    fun setFilter(glFilter: GlFilter?) {
//        if (glFilter == null) {
//            return
//        }
        ePlayerView?.setGlFilter(glFilter)
    }

    fun updateRotatePlayerView() {
        ePlayerView?.updateRotate()
    }

    fun getRotate(): Int {
        return if (ePlayerView == null) 0 else ePlayerView?.rotation!!.toInt()
    }

    fun setLockGesture(isLock: Boolean) {
        ePlayerView?.setLockGesture(isLock)
    }

    private var usePlayerView = false
    fun setPlayerView(usePlayerView: Boolean, useController: Boolean) {
        this.usePlayerView = usePlayerView
        if (usePlayerView) {
            ePlayerView?.visibility = View.GONE
            ePlayerView?.setExoPlayer(null)
            playerView?.visibility = View.VISIBLE
            playerView?.player = player
            playerView?.useController = useController
            playerView?.setControllerVisibilityListener(this)
            playerView?.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIT
//        playerView.requestFocus();
        } else {
            playerView?.visibility = View.GONE
            playerView?.player = null
            ePlayerView?.visibility = View.VISIBLE
            ePlayerView?.setExoPlayer(player)
            ePlayerView?.onResume()
        }
    }

    private var mFullScreenDialog: Dialog? = null
    private var mExoPlayerFullscreen = false
    var onFullScreenButtonListener: OnFullScreenButtonListener? = null

    private fun initFullscreenDialog() {
        mFullScreenDialog = object : Dialog(
            requireActivity(),
            android.R.style.Theme_Black_NoTitleBar_Fullscreen
        ) {
            override fun onBackPressed() {
                if (mExoPlayerFullscreen) {
                    requireActivity().requestedOrientation =
                        ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                    closeFullscreenDialog()
                    if (onFullScreenButtonListener == null) {
                        return
                    }
                    if (mExoPlayerFullscreen) {
                        onFullScreenButtonListener?.onExpand()
                    } else {
                        onFullScreenButtonListener?.onMinimize()
                    }
                }
                super.onBackPressed()
            }
        }
    }

    private fun openFullscreenDialog() {
        /*buttonFullScreen.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.mipmap
        .ic_fullscreen_minimize));
        mExoPlayerFullscreen = true;
        playerView.setVisibility(View.GONE);
        playerViewFullscreen.setVisibility(View.VISIBLE);
        PlayerView.switchTargetView(player, playerView, playerViewFullscreen);
        if (6 % 2 == 0) {
            return;
        }*/
        if (mFullScreenDialog == null) {
            initFullscreenDialog()
        }
        if (playerView == null || buttonFullScreen == null) {
            return
        }
        (playerView.parent as ViewGroup).removeView(playerView)
        mFullScreenDialog!!.addContentView(
            playerView,
            ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )
        buttonFullScreen.setImageDrawable(
            ContextCompat.getDrawable(
                requireActivity(),
                R.mipmap.ic_fullscreen_minimize
            )
        )
        mExoPlayerFullscreen = true
        mFullScreenDialog!!.show()
    }

    private fun closeFullscreenDialog() {
        if (mFullScreenDialog == null) {
            initFullscreenDialog()
        }
        if (playerView == null || buttonFullScreen == null) {
            return
        }
        (playerView.parent as ViewGroup).removeView(playerView)
        (rootView.findViewById<View>(R.id.framePlayer) as FrameLayout).addView(playerView)
        mExoPlayerFullscreen = false
        mFullScreenDialog!!.dismiss()
        buttonFullScreen.setImageDrawable(
            ContextCompat.getDrawable(
                requireActivity(),
                R.mipmap.ic_fullscreen_expand
            )
        )
    }

    interface OnFullScreenButtonListener {
        fun onExpand()
        fun onMinimize()
        fun onScrollToTop()
    }

    var listPointTrim: ArrayList<Long>? = null
    private fun concateVideoSource(): MediaSource? {
//    private fun concateVideoSource(): ArrayList<MediaSource>? {
        val mDuration = OptiCommonMethods.getVideoDuration(requireActivity(), fileUri!!)
        val treeSetPoint = TreeSet<Long>()
        var mediaSource: MediaSource? = null
        val list: ArrayList<MediaSource> = ArrayList()
        treeSetPoint.add(0)
        treeSetPoint.add(mDuration)
        for (i in 0 until itemVideoTimeLineList.size) {
            val obj = itemVideoTimeLineList.get(i)
            if (obj.type == Constant.TYPE_VIDEO) {
                treeSetPoint.add(obj.startPosition)
                treeSetPoint.add(obj.endPosition)
            }
        }
        listPointTrim = java.util.ArrayList<Long>(treeSetPoint)
        for (j in 0 until listPointTrim!!.size - 1) {
            var isReplaceVideo = false
            var videoMediaSource: MediaSource? = null
            for (k in itemVideoTimeLineList.size - 1 downTo 0) {
                val obj = itemVideoTimeLineList.get(k)
                if (isReplaceVideo) {
                    break
                }
                if (obj.type == Constant.TYPE_VIDEO && !isReplaceVideo && (listPointTrim!!.get(j) >= obj.startPosition) && (listPointTrim!!.get(
                        j + 1
                    ) <= obj.endPosition)
                ) {

                    isReplaceVideo = true
                    val videoUri = Uri.parse(obj.filePath)
                    val videoSource = buildMediaSource(videoUri!!, "")
                    videoMediaSource = ClippingMediaSource(
                        videoSource!!,
                        (obj.originStartPosition) * 1000,
                        (((listPointTrim!!.get(j + 1) - listPointTrim!!.get(j)) + obj.originStartPosition) * 1000)
                    )
                }
            }
            if (!isReplaceVideo) {
                val videoSource = buildMediaSource(fileUri!!, "")
                videoMediaSource = ClippingMediaSource(
                    videoSource!!,
                    (listPointTrim!!.get(j)) * 1000,
                    ((listPointTrim!!.get(j + 1)) * 1000)
                )
            }
//            for(i in 0 until list.size){
//                mediaSource = ConcatenatingMediaSource
//            }
            list.add(videoMediaSource!!)
            if (mediaSource != null) {
                mediaSource = ConcatenatingMediaSource(mediaSource, videoMediaSource!!)
            } else {
                mediaSource = videoMediaSource!!
            }
        }
        return mediaSource
//        return list
    }

    private fun seekToPlayer(player: ExoPlayer, position: Long) {
        val timeLine = player.currentTimeline
        val window = Timeline.Window()
        var windowIndex = 0
        var needSeekTo = position
        var stepDuration: Long = 0
        val totalWindowIndex = timeLine.windowCount
        if (totalWindowIndex > 1) {
            for (j in 0 until totalWindowIndex) {
                var durationWindowIndex = timeLine.getWindow(j, window).durationMs
                if (durationWindowIndex <= 0) {// check
//                    durationWindowIndex = listPointTrim!!.get(j+1)
                }
                if (position > (stepDuration + durationWindowIndex)) {
                    stepDuration = stepDuration + durationWindowIndex
                    continue
                }
                windowIndex = j
                needSeekTo = position - stepDuration
                break
            }
        }
        Loggers.e(
            "seekToPlayer",
            "position = $position, totalWindowIndex = $totalWindowIndex, windowIndex = $windowIndex, needSeekTo = $needSeekTo"
        )
        if (needSeekTo < 0) {
            return
        }
        player.seekTo(windowIndex, needSeekTo)
    }

    fun getCurrentRealPosition(): Long? {
        if (player == null) {
            return 0
        }
        var realPosition = player!!.currentPosition
        val timeLine = player!!.currentTimeline

        val window = Timeline.Window()
        var stepDuration: Long = 0
        val currentWindowIndex = player!!.currentWindowIndex
        if (currentWindowIndex > 0) {
            for (j in 0 until currentWindowIndex) {
                var durationWindowIndex = timeLine.getWindow(j, window).durationMs
                if (durationWindowIndex <= 0) {// check
//                    durationWindowIndex = listPointTrim!!.get(j+1)
                }
                stepDuration = stepDuration + durationWindowIndex
            }
        }
        realPosition = realPosition + stepDuration
        Loggers.e(
            "seekToPlayer",
            "realposition = $realPosition, stepDuration = $stepDuration, currentWindowIndex = $currentWindowIndex, duration = " + player!!.duration
        )
        return realPosition
    }

    private var currentPos = 0L
    private var isResumeCurrentPos = false
    fun saveAndStartCurPos() {
        isResumeCurrentPos = true
        currentPos = getCurrentRealPosition()!!
    }
}