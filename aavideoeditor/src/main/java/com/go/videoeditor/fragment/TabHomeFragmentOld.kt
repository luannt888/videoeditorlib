package com.go.videoeditor.fragment

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.go.videoeditor.stream.RecordAudioFragment
import com.go.videoeditor.R
import com.go.videoeditor.activity.*
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.downloadVideo.DownloadedVideoFragment
import com.go.videoeditor.downloadVideo.UploadedVideoFragment
import com.go.videoeditor.utils.AppUtils
import com.go.videoeditor.utils.OptiCommonMethods
import com.go.videoeditor.utils.ScreenSize
import com.skydoves.powermenu.*
import kotlinx.android.synthetic.main.fragment_tab_home_ve.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class TabHomeFragmentOld : Fragment() {
    var isTabSelected: Boolean = false
    var isViewCreated: Boolean = false
    var isHandling = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tab_home_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isViewCreated = true
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 1000)
            return
        }
        if (isTabSelected) {
            return
        }
        isTabSelected = true
        init()
    }

    private fun init() {
        initUI()
        initControl()
    }

    fun initUI() {
    }

    fun goLivestreamActivity() {
        if (isHandling) {
            return
        }
        isHandling = true
        if (!(activity as MainVideoEditorActivity).checkPermission()) {
            isHandling = false
            return
        }
        isHandling = true
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            (activity as MainVideoEditorActivity).gotoActivity(LivestreamActivity::class.java)
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                isHandling = false
            }, 1000)
        }, Constant.DELAY_HANDLE)
    }

    fun goFolderFragment(folder: String) {
        if (!(activity as MainVideoEditorActivity).checkPermission()) {
            return
        }
        val fragment = DownloadedVideoFragment()
        val bundle = Bundle()
        bundle.putString(Constant.DATA, folder)
        (activity as? BaseActivity)?.showBottomSheetDialog(fragment, bundle)
    }

    fun initControl() {
        buttonItemDraft.setOnClickListener {
            if (!(activity as MainVideoEditorActivity).checkPermission()) {
                return@setOnClickListener
            }
            (activity as MainVideoEditorActivity).showToast(R.string.msg_cooming_soon)
        }
        buttonItemMediaStorage.setOnClickListener {
            if (!(activity as MainVideoEditorActivity).checkPermission()) {
                return@setOnClickListener
            }
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                (activity as MainVideoEditorActivity).gotoActivity(MediaStorageActivity::class.java)
            }, Constant.DELAY_HANDLE)
        }
        buttonItemEdit.setOnClickListener {
            if (!(activity as MainVideoEditorActivity).checkPermission()) {
                return@setOnClickListener
            }
            showSelectVideoPopup()
        }
        buttonItemUploaded.setOnClickListener {
            if (!(activity as MainVideoEditorActivity).checkPermission()) {
                return@setOnClickListener
            }
            val fragment = UploadedVideoFragment()
            (activity as? BaseActivity)?.showBottomSheetDialog(fragment, null)
        }
        buttonItemDownloaded.setOnClickListener {
            goFolderFragment(AppUtils.FOLDER_DOWNLOAD)
        }
        buttonItemEdited.setOnClickListener {
            goFolderFragment(AppUtils.FOLDER_EDITED)
        }
        buttonItemRecordAudio.setOnClickListener {
            if (!(activity as MainVideoEditorActivity).checkPermission()) {
                return@setOnClickListener
            }
            val fragment = RecordAudioFragment()
            (activity as BaseActivity).showBottomSheetDialog(fragment, null)
        }
        buttonItemRecordVideo.setOnClickListener {
            goLivestreamActivity()
        }
        buttonItemTakePicture.setOnClickListener {
            goLivestreamActivity()
        }
    }

    //    TODO: START: CHANGE LIVE THUMBNAIL
    var popupSelectVideo: PowerMenu? = null
    fun showSelectVideoPopup() {
        val itemList: ArrayList<PowerMenuItem> = ArrayList()
        itemList.add(PowerMenuItem(getString(R.string.select_from_gallery)))
        itemList.add(PowerMenuItem(getString(R.string.select_from_camera)))
        if (popupSelectVideo != null) {
            popupSelectVideo!!.showAsDropDown(buttonItemEdit)
            return
        }
        val textView = TextView(activity)
        textView.textSize = 17f
        textView.setTextColor(ContextCompat.getColor(requireActivity(), R.color.color_primary_dark))
        textView.text = getString(R.string.select_video_title)
        textView.setPadding(25, 15, 15, 15)
        textView.setBackgroundColor(Color.parseColor("#f8f8f8"))
        textView.typeface = Typeface.create("sans-serif-medium", Typeface.BOLD)

        val screenSize = ScreenSize(requireActivity())
        val popupWidth = screenSize.width * 2 / 3
//        val popupHeight = screenSize.height / 2
        popupSelectVideo = PowerMenu.Builder(requireActivity())
            .addItemList(itemList)
            .setAnimation(MenuAnimation.SHOWUP_TOP_LEFT)
            .setAutoDismiss(true)
            .setShowBackground(true)
            .setWidth(popupWidth)
//                .setHeight(popupHeight)
            .setMenuRadius(10f)
            .setMenuShadow(10f)
            .setDividerHeight(1)
            .setDivider(ColorDrawable(ContextCompat.getColor(requireActivity(), R.color.dark_text)))
            .setCircularEffect(CircularEffect.BODY)
            .setTextSize(16)
            .setTextGravity(Gravity.START)
            .setTextColor(ContextCompat.getColor(requireActivity(), R.color.dark_text))
            .setTextTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL))
            .setSelectedTextColor(ContextCompat.getColor(requireActivity(), R.color.dark_text))
            .setMenuColor(Color.parseColor("#f8f8f8"))
            .setSelectedMenuColor(Color.parseColor("#f8f8f8"))
            .setOnMenuItemClickListener(object : OnMenuItemClickListener<PowerMenuItem> {
                override fun onItemClick(position: Int, item: PowerMenuItem?) {
                    popupSelectVideo!!.dismiss()
                    if (item == null) {
                        return
                    }
                    val title = item.title
                    if (title.equals(getString(R.string.select_from_gallery))) {
                        openGallery()
                        return
                    }
                    if (title.equals(getString(R.string.select_from_camera))) {
                        openCamera()
                    }
                }

            })
            .setHeaderView(textView)
            .build()
        popupSelectVideo!!.showAsDropDown(buttonItemEdit)
    }

    fun openGallery() {
        refreshGalleryAlone()
        val i = Intent(
            Intent.ACTION_PICK,
            MediaStore.Video.Media.EXTERNAL_CONTENT_URI
        )
        i.type = "video/*"
        i.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("video/*"))
        resultLauncherGallery.launch(i)
    }
    var resultLauncherGallery =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                result.data?.let {
                    setFilePath(result.resultCode, it, Constant.REQUEST_CODE_GALLERY)
                }
            }
        }

    var videoFile: File? = null
    fun openCamera() {
        val intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        var videoFile: File? = null
        try {
            videoFile = createFile()
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
        if (videoFile != null) {
            val videoURI: Uri = FileProvider.getUriForFile(
                requireActivity(), requireActivity().packageName + ".provider",
                videoFile
            )
            this.videoFile = videoFile
            intent.putExtra(MediaStore.EXTRA_OUTPUT, videoURI)
            intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 240) //4 minutes
//                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1)
            resultLauncherCamera.launch(intent)
        }
    }
    var resultLauncherCamera =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                if (videoFile == null) {
                    return@registerForActivityResult
                }
                val intent = Intent(activity, VideoEditorActivity::class.java)
                val bundle = Bundle()
                bundle.putString(Constant.FILE_PATH, videoFile.toString())
                intent.putExtras(bundle)
                startActivity(intent)
            }
        }
    private fun createFile(): File? {
        val timeStamp = SimpleDateFormat("ddMMyyyyHHmmss").format(Date())
        val mFileName = "MDT_" + timeStamp + "_"
        val storageDir: File? = requireActivity().getExternalFilesDir(Environment.DIRECTORY_MOVIES)
        if (storageDir == null) {
            return null
        }
        return File.createTempFile(mFileName, ".mp4", storageDir)
    }

    fun refreshGalleryAlone() {
        try {
            val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
            requireActivity().sendBroadcast(mediaScanIntent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setFilePath(resultCode: Int, data: Intent, requestCode: Int) {
        if (resultCode == AppCompatActivity.RESULT_OK) {
            try {
                val selectedVideo = data.data
                val filePath = OptiCommonMethods.getRealPathFromUri(requireActivity(), selectedVideo)
                if (filePath.isNullOrEmpty()) {
                    return
                }
                Log.e("TrimVideo", "filePath: $filePath")
                if (requestCode == Constant.REQUEST_CODE_GALLERY) {
                    val videoFile = File(filePath)
                    val extension = OptiCommonMethods.getFileExtension(videoFile.absolutePath)
                    val timeInMillis =
                        OptiCommonMethods.getVideoDuration(requireActivity(), videoFile)
                    Log.e("TrimVideo", "timeInMillis: $timeInMillis")
                    val duration = OptiCommonMethods.convertDurationInMin(timeInMillis)
                    Log.e("TrimVideo", "duration: $duration")
                    val second = TimeUnit.MILLISECONDS.toSeconds(timeInMillis)
                    Log.e("TrimVideo", "second: $second")

                    //check if video is more than 4 minutes
                    if (duration < Constant.VIDEO_LIMIT) {
                        //check video format before playing into exoplayer
                        if (extension == Constant.AVI_FORMAT) {
//                                convertAviToMp4() //avi format is not supported in exoplayer
                            (activity as BaseActivity).showToast(R.string.msg_file_note_support)
                        } else {
                            /*playbackPosition = 0
                            currentWindow = 0
                            initializePlayer()*/
//                                getAllFrameVideo()
                            val intent = Intent(activity, VideoEditorActivity::class.java)
                            val bundle = Bundle()
                            bundle.putString(Constant.FILE_PATH, filePath)
                            intent.putExtras(bundle)
                            Loggers.e("START", "MainActivity")
                            startActivity(intent)
                        }
                    } else {
                        val msg = String.format(
                            Locale.ENGLISH,
                            getString(R.string.msg_max_duration),
                            Constant.VIDEO_LIMIT
                        )
                        (activity as BaseActivity).showToast(msg)

                        /* isLargeVideo = true
                         val uri = Uri.fromFile(masterVideoFile)
                         val intent = Intent(context, OptiTrimmerActivity::class.java)
                         intent.putExtra("VideoPath", filePath)
                         intent.putExtra(
                             "VideoDuration",
                             OptiCommonMethods.getMediaDuration(context, uri)
                         )
                         startActivityForResult(intent, OptiConstant.MAIN_VIDEO_TRIM)*/
                    }
                }


                /*val filePathColumn = arrayOf(MediaStore.MediaColumns.DATA)
                val cursor = contentResolver
                    .query(selectedVideo!!, filePathColumn, null, null, null)
                if (cursor != null) {
                    cursor.moveToFirst()
                    val columnIndex = cursor
                        .getColumnIndex(filePathColumn[0])
                    val filePath = cursor.getString(columnIndex)
                    cursor.close()

                }*/
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


}