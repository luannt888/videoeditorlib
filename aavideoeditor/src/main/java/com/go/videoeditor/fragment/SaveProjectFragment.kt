package com.go.videoeditor.fragment

import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.app.Constant
import com.go.videoeditor.listener.OnDismissListener
import com.go.videoeditor.utils.EncryptUtil
import com.go.videoeditor.utils.SharedPreferencesManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_save_project_ve.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*
import org.json.JSONArray

class SaveProjectFragment : BottomSheetDialogFragment() {
    var onDismissListener: OnDismissListener? = null
    var isSuccess = false
    var bottomSheetDialog: BottomSheetDialog? = null
    var projectName: String? = null
    var titleList: ArrayList<String> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_save_project_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = true
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        initData()
        initControl()
    }

    fun initData() {
        textHeaderTitle.text = getString(R.string.save_draft)
        projectName = arguments?.getString(Constant.PROJECT_NAME, null)
        if (!projectName.isNullOrEmpty()) {
            editContent.setText(projectName)
            editContent.setSelection(projectName!!.length)
        }
        val videoList = SharedPreferencesManager.getVideoList(requireActivity())
        if (videoList == null || videoList.size == 0) {
            return
        }
        for (i in 0 until videoList.size) {
            val item = videoList.get(i)
            val arr = item.split(Constant.KEY_SPLIT_DRAFT_VIDEO_ITEM_CONTENT)
            val editData = EncryptUtil.base64Decode(arr[1])
            try {
                val dataArr = JSONArray(editData)
                for (j in 0 until dataArr.length()) {
                    val itemObj = dataArr.getJSONObject(j)
                    val keys = itemObj.keys()
                    while (keys.hasNext()) {
                        val key = keys.next()
                        val value = itemObj.get(key) as String
                        when (key) {
                            Constant.PROJECT_NAME -> {
                                if (projectName.isNullOrEmpty() || !projectName.equals(value)) {
                                    titleList.add(value)
                                }
                                break
                            }
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            (activity as? BaseActivity)?.showKeyboardFocus(editContent)
        }, 1000)
    }

    fun initControl() {
        buttonClose.setOnClickListener {
            isSuccess = false
            dismiss()
        }
        buttonDone.setOnClickListener {
            val content = editContent.text.toString().trim()
            if (content.isEmpty()) {
                (activity as? BaseActivity)?.hideKeyboard(editContent)
                textInputLayout.error = getString(R.string.title_empty)
                return@setOnClickListener
            }
            for (i in 0 until titleList.size) {
                if (content.equals(titleList.get(i))) {
                    (activity as? BaseActivity)?.hideKeyboard(editContent)
                    textInputLayout.error = getString(R.string.title_exist)
                    return@setOnClickListener
                }
            }
            isSuccess = true
            dismiss()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        val content = editContent.text.toString().trim()
        onDismissListener?.onDismiss(isSuccess, content, null)
        super.onDismiss(dialog)
    }

    override fun onCancel(dialog: DialogInterface) {
        isSuccess = false
        onDismissListener?.onDismiss(isSuccess, "", null)
        super.onCancel(dialog)
    }
}