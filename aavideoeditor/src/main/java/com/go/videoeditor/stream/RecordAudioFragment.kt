package com.go.videoeditor.stream

import android.content.DialogInterface
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.content.ContextCompat
import com.go.videoeditor.R
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.utils.AppUtils
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.warkiz.widget.IndicatorSeekBar
import com.warkiz.widget.OnSeekChangeListener
import com.warkiz.widget.SeekParams
import kotlinx.android.synthetic.main.fragment_record_audio_ve.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*
import java.util.*

class RecordAudioFragment : BottomSheetDialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_record_audio_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog.behavior.skipCollapsed = true
            bottomSheetDialog.behavior.isDraggable = true
            bottomSheetDialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
        buttonDone.visibility = View.GONE
        textHeaderTitle.text = getString(R.string.item_main_record_audio)
        textHeaderTitle.setTextColor(ContextCompat.getColor(activity!!, R.color.dark_text))
        buttonClose.setImageResource(R.mipmap.ic_close_ve)
    }

    var timerSeekbar: Timer? = null
    fun initData() {
        textHeaderTitle.text = getString(R.string.record_audio)
    }

    var timerCountDuration: Timer? = null
    fun startCountDuration() {
        var duration = 0L
        val period = 1000L
        cancelTimerCountDuration()
        activity?.runOnUiThread {
            textDuration.text = AppUtils.convertSecondsToTime(0)
        }
        timerCountDuration = Timer()
        timerCountDuration!!.schedule(object : TimerTask() {
            override fun run() {
                duration++
                if (isDetached) {
                    cancelTimerCountDuration()
                    return
                }
                activity?.runOnUiThread {
                    textDuration.text = AppUtils.convertSecondsToTime(duration)
                }
            }
        }, 1000, period)
    }

    fun cancelTimerCountDuration() {
        if (timerCountDuration == null) {
            return
        }
        try {
            timerCountDuration?.cancel()
            timerCountDuration?.purge()
            timerCountDuration = null
            activity?.runOnUiThread { textDuration.text = "00:00" }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    var mediaRecorder: MediaRecorder? = null
    var filePath: String = ""
    fun startRecordAudio() {
        buttonRecord.isSelected = true
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        startCountDuration()
        filePath = AppUtils.createAudioFilePath(activity!!)
        mediaRecorder = MediaRecorder()
        mediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
        mediaRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
        mediaRecorder!!.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
        mediaRecorder!!.setOutputFile(filePath)
        mediaRecorder!!.prepare()
        mediaRecorder!!.start()
    }

    fun stopRecordAudio() {
        buttonRecord.isSelected = false
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        cancelTimerCountDuration()
        mediaRecorder?.stop()
        mediaRecorder?.reset()
        mediaRecorder?.release()
        mediaRecorder = null
        AppUtils.addToGalleryRunBackground(activity!!, filePath, AppUtils.FOLDER_STORAGE)
    }

    var mediaPlayer: MediaPlayer? = null
    fun initPlayer() {
        Loggers.e("initPlayer", "-- init --")
        releasePlayer()
        mediaPlayer = MediaPlayer()
        try {
            mediaPlayer!!.setDataSource(filePath)
            mediaPlayer!!.prepare()
            val duration = mediaPlayer!!.duration / 1000f
            Loggers.e("timerSeekbar_duration", "$duration")
            textTotalDuration.text = AppUtils.convertSecondsToTime(duration.toLong())
            seekBar.max = duration
            mediaPlayer!!.setOnPreparedListener(object : MediaPlayer.OnPreparedListener {
                override fun onPrepared(player: MediaPlayer?) {
                    seekBar.setProgress(0f)
                }
            })
            mediaPlayer!!.setOnCompletionListener(object : MediaPlayer.OnCompletionListener {
                override fun onCompletion(player: MediaPlayer?) {
                    cancelTimerSeekbar()
                    mediaPlayer!!.pause()
                    seekBar.setProgress(0f)
                    mediaPlayer!!.seekTo(0)
                    buttonPlay.isSelected = false
                }
            })
            layoutPlayer.visibility = View.VISIBLE
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun releasePlayer() {
        try {
            mediaPlayer?.stop()
            mediaPlayer?.release()
            mediaPlayer = null
            buttonPlay.isSelected = false
            seekBar.setProgress(0f)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun initControl() {
        buttonRecord.setOnClickListener {
            if (buttonRecord.isSelected) {
                stopRecordAudio()
                initPlayer()
                return@setOnClickListener
            }
            startRecordAudio()
        }
        buttonClose.setOnClickListener {
            dismiss()
        }
        buttonPlay.setOnClickListener {
            if (mediaPlayer == null) {
                return@setOnClickListener
            }
            if (mediaPlayer!!.isPlaying) {
                mediaPlayer!!.pause()
                buttonPlay.isSelected = false
                cancelTimerSeekbar()
            } else {
                mediaPlayer!!.start()
                buttonPlay.isSelected = true
                startTimerSeekbar()
            }
        }
        seekBar.onSeekChangeListener = object : OnSeekChangeListener {
            override fun onSeeking(seekParams: SeekParams?) {
            }

            override fun onStartTrackingTouch(seekBar: IndicatorSeekBar?) {
                if (mediaPlayer == null || filePath.isEmpty()) {
                    return
                }
                if (mediaPlayer!!.isPlaying) {
                    mediaPlayer!!.pause()
                    cancelTimerSeekbar()
                }
                buttonPlay.isSelected = false
            }

            override fun onStopTrackingTouch(indicatorSeekBar: IndicatorSeekBar?) {
                if (mediaPlayer == null || filePath.isEmpty()) {
                    return
                }
                val progress = seekBar.progress
                val duration = progress * 1000
                mediaPlayer!!.seekTo(duration)
                startTimerSeekbar()
                mediaPlayer!!.start()
                buttonPlay.isSelected = true
            }
        }
        /*seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (!fromUser) {
                    return
                }
                if (mediaPlayer == null || filePath.isEmpty()) {
                    return
                }
                val duration = progress * 1000
                mediaPlayer!!.seekTo(duration)
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }

        })*/
    }

    fun startTimerSeekbar() {
        cancelTimerSeekbar()
        if (mediaPlayer == null || filePath.isEmpty()) {
            return
        }
        timerSeekbar = Timer()
        timerSeekbar!!.schedule(object : TimerTask() {
            override fun run() {
                if (isDetached) {
                    cancelTimerSeekbar()
                    return
                }
                val duration = mediaPlayer!!.currentPosition / 1000f
                Loggers.e("timerSeekbar_run", "$duration")
                seekBar.setProgress(duration)
            }

        }, 0, 100)
    }

    fun cancelTimerSeekbar() {
        try {
            timerSeekbar?.cancel()
            timerSeekbar?.purge()
            timerSeekbar = null
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        cancelAll()
        super.onDismiss(dialog)
    }

    /*override fun onCancel(dialog: DialogInterface) {
        cancelAll()
        super.onCancel(dialog)
    }*/

    fun cancelAll() {
        stopRecordAudio()
        releasePlayer()
        cancelTimerCountDuration()
        cancelTimerSeekbar()
    }

    override fun onDetach() {
        super.onDetach()
    }
}