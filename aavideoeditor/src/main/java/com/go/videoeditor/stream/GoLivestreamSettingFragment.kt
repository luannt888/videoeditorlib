package com.go.videoeditor.stream

import android.content.DialogInterface
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.media.audiofx.AcousticEchoCanceler
import android.media.audiofx.NoiseSuppressor
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.go.videoeditor.R
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.listener.OnGoLivestreamSettingDismissListener
import com.go.videoeditor.utils.AppUtils
import com.go.videoeditor.utils.ScreenSize
import com.go.videoeditor.utils.SharedPreferencesManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.skydoves.powermenu.*
import kotlinx.android.synthetic.main.fragment_go_livestream_setting.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*

class GoLivestreamSettingFragment : BottomSheetDialogFragment() {
    var itemLivestreamConfig: ItemLivestreamConfig? = null
    var onGoLivestreamSettingDismissListener: OnGoLivestreamSettingDismissListener? = null
    lateinit var itemQualityList: ArrayList<PowerMenuItem>
    var bottomSheetDialog: BottomSheetDialog? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_go_livestream_setting, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog?.behavior?.skipCollapsed = true
            bottomSheetDialog?.behavior?.isDraggable = true
            bottomSheetDialog?.behavior?.state = BottomSheetBehavior.STATE_EXPANDED
        }
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
        scrollView.viewTreeObserver.addOnScrollChangedListener {
            val isTop = !scrollView.canScrollVertically(-1)
//            val isBottom = !scrollView.canScrollVertically(1)
            bottomSheetDialog?.behavior?.isDraggable = isTop
        }
    }

    fun initData() {
        itemLivestreamConfig = arguments?.getParcelable(Constant.DATA)
        if (itemLivestreamConfig == null) {
            dismiss()
            return
        }
        textHeaderTitle.text = getString(R.string.setting_livestream)
        textHeaderTitle.setTextColor(ContextCompat.getColor(requireActivity(), R.color.dark_text))
        buttonClose.setImageResource(R.mipmap.ic_close_ve)
        buttonDone.visibility = View.GONE

        if (!NoiseSuppressor.isAvailable()) {
            layoutNoiseSuppressor.visibility = View.GONE
            viewNoiseSuppressor.visibility = View.GONE
        }

        if (!AcousticEchoCanceler.isAvailable()) {
            layoutEchoCanceler.visibility = View.GONE
            viewEchoCanceler.visibility = View.GONE
        }
        checkAudioMute.isChecked = itemLivestreamConfig!!.isAudioMute
        checkNoiseSuppressor.isChecked = itemLivestreamConfig!!.isNoiseSuppressor
        checkAntiAliasing.isChecked = itemLivestreamConfig!!.isAntiAliasing
        checkEchoCanceler.isChecked = itemLivestreamConfig!!.isEchoCanceler
        checkLandscape.isChecked = itemLivestreamConfig!!.isLandscape
        textAudioChannel.text =
            if (itemLivestreamConfig!!.isStereo) getString(R.string.item_stereo) else getString(R.string.item_mono)

        itemQualityList = AppUtils.getQualityList()
        val videoQuality = SharedPreferencesManager.getVideoQuality(requireActivity())
        var existQualityInList = false
        for (i in 0 until itemQualityList.size) {
            if (videoQuality.equals(itemQualityList.get(i).title.toString(), true)) {
                existQualityInList = true
                break
            }
        }
        if (existQualityInList) {
            Loggers.e("initItemLivestreamConfig_B1", "videoQuality = $videoQuality")
            textVideoQuality.text = videoQuality
        } else {
            textVideoQuality.text = itemQualityList.get(0).title.toString()
            Loggers.e("initItemLivestreamConfig_B2", "videoQuality = ${textVideoQuality.text}")
        }
    }

    fun initControl() {
        buttonClose.setOnClickListener {
            dismiss()
        }
        layoutVideoQuality.setOnClickListener {
            showPopupMenu(TYPE_VIDEO_QUALITY)
        }
        layoutAudioChannel.setOnClickListener {
            showPopupMenu(TYPE_AUDIO_CHANNEL)
        }
        layoutAudioMute.setOnClickListener {
            checkAudioMute.performClick()
        }
        layoutNoiseSuppressor.setOnClickListener {
            checkNoiseSuppressor.performClick()
        }
        layoutAntiAliasing.setOnClickListener {
            checkAntiAliasing.performClick()
        }
        layoutEchoCanceler.setOnClickListener {
            checkEchoCanceler.performClick()
        }
        layoutLandscape.setOnClickListener {
            checkLandscape.performClick()
        }
    }

    var popupMenu: PowerMenu? = null
    val TYPE_VIDEO_QUALITY = 1
    val TYPE_AUDIO_CHANNEL = 2
    fun showPopupMenu(type: Int) {
        /*if (popupMenuVideoQuality != null) {
            popupMenuVideoQuality!!.showAsAnchorRightBottom(layoutVideoQuality)
            popupMenuVideoQuality!!.setSelection(popupMenuVideoQuality!!.selectedPosition)
            return
        }*/
        val viewAnchor: View
        val itemSelected: String
        val itemList: ArrayList<PowerMenuItem>
        when (type) {
            TYPE_VIDEO_QUALITY -> {
                viewAnchor = layoutVideoQuality
                itemSelected = textVideoQuality.text.toString()
//                itemList = initQualityList()
                itemList = itemQualityList
            }
            else -> {//TYPE_AUDIO_CHANNEL
                viewAnchor = layoutAudioChannel
                itemSelected = textAudioChannel.text.toString()
                itemList = ArrayList()
                itemList.add(PowerMenuItem(getString(R.string.item_stereo)))
                itemList.add(PowerMenuItem(getString(R.string.item_mono)))
            }
        }
        var selectedPosition = -1
        for (i in 0 until itemList.size) {
            if (itemList.get(i).title.equals(itemSelected)) {
                selectedPosition = i
                break
            }
        }

        val screenSize = ScreenSize(requireActivity())
//        val popupWidth = screenSize.width / 3
//        val popupHeight = screenSize.height / 3
        val maxPopHeight = screenSize.height * 3 / 4
        var popupHeight = resources.getDimensionPixelSize(R.dimen.dimen_55) * itemList.size
        if (popupHeight > maxPopHeight) {
            popupHeight = maxPopHeight
        }
        popupMenu = PowerMenu.Builder(requireActivity())
            .addItemList(itemList)
            .setAnimation(MenuAnimation.SHOWUP_TOP_RIGHT)
            .setShowBackground(true)
            .setAutoDismiss(true)
//            .setWidth(popupWidth)
            .setHeight(popupHeight)
            .setMenuRadius(10f)
            .setMenuShadow(10f)
            .setDividerHeight(1)
            .setDivider(ColorDrawable(ContextCompat.getColor(requireActivity(), R.color.gray2)))
            .setCircularEffect(CircularEffect.BODY)
            .setTextSize(16)
            .setTextGravity(Gravity.START)
            .setTextColor(ContextCompat.getColor(requireActivity(), R.color.dark_text))
            .setTextTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL))
            .setSelectedTextColor(ContextCompat.getColor(requireActivity(), R.color.color_primary))
            .setMenuColor(ContextCompat.getColor(requireActivity(), R.color.white))
            .setSelectedMenuColor(ContextCompat.getColor(requireActivity(), R.color.white))
            .setOnMenuItemClickListener(object : OnMenuItemClickListener<PowerMenuItem> {
                override fun onItemClick(position: Int, item: PowerMenuItem?) {
                    popupMenu!!.selectedPosition = position
                    popupMenu!!.dismiss()
                    when (type) {
                        TYPE_VIDEO_QUALITY -> {
                            textVideoQuality.text = item!!.title
                        }
                        else -> {//TYPE_AUDIO_CHANNEL
                            textAudioChannel.text = item!!.title
                        }
                    }
                }
            })
            .build()
        if (selectedPosition != -1) {
            popupMenu!!.selectedPosition = selectedPosition
            popupMenu!!.selectedPosition = selectedPosition
        }
//        popupMenu!!.showAsAnchorRightBottom(viewAnchor)
        popupMenu!!.showAtCenter(viewAnchor)
    }

    fun saveSetting() {
        popupMenu?.dismiss()
        if (itemLivestreamConfig == null) {
            return
        }
        itemLivestreamConfig!!.isAudioMute = checkAudioMute.isChecked
        itemLivestreamConfig!!.isNoiseSuppressor = checkNoiseSuppressor.isChecked
        itemLivestreamConfig!!.isAntiAliasing = checkAntiAliasing.isChecked
        itemLivestreamConfig!!.isEchoCanceler = checkEchoCanceler.isChecked
        itemLivestreamConfig!!.isLandscape = checkLandscape.isChecked
        val videoQuality = textVideoQuality.text.toString()
        SharedPreferencesManager.setVideoQuality(requireActivity(), videoQuality)
        SharedPreferencesManager.setBooleanValue(
            requireActivity(),
            SharedPreferencesManager.KEY_AUDIO_MUTE,
            checkAudioMute.isChecked
        )
        SharedPreferencesManager.setBooleanValue(
            requireActivity(),
            SharedPreferencesManager.KEY_NOISE_SUPPRESSOR,
            checkNoiseSuppressor.isChecked
        )
        SharedPreferencesManager.setBooleanValue(
            requireActivity(),
            SharedPreferencesManager.KEY_ANTI_ALIASING,
            checkAntiAliasing.isChecked
        )
        SharedPreferencesManager.setBooleanValue(
            requireActivity(),
            SharedPreferencesManager.KEY_ECHO_CANCELER,
            checkEchoCanceler.isChecked
        )
        SharedPreferencesManager.setBooleanValue(
            requireActivity(),
            SharedPreferencesManager.KEY_STEREO,
            textAudioChannel.text.toString().equals(getString(R.string.item_stereo))
        )
        SharedPreferencesManager.setBooleanValue(
            requireActivity(),
            SharedPreferencesManager.KEY_LANDSCAPE,
            checkLandscape.isChecked
        )
        itemLivestreamConfig = AppUtils.getVideoConfig(videoQuality, itemLivestreamConfig)
        onGoLivestreamSettingDismissListener?.onDismiss(itemLivestreamConfig)
    }

    override fun dismiss() {
        saveSetting()
        super.dismiss()
    }

    override fun onCancel(dialog: DialogInterface) {
        saveSetting()
        super.onCancel(dialog)
    }

}