package com.go.videoeditor.stream

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.media.audiofx.AcousticEchoCanceler
import android.media.audiofx.NoiseSuppressor
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.provider.Settings
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.activity.LivestreamActivity
import com.go.videoeditor.add.addLogo.AddLogoFragment
import com.go.videoeditor.add.addLogo.FragmentSelectImge
import com.go.videoeditor.add.addText.AddTextFragment
import com.go.videoeditor.add.stickerLib.*
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.listener.OnCancelListener
import com.go.videoeditor.listener.OnGoLivestreamSettingDismissListener
import com.go.videoeditor.service.MyHttpRequest
import com.go.videoeditor.service.RequestParams
import com.go.videoeditor.service.ServiceUtil
import com.go.videoeditor.utils.*
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mukesh.permissions.EasyPermissions
import com.mukesh.permissions.OnPermissionListener
import com.pedro.encoder.input.gl.SpriteGestureController
import com.pedro.encoder.input.gl.render.filters.*
import com.pedro.encoder.input.gl.render.filters.`object`.GifObjectFilterRender
import com.pedro.encoder.input.gl.render.filters.`object`.ImageObjectFilterRender
import com.pedro.encoder.input.video.CameraHelper
import com.pedro.encoder.utils.gl.TranslateTo
import com.pedro.rtmp.utils.ConnectCheckerRtmp
import com.pedro.rtplibrary.rtmp.RtmpCamera2
import com.pedro.rtplibrary.view.TakePhotoCallback
import com.skydoves.powermenu.*
import kotlinx.android.synthetic.main.fragment_go_livestream.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class GoLivestreamFragment : Fragment() {
    var rtmpCamera: RtmpCamera2? = null
    lateinit var itemLivestreamConfig: ItemLivestreamConfig
    var facingCamera: CameraHelper.Facing = CameraHelper.Facing.BACK
    var isLandscape = false
    var myHttpRequest: MyHttpRequest? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_go_livestream, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkPermission()
    }

    fun init() {
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
//        initUI()
        initData()
        initControl()
        initStickerView()
        /*for (size in rtmpCamera!!.getResolutionsBack()) {
            Loggers.e("resolution_A", "res = ${size.width} x ${size.height}")
        }*/
    }

    fun initData() {
        initItemLivestreamConfig()
        initOpenSurfaceView()
        surfaceView.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(view: View?, motionEvent: MotionEvent?): Boolean {
                if (spriteGestureController.spriteTouched(view, motionEvent)) {
                    spriteGestureController.moveSprite(view, motionEvent)
                    spriteGestureController.scaleSprite(motionEvent)
                    return true
                }
                return false
            }
        })
    }

    fun initItemLivestreamConfig() {
        var videoQuality = SharedPreferencesManager.getVideoQuality(requireActivity())
        val itemQualityList = AppUtils.getQualityList()
        var existQualityInList = false
        for (i in 0 until itemQualityList.size) {
            if (videoQuality.equals(itemQualityList.get(i).title.toString(), true)) {
                existQualityInList = true
                break
            }
        }
        if (!existQualityInList) {
            videoQuality = itemQualityList.get(0).title.toString()
        }
        Loggers.e("initItemLivestreamConfig", "videoQuality = $videoQuality")
        val isLandscape = SharedPreferencesManager.getBooleanValue(
            requireActivity(),
            SharedPreferencesManager.KEY_LANDSCAPE
        )
        this@GoLivestreamFragment.isLandscape = isLandscape
        if (isLandscape) {
            requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
        } else {
            requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        }
        val isNoiseSuppressor =
            if (!NoiseSuppressor.isAvailable()) false else SharedPreferencesManager.getBooleanValue(
                requireActivity(),
                SharedPreferencesManager.KEY_NOISE_SUPPRESSOR
            )
        val isEchoCanceler =
            if (!AcousticEchoCanceler.isAvailable()) false else SharedPreferencesManager.getBooleanValue(
                requireActivity(),
                SharedPreferencesManager.KEY_ECHO_CANCELER
            )
        val isStereo = SharedPreferencesManager.getBooleanValue(
            requireActivity(),
            SharedPreferencesManager.KEY_STEREO
        )
        val isAudioMute = SharedPreferencesManager.getBooleanValue(
            requireActivity(),
            SharedPreferencesManager.KEY_AUDIO_MUTE
        )
        val isAntiAliasing = SharedPreferencesManager.getBooleanValue(
            requireActivity(),
            SharedPreferencesManager.KEY_ANTI_ALIASING
        )
        itemLivestreamConfig = ItemLivestreamConfig()
        itemLivestreamConfig = AppUtils.getVideoConfig(videoQuality, itemLivestreamConfig)
        itemLivestreamConfig = ItemLivestreamConfig(
            itemLivestreamConfig.videoBitrate,
            itemLivestreamConfig.fps,
            itemLivestreamConfig.videoWidth,
            itemLivestreamConfig.videoHeight,
            itemLivestreamConfig.audioBitrate,
            itemLivestreamConfig.sampleRate,
            isLandscape,
            isNoiseSuppressor,
            isEchoCanceler,
            isStereo,
            isAudioMute,
            isAntiAliasing
        )
    }

    fun initOpenSurfaceView() {
        rtmpCamera = RtmpCamera2(surfaceView, object : ConnectCheckerRtmp {
            override fun onConnectionSuccessRtmp() {
                Loggers.e("initOpenSurfaceView", "onConnectionSuccessRtmp")
                isStreaming = true
                activity?.runOnUiThread {
                    loadingBottom.hide()
                }
                startCountDuration()
            }

            override fun onConnectionFailedRtmp(reason: String) {
                Loggers.e("initOpenSurfaceView", "onConnectionFailedRtmp = $reason")
//                activity?.runOnUiThread { (activity as BaseActivity).showToast("Error livestream") }
                reloadUIWidthStopStream()
            }

            override fun onConnectionStartedRtmp(rtmpUrl: String) {
            }

            override fun onNewBitrateRtmp(bitrate: Long) {
                Loggers.e("initOpenSurfaceView", "onNewBitrateRtmp = $bitrate")
            }

            override fun onDisconnectRtmp() {
                Loggers.e("initOpenSurfaceView", "onDisconnectRtmp")
//                activity?.runOnUiThread { (activity as BaseActivity).showToast("Ngắt kết nối") }
                reloadUIWidthStopStream()
//                rtmpCamera?.reConnect(3000)
            }

            override fun onAuthErrorRtmp() {
                Loggers.e("initOpenSurfaceView", "onAuthErrorRtmp")
                activity?.runOnUiThread { (activity as BaseActivity).showToast("Lỗi xác thực") }
                reloadUIWidthStopStream()
            }

            override fun onAuthSuccessRtmp() {
                Loggers.e("initOpenSurfaceView", "onAuthSuccessRtmp")
            }

        })
        prepareEncoders()
        surfaceView.holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(p0: SurfaceHolder) {
            }

            override fun surfaceChanged(surfaceHolder: SurfaceHolder, p1: Int, p2: Int, p3: Int) {
                Loggers.e(
                    "streamSize",
                    "${rtmpCamera!!.streamWidth} x ${rtmpCamera!!.streamHeight}"
                )
//                val rotation = if (itemLivestreamConfig.isLandscape) 0 else 90
//                rtmpCamera!!.startPreview(facingCamera, rotation)
                rtmpCamera!!.startPreview(facingCamera)
            }

            override fun surfaceDestroyed(p0: SurfaceHolder) {
                if (rtmpCamera!!.isRecording) {
                    rtmpCamera!!.stopRecord()
                }
                if (rtmpCamera!!.isStreaming) {
                    rtmpCamera!!.stopStream()
                }
                rtmpCamera!!.stopPreview()
            }
        })
        surfaceView.setOnTouchListener { view, motionEvent ->
            val action = motionEvent.action
            if (motionEvent.pointerCount > 1) {
                if (action == MotionEvent.ACTION_MOVE) {
                    rtmpCamera?.setZoom(motionEvent)
                }
            }
            true
        }
    }

    fun reloadUIWidthStopStream() {
        cancelTimerCountDuration()
        activity?.runOnUiThread {
            rtmpCamera?.stopStream()
            buttonLive.isSelected = false
            buttonLive.isEnabled = true
            layoutTitle.visibility = View.VISIBLE
            textDuration.visibility = View.GONE
            loadingBottom.hide()
        }
    }

    var timerCountDuration: Timer? = null
    fun startCountDuration() {
        var duration = 0L
        val period = 1000L
        cancelTimerCountDuration()
        activity?.runOnUiThread {
            textDuration.visibility = View.VISIBLE
            textDuration.text = AppUtils.convertSecondsToTime(0)
        }
        timerCountDuration = Timer()
        timerCountDuration!!.schedule(object : TimerTask() {
            override fun run() {
                duration++
                if (isDetached) {
                    cancelTimerCountDuration()
                    return
                }
                activity?.runOnUiThread {
                    textDuration.text = AppUtils.convertSecondsToTime(duration)
                }
            }
        }, 1000, period)
    }

    fun cancelTimerCountDuration() {
        if (timerCountDuration == null) {
            return
        }
        try {
            timerCountDuration?.cancel()
            timerCountDuration?.purge()
            timerCountDuration = null
            activity?.runOnUiThread { textDuration.text = "" }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun startCountDownToStartLive() {
        layoutTitle.visibility = View.GONE
        var currentNumber = 3
        val animZoomIn = AnimationUtils.loadAnimation(requireActivity(), R.anim.zoom_in_count_down)
        val animZoomOut =
            AnimationUtils.loadAnimation(requireActivity(), R.anim.zoom_out_count_down)
        textCountDown.text = "$currentNumber"
        textCountDown.visibility = View.VISIBLE
        animZoomIn.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(p0: Animation?) {
            }

            override fun onAnimationEnd(p0: Animation?) {
                if (isDetached) {
                    return
                }
                textCountDown.startAnimation(animZoomOut)
            }

            override fun onAnimationRepeat(p0: Animation?) {
            }
        })
        animZoomOut.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(p0: Animation?) {
            }

            override fun onAnimationEnd(p0: Animation?) {
                if (isDetached) {
                    return
                }
                currentNumber--
                if (currentNumber <= 0) {
                    currentNumber = 3
                    textCountDown.visibility = View.GONE
                    Handler(Looper.getMainLooper()).postDelayed(Runnable {
                        startLive()
                    }, Constant.DELAY_HANDLE)
                    return
                }
                textCountDown.text = "$currentNumber"
                textCountDown.startAnimation(animZoomIn)
            }

            override fun onAnimationRepeat(p0: Animation?) {
            }
        })
        textCountDown.startAnimation(animZoomIn)
    }

    fun startLive() {
        loadingBottom.show()
        if (rtmpCamera!!.isRecording || prepareEncoders()) {
            buttonLive.isSelected = true
            layoutTitle.visibility = View.GONE
            textDuration.visibility = View.VISIBLE
            textDuration.text = AppUtils.convertSecondsToTime(0)
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                Loggers.e("Stream_url", "${itemLive!!.streamUrl}")
                buttonLive.isEnabled = true
                rtmpCamera!!.startStream(itemLive!!.streamUrl)
            }, Constant.DELAY_HANDLE)
        } else {
            (activity as BaseActivity).showToast("Xảy ra lỗi khi chuẩn bị stream")
            loadingBottom.hide()
            reloadUIWidthStopStream()
        }
    }

    fun initStickerView() {
        stickerView.layoutParams = surfaceView.layoutParams
        stickerView.setOnStickerOperationListener(object : EventSticker {
            override fun onStickerZoomFinished(sticker: Sticker) {
                Loggers.e("stickerView", "onStickerZoomFinished")
            }

            override fun onStickerClicked(sticker: Sticker) {
                Loggers.e("stickerView", "onStickerClicked")
                if (sticker is StickerText) {
                    openAddTextFragment()
                    buttonDoneAddLogo.visibility = View.VISIBLE
                } else if (sticker is StickerDrawable) {
                    openAddLogoFragment()
                    buttonDoneAddLogo.visibility = View.VISIBLE
                }
            }

            override fun onStickerTouchedDown(sticker: Sticker) {
                Loggers.e("stickerView", "onStickerTouchedDown")
                if (buttonDoneAddLogo.visibility != View.VISIBLE) {
                    viewTransparentAlpha.visibility = View.VISIBLE
                    /*rtmpCamera?.glInterface?.setFilter(
                        POSITION_FILTER_STICKER_VIEW,
                        NoFilterRender()
                    )*/
                }
                buttonDoneAddLogo.visibility = View.VISIBLE
            }

            override fun onStickerDoubleTapped(sticker: Sticker) {
                Loggers.e("stickerView", "onStickerDoubleTapped")
            }

            override fun onStickerDragFinished(sticker: Sticker) {
                Loggers.e("stickerView", "onStickerDragFinished")
            }

            override fun onStickerFlipped(sticker: Sticker) {
                Loggers.e("stickerView", "onStickerFlipped")
            }

            override fun onZoomandrotateSticker(sticker: Sticker) {
                Loggers.e("stickerView", "onZoomandrotateSticker")
            }

            override fun onClickOutSticker() {
                Loggers.e("stickerView", "onClickOutSticker")
                clearFocusStickerview()
            }

            override fun onStickerDeleted(sticker: Sticker) {
                Loggers.e("stickerView", "onStickerDeleted")
//                removeRangeSeekBar(sticker)
            }

            override fun onStickerAdded(sticker: Sticker) {
                Loggers.e("stickerView", "onStickerAdded")
                buttonDoneAddLogo.visibility = View.VISIBLE
                viewTransparentAlpha.visibility = View.VISIBLE
//                addRangeSeekBar(sticker)
            }
        })

        buttonDoneAddLogo.setOnClickListener {
            buttonDoneAddLogo.visibility = View.INVISIBLE
            viewTransparentAlpha.visibility = View.GONE
            clearFocusStickerview()
            pushStickerViewToStream()
        }
    }

    val POSITION_FILTER_STICKER_VIEW = 1
    val POSITION_FILTER_GIF = 2
    fun pushStickerViewToStream() {
        if (stickerView.stickerCount == 0) {
            rtmpCamera?.glInterface?.setFilter(POSITION_FILTER_STICKER_VIEW, NoFilterRender())
            return
        }
        val filterObj = ImageObjectFilterRender()
        rtmpCamera!!.glInterface.setFilter(POSITION_FILTER_STICKER_VIEW, filterObj)
        val bitmap = stickerView.createBitmapfromStickerView(
            itemLivestreamConfig.videoWidth,
            itemLivestreamConfig.videoHeight
        )
        filterObj.setImage(bitmap)
        filterObj.setDefaultScale(
            rtmpCamera!!.streamWidth,
            rtmpCamera!!.streamHeight
        )
        filterObj.setPosition(0f, 0f)
    }

    fun clearFocusStickerview() {
        stickerView?.handlingSticker = null
        stickerView?.invalidate()
    }

    val spriteGestureController = SpriteGestureController()

    fun pushGifToStream(uri: Uri) {
        val fileCompressor = FileCompressor(requireActivity())
        val filePath = fileCompressor.getRealPathFromUri(requireActivity(), uri)
        if (filePath.isNullOrEmpty()) {
            (activity as BaseActivity).showToast(R.string.msg_not_file)
            return
        }
        if (!filePath.endsWith("gif", true)) {
            (activity as BaseActivity).showToast(R.string.msg_select_gif_image)
            return
        }

        Loggers.e("pushGifToStream_filePath", "$filePath")
        Loggers.e(
            "pushGifToStream_streamSize",
            "${rtmpCamera!!.streamWidth} x ${rtmpCamera!!.streamHeight}"
        )

        try {
            val imageSize = getImageSize(filePath)
            Loggers.e("pushGifToStream_imageSize", "" + imageSize[0] + " x " + imageSize[1])

            val inputStream = requireActivity().contentResolver.openInputStream(uri)
            if (inputStream == null) {
                return
            }
            val filterObj = GifObjectFilterRender()
            rtmpCamera!!.glInterface.setFilter(POSITION_FILTER_GIF, filterObj)
            filterObj.setGif(inputStream)
            filterObj.setDefaultScale(
                rtmpCamera!!.streamWidth,
                rtmpCamera!!.streamHeight
            )

            var scaleX = 0f
            var scaleY = 0f
            if (itemLivestreamConfig.isLandscape) {
                scaleX = imageSize[0] * 1f / rtmpCamera!!.streamWidth
                scaleY = imageSize[1] * 1f / rtmpCamera!!.streamHeight
            } else {
                scaleX = imageSize[0] * 1f / rtmpCamera!!.streamHeight
                scaleY = imageSize[1] * 1f / rtmpCamera!!.streamWidth
            }
            Loggers.e("pushGifToStream_scale", "$scaleX _ $scaleY")
            Loggers.e("pushGifToStream_", "----- 3 -------")

            filterObj.setScale(scaleX * 100, scaleY * 100)
//            filterObj.setScale(0.2f * 100, scaleY * 100)
            filterObj.setPosition(TranslateTo.CENTER)
            spriteGestureController.setBaseObjectFilterRender(filterObj)
            spriteGestureController.setPreventMoveOutside(false)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getImageSize(filePath: String): IntArray {
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(File(filePath).absolutePath, options)
        val imageHeight = options.outHeight
        val imageWidth = options.outWidth
        Loggers.e("setGifToStream", "getDropboxIMGSize = $imageWidth x $imageHeight")
        return intArrayOf(imageWidth, imageHeight)
    }

    fun stopRecord() {
        buttonRecord.isSelected = false
        buttonRecord.clearColorFilter()
        rtmpCamera!!.stopRecord()
        AppUtils.addToGalleryRunBackground(
            requireActivity(),
            currentRecordFilePath,
            AppUtils.FOLDER_STORAGE
        )
    }

    var itemLive: ItemLive? = null
    var currentThumbnailPath: String? = null
    var isNeedReCallApiLive = true

    fun prepareStartLive() {
        if (!MyApplication.getInstance()!!.isNetworkConnect()) {
            showErrorNetwork()
            reloadUIWidthStopStream()
            return
        }
        val name = editTitle.text.toString().trim()
        if (!isNeedReCallApiLive && itemLive != null && !itemLive!!.streamUrl.isNullOrEmpty() && itemLive!!.title.equals(
                name
            )
        ) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                startCountDownToStartLive()
            }, Constant.DELAY_HANDLE)
            return
        }
        getData()
    }

    fun getData() {
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(requireActivity())
        } else {
            myHttpRequest!!.cancel()
        }
        val itemUser = MyApplication.getInstance()?.getDataManager()?.itemUser
        if (itemUser == null) {
            reloadUIWidthStopStream()
            return
        }
        (activity as BaseActivity).showLoadingDialog(
            true,
            R.string.msg_prepare_stream,
            object : OnCancelListener {
                override fun onCancel(isCancel: Boolean) {
                    myHttpRequest!!.cancel()
                    isNeedReCallApiLive = true
                    reloadUIWidthStopStream()
                }
            })
        val name = editTitle.text.toString().trim()
        val token = itemUser.accessToken
        val requestParams = RequestParams()
        requestParams.put("type", "android")
        requestParams.put("access_token", token)
        requestParams.put("name", name)
        if (!currentThumbnailPath.isNullOrEmpty()) {
            requestParams.put("image_file", currentThumbnailPath)
        }
        if (itemLive != null) {
            requestParams.put("id", "${itemLive!!.id}")
        }

        myHttpRequest!!.request(
            true,
            ServiceUtil.validAPI(Constant.API_LIVE_CONFIG),
            requestParams,
            object : MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    if (isDetached) {
                        return
                    }
                    isNeedReCallApiLive = true
                    requireActivity().runOnUiThread {
                        reloadUIWidthStopStream()
                        (activity as BaseActivity).hideLoadingDialog()
                        showErrorNetwork()
                    }
                }

                override fun onSuccess(statusCode: Int, responseString: String?) {
                    if (isDetached) {
                        return
                    }
                    isNeedReCallApiLive = true
                    requireActivity().runOnUiThread { (activity as BaseActivity).hideLoadingDialog() }
                    handleData(responseString)
                }
            })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            requireActivity().runOnUiThread { showEmptyData() }
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            requireActivity().runOnUiThread { showEmptyData() }
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            reloadUIWidthStopStream()
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            requireActivity().runOnUiThread { (activity as BaseActivity).showDialog(message) }
            return
        }
        val resultObj = JsonParser.getJsonObject(jsonObject, "result")
        val id = JsonParser.getInt(resultObj, "id")
        val url = JsonParser.getString(resultObj, "url_in")
        if (id == null || id == 0 || url.isNullOrEmpty()) {
            requireActivity().runOnUiThread { showEmptyData() }
            return
        }
        val name = editTitle.text.toString().trim()
        itemLive = ItemLive(id, url, name)
        isNeedReCallApiLive = false
        requireActivity().runOnUiThread { startCountDownToStartLive() }
    }

    fun showErrorNetwork() {
        reloadUIWidthStopStream()
        (activity as LivestreamActivity).showDialog(getString(R.string.msg_network_error))
    }

    fun showEmptyData() {
        reloadUIWidthStopStream()
        (activity as LivestreamActivity).showDialog(getString(R.string.msg_empty_data))
    }

    fun initControl() {
        buttonRecordAudio.setOnClickListener {
            if (rtmpCamera!!.isStreaming || rtmpCamera!!.isRecording) {
                (activity as BaseActivity).showToast(R.string.msg_can_not_record_audio_in_streaming)
                return@setOnClickListener
            }
            val fragment = RecordAudioFragment()
            (activity as BaseActivity).showBottomSheetDialog(fragment, null)
        }
        buttonSwitchCamera.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                rtmpCamera?.switchCamera()
                facingCamera =
                    if (rtmpCamera?.isFrontCamera!!) CameraHelper.Facing.FRONT else CameraHelper.Facing.BACK
            }, Constant.DELAY_HANDLE)
        }
        buttonLive.setOnClickListener {
            if (rtmpCamera == null) {
                isStreaming = false
                return@setOnClickListener
            }
            if (rtmpCamera!!.isStreaming) {
                buttonLive.isSelected = false
                isStreaming = false
                Handler(Looper.getMainLooper()).postDelayed(Runnable {
                    rtmpCamera!!.stopStream()
//                    rtmpCamera?.glInterface?.setFilter(NoFilterRender())
                    cancelTimerCountDuration()
                    buttonLive.isEnabled = true
                }, Constant.DELAY_HANDLE)
                return@setOnClickListener
            }
            buttonLive.isEnabled = false
            prepareStartLive()
        }
        buttonCamera.setOnClickListener {
            rtmpCamera!!.glInterface.takePhoto(object : TakePhotoCallback {
                override fun onTakePhoto(bitmap: Bitmap?) {
                    GlobalScope.async(Dispatchers.IO) {
                        val filePath = AppUtils.convertBitmapToFile(requireActivity(), bitmap)
                        if (!isDetached && !filePath.isNullOrEmpty()) {
                            AppUtils.addToGalleryRunBackground(
                                requireActivity(),
                                filePath,
                                AppUtils.FOLDER_STORAGE
                            )
                        }
                    }
                }
            })
        }
        buttonRecord.setOnClickListener {
            if (!rtmpCamera!!.isRecording) {
                try {
                    currentRecordFilePath = AppUtils.createVideoFilePath(requireActivity())
                    Loggers.e("fileName", "$currentRecordFilePath")
                    if (!rtmpCamera!!.isStreaming) {
                        if (prepareEncoders()) {
                            buttonRecord.isSelected = true
                            buttonRecord.setColorFilter(
                                ContextCompat.getColor(
                                    requireActivity(),
                                    R.color.red
                                )
                            )
                            rtmpCamera!!.startRecord(currentRecordFilePath)
                        } else {
                            (activity as BaseActivity).showToast(R.string.msg_error_config_record)
                        }
                        return@setOnClickListener
                    }
                    buttonRecord.isSelected = true
                    buttonRecord.setColorFilter(
                        ContextCompat.getColor(
                            requireActivity(),
                            R.color.red
                        )
                    )
                    rtmpCamera!!.startRecord(currentRecordFilePath)
                } catch (e: Exception) {
                    buttonRecord.isSelected = false
                    currentRecordFilePath = ""
                    e.printStackTrace()
                }
                return@setOnClickListener
            }
            stopRecord()
        }
        buttonFilter.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                showPopupMenuFilter()
            }, Constant.DELAY_HANDLE)
        }
        buttonSetting.setOnClickListener {
            if (buttonLive.isSelected) {
                (activity as BaseActivity).showToast(R.string.msg_not_setting_while_streaming)
                return@setOnClickListener
            }
//            rtmpCamera!!.stopPreview()
            val fragment = GoLivestreamSettingFragment()
            val bundle = Bundle()
            bundle.putParcelable(Constant.DATA, itemLivestreamConfig)
            fragment.onGoLivestreamSettingDismissListener =
                object : OnGoLivestreamSettingDismissListener {
                    override fun onDismiss(itemLivestreamConfig: ItemLivestreamConfig?) {
                        if (itemLivestreamConfig != null) {
                            this@GoLivestreamFragment.itemLivestreamConfig = itemLivestreamConfig
                            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                                if (this@GoLivestreamFragment.isLandscape == itemLivestreamConfig.isLandscape) {
                                    reloadPreview(false)
                                } else {
                                    isLandscape =
                                        this@GoLivestreamFragment.itemLivestreamConfig.isLandscape
                                    if (isLandscape) {
                                        requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
                                    } else {
                                        requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                                    }
                                    isNeedReloadPreview = true
                                    /*Handler(Looper.getMainLooper()).postDelayed(Runnable {
                                        reloadPreview()
                                    }, 2000)*/
                                }
                            }, 400)
                        }
                    }
                }
            (activity as BaseActivity).showBottomSheetDialog(fragment, bundle)
        }
        imageThumbnail.setOnClickListener {
            showSelectThumbnailStreamPopup()
        }
    }

    //    TODO: START: CHANGE LIVE THUMBNAIL
    var popupLiveThumbnail: PowerMenu? = null
    var fileCompressor: FileCompressor? = null
    fun showSelectThumbnailStreamPopup() {
        val itemList: ArrayList<PowerMenuItem> = ArrayList()
        itemList.add(PowerMenuItem(getString(R.string.select_from_gallery)))
        itemList.add(PowerMenuItem(getString(R.string.select_from_camera)))
        if (popupLiveThumbnail != null) {
            popupLiveThumbnail!!.showAsDropDown(imageThumbnail)
            return
        }
        val textView = TextView(activity)
        textView.textSize = 17f
        textView.setTextColor(ContextCompat.getColor(requireActivity(), R.color.color_primary_dark))
        textView.text = getString(R.string.stream_thumbnail_title)
        textView.setPadding(25, 15, 15, 15)
        textView.setBackgroundColor(Color.parseColor("#f8f8f8"))
        textView.setTypeface(Typeface.create("sans-serif-medium", Typeface.BOLD))

        val screenSize = ScreenSize(requireActivity())
        val popupWidth = screenSize.width * 2 / 3
//        val popupHeight = screenSize.height / 2
        popupLiveThumbnail = PowerMenu.Builder(requireActivity())
            .addItemList(itemList)
            .setAnimation(MenuAnimation.SHOWUP_TOP_LEFT)
            .setAutoDismiss(true)
            .setShowBackground(true)
            .setWidth(popupWidth)
//                .setHeight(popupHeight)
            .setMenuRadius(10f)
            .setMenuShadow(10f)
            .setDividerHeight(1)
            .setDivider(ColorDrawable(ContextCompat.getColor(requireActivity(), R.color.dark_text)))
            .setCircularEffect(CircularEffect.BODY)
            .setTextSize(16)
            .setTextGravity(Gravity.START)
            .setTextColor(ContextCompat.getColor(requireActivity(), R.color.dark_text))
            .setTextTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL))
            .setSelectedTextColor(ContextCompat.getColor(requireActivity(), R.color.dark_text))
            .setMenuColor(Color.parseColor("#f8f8f8"))
            .setSelectedMenuColor(Color.parseColor("#f8f8f8"))
            .setOnMenuItemClickListener(object : OnMenuItemClickListener<PowerMenuItem> {
                override fun onItemClick(position: Int, item: PowerMenuItem?) {
                    popupLiveThumbnail!!.dismiss()
                    if (item == null) {
                        return
                    }
                    val title = item.title
                    if (title.equals(getString(R.string.select_from_gallery))) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(
                                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                                Constant.REQUEST_CODE_GALLERY
                            )
                            return
                        }
                        openGallery()
                        return
                    }
                    if (title.equals(getString(R.string.select_from_camera))) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(
                                arrayOf(Manifest.permission.CAMERA),
                                Constant.REQUEST_CODE_CAMERA
                            )
                            return
                        }
                        openCamera()
                    }
                }

            })
            .setHeaderView(textView)
            .build()
        popupLiveThumbnail!!.showAsDropDown(imageThumbnail)
    }

    fun openGallery() {
//        val intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "image/*"
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
//        intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
        startActivityForResult(intent, Constant.REQUEST_CODE_GALLERY)
    }

    private fun createImageFile(): File? {
        val fileName = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? =
            requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        if (storageDir == null) {
            return null
        }
        return File.createTempFile(fileName, ".jpg", storageDir)
    }

    var photoFile: File? = null
    fun openCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(requireActivity().packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            if (photoFile != null) {
                val photoURI: Uri = FileProvider.getUriForFile(
                    requireActivity(), requireActivity().packageName + ".provider",
                    photoFile
                )
                this.photoFile = photoFile
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(intent, Constant.REQUEST_CODE_CAMERA)
            }
        }
    }

    fun callPermissionSettings(requestCode: Int) {
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.fromParts("package", requireActivity().packageName, null)
        intent.data = uri
        startActivityForResult(intent, requestCode)
    }

    fun uploadImageToServer(filePath: String) {
        isNeedReCallApiLive = true
        currentThumbnailPath = filePath
    }

//    TODO: END: CHANGE LIVE THUMBNAIL

    fun reloadPreview(isReloadPreview: Boolean) {
        if (isReloadPreview) {
            prepareEncoders()
            rtmpCamera!!.stopPreview()
            rtmpCamera!!.startPreview(facingCamera)
            return
        }
        if (rtmpCamera!!.streamWidth != itemLivestreamConfig.videoWidth) {
            Loggers.e("TAG", "reloadPreview")
            prepareEncoders()
            rtmpCamera!!.stopPreview()
            rtmpCamera!!.startPreview(facingCamera)
        } else {
            Loggers.e("TAG", "not reloadPreview")
            prepareEncoders()
        }
    }

    var currentRecordFilePath = ""

    fun prepareEncoders(): Boolean {
        if (rtmpCamera == null) {
            return false
        }
        //VIDEO
        val videoBitrate = itemLivestreamConfig.videoBitrate * 1024 // H264 in bps.
        rtmpCamera!!.setVideoBitrateOnFly(videoBitrate)
        rtmpCamera!!.glInterface.enableAA(itemLivestreamConfig.isAntiAliasing) // Anti aliasing: khử răng cưa
//        val rotation = if (itemLivestreamConfig.isLandscape) 0 else 90
        val rotation = CameraHelper.getCameraOrientation(activity)
        Loggers.e(
            "prepareEncoders_rotation",
            "$rotation _ ${CameraHelper.getCameraOrientation(activity)}"
        )
//        rtmpCamera!!.glInterface.setStreamRotation(rotation)
//        Loggers.e("streamSize", "${itemLivestreamConfig.videoWidth} x ${itemLivestreamConfig.videoHeight}")

//        val rotation =
//            CameraHelper.getCameraOrientation(activity) // rotation could be 90, 180, 270 or 0 (Normally 0 if you are streaming in landscape or 90 if you are streaming in Portrait). This only affect to stream result. NOTE: Rotation with encoder is silence ignored in some devices.

//        rtmpCamera!!.glInterface.setRotation(rotation)
//        rtmpCamera!!.glInterface.setStreamRotation(rotation)
//        Loggers.e("videoSize", "$videoWidth x $videoHeight")
        val isPrepareVideo = rtmpCamera!!.prepareVideo(
            itemLivestreamConfig.videoWidth,
            itemLivestreamConfig.videoHeight,
            itemLivestreamConfig.fps,
            videoBitrate,
            rotation
        )
//        rtmpCamera!!.glInterface.setRotation(90)

        //AUDIO
        val audioBitrate = itemLivestreamConfig.audioBitrate * 1024 // AAC in kb
        val isPrepareAudio = rtmpCamera!!.prepareAudio(
            audioBitrate,
            itemLivestreamConfig.sampleRate,
            itemLivestreamConfig.isStereo,
            itemLivestreamConfig.isEchoCanceler,
            itemLivestreamConfig.isNoiseSuppressor
        )
        if (itemLivestreamConfig.isAudioMute) {
            rtmpCamera!!.disableAudio()
        } else {
            rtmpCamera!!.enableAudio()
        }
//        rtmpCamera!!.setAuthorization("", "")
        return isPrepareVideo && isPrepareAudio
    }

    //    TODO: START: CHECK PERMISSION
    var easyPermission: EasyPermissions? = null
    private val ALL_PERMISSIONS = arrayOf(
        Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.RECORD_AUDIO
    )

    fun checkPermission() {
        easyPermission = EasyPermissions.Builder()
            .with(requireActivity())
            .listener(object : OnPermissionListener {
                override fun onAllPermissionsGranted(permissions: MutableList<String>) {
                    /*for (i in 0 until permissions.size) {
                        Loggers.e("A_onAllGranted_$i", "${permissions.get(i)}")
                    }*/
                    init()
                }

                override fun onPermissionsGranted(permissions: MutableList<String>) {
                    /*for (i in 0 until permissions.size) {
                        Loggers.e("A_Granted_$i", "${permissions.get(i)}")
                    }*/
                    if (permissions.size == ALL_PERMISSIONS.size) {
                        init()
                        return
                    }
                    if (easyPermission?.hasPermission(*ALL_PERMISSIONS)!!) {
                        init()
                    }
                }

                override fun onPermissionsDenied(permissions: MutableList<String>) {
                    /*for (i in 0 until permissions.size) {
                        Loggers.e("A_Denied_$i", "${permissions.get(i)}")
                    }*/
                    activity?.runOnUiThread { (activity as BaseActivity).showToast("Vui lòng cho phép các quyền để tiếp tục sử dụng") }
                    checkPermission()
                }
            }).build()
        if (easyPermission?.hasPermission(*ALL_PERMISSIONS)!!) {
            init()
            return
        }
        easyPermission?.request(*ALL_PERMISSIONS)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        easyPermission?.onRequestPermissionsResult(permissions, grantResults)
        when (requestCode) {
            Constant.REQUEST_CODE_GALLERY, Constant.REQUEST_CODE_CAMERA -> {
                for (permission in permissions) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            requireActivity(),
                            permission
                        )
                    ) {
                        (activity as? BaseActivity)?.showDialog(getString(R.string.msg_accept_permission))
                        break
                    } else {
                        if (ActivityCompat.checkSelfPermission(
                                requireActivity(),
                                permission
                            ) == PackageManager.PERMISSION_GRANTED
                        ) {
                            if (requestCode == Constant.REQUEST_CODE_GALLERY) {
                                openGallery()
                            } else if (requestCode == Constant.REQUEST_CODE_CAMERA) {
                                openCamera()
                            }
                        } else {
                            callPermissionSettings(requestCode)
                        }
                    }
                }
            }
        }
    }
//    TODO: END: CHECK PERMISSION

    var mFragment: BottomSheetDialogFragment? = null
    private fun openAddLogoFragment() {
        loadingBottom.show()
        if (!(mFragment is AddLogoFragment)) {
            mFragment?.dismiss()
        }
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            val fragment = AddLogoFragment()
            fragment.stickerView = stickerView
            mFragment = fragment
            (activity as BaseActivity).showBottomSheetDialog(fragment, null)
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                loadingBottom.hide()
            }, 2000)
        }, Constant.DELAY_HANDLE)
    }

    private fun openAddTextFragment() {
        if (!(mFragment is AddTextFragment)) {
            mFragment?.dismiss()
        }
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            val fragment = AddTextFragment()
            fragment.stickerView = stickerView
            if (stickerView?.currentSticker is StickerText) {
                fragment.setText((stickerView?.currentSticker!! as StickerText).text)
            }
            mFragment = fragment
            (activity as BaseActivity).showBottomSheetDialog(fragment, null)
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                loadingBottom.hide()
            }, 2000)
        }, Constant.DELAY_HANDLE)
    }

    private fun openAddGifFragment() {
        if (rtmpCamera!!.isStreaming) {
            (activity as BaseActivity).showToast(R.string.not_use_this_feature_in_livestreaming)
            return
        }
        if (!(mFragment is AddLogoFragment)) {
            mFragment?.dismiss()
        }
        GetImage.openGellaryOnlyGifImage(FragmentSelectImge.REQUEST_GIF_IMAGE, activity)
    }

    //    TODO: START: FILTER DIALOG
    fun initFilterList(): ArrayList<PowerMenuItem> {
        val itemList: ArrayList<PowerMenuItem> = ArrayList()
        itemList.add(PowerMenuItem(getString(R.string.item_filter_no_filter)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_image)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_text)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_gif)))
//        itemList.add(PowerMenuItem(getString(R.string.item_filter_surface_filter)))
//        itemList.add(PowerMenuItem(getString(R.string.item_filter_add_view)))
//        itemList.add(PowerMenuItem(getString(R.string.item_filter_analog_tv)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_snow)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_blur)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_beauty)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_black)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_brightness)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_cartoon)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_circle)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_duotone)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_earlybird)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_edge_detection)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_fire)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_glitch)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_grey_scale)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_halftone_lines)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_lamoish)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_money)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_negative)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_pixelated)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_polygonization)))
//        itemList.add(PowerMenuItem(getString(R.string.item_filter_rgb_saturate)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_ripple)))
//        itemList.add(PowerMenuItem(getString(R.string.item_filter_rotation)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_saturation)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_sepia)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_sharpness)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_swirl)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_rainbow)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_temperature)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_zebra)))
        itemList.add(PowerMenuItem(getString(R.string.item_filter_basic_deformation)))
        return itemList
    }

    var popupMenuFilter: PowerMenu? = null
    fun showPopupMenuFilter() {
        if (popupMenuFilter != null) {
            popupMenuFilter!!.showAsAnchorRightTop(buttonFilter)
            popupMenuFilter!!.setSelection(popupMenuFilter!!.selectedPosition)
            return
        }
        val itemList = initFilterList()
        val screenSize = ScreenSize(requireActivity())
        val popupWidth = screenSize.width / 3
//        val popupHeight = screenSize.height / 3
        val maxPopHeight = screenSize.height * 3 / 4
        var popupHeight = resources.getDimensionPixelSize(R.dimen.dimen_50) * itemList.size
        if (popupHeight > maxPopHeight) {
            popupHeight = maxPopHeight
        }
        popupMenuFilter = PowerMenu.Builder(requireActivity())
            .addItemList(itemList)
            .setAnimation(MenuAnimation.SHOWUP_BOTTOM_RIGHT)
            .setShowBackground(true)
            .setAutoDismiss(true)
//            .setWidth(popupWidth)
            .setHeight(popupHeight)
            .setMenuRadius(10f)
            .setMenuShadow(10f)
            .setDividerHeight(1)
            .setDivider(ColorDrawable(ContextCompat.getColor(requireActivity(), R.color.gray2)))
            .setCircularEffect(CircularEffect.BODY)
            .setTextSize(16)
            .setTextGravity(Gravity.START)
            .setTextColor(ContextCompat.getColor(requireActivity(), R.color.dark_text))
            .setTextTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL))
            .setSelectedTextColor(ContextCompat.getColor(requireActivity(), R.color.color_primary))
            .setMenuColor(ContextCompat.getColor(requireActivity(), R.color.white))
            .setSelectedMenuColor(ContextCompat.getColor(requireActivity(), R.color.white))
            .setOnMenuItemClickListener(object : OnMenuItemClickListener<PowerMenuItem> {
                override fun onItemClick(position: Int, item: PowerMenuItem?) {
                    popupMenuFilter!!.selectedPosition = position
                    popupMenuFilter!!.dismiss()
                    Handler(Looper.getMainLooper()).postDelayed(Runnable {
                        handleItemMenuFilterSelected(position, item)
                    }, 200)
                }
            })
            .build()
        popupMenuFilter!!.showAsAnchorRightTop(buttonFilter)
    }

    var numberFilter = 0
    var filterList: ArrayList<BaseFilterRender> = ArrayList()
    fun handleItemMenuFilterSelected(position: Int, item: PowerMenuItem?) {
        if (item == null) {
            return
        }
        var filter: BaseFilterRender? = null
        when (item.title) {
            getString(R.string.item_filter_no_filter) -> filter = NoFilterRender()
            getString(R.string.item_filter_image) -> {
                popupMenuFilter!!.selectedPosition = -1
                openAddLogoFragment()
                return
            }
            getString(R.string.item_filter_text) -> {
                popupMenuFilter!!.selectedPosition = -1
                openAddTextFragment()
                return

            }
            getString(R.string.item_filter_gif) -> {
                popupMenuFilter!!.selectedPosition = -1
                openAddGifFragment()
                return
            }
            getString(R.string.item_filter_surface_filter) -> {

            }
            getString(R.string.item_filter_add_view) -> {

            }
            getString(R.string.item_filter_analog_tv) -> filter = AnalogTVFilterRender()
            getString(R.string.item_filter_snow) -> filter = SnowFilterRender()
            getString(R.string.item_filter_blur) -> filter = BlurFilterRender()
            getString(R.string.item_filter_beauty) -> filter = BeautyFilterRender()
            getString(R.string.item_filter_black) -> filter = BlackFilterRender()
            getString(R.string.item_filter_brightness) -> filter = BrightnessFilterRender()
            getString(R.string.item_filter_cartoon) -> filter = CartoonFilterRender()
            getString(R.string.item_filter_circle) -> filter = CircleFilterRender()
            getString(R.string.item_filter_duotone) -> filter = DuotoneFilterRender()
            getString(R.string.item_filter_earlybird) -> filter = EarlyBirdFilterRender()
            getString(R.string.item_filter_edge_detection) -> filter = EdgeDetectionFilterRender()
            getString(R.string.item_filter_fire) -> filter = FireFilterRender()
            getString(R.string.item_filter_glitch) -> filter = GlitchFilterRender()
            getString(R.string.item_filter_grey_scale) -> filter = GreyScaleFilterRender()
            getString(R.string.item_filter_halftone_lines) -> filter = HalftoneLinesFilterRender()
            getString(R.string.item_filter_lamoish) -> filter = LamoishFilterRender()
            getString(R.string.item_filter_money) -> filter = MoneyFilterRender()
            getString(R.string.item_filter_negative) -> filter = NegativeFilterRender()
            getString(R.string.item_filter_pixelated) -> filter = PixelatedFilterRender()
            getString(R.string.item_filter_polygonization) -> filter = PolygonizationFilterRender()
//            getString(R.string.item_filter_rgb_saturate) -> filter = AnalogTVFilterRender()
            getString(R.string.item_filter_ripple) -> filter = RippleFilterRender()
//            getString(R.string.item_filter_rotation) -> filter = AnalogTVFilterRender()
            getString(R.string.item_filter_saturation) -> filter = SaturationFilterRender()
            getString(R.string.item_filter_sepia) -> filter = SepiaFilterRender()
            getString(R.string.item_filter_sharpness) -> filter = SharpnessFilterRender()
            getString(R.string.item_filter_swirl) -> filter = SwirlFilterRender()
            getString(R.string.item_filter_rainbow) -> filter = RainbowFilterRender()
            getString(R.string.item_filter_temperature) -> filter = TemperatureFilterRender()
            getString(R.string.item_filter_zebra) -> filter = ZebraFilterRender()
            getString(R.string.item_filter_basic_deformation) -> filter =
                BasicDeformationFilterRender()
        }
        if (filter != null) {
            rtmpCamera?.glInterface?.setFilter(0, filter)
//            filterList.add(filter)
//            numberFilter++
//            if (numberFilter >= 10) {
//                numberFilter = 0
//            }
        }

    }

    fun isPopupMenuFilterShowing(): Boolean {
        clearFocusStickerview()
        if (popupMenuFilter == null || !popupMenuFilter!!.isShowing()) {
            return false
        }
        popupMenuFilter!!.dismiss()
        return true
    }
//    TODO: END: FILTER DIALOG

    var isStreaming = false

    override fun onPause() {
        if (rtmpCamera == null) {
            super.onPause()
            return
        }
        isStreaming = rtmpCamera!!.isStreaming
        Loggers.e("onPause", "isStreaming = $isStreaming")
        if (rtmpCamera!!.isRecording) {
            stopRecord()
        }
        super.onPause()
    }

    override fun onResume() {
        if (isStreaming) {
            startLive()
        }
        super.onResume()
    }

    override fun onDetach() {
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        popupMenuFilter?.dismiss()
        rtmpCamera!!.stopStream()
        cancelTimerCountDuration()
        super.onDetach()
    }

    var isNeedReloadPreview = false
    override fun onConfigurationChanged(newConfig: Configuration) {
        Loggers.e("TAG", "onConfigurationChanged")
//        Handler(Looper.getMainLooper()).postDelayed(Runnable {
        if (isNeedReloadPreview) {
            reloadPreview(true)
            isNeedReloadPreview = false
        }
//            reloadPreview(true)
//        }, 4000)
        super.onConfigurationChanged(newConfig)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        when (requestCode) {
            FragmentSelectImge.REQUEST_GIF_IMAGE -> {
                if (data == null) return
                val uri = data.data
                if (uri == null) return
                pushGifToStream(uri)
            }
            Constant.REQUEST_CODE_GALLERY -> {
                if (data == null) return
                data.let {
                    val selectedImage = data.data
                    if (selectedImage == null) {
                        return
                    }
                    if (fileCompressor == null) {
                        fileCompressor = FileCompressor(requireActivity())
                    }
                    val realPath =
                        fileCompressor!!.getRealPathFromUri(requireActivity(), selectedImage)
                    if (realPath.isNullOrEmpty()) {
                        return
                    }
                    photoFile = fileCompressor!!.compressToFile(File(realPath))
                    MyApplication.getInstance()
                        ?.loadImage(requireActivity(), imageThumbnail, photoFile, true)
                    uploadImageToServer(photoFile.toString())
                    photoFile = null
                }
            }
            Constant.REQUEST_CODE_CAMERA -> {
                if (photoFile == null) {
                    return
                }
                if (fileCompressor == null) {
                    fileCompressor = FileCompressor(requireActivity())
                }
                photoFile = fileCompressor!!.compressToFile(photoFile)
                MyApplication.getInstance()
                    ?.loadImage(requireActivity(), imageThumbnail, photoFile, true)
                uploadImageToServer(photoFile.toString())
                photoFile = null
            }
        }
    }
}