package com.go.videoeditor.stream

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemLivestreamConfig(
    var videoBitrate: Int = 0,
    var fps: Int = 0,
    var videoWidth: Int = 0,
    var videoHeight: Int = 0,
    var audioBitrate: Int = 0,
    var sampleRate: Int = 0,
    var isLandscape: Boolean = false,
    var isNoiseSuppressor: Boolean = true,
    var isEchoCanceler: Boolean = true,
    var isStereo: Boolean = true,
    var isAudioMute: Boolean = false,
    var isAntiAliasing: Boolean = true
) : Parcelable