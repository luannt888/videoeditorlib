package com.go.videoeditor.add.addMusic

import androidx.recyclerview.widget.RecyclerView

interface OnStartDragMusicListener {
    fun onStartDrag(viewHolder: RecyclerView.ViewHolder)
}