/*
 *
 *  Created by Optisol on Aug 2019.
 *  Copyright © 2019 Optisol Business Solutions pvt ltd. All rights reserved.
 *
 */

package com.go.videoeditor.add.addLogo

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.AssetManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.go.videoeditor.R
import com.go.videoeditor.activity.VideoEditorActivity
import com.go.videoeditor.add.addLogo.AdapterColorIconFilter.ColorFilerListener
import com.go.videoeditor.add.stickerLib.GetImage
import com.go.videoeditor.add.stickerLib.ManagerAssets
import com.go.videoeditor.add.stickerLib.StickerDrawable
import com.go.videoeditor.add.stickerLib.StickerView
import com.go.videoeditor.app.Constant
import com.go.videoeditor.customView.MyBottomSheetDialogFragment
import com.go.videoeditor.model.ItemCmd
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_add_logo.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.util.*
import kotlin.collections.ArrayList

class AddLogoFragment : MyBottomSheetDialogFragment(), View.OnClickListener {
    private var tagName: String = AddLogoFragment::class.java.simpleName
    private lateinit var rootView: View
    private lateinit var linearLayoutManagerOne: LinearLayoutManager
    private lateinit var linearLayoutManagerTwo: LinearLayoutManager
    private lateinit var rvClipArt: RecyclerView
    private lateinit var rvPosition: RecyclerView
    private lateinit var ivClose: ImageView
    private lateinit var ivDone: ImageView
    private var videoFile: File? = null
    private var listIconPath: ArrayList<String> = ArrayList()
    private var positionList: ArrayList<String> = ArrayList()
    private lateinit var optiClipArtAdapter: LogoRcvAdapter
    private var selectedPositionItem: String? = null
    private var selectedFilePath: String? = null
    private var mContext: Context? = null
    var stickerView: StickerView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_add_logo, container, false)
        return rootView
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }

    override fun onResume() {
        super.onResume()
    }

    var bottomSheetDialog: BottomSheetDialog? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = true
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        rvClipArt = rootView.findViewById(R.id.rvClipArt)
        ivClose = rootView.findViewById(R.id.buttonClose)
        ivDone = rootView.findViewById(R.id.buttonDone)
        initView()
        linearLayoutManagerOne =
            LinearLayoutManager(activity!!.applicationContext)
        linearLayoutManagerOne.orientation = LinearLayoutManager.HORIZONTAL
        rvClipArt.layoutManager = linearLayoutManagerOne
        mContext = context
        listIconPath = ManagerAssets.getLogoPathAssetList(activity)
        Collections.reverse(listIconPath)
        optiClipArtAdapter =
            LogoRcvAdapter(
                listIconPath,
                activity!!.applicationContext,
                object : LogoRcvAdapter.OnSelectLogoListener {
                    override fun onSelectedLogo(path: String) {
                        selectedFilePath = path
                        stickerView?.let { createStickerDrawAble(selectedFilePath!!) }
                    }
                })
        rvClipArt.adapter = optiClipArtAdapter
        optiClipArtAdapter.notifyDataSetChanged()

        ivClose.setOnClickListener {
            dismiss()
        }

        ivDone.setOnClickListener {
            //add cmd at here
            val cmd = "add Logo"
            if (activity is VideoEditorActivity) {
                (activity as VideoEditorActivity).addCmd(ItemCmd(Constant.KEY_ADD_LOGO, cmd))
            }
            dismiss()
        }
    }

    fun setFilePathFromSource(file: File) {
        videoFile = file
    }

    private fun createStickerDrawAble(path: String) {
//        if (path.contains("camera")) {
//            var fragmentSelectImge = FragmentSelectImge()
//
//        }

        //get icon from asses
        val assetManager: AssetManager = activity!!.getAssets()
        val inputStream: InputStream
        var bitmap: Bitmap? = null
        try {
            inputStream = assetManager.open(path)
            bitmap = BitmapFactory.decodeStream(inputStream)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        if (bitmap != null) {
            val drawable: Drawable =
                BitmapDrawable(resources, bitmap)

            var sticker = stickerView?.currentSticker
            if (sticker != null && sticker is StickerDrawable) {
                sticker.setDrawable(drawable)
            } else {
                val stickerDrawable = StickerDrawable(drawable, activity)
                stickerDrawable.setAddImage(true)
                stickerView?.addSticker(stickerDrawable)
            }
            stickerView?.invalidate()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        stickerView?.handlingSticker = null
        stickerView?.invalidate()
    }

    val REQUEST_CAMERA = 1000
    val PERMISS_CAMERA = 2001
    val REQUEST_GALL = 1001
    private var filerRcv: RecyclerView? = null
    private var colorFiltersAdapter: AdapterColorIconFilter? = null
    private var linearLayoutManagerColorFilter: LinearLayoutManager? = null

    private fun initView() {
        textHeaderTitle.text = getString(R.string.item_add_logo)
        getPhoto_camera.setOnClickListener(this)
        getPhoto_gallery.setOnClickListener(this)
        resetAlpha.setOnClickListener(this)
        resetFiler.setOnClickListener(this)
        loadRcvFileter(rootView)
        setupAlpha(rootView)
        updateStatusMenuOfIcon()
    }

    private var iconalpha: SeekBar? = null
    private fun setupAlpha(v: View) {
        iconalpha = v.findViewById(R.id.iconalpha)
        val sticker = stickerView!!.currentSticker
        if (sticker is StickerDrawable) {
            iconalpha?.setProgress(sticker.alpha)
        }
        iconalpha?.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    val sticker = stickerView!!.currentSticker
                    if (sticker is StickerDrawable) {
                        sticker.alpha = progress
                        stickerView!!.invalidate()
                    }
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })
    }


    private fun initColorList(): List<Int> {
        val listColors: MutableList<Int> = java.util.ArrayList()
        val typedArray = resources.obtainTypedArray(R.array.origil_colors)
        //bo mau tranparent
        for (i in 1 until typedArray.length()) {
            listColors.add(typedArray.getColor(i, 0))
        }
        typedArray.recycle()
        return listColors
    }

    private fun loadRcvFileter(v: View) {
        filerRcv = v.findViewById(R.id.list_filter)
        val listColor = initColorList()
        colorFiltersAdapter = AdapterColorIconFilter(
            activity,
            ColorFilerListener { colorFilter, pos ->
                val sticker = stickerView!!.currentSticker
                if (sticker is StickerDrawable) {
                    sticker.setColorSticker(colorFilter, pos)
                    stickerView!!.invalidate()
                }
            }, listColor
        )
        linearLayoutManagerColorFilter = LinearLayoutManager(
            activity,
            RecyclerView.HORIZONTAL,
            false
        )
        filerRcv?.setAdapter(colorFiltersAdapter)
        filerRcv?.setLayoutManager(linearLayoutManagerColorFilter)
        filerRcv?.setItemAnimator(DefaultItemAnimator())
    }

    fun updateStatusMenuOfIcon() {
        if (stickerView == null) return
        val sticker = stickerView!!.currentSticker
        if (sticker is StickerDrawable) {
            //update
            if (filerRcv != null && filerRcv?.getVisibility() == View.VISIBLE) {
                colorFiltersAdapter!!.pos = sticker.posColor
                colorFiltersAdapter!!.notifyDataSetChanged()
                linearLayoutManagerColorFilter!!.scrollToPositionWithOffset(
                    sticker.posColor,
                    filerRcv!!.getWidth() / 2
                )
            }
            if (iconalpha != null && iconalpha!!.getVisibility() == View.VISIBLE) {
                iconalpha!!.setProgress(sticker.alpha)
            }
        }
    }

    private var getImageCameraFile: File? = null

    override fun onClick(v: View) {
        val id = v.id
        if (id == R.id.getPhoto_camera) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (activity!!.checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    getImageCameraFile = GetImage.openCamera(
                        FragmentSelectImge.REQUEST_CAMERA,
                        activity
                    )
                } else {
                    ActivityCompat.requestPermissions(
                        activity!!,
                        arrayOf(Manifest.permission.CAMERA),
                        FragmentSelectImge.PERMISS_CAMERA
                    )
                }
            } else {
                getImageCameraFile = GetImage.openCamera(
                    FragmentSelectImge.REQUEST_CAMERA,
                    activity
                )
            }
        } else if (id == R.id.getPhoto_gallery) {
            GetImage.openGellary(FragmentSelectImge.REQUEST_GALL, activity)
        } else if (id == R.id.resetAlpha) {
            val sticker = stickerView!!.currentSticker
            if (sticker is StickerDrawable) {
                sticker.alpha = 255
                stickerView!!.invalidate()
                iconalpha!!.progress = 255
            }
        } else if (id == R.id.resetFiler) {
            val sticker2 = stickerView!!.currentSticker
            if (sticker2 is StickerDrawable) {
                sticker2.setColorSticker(Color.WHITE, 0)
                stickerView!!.invalidate()
            }
            if (filerRcv != null && filerRcv!!.getVisibility() == View.VISIBLE) {
                colorFiltersAdapter!!.pos = 0
                colorFiltersAdapter!!.notifyDataSetChanged()
                linearLayoutManagerColorFilter!!.scrollToPositionWithOffset(
                    0,
                    filerRcv!!.getWidth() / 2
                )
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CAMERA && resultCode == Activity.RESULT_OK) {
            getImageCameraFile?.let {
                openCropFragment(
                    stickerView!!, Uri.fromFile(
                        getImageCameraFile
                    )
                )
            }
        } else if (requestCode == REQUEST_GALL && resultCode == Activity.RESULT_OK) {
            var uri = data!!.data
            uri?.let { openCropFragment(stickerView!!, uri) }
        }
    }

    private fun openCropFragment(stickerView: StickerView, uri: Uri) {
        var fragment = FragmentCrop(stickerView, uri)
        fragment.show(activity!!.supportFragmentManager, fragment.tag)
    }

    private fun createStickerDrawAbleFile(uri: Uri) {
        var bitmap: Bitmap? = null
        try {
            bitmap = MediaStore.Images.Media.getBitmap(activity!!.getContentResolver(), uri);
        } catch (e: IOException) {
            e.printStackTrace()
        }

        if (bitmap != null) {
            val drawable: Drawable =
                BitmapDrawable(resources, bitmap)

            var sticker = stickerView?.currentSticker
            if (sticker != null && sticker is StickerDrawable) {
                sticker.setDrawable(drawable)
            } else {
                val stickerDrawable = StickerDrawable(drawable, activity)
                stickerDrawable.setAddImage(true)
                stickerView?.addSticker(stickerDrawable)
            }
            stickerView?.invalidate()
        }
    }
}