package com.go.videoeditor.add.stickerLib;

import android.util.Log;
import android.view.MotionEvent;


public class EventDeleteIcon implements EventStickerIcon {
  @Override
  public void onActionDown(StickerView stickerView, MotionEvent event) {

  }

  @Override
  public void onActionMove(StickerView stickerView, MotionEvent event) {

  }

  @Override
  public void onActionUp(StickerView stickerView, MotionEvent event) {
    stickerView.removeCurrentSticker();
    Log.d("TAG", "onActionUp: click vo deleta icon"+stickerView.getCurrentSticker() );
  }
}
