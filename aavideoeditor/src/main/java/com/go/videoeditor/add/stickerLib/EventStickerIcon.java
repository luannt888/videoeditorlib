package com.go.videoeditor.add.stickerLib;

import android.view.MotionEvent;

public interface EventStickerIcon {
  void onActionDown(StickerView stickerView, MotionEvent event);

  void onActionMove(StickerView stickerView, MotionEvent event);

  void onActionUp(StickerView stickerView, MotionEvent event);
}
