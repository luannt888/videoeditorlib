package com.go.videoeditor.add.addText;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.go.videoeditor.R;

import java.util.List;

public class AdapterFontText extends RecyclerView.Adapter<AdapterFontText.MHolder> {
    public static final int NUMBER_FONT = 60;
    public Context context;
    private LayoutInflater layoutInflater;
    private List<Typeface> listFont;
    private int selectpos = -1;


    public static final String TYPEFONT =  ".ttf";
    public static final String FONT_FOLDER = "font/";

    public List<Typeface> getLisF() {
        return listFont;
    }

    public AdapterFontText(Context context, List<Typeface> listFont, ClickToFontTextItem click) {
        this.clickToFontTextItem = click;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);

        //getData

        this.listFont = listFont;
    }

    @NonNull
    @Override
    public MHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MHolder(layoutInflater.inflate(R.layout.item_font_ve,parent,false) );
    }

    @Override
    public void onBindViewHolder(@NonNull MHolder holder, int font_index) {
        holder.text_font.setTypeface(listFont.get(font_index));

        //select
        if (selectpos != font_index) {
//            holder.select.setVisibility(View.VISIBLE);
            holder.text_font.setTextColor(context.getResources().getColor(R.color.white));

        }else {
//            holder.select.setVisibility(View.INVISIBLE);
            holder.text_font.setTextColor(context.getResources().getColor(R.color.red));

        }
    }

    @Override
    public int getItemCount() {
        return listFont.size();
    }

    public class MHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView text_font;
//        private ImageView select;
        public MHolder(@NonNull View itemView) {
            super(itemView);
            text_font = itemView.findViewById(R.id.text_font);
//            select = itemView.findViewById(R.id.text_selectFont);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            try {
                int pos = getAdapterPosition();
                selectpos = pos;
                clickToFontTextItem.onClickFontTexItem(listFont.get(pos), getAdapterPosition());
                notifyDataSetChanged();
            } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        }
    }

    public void selected(int selected) {
        this.selectpos = selected;
    }

    public interface ClickToFontTextItem {
        void onClickFontTexItem(Typeface font, int posInList);
    }

    public ClickToFontTextItem clickToFontTextItem;
}
