package com.go.videoeditor.add.outputSize;

import android.content.Context;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.go.videoeditor.R;

import java.util.ArrayList;

public class AdapterRcvSize extends RecyclerView.Adapter<AdapterRcvSize.MyViewHolder> {
    private Context context;
    private ArrayList<Size> list;
    private OnClickItemListener listener;
    private int selectedPosition = -1;

    public AdapterRcvSize(Context context, ArrayList<Size> list, OnClickItemListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public AdapterRcvSize.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_output_size_ve, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRcvSize.MyViewHolder holder, int position) {
        Size item = list.get(position);
        String str = item.getWidth() + " x " + item.getHeight() + " px";
        if (position == 0) {
            str = str + " (Origin)";
        }
        holder.tv_title.setText(str);
        holder.root_view.setSelected(position == selectedPosition);
        holder.root_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                notifyDataSetChanged();
                if (listener != null) {
                    listener.onClick(list.get(position), position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnClickItemListener {
        void onClick(Size size, int position);
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView tv_title;
        private View root_view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img_thumb);
            tv_title = itemView.findViewById(R.id.tv_title);
            root_view = itemView.findViewById(R.id.root_view);
        }
    }
}
