package com.go.videoeditor.add.stickerLib;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.go.videoeditor.add.stickerLib.StickerView.SIZE_CIRCLE_RADIUS_DP;


//Class "paint,get/set cac thong so cua icon, eventHandle" StickerIcon
public class StickerBitmapIcon extends StickerDrawable implements EventStickerIcon {
    //may su dung px -> convert to dp//    public static final float DEFAULT_ICON_EXTRA_RADIUS = 10f;

    @IntDef({LEFT_TOP, RIGHT_TOP, LEFT_BOTTOM, RIGHT_BOTOM, RIGHT_MID})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Gravity {

    }

    public static final int LEFT_TOP = 0;
    public static final int RIGHT_TOP = 1;
    public static final int LEFT_BOTTOM = 2;
    public static final int RIGHT_BOTOM = 3;
    public static final int RIGHT_MID = 4;

    private float x;
    private float y;
    @Gravity
    private int position = LEFT_TOP;

    private EventStickerIcon iconEvent;

    public StickerBitmapIcon(Drawable drawable, @Gravity int gravity, Context ct) {
        super(drawable,ct);
        this.position = gravity;
    }

    public void draw(Canvas canvas, Paint paint) {
        canvas.drawCircle(x, y, SIZE_CIRCLE_RADIUS_DP, paint);
        super.draw(canvas);
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getIconRadius() {
        return SIZE_CIRCLE_RADIUS_DP;
    }

    @Override
    public void onActionDown(StickerView stickerView, MotionEvent event) {
        if (iconEvent != null) {
            iconEvent.onActionDown(stickerView, event);
        }
    }

    @Override
    public void onActionMove(StickerView stickerView, MotionEvent event) {
        if (iconEvent != null) {
            iconEvent.onActionMove(stickerView, event);
        }
    }

    @Override
    public void onActionUp(StickerView stickerView, MotionEvent event) {
        if (iconEvent != null) {
            iconEvent.onActionUp(stickerView, event);
        }
    }

    public EventStickerIcon getIconEvent() {
        return iconEvent;
    }

    public void setIconEvent(EventStickerIcon iconEvent) {
        this.iconEvent = iconEvent;
    }

    @Gravity
    public int getPosition() {
        return position;
    }

    public void setPosition(@Gravity int position) {
        this.position = position;
    }
}
