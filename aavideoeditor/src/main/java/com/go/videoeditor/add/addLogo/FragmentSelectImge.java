package com.go.videoeditor.add.addLogo;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.go.videoeditor.R;
import com.go.videoeditor.add.stickerLib.Sticker;
import com.go.videoeditor.add.stickerLib.StickerDrawable;
import com.go.videoeditor.add.stickerLib.StickerView;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.go.videoeditor.add.stickerLib.GetImage.openCamera;
import static com.go.videoeditor.add.stickerLib.GetImage.openGellary;

//CREATE FRAGMENT >> new instance >> onCreate >> onCreateView
public class FragmentSelectImge extends BottomSheetDialogFragment implements View.OnClickListener {
    public static final int REQUEST_CAMERA = 1000;
    public static final int PERMISS_CAMERA = 2001;
    public static final int REQUEST_GALL = 1001;
    public static final int REQUEST_GIF_IMAGE = 1002;
    private Activity activity;
    private RecyclerView filerRcv;
    private AdapterColorIconFilter colorFiltersAdapter;
    private LinearLayoutManager linearLayoutManagerColorFilter;

    private SeekBar iconalpha;
    private StickerView stickerView;

    public void setStickerView(StickerView stickerView) {
        this.stickerView = stickerView;
    }

    public static FragmentSelectImge newInstance() {

//        Bundle args = new Bundle();
//        fragment.setArguments(args);
        return new FragmentSelectImge();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_NoTitleBar_Fullscreen);
        activity = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_select_img_ve, container, false);
        long t0 = System.currentTimeMillis();
        v.findViewById(R.id.getPhoto_camera).setOnClickListener(this);
        v.findViewById(R.id.getPhoto_gallery).setOnClickListener(this);
        v.findViewById(R.id.resetAlpha).setOnClickListener(this);
        v.findViewById(R.id.resetFiler).setOnClickListener(this);
        loadRcvFileter(v);
        long t1 = System.currentTimeMillis();

        Log.d("TAG", "onCreateView111:  >>" + (t1 - t0));

        setupAlpha(v);
        long t2 = System.currentTimeMillis();
        Log.d("TAG", "onCreateView111: " + (t2 - t1));

        updateStatusMenuOfIcon();
        long t3 = System.currentTimeMillis();
        Log.d("TAG", "onCreateView111: " + (t3 - t2));


        v.findViewById(R.id.selectImg_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return v;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        if(dialog.getWindow() !=null){
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
        }
        return dialog;
    }

    private void setupAlpha(View v) {

        iconalpha = v.findViewById(R.id.iconalpha);
        Sticker sticker = stickerView.getCurrentSticker();

        if (sticker instanceof StickerDrawable) {
            iconalpha.setProgress(((StickerDrawable) sticker).getAlpha());
        }

        iconalpha.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    Sticker sticker = stickerView.getCurrentSticker();
                    if (sticker instanceof StickerDrawable) {
                        ((StickerDrawable) sticker).setAlpha(progress);
                        stickerView.invalidate();
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }


    private List<Integer> initColorList() {
        List<Integer> listColors = new ArrayList<>();
        TypedArray typedArray = getResources().obtainTypedArray(R.array.origil_colors);
        //bo mau tranparent
        for (int i = 1; i < typedArray.length(); i++) {
            listColors.add(typedArray.getColor(i, 0));
        }
        typedArray.recycle();
        return listColors;

    }

    private void loadRcvFileter(View v) {
        filerRcv = v.findViewById(R.id.list_filter);
        List<Integer> listColor = initColorList();
        colorFiltersAdapter = new AdapterColorIconFilter(activity, new AdapterColorIconFilter.ColorFilerListener() {
            @Override
            public void onClickColorFilterListener(int colorFilter, int pos) {
                Sticker sticker = stickerView.getCurrentSticker();
                if (sticker instanceof StickerDrawable) {
                    ((StickerDrawable) sticker).setColorSticker(colorFilter, pos);
                    stickerView.invalidate();
                }
            }
        }, listColor);
        linearLayoutManagerColorFilter = new LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false);
        filerRcv.setAdapter(colorFiltersAdapter);
        filerRcv.setLayoutManager(linearLayoutManagerColorFilter);
        filerRcv.setItemAnimator(new DefaultItemAnimator());

    }

    public void updateStatusMenuOfIcon() {
        if (stickerView == null) return;
        Sticker sticker = stickerView.getCurrentSticker();
        if (sticker instanceof StickerDrawable) {
            //update
            if (filerRcv != null && filerRcv.getVisibility() == View.VISIBLE) {
                colorFiltersAdapter.setPos(((StickerDrawable) sticker).getPosColor());
                colorFiltersAdapter.notifyDataSetChanged();
                linearLayoutManagerColorFilter.scrollToPositionWithOffset(((StickerDrawable) sticker).getPosColor(), filerRcv.getWidth() / 2);

            }

            if (iconalpha != null && iconalpha.getVisibility() == View.VISIBLE) {
                iconalpha.setProgress(((StickerDrawable) sticker).getAlpha());
            }
        }
    }

    private File getImageCameraFile;

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.getPhoto_camera) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (activity.checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    getImageCameraFile = openCamera(REQUEST_CAMERA, activity);
                } else {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, PERMISS_CAMERA);
                }
            } else {
                getImageCameraFile = openCamera(REQUEST_CAMERA, activity);
            }
        } else if (id == R.id.getPhoto_gallery) {
            openGellary(REQUEST_GALL, activity);
        } else if (id == R.id.resetAlpha) {
            Sticker sticker = stickerView.getCurrentSticker();
            if (sticker instanceof StickerDrawable) {
                ((StickerDrawable) sticker).setAlpha(255);
                stickerView.invalidate();
                iconalpha.setProgress(255);
            }
        } else if (id == R.id.resetFiler) {
            Sticker sticker2 = stickerView.getCurrentSticker();
            if (sticker2 instanceof StickerDrawable) {
                ((StickerDrawable) sticker2).setColorSticker(Color.WHITE, 0);
                stickerView.invalidate();
            }

            if (filerRcv != null && filerRcv.getVisibility() == View.VISIBLE) {
                colorFiltersAdapter.setPos(0);
                colorFiltersAdapter.notifyDataSetChanged();
                linearLayoutManagerColorFilter.scrollToPositionWithOffset(0, filerRcv.getWidth() / 2);

            }
        }
    }
    Uri uri;
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA && resultCode == Activity.RESULT_OK) {
             uri = Uri.fromFile(getImageCameraFile);
             dismiss();
        } else if (requestCode == REQUEST_GALL && resultCode == Activity.RESULT_OK) {
             if (data != null) uri = data.getData();
            dismiss();
        }
    }
//1 >>oncreateView >>null
}
