package com.go.videoeditor.add.cropVideo.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.go.videoeditor.transcoder.mp4Compose.playerFilter.EPlayerView;
import com.go.videoeditor.transcoder.mp4Compose.tranform.Rotation;


@SuppressLint("ViewConstructor")
public class GestureEPlayerView extends EPlayerView implements View.OnTouchListener {

    private final AllGestureDetector allGestureDetector;
    private boolean isLockGesture = false;

    // 基準となる枠のサイズ
    private float baseWidthSize = 0;

    public GestureEPlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnTouchListener(this);
        allGestureDetector = new AllGestureDetector(this);
        allGestureDetector.setLimitScaleMin(0.1f);
        allGestureDetector.noRotate();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (isLockGesture) {
            allGestureDetector.onTouch(event);
        }
        return true;
    }

    public void setBaseWidthSize(float baseSize) {
        this.baseWidthSize = baseSize;
        requestLayout();
    }

    public void updateRotate() {
        final int rotation = (int) getRotation();

        switch (rotation) {
            case 0:
                super.setRotation(90f);
               getRenderer().setRotation(Rotation.fromInt(90));
                break;
            case 90:
                super.setRotation(180f);
                getRenderer().setRotation(Rotation.fromInt(180));
                break;
            case 180:
                super.setRotation(270f);
                getRenderer().setRotation(Rotation.fromInt(270));
                break;
            case 270:
                super.setRotation(0f);
                getRenderer().setRotation(Rotation.fromInt(0));
                break;
        }

        allGestureDetector.updateAngle();
    }

    public void setLockGesture(boolean lockGesture) {
        isLockGesture = lockGesture;
    }

    @Override
    public void setRotation(float rotation) {
        // do nothing
    }


    public void resetLayout(){
      setScaleX(1);
      setScaleY(1);
      setTranslationX(0);
      setTranslationY(0);
      setTranslationZ(0);
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        if (videoAspect == DEFAULT_ASPECT || baseWidthSize == 0) return;
//
//        // 正方形
//        if (videoAspect == 1.0f) {
//            setMeasuredDimension((int) baseWidthSize, (int) baseWidthSize);
//            return;
//        }
//
//        // 縦長 or 横長
//        setMeasuredDimension((int) baseWidthSize, (int) (baseWidthSize / videoAspect));

    }
}

