package com.go.videoeditor.add.addFilter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.go.videoeditor.R;
import com.go.videoeditor.transcoder.mp4Compose.filter.FilterType;

import java.util.List;

public class AdapterRcvFilter extends RecyclerView.Adapter<AdapterRcvFilter.MyViewHolder> {
    private Context context;
    private List<FilterType> list;
    private OnClickItemListener listener;
    private int selectedPosition = -1;

    public AdapterRcvFilter(Context context, List<FilterType> list, OnClickItemListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public AdapterRcvFilter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_filter_ve, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRcvFilter.MyViewHolder holder, int pos) {
        FilterType item = list.get(pos);
        holder.tv_title.setText(item.name());
        holder.root_view.setSelected(pos == selectedPosition);
        holder.root_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = pos;
                notifyDataSetChanged();
                if (listener != null) {
                    listener.onClick(list.get(pos),pos);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnClickItemListener {
        void onClick(FilterType filterType, int position);
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView tv_title;
        private View root_view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img_thumb);
            tv_title = itemView.findViewById(R.id.tv_title);
            root_view = itemView.findViewById(R.id.root_view);
        }
    }
}
