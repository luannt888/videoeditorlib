package com.go.videoeditor.add.addText;

public interface ListItemListener {
    void onListItemClick(int clickItemIndex);
}
