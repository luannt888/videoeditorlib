package com.go.videoeditor.add.stickerLib;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.ViewTreeObserver;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;


public class ManagerAssets {

    public static final int LAYOUT_NORMAL_INDEX = 10;
    public static final int LAYOUT_COVER_INDEX = 8;
    public static final String LAYOUT = "layout";
    public static final String ICON = "icon";
    public static final String FONT = "font";
    private static final int COLUMN_PATTERN = 4;
    public static final String ACSSESPATH = "file:///android_asset/";

    public static ArrayList<String> getNameFileAssetList(Activity activity, String nameForderAsset) {
        ArrayList<String> listPage = new ArrayList<>();
        AssetManager assetManager = activity.getAssets();
        try {
            String[] namePages = assetManager.list(nameForderAsset);
            Collections.addAll(listPage, namePages);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listPage;
    }

    public static ArrayList<String> getPathFileAssetList(Activity activity, String nameForderAsset) {
        ArrayList<String> listPage = getNameFileAssetList(activity, nameForderAsset);
        for (int i = 0; i < listPage.size(); i++) {
            String path = nameForderAsset + File.separator + listPage.get(i);
            listPage.set(i, path);
        }
        return listPage;
    }

    public static ArrayList<String> getFontPathAssetList(Activity activity) {
        ArrayList<String> listPage = getPathFileAssetList(activity, FONT);
        return listPage;
    }

    public static ArrayList<String> getLogoPathAssetList(Activity activity) {
        ArrayList<String> listPage = getPathFileAssetList(activity, ICON);
        return listPage;
    }

    public static ArrayList<String> getNameFileAssetList(String pathFolder) {
        ArrayList<String> listPage = new ArrayList<>();
        File folderFile = new File(pathFolder);
        if (folderFile.exists()) {
            String[] namePages = folderFile.list();
            if (namePages != null) {
                Collections.addAll(listPage, namePages);
            }
        }
        return listPage;
    }

    public static ArrayList<String> getPathFileList(String pathFolder) {
        ArrayList<String> list = new ArrayList<>();
        File folderFile = new File(pathFolder);
        if (folderFile.exists()) {
            File[] files = folderFile.listFiles();
            if (files != null && files.length > 0) {
                for (File file : files) {
                    list.add(file.getAbsolutePath());
                }
            }
        }
        return list;
    }


    public static boolean isAssetExists(Context context, String pathInAssetsDir) {
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = null;
        try {
            inputStream = assetManager.open(pathInAssetsDir);
            if (null != inputStream) {
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }


    public static Bitmap getBitmapFromAsset(Context context, String path) {
        AssetManager mgr = context.getAssets();
        InputStream is = null;
        Bitmap bitmap = null;
        try {
            is = mgr.open(path);
            bitmap = BitmapFactory.decodeStream(is);
        } catch (final IOException e) {
            bitmap = null;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ignored) {
                }
            }
        }
        return bitmap;
    }

    public static Bitmap getBitmapFromAsset(Context context, String path, int width, int height) {
        AssetManager mgr = context.getAssets();
        InputStream is = null;
        Bitmap bitmap = null;
        try {
            is = mgr.open(path);
            bitmap = BitmapFactory.decodeStream(is);
        } catch (final IOException e) {
            bitmap = null;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ignored) {
                }
            }
        }
        return bitmap;
    }

    private static boolean isGetDim = false;

    public static void loadPaterntoView(Context context, View view, String path) {
        isGetDim = false;
        Bitmap bmPatern = getBitmapFromAsset(context, path);
        BitmapDrawable bm = new BitmapDrawable(context.getResources(), bmPatern);
        bm.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        if (view.getHeight() > 0) {
            setBitmap(context, view, path, view.getWidth(), view.getHeight(), COLUMN_PATTERN);
        } else {
            view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if (!isGetDim) {
//                        setBitmap(context, view, path, view.getWidth(), view.getHeight(), SyncStateContract.Constants.COLUMN_PATTERN);
                        isGetDim = true;
                    }
                }
            });
        }
    }

    public static void loadPaterntoView(Context context, View view, String path, int width, int height) {
        setBitmapFullHeight(context, view, path, width, height);
    }

    private static void setBitmap(Context context, View view, String path, int width, int height, int columnPattern) {
        Bitmap bmPatern = getBitmapFromAsset(context, path);
        if (bmPatern == null) {
            return;
        }
        int newH = width * bmPatern.getHeight() / (columnPattern * bmPatern.getWidth());
        Bitmap dstBitmap = Bitmap.createScaledBitmap(bmPatern, width / columnPattern, newH, false);
        BitmapDrawable bm = new BitmapDrawable(context.getResources(), dstBitmap);
        bm.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        dstBitmap = Bitmap.createBitmap(
                width, // Width
                height, // Height
                Bitmap.Config.ARGB_8888 // Config
        );
        Canvas canvas = new Canvas(dstBitmap);
        bm.setBounds(canvas.getClipBounds());
        bm.draw(canvas);
        view.setBackground(new BitmapDrawable(context.getResources(), dstBitmap));
    }

    private static void setBitmapFullHeight(Context context, View view, String path, int width, int height) {
//        Bitmap bmPatern = getBitmapFromAsset(context, path);
        Bitmap bmPatern = null;
        bmPatern = BitmapFactory.decodeFile(path);
        if (bmPatern == null) {
            return;
        }
        int newW = height * bmPatern.getWidth() / bmPatern.getHeight();
        Bitmap dstBitmap = Bitmap.createScaledBitmap(bmPatern, newW, height, false);
        BitmapDrawable bm = new BitmapDrawable(context.getResources(), dstBitmap);
        bm.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        dstBitmap = Bitmap.createBitmap(
                width, // Width
                height, // Height
                Bitmap.Config.ARGB_8888 // Config
        );
        Canvas canvas = new Canvas(dstBitmap);
        bm.setBounds(canvas.getClipBounds());
        bm.draw(canvas);
        view.setBackground(new BitmapDrawable(context.getResources(), dstBitmap));
    }


}
