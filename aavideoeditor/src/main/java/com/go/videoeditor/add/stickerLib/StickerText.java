package com.go.videoeditor.add.stickerLib;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;

import androidx.annotation.ColorInt;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.go.videoeditor.R;


public class StickerText extends Sticker {

    private String text;
    //1
    private int fontIndex = 0;
    //2
    private int colortext = Color.BLACK;
    private int colorPositionText = 2;
    //3
    private int posColorBgText = 0;
    private int colorBgText = Color.TRANSPARENT;
    //5
    private int opacitySize = 255;
    //6
    private Boolean bold = false;
    private Boolean itali = false;
    private Boolean underline = false;
    //
    private int colorShadow = Color.RED;
    private int radiusShadow = 0;
    private float dxShadow = 0f;
    private float dyShadow = 0f;
    //
    private float strokePaintWidth = 0;
    private int strokePaintColor = Color.RED;
    private Boolean isStroke = false;
    private Boolean isShadow = false;

    private float maxTextSizePixels;
    private float minTextSizePixels;
    private float lineSpacingMultiplier = 1.0f;
    private float lineSpacingExtra = 0.0f;
    private final Rect realBounds;
    private final Rect textRect;


    private transient Paint bgPaint;
    private int bgColor = Color.TRANSPARENT;
    private transient TextPaint strokePaint;

    //...
    private transient Context context;
    private transient TextPaint textPaint;
    private transient Drawable drawable;
    private transient Drawable drawableChanceColor;
    private transient StaticLayout layoutSolid;
    private transient StaticLayout layoutStroke;
    private transient Layout.Alignment alignment;

    public StickerText(@NonNull Context context) {
        this(context, null);
    }

    public StickerText(@NonNull Context context, @Nullable Drawable drawable) {
        this.context = context;
        this.drawable = drawable;

        if (drawable == null) {
            this.drawable = ContextCompat.getDrawable(context, R.drawable.sticker_transparent_background);
        }

        //mcode
        widthTextBox = this.drawable.getIntrinsicWidth();
        heightTextBox = this.drawable.getIntrinsicHeight();

        textPaint = new TextPaint(TextPaint.ANTI_ALIAS_FLAG);
        textPaint.setAntiAlias(true);
        realBounds = new Rect(0, 0, getWidth(), getHeight());
        textRect = new Rect(0, 0, getWidth(), getHeight());
        minTextSizePixels = context.getResources().getDimension(R.dimen.minTextSizePixels);
        maxTextSizePixels = context.getResources().getDimension(R.dimen.maxTextSizePixels);
        alignment = Layout.Alignment.ALIGN_CENTER;
        textPaint.setTextSize(maxTextSizePixels);
        bgPaint = new Paint();
        bgPaint.setAntiAlias(true);
        bgPaint.setColor(bgColor);

    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        if (!isShow()) {
            return;
        }
        Matrix matrix = getMatrix();
        canvas.save();
        canvas.concat(matrix);
        //draw bg
        // >>bg
        Log.d("TAG", "draw: >>>>>" + text + ": " + getWidth() + " * " + getHeight());
        if (drawable != null) {
            textRect.set(0, 0, getWidth(), getHeight());
            realBounds.set(0, 0, getWidth(), getHeight());
//            drawable.setBounds(realBounds);
//            drawable.draw(canvas);
            canvas.drawRect(realBounds, bgPaint);
        }
        canvas.restore();

        // >>center text
        canvas.save();
        canvas.concat(matrix);
        if (textRect.width() == getWidth()) {
            int dy = getHeight() / 2 - layoutSolid.getHeight() / 2;
            // center vertical
            canvas.translate(0, dy);
        } else {
            int dx = textRect.left;
            int dy = textRect.top + textRect.height() / 2 - layoutSolid.getHeight() / 2;
            canvas.translate(dx, dy);
        }

        // >>Text
//        if (isStroke) {
//            setStrokePaint();
//            adjustTextScaleX(textRect.width(), strokePaint, this.text, canvas, layoutStroke, isStroke);
//            layoutStroke.draw(canvas);
//            setHeightTextBox(layoutStroke.getHeight());
//        }
//        else{//not stroke
//            textPaint.setShadowLayer(radiusShadow, dxShadow, dyShadow, colorShadow);
//        }
//        adjustTextScaleX(textRect.width(), textPaint, this.text, canvas, layoutSolid, !isStroke);

        layoutSolid.draw(canvas);
        canvas.restore();

    }

    private void setStrokePaint() {
        strokePaint.setStrokeJoin(Paint.Join.ROUND);
        strokePaint.setAntiAlias(true);
        strokePaint.setStyle(Paint.Style.STROKE);
        strokePaint.setStrokeWidth(strokePaintWidth);
        strokePaint.setColor(strokePaintColor);
        //bo gradient,texture cua stroke
        strokePaint.setShader(null);
        //thay shadow text -> cua stroke
        strokePaint.setShadowLayer(radiusShadow, dxShadow, dyShadow, colorShadow);
        textPaint.setShadowLayer(0, 0, 0, Color.WHITE);
        strokePaint.setAlpha(opacitySize);
        if (itali) {
            strokePaint.setTextSkewX(-0.25f);
        } else {
            strokePaint.setTextSkewX(0);
        }
        strokePaint.setFakeBoldText(bold);
        strokePaint.setUnderlineText(underline);
    }

    @NonNull
    @Override
    public StickerText setAlpha(@IntRange(from = 0, to = 255) int alpha) {
        textPaint.setAlpha(alpha);
        return this;
    }

    @NonNull
    @Override
    public Drawable getDrawable() {
        return drawable;
    }

    @Override
    public StickerText setDrawable(@NonNull Drawable drawable) {
        this.drawable = drawable;
        realBounds.set(0, 0, getWidth(), getHeight());
        textRect.set(0, 0, getWidth(), getHeight());
        return this;
    }

    @Override
    public int getWidth() {
        return widthTextBox;
    }

    @Override
    public int getHeight() {
        return heightTextBox;
    }

    @Override
    public void release() {
        super.release();
        if (drawable != null) {
            drawable = null;
        }
    }

    @NonNull
    public StickerText setDrawable(@NonNull Drawable drawable, @Nullable Rect region) {
        this.drawable = drawable;
        realBounds.set(0, 0, getWidth(), getHeight());
        if (region == null) {
            textRect.set(0, 0, getWidth(), getHeight());
        } else {
            textRect.set(region.left, region.top, region.right, region.bottom);
        }
        return this;
    }

    //Width  = wid defaut
    @NonNull
    public StickerText resizeTextInit() {

        CharSequence text = getText();
        if (text == null
                || text.length() <= 0
                || this.drawable.getIntrinsicHeight() <= 0
                || this.drawable.getIntrinsicWidth() <= 0
                || maxTextSizePixels <= 0) {
            return this;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            textPaint.setLetterSpacing(0.0f);
        }
        lineSpacingMultiplier = 1.0f;

        int wiSwarpMaxlengtxt = mResizeText(this.text, this.drawable.getIntrinsicWidth());
        layoutSolid = new StaticLayout(this.text, textPaint, wiSwarpMaxlengtxt, alignment, lineSpacingMultiplier, lineSpacingExtra, true);
        //reset HCN - after finish resize

        setWidthTextBox(layoutSolid.getWidth());
        setHeightTextBox(layoutSolid.getHeight());

        return this;
    }

    //resize khi textkhong thay doi
    public void resizeText_after() {

        CharSequence text = getText();
        if (text == null
                || text.length() <= 0
                || this.drawable.getIntrinsicHeight() <= 0
                || this.drawable.getIntrinsicWidth() <= 0
                || maxTextSizePixels <= 0) {
            return;
        }

        //>> create new swap
        String t = maxLengthString;
        float curWi = textPaint.measureText(t, 0, t.length());
        int intCurWi = (int) curWi;
        try {
            layoutSolid = new StaticLayout(this.text, textPaint, intCurWi,
                    alignment, lineSpacingMultiplier, lineSpacingExtra, true);
        } catch (IllegalArgumentException e) {
            //
            e.printStackTrace();
            Log.d("TAG", "erro ------->>: ");
        }
        setHeightTextBox(layoutSolid.getHeight());
        setWidthTextBox(intCurWi);

        return;
    }

    //su dung de init EditView
    public void resizeToRect(int avaiWi, int avaHe) {
//        Log.d("TAG", "placeSticker: ---->> " +text+":  " + avaiWi+"*"+avaHe);
        CharSequence text = getText();
        if (text == null
                || text.length() <= 0
                || this.drawable.getIntrinsicHeight() <= 0
                || this.drawable.getIntrinsicWidth() <= 0
                || maxTextSizePixels <= 0) {
            return;
        }

        // hieu nang rat thap
        double x0 = System.currentTimeMillis();
        caculateSizeHe(this.text, avaiWi, avaHe);
        double x1 = System.currentTimeMillis(); //= 100 - 700
        Log.d("TAG", "TimeCaculate >>>caculateSizeHe: " + (x1 - x0));
        caculateSizeWi(this.text, avaiWi);
        double x2 = System.currentTimeMillis(); //=10 ms

        layoutSolid = new StaticLayout(this.text, textPaint, (int) textPaint.measureText(maxLengthString), alignment, lineSpacingMultiplier, lineSpacingExtra, true);
        //reset HCN - after finish resize

        setWidthTextBox(layoutSolid.getWidth());
        setHeightTextBox(layoutSolid.getHeight());
        Log.d("TAG", "placeSticker: ---====->> " + text + ":  " + layoutSolid.getWidth() + "*" + layoutSolid.getHeight());


    }

    public void restoreHistory(float size) {
//        Log.d("TAG", "placeSticker: ---->> " +text+":  " + avaiWi+"*"+avaHe);
        CharSequence text = getText();
        if (text == null
                || text.length() <= 0
                || this.drawable.getIntrinsicHeight() <= 0
                || this.drawable.getIntrinsicWidth() <= 0
                || maxTextSizePixels <= 0) {
            return;
        }
        maxLengthString = getMostLengthLine(this.text, textPaint);
        textPaint.setTextSize(size);
        layoutSolid = new StaticLayout(this.text, textPaint, (int) textPaint.measureText(maxLengthString), alignment, lineSpacingMultiplier, lineSpacingExtra, true);
        //reset HCN - after finish resize

        setWidthTextBox(layoutSolid.getWidth());
        setHeightTextBox(layoutSolid.getHeight());
        Log.d("TAG", "placeSticker: ---====->> " + text + ":  " + layoutSolid.getWidth() + "*" + layoutSolid.getHeight());


    }

    //output = mTextPaint.setTextScaleX(xscale)+ translate canvat
    private Rect tempRect = new Rect();

    private int mResizeText(String s, int withBox) {
        maxLengthString = getMostLengthLine(s, textPaint);
        if (maxLengthString != null) {
            //c1 >dung while
//            float resize = caculateSize( withBox);
//            textPaint.setTextSize(resize);
            //c2 >dung ti le

            //>> max length nho hon Wi De fau >> set size mac dinh
            //>> nguoc lai giam size di cho no vua  Wi De fau
            textPaint.setTextSize(context.getResources().getDimension(R.dimen.sticker_text_defaut_Textsize));
            float textWi = textPaint.measureText(maxLengthString);
            if (textWi > context.getResources().getDimension(R.dimen.sticker_text_defaut_size)) {
                setTextSizeForWidth(textPaint, withBox, maxLengthString);
                return withBox;
            }
            return (int) textWi;
        }
        return 0;
    }

    private float caculateSize(int avalableWidth) {
        float targetTextSizePixels = maxTextSizePixels;
        Boolean isFit = isFitWidth(avalableWidth, targetTextSizePixels);
        // Until we either fit within our TextView
        // or we have reached our minimum text size,
        // incrementally try smaller sizes

        while (!isFit && targetTextSizePixels > minTextSizePixels) {
            targetTextSizePixels = Math.max(targetTextSizePixels - 0.2f, minTextSizePixels);
            isFit = isFitWidth(avalableWidth, targetTextSizePixels);
        }
        return targetTextSizePixels;

    }

    //isFitWidth bo dau cach hai dau cua string
    private Boolean isFitWidth(int availableWidthPixels, float textSizePixels) {
        textPaint.setTextSize(textSizePixels);
        return textPaint.measureText(maxLengthString) <= availableWidthPixels;
    }

    private String maxLengthString;

    private String getMostLengthLine(String s, TextPaint mTextpaint) {
        TextPaint copyPaint = new TextPaint(textPaint);
        copyPaint.setTextSize(20);
        if (text == null) return null;
        String[] lines = text.split("\r\n|\r|\n");
        Log.d("TAG", "getMostLengthLine: " + "______________________________________>" + text);
        if (lines.length == 0) return null;
        int max = 0;
        int pos = 0;

        int i;
        for (i = 0; i < lines.length; i++) {
            float wi = mTextpaint.measureText(lines[i]);
            if (max < wi) {
                max = (int) (wi);
                pos = i;
            }
        }

        return lines[pos];
    }

    private float setTextSizeForWidth(Paint paint, float desiredWidth, String text) {
        float testTextSize = 48f;
        paint.setTextSize(testTextSize);
        float testTextWi = textPaint.measureText(text);

        float desiredTextSize = desiredWidth * (testTextSize / (testTextWi));
        paint.setTextSize(desiredTextSize);
        return desiredTextSize;
    }

    public Boolean getBold() {
        return bold;
    }

    public void setBold(Boolean bold) {
        textPaint.setFakeBoldText(bold);
        this.bold = bold;
    }

    public Boolean getItali() {
        return itali;
    }

    public void setItali(Boolean itali) {
        if (itali) {
            textPaint.setTextSkewX(-0.25f);
        } else {
            textPaint.setTextSkewX(0);
        }
        this.itali = itali;
    }

    public Boolean getUnderline() {
        return underline;
    }

    public int getCurAlig() {
        if (alignment == Layout.Alignment.ALIGN_OPPOSITE) return 1;
        else if (alignment == Layout.Alignment.ALIGN_NORMAL) return 2;
        else return 3;
    }

    public void setUnderline(Boolean underline) {
        textPaint.setUnderlineText(underline);
        this.underline = underline;
    }

    private int widthTextBox;
    private int heightTextBox;

    public int getWidthTextBox() {
        return widthTextBox;
    }

    public void setWidthTextBox(int widthTextBox) {
        this.widthTextBox = widthTextBox;
    }

    public int getHeightTextBox() {
        return heightTextBox;
    }

    public void setHeightTextBox(int heightTextBox) {
        this.heightTextBox = heightTextBox;
    }


    @NonNull
    public StickerText setTypeface(@Nullable Typeface typeface, int posInList) {
        fontIndex = posInList;
        textPaint.setTypeface(typeface);
        return this;
    }


    @NonNull
    public StickerText setTextColor(@ColorInt int color, int colorPosition) {
        textPaint.setColor(color);
        this.colortext = color;
        this.colorPositionText = colorPosition;
        return this;
    }

    @NonNull
    public StickerText setTextAlign(@NonNull Layout.Alignment alignment) {
        this.alignment = alignment;
        return this;
    }

    @NonNull
    public Layout.Alignment getTextAlign() {
        return alignment;
    }

    //erro Line Space bi resize
    @NonNull
    public StickerText setMutiLineSpacing(float multiplier) {
        lineSpacingMultiplier = multiplier;
        resizeText_after();
        return this;
    }

    @NonNull
    public StickerText setLineSpacing(float space) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            textPaint.setLetterSpacing(space);
            resizeText_after();
        }
        return this;
    }


    @NonNull
    public StickerText setText(@Nullable String text) {
        this.text = text;
        return this;
    }

    @Nullable
    public String getText() {
        return text;
    }

    public void setBgColor(int color, int posBGText) {
        bgPaint.setColor(color);
        this.colorBgText = color;
        this.posColorBgText = posBGText;

    }

    //1 wi
    private float maxLetterSpace = 5f;
    private float minLetterSpace = 0f;

    private void caculateSizeWi(String s, int avaiWi) {
        maxLengthString = getMostLengthLine(s, textPaint);
        if (maxLengthString != null) {
            float targetLetterSpace = maxLetterSpace;
            Boolean isFit = isFitWi(targetLetterSpace, avaiWi);
            while (!isFit && targetLetterSpace > minLetterSpace) {
                targetLetterSpace = Math.max(targetLetterSpace - 0.1f, minLetterSpace);
                isFit = isFitWi(targetLetterSpace, avaiWi);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                textPaint.setLetterSpacing(targetLetterSpace);
            }
        }
    }

    private Boolean isFitWi(float targetLetterSpace, int avaiWi) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            textPaint.setLetterSpacing(targetLetterSpace);
        }
        return textPaint.measureText(maxLengthString) <= avaiWi;

    }

    //2 Hi
    private void caculateSizeHe(String s, int avaiWi, int avaiHei) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            textPaint.setLetterSpacing(0f);
        }
        int countLine = countLines(s);
        float targetTextSizePixels = maxTextSizePixels;
        Boolean isFit = isFitHei(countLine, targetTextSizePixels, avaiWi, avaiHei);
        while (!isFit && targetTextSizePixels > minTextSizePixels) {
            targetTextSizePixels = Math.max(targetTextSizePixels - 6f, minTextSizePixels);
            isFit = isFitHei(countLine, targetTextSizePixels, avaiWi, avaiHei);

        }
        textPaint.setTextSize(targetTextSizePixels + 4f);
    }

    private StaticLayout tempStaticLayout;

    //isFitWidth bo dau cach hai dau cua string
    //FIXME Hieu nang thap
    private Boolean isFitHei(int countLine, float textSizePixels, int avaiWi, int avaiHei) {
        textPaint.setTextSize(textSizePixels);
        tempStaticLayout = new StaticLayout(this.text, this.textPaint, avaiWi, this.alignment,
                this.lineSpacingMultiplier, this.lineSpacingExtra, true);
        return tempStaticLayout.getHeight() <= avaiHei && tempStaticLayout.getLineCount() <= countLine;
    }

    private int countLines(String str) {
        String[] lines = str.split("\r\n|\r|\n");
        return lines.length;
    }

    public int getFontIndex() {
        return fontIndex;
    }

    public int getColorPositionText() {
        return colorPositionText;
    }

    public int getPosColorBgText() {
        return posColorBgText;
    }

    public int getColortext() {
        return colortext;
    }

    public void setColortext(int colortext) {
        this.colortext = colortext;
    }

    public float getCurrentLetterSpace() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return textPaint.getLetterSpacing();
        } else return 0f;
    }

    public float getLineSpacingMultiplier() {
        return lineSpacingMultiplier;
    }

    public float getCurrentSizeText() {
        return textPaint.getTextSize();
    }


    public int getColorBgText() {
        return colorBgText;
    }

    public void setColorBgText(int colorBgText) {
        this.colorBgText = colorBgText;
    }
}
