package com.go.videoeditor.add.addLogo;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.go.videoeditor.R;
import com.go.videoeditor.add.stickerLib.Sticker;
import com.go.videoeditor.add.stickerLib.StickerDrawable;
import com.go.videoeditor.add.stickerLib.StickerView;
import com.yalantis.ucrop.callback.BitmapCropCallback;
import com.yalantis.ucrop.view.GestureCropImageView;
import com.yalantis.ucrop.view.OverlayView;
import com.yalantis.ucrop.view.TransformImageView;
import com.yalantis.ucrop.view.UCropView;

import java.io.File;
import java.io.InputStream;


//>> uri_in, ratio
//<< uri out

public class FragmentCrop extends DialogFragment implements View.OnClickListener {
    private static final String FOLDER_TEMP_STICKER = "CropImage";
    private static final String ProjectName = "crop";
    //Create: new instance >> oncreate >> oncreateView

    private View blockView;
    private UCropView uCropView;
    private GestureCropImageView gestureCropImageView;
    private OverlayView overlayView;
    private StickerView stickerView;
    private Uri uriIn, uriOut;

    public void setStickerView(StickerView stickerView) {
        this.stickerView = stickerView;
    }

    TransformImageView.TransformImageListener mImageListener = new TransformImageView.TransformImageListener() {
        @Override
        public void onRotate(float currentAngle) {
//            setAngleText(currentAngle);
        }

        @Override
        public void onScale(float currentScale) {
        }

        @Override
        public void onLoadComplete() {
            uCropView.animate().alpha(1).setDuration(300).setInterpolator(new AccelerateInterpolator());
            blockView.setClickable(false);
        }

        @Override
        public void onLoadFailure(@NonNull Exception e) {
//            Toast.makeText(ActivityCrop.this, "Somthing erro!!!", Toast.LENGTH_SHORT).show();
//            finish();
        }
    };

    public FragmentCrop(StickerView stickerView, Uri uriIn) {
        this.stickerView = stickerView;
        this.uriIn = uriIn;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (stickerView == null || uriIn == null) {
            dismiss();
        }
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_NoTitleBar_Fullscreen);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_crop_ve, container, false);
        //
        v.findViewById(R.id.crop_back).setOnClickListener(this);
        v.findViewById(R.id.crop_crop).setOnClickListener(this);
        blockView = v.findViewById(R.id.crop_blockView);
        blockView.bringToFront();
        blockView.setClickable(true);
        setUpcropView(v, uriIn);

        return v;
    }

    private void setUpcropView(View v, Uri in) {
        File root = new File(getActivity().getCacheDir(), FOLDER_TEMP_STICKER);
        root.mkdir();
        File project = new File(root, ProjectName);
        project.mkdir();

        File ou = new File(project, "temp_sticker_" + System.currentTimeMillis());
        uCropView = v.findViewById(R.id.crop_cropview);
        gestureCropImageView = uCropView.getCropImageView();
        overlayView = uCropView.getOverlayView();

        Sticker sticker = stickerView.getCurrentSticker();
        if (sticker instanceof StickerDrawable ) {
            overlayView.setTargetAspectRatio(sticker.getWidth() / (float) sticker.getHeight());
        } else {
            overlayView.setFreestyleCropEnabled(true);
        }
        try {
//        FIXME convert chua chuan >> Uri.fromFile(ou)
            gestureCropImageView.setImageUri(in, Uri.fromFile(ou));
        } catch (Exception e) {
            e.printStackTrace();
        }
        gestureCropImageView.setTransformImageListener(mImageListener);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.crop_back) {
            dismiss();
        } else if (id == R.id.crop_crop) {
            onCrop();
        }
    }

    public void onCrop() {
        gestureCropImageView.cropAndSaveImage(Bitmap.CompressFormat.PNG, 100, new BitmapCropCallback() {

            @Override
            public void onBitmapCropped(@NonNull Uri resultUri, int offsetX, int offsetY, int imageWidth, int imageHeight) {
                blockView.setClickable(true);
                Sticker sticker = stickerView.getCurrentSticker();
                try {
                    //FIXME >> get drawable chua chuan >>co loi >>ROTATE, OM?
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(resultUri);
                    Drawable drawable = Drawable.createFromStream(inputStream, resultUri.toString());
                    if (!(sticker instanceof StickerDrawable)) {
                        sticker = new StickerDrawable(drawable, getContext());
                        stickerView.addSticker(sticker);
                    } else {
                        sticker.setDrawable(drawable);
                    }
                    ((StickerDrawable) sticker).setCurUri(resultUri);
                    ((StickerDrawable) sticker).setAddImage(true);
                    stickerView.invalidate();
                    dismiss();

                } catch (Exception e) {
                    dismiss();
                }

            }

            @Override
            public void onCropFailure(@NonNull Throwable t) {
                dismiss();
            }

        });
    }
}
