package com.go.videoeditor.add.addText

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.Layout
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo.IME_ACTION_DONE
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.*
import com.go.videoeditor.R
import com.go.videoeditor.activity.VideoEditorActivity
import com.go.videoeditor.app.Constant
import com.go.videoeditor.add.stickerLib.ManagerAssets
import com.go.videoeditor.add.stickerLib.Sticker
import com.go.videoeditor.add.stickerLib.StickerText
import com.go.videoeditor.add.stickerLib.StickerView
import com.go.videoeditor.customView.MyBottomSheetDialogFragment
import com.go.videoeditor.model.ItemCmd
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*

class AddTextFragment : MyBottomSheetDialogFragment(), View.OnClickListener,
    AdapterFontText.ClickToFontTextItem,
    AdapterColorText.ClickItemColorText,
    OnSeekBarChangeListener {

    private var tagName: String = AddTextFragment::class.java.simpleName
    private lateinit var rootView: View
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var rvPosition: RecyclerView
    private lateinit var ivClose: ImageView
    private lateinit var ivDone: ImageView
    private var etText: EditText? = null
    private var positionStr: String? = null
    private var mContext: Context? = null
    private var text: String? = null
    var stickerView: StickerView? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_add_text, container, false)
        initView(rootView)
        return rootView
    }

    /*override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog: Dialog = super.onCreateDialog(savedInstanceState)
        if (dialog.getWindow() != null) {
            dialog.getWindow()?.setBackgroundDrawableResource(android.R.color.transparent)
            dialog.getWindow()?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        }
        return dialog
    }*/

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog.behavior.skipCollapsed = true
            bottomSheetDialog.behavior.isDraggable = false
            bottomSheetDialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        textHeaderTitle.text = getString(R.string.item_add_text)
        ivClose = rootView.findViewById(R.id.buttonClose)
        ivDone = rootView.findViewById(R.id.buttonDone)
        etText = rootView.findViewById(R.id.etText)
        if (!text.isNullOrEmpty()) {
            etText?.setText(text)
        }
        if (stickerView!!.currentSticker is StickerText) {
            ivDone.visibility = View.GONE
        } else {
            ivDone.visibility = View.VISIBLE
        }
        etText?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val text = etText!!.text.toString().trim()
                Log.v(tagName, "userText: $text")
                val sticker = stickerView!!.currentSticker
                if (sticker is StickerText) {
                    sticker.setText(text)
                    sticker.resizeTextInit()
                    stickerView?.invalidate()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
        etText?.setOnEditorActionListener { textView, actionId, keyEvent ->
            if (actionId == IME_ACTION_DONE) {
                if (stickerView?.currentSticker == null) {
                    createNewSticker()
                }
                hideKeyboard()
                true
            } else false
        }
        mContext = context

        ivClose.setOnClickListener {
            dismiss()
        }

        ivDone.setOnClickListener {
            //add cmd at here
            hideKeyboard()
            if (stickerView?.currentSticker == null) {
                createNewSticker()
            }
            val cmd = "add Text"
            if (activity is VideoEditorActivity) {
                (activity as VideoEditorActivity).addCmd(ItemCmd(Constant.KEY_ADD_TEXT, cmd))
            }
//            dismiss()
            Handler(Looper.getMainLooper()).postDelayed({
                dismiss()
            },200)
        }
    }

    fun setText(str: String?) {
        text = str
    }

    fun createNewSticker() {
        var str = etText!!.text.toString()
        if (str.isNullOrEmpty()) {
            return
        }
        val textSticker = StickerText(activity!!)
        textSticker.setText(str)
        textSticker.resizeTextInit()
        stickerView?.addSticker(textSticker)
//        stickerView?.invalidate()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        stickerView?.handlingSticker = null
        stickerView?.invalidate()
    }

    private val MODE_TEXT_MAUTEXT = 1001
    private val MODE_TEXT_MAUNENTEXT = 1002
    private val MODE_TEXT_SPACELINE = 1003
    private val MODE_TEXT_SPACETEXT = 1004
    private var COLOR_HIGHT = Color.BLUE

    //xuly hight light of viewground co detail xuat hien
    private val STATUS_COLORTEXT = 1
    private val STATUS_COLORBGTEXT = 2
    private val STATUS_SPACE_MUTILINE = 3
    private val STATUS_SPACE = 4
    private val STATUS_NONE = 0
    private var curentStatusinColorvsSizeGround = STATUS_NONE
    private var linearLayoutManagerFont: LinearLayoutManager? = null
    private var linearLayoutManagerColor: LinearLayoutManager? = null
    private var curView: View? = null
    private var editText: ImageView? = null
    private var txt_rcv_color_mautext: ImageView? = null
    private var txt_rcv_color_bgtext: android.widget.ImageView? = null
    private var txt_space: ImageView? = null
    private var txt_space_muti: android.widget.ImageView? = null
    private var txt_typebold: ImageView? = null
    private var txt_typeitali: android.widget.ImageView? = null
    private var txt_underline: android.widget.ImageView? = null
    private var txt_aligRight: ImageView? = null
    private var txt_aligCenter: android.widget.ImageView? = null
    private var txt_aligLeft: android.widget.ImageView? = null
    private var text_size_Sb: SeekBar? = null
    private var textFontRcv: RecyclerView? = null
    private var textColorRcv: RecyclerView? = null
    private var adapterFontText: AdapterFontText? = null
    private var adapterColorText: AdapterColorText? = null

    private fun initView(v: View) {
        COLOR_HIGHT = ContextCompat.getColor(activity!!, R.color.red)
        curentStatusinColorvsSizeGround = STATUS_NONE
        txt_rcv_color_mautext = v.findViewById(R.id.txt_rcv_color_mautext)
        txt_rcv_color_bgtext = v.findViewById(R.id.txt_rcv_color_maubg)

        txt_space = v.findViewById(R.id.txt_space)
        txt_space_muti = v.findViewById(R.id.txt_space_muti)

        txt_typebold = v.findViewById(R.id.txt_typebold)
        txt_typeitali = v.findViewById(R.id.txt_typeitali)
        txt_underline = v.findViewById(R.id.txt_underline)

        text_size_Sb = v.findViewById(R.id.text_size_Sb)
        textColorRcv = v.findViewById(R.id.textColorRcv)

        txt_aligRight = v.findViewById(R.id.txt_aligRight)
        txt_aligCenter = v.findViewById(R.id.txt_aligCenter)
        txt_aligLeft = v.findViewById(R.id.txt_aligLeft)
        txt_rcv_color_mautext!!.setOnClickListener(this)
        txt_rcv_color_bgtext!!.setOnClickListener(this)
        txt_underline!!.setOnClickListener(this)
        txt_space!!.setOnClickListener(this)
        txt_space_muti!!.setOnClickListener(this)
        txt_typebold!!.setOnClickListener(this)
        txt_typeitali!!.setOnClickListener(this)


        txt_aligRight!!.setOnClickListener(this)
        txt_aligCenter!!.setOnClickListener(this)
        txt_aligLeft!!.setOnClickListener(this)
        textFontRcv = v.findViewById(R.id.rcv_font)

        loadListFont()
        adapterFontText = AdapterFontText(activity, listFont, this)
        textFontRcv?.setAdapter(adapterFontText)

        val helper: SnapHelper = LinearSnapHelper()
        helper.attachToRecyclerView(textFontRcv)


        linearLayoutManagerFont = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        textFontRcv!!.setLayoutManager(linearLayoutManagerFont)
        textFontRcv!!.setItemAnimator(DefaultItemAnimator())

        linearLayoutManagerColor = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)


        text_size_Sb!!.setOnSeekBarChangeListener(this)
        updateStatusMenu()
    }

    var listFont: ArrayList<Typeface> = ArrayList()
    private fun loadListFont() {
        listFont = ArrayList()
        var font: Typeface?
        var listFontPath = ManagerAssets.getFontPathAssetList(activity!!)
        for (i in 0 until listFontPath.size) {
            font = Typeface.createFromAsset(activity!!.assets, listFontPath.get(i))
            listFont.add(font)
        }
    }

    override fun onClickFontTexItem(font: Typeface?, posInList: Int) {
        val s: Sticker? = stickerView!!.currentSticker
        if (s is StickerText) {
            (s as StickerText?)!!.setTypeface(font, posInList)
            (s as StickerText?)!!.resizeText_after()
            stickerView!!.invalidate()
        }
    }

    override fun onClick(v: View) {
//        hightlightSelect(v);
        val s: Sticker? = stickerView!!.currentSticker
        if (s is StickerText) {
            val st = s as StickerText?
            if (curView != null) {
                //xoa detail cu => ca hai GONE
                curView?.setVisibility(View.GONE)
            }
            when (v.id) {
                R.id.txt_rcv_color_mautext -> {
                    curentStatusinColorvsSizeGround = STATUS_COLORTEXT
                    textColorRcv!!.visibility = View.VISIBLE
                    text_size_Sb!!.visibility = View.GONE
                    curView = textColorRcv
                    colorMode = COLOR_TEXT
                    if (adapterColorText == null) {
                        adapterColorText = AdapterColorText(activity, this)
                        textColorRcv!!.itemAnimator = DefaultItemAnimator()
                        textColorRcv!!.layoutManager = linearLayoutManagerColor
                        textColorRcv!!.adapter = adapterColorText
                    } else {
                        textColorRcv!!.itemAnimator = DefaultItemAnimator()
                        textColorRcv!!.layoutManager = linearLayoutManagerColor
                        textColorRcv!!.adapter = adapterColorText
                    }
                }
                R.id.txt_rcv_color_maubg -> {
                    curentStatusinColorvsSizeGround = STATUS_COLORBGTEXT
                    textColorRcv!!.visibility = View.VISIBLE
                    text_size_Sb!!.visibility = View.GONE
                    curView = textColorRcv
                    colorMode = COLOR_BG
                    if (adapterColorText == null) {
                        //
                        adapterColorText = AdapterColorText(activity, this)
                        textColorRcv!!.itemAnimator = DefaultItemAnimator()
                        textColorRcv!!.layoutManager = linearLayoutManagerColor
                        textColorRcv!!.adapter = adapterColorText
                        //
                    } else {
                        textColorRcv!!.itemAnimator = DefaultItemAnimator()
                        textColorRcv!!.layoutManager = linearLayoutManagerColor
                        textColorRcv!!.adapter = adapterColorText
                    }
                }
                R.id.txt_space -> {
                    curentStatusinColorvsSizeGround = STATUS_SPACE
                    text_size_Sb!!.visibility = View.VISIBLE
                    curView = text_size_Sb
                    modeSeekBar = SPACE
                }
                R.id.txt_space_muti -> {
                    curentStatusinColorvsSizeGround =
                        STATUS_SPACE_MUTILINE
                    text_size_Sb!!.visibility = View.VISIBLE
                    curView = text_size_Sb
                    modeSeekBar = MUTI_SPACE
                }
                R.id.txt_typeitali -> {
                    curentStatusinColorvsSizeGround = STATUS_NONE
                    curView = null
                    //
                    st!!.itali = !st.itali
                }
                R.id.txt_typebold -> {
                    curentStatusinColorvsSizeGround = STATUS_NONE
                    curView = null
                    //
                    st!!.bold = !st.bold
                }
                R.id.txt_underline -> {
                    curentStatusinColorvsSizeGround = STATUS_NONE
                    curView = null
                    st!!.underline = !st.underline
                }
                R.id.txt_aligRight -> {
                    curentStatusinColorvsSizeGround = STATUS_NONE
                    curView = null
                    st!!.textAlign = Layout.Alignment.ALIGN_OPPOSITE
                    st.resizeText_after()
                    stickerView!!.replace(s)
                }
                R.id.txt_aligCenter -> {
                    curentStatusinColorvsSizeGround = STATUS_NONE
                    curView = null
                    st!!.textAlign = Layout.Alignment.ALIGN_CENTER
                    st.resizeText_after()
                    stickerView!!.replace(s)
                }
                R.id.txt_aligLeft -> {
                    curentStatusinColorvsSizeGround = STATUS_NONE
                    curView = null
                    st!!.textAlign = Layout.Alignment.ALIGN_NORMAL
                    st.resizeText_after()
                    stickerView!!.replace(s)
                }
            }
            updateStatusMenu()
            stickerView!!.invalidate()
        }
    }

    private var colorMode = 0
    private val COLOR_TEXT = 1
    private val COLOR_BG = 2

    override fun onClickColorText(color: Int, pos: Int) {
        val s: Sticker? = stickerView!!.currentSticker
        if (s is StickerText) {
            //TH mau text
            if (colorMode == COLOR_TEXT) {
                (s as StickerText?)!!.setTextColor(color, pos)
            } else if (colorMode == COLOR_BG) {
                (s as StickerText?)!!.setBgColor(color, pos)
            }
        }
        stickerView!!.invalidate()
        Log.d("TAG", "onClickColorText: $colorMode")
    }

    private val SPACE = 1
    private val MUTI_SPACE = 2
    private var modeSeekBar = SPACE

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        if (fromUser) {
            val s: Sticker? = stickerView!!.currentSticker
            if (s is StickerText) {
                if (modeSeekBar == MUTI_SPACE) {
                    (s as StickerText?)!!.setMutiLineSpacing((progress + 8) / 50f) // defaut =1, 0,16>>2.16
                } else if (modeSeekBar == SPACE) {
                    (s as StickerText?)!!.setLineSpacing((progress - 10) / 50f) //defaut =0 => -0.3>>0.7
                }
            }
            stickerView!!.invalidate()
        }
        val s: Sticker? = stickerView!!.currentSticker
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {}

    override fun onStopTrackingTouch(seekBar: SeekBar?) {}

    //chi ap dung textStickerView
    fun updateStatusMenu() {
        if (stickerView == null) return
        val s: Sticker? = stickerView!!.currentSticker
        if (s is StickerText) {
            //1 ground TextStyle
            if ((s as StickerText?)!!.bold) {
                txt_typebold!!.setColorFilter(COLOR_HIGHT)
            } else {
                txt_typebold!!.colorFilter = null
            }
            if ((s as StickerText?)!!.itali) {
                txt_typeitali!!.setColorFilter(COLOR_HIGHT)
            } else {
                txt_typeitali!!.colorFilter = null
            }
            if ((s as StickerText?)!!.underline) {
                txt_underline!!.setColorFilter(COLOR_HIGHT)
            } else {
                txt_underline!!.colorFilter = null
            }

            //2 ground Align
            //
            txt_aligRight!!.colorFilter = null
            txt_aligCenter!!.colorFilter = null
            txt_aligLeft!!.colorFilter = null
            if ((s as StickerText?)!!.textAlign === Layout.Alignment.ALIGN_OPPOSITE) {
                txt_aligRight!!.setColorFilter(COLOR_HIGHT)
            } else if ((s as StickerText?)!!.textAlign === Layout.Alignment.ALIGN_NORMAL) {
                txt_aligLeft!!.setColorFilter(COLOR_HIGHT)
            } else {
                txt_aligCenter!!.setColorFilter(COLOR_HIGHT)
            }


            //3 ground have Detail
            //
            txt_rcv_color_mautext!!.colorFilter = null
            txt_rcv_color_bgtext!!.colorFilter = null
            txt_space!!.colorFilter = null
            txt_space_muti!!.colorFilter = null
            if (curentStatusinColorvsSizeGround == STATUS_SPACE_MUTILINE) {
                txt_space_muti!!.setColorFilter(COLOR_HIGHT)
                val spaceMultiLine = (s as StickerText?)!!.lineSpacingMultiplier
                var index = (50 * spaceMultiLine - 8).toInt() //suy ra su save index
                if (index > 100) index = 100
                if (index < 0) index = 0
                text_size_Sb!!.progress = index
                Log.d("TAG", "STATUS_SPACE_MUTILINE: >>$index")
            } else if (curentStatusinColorvsSizeGround == STATUS_SPACE) {
                txt_space!!.setColorFilter(COLOR_HIGHT)
                val spaceLine = (s as StickerText?)!!.currentLetterSpace
                var index = (50 * spaceLine + 10).toInt() //suy ra su save index
                if (index > 100) index = 100
                if (index < 0) index = 0
                text_size_Sb!!.progress = index
                Log.d("TAG", "STATUS_SPACE: >>$index")
            } else if (curentStatusinColorvsSizeGround == STATUS_COLORBGTEXT) {
                txt_rcv_color_bgtext!!.setColorFilter(COLOR_HIGHT)
                val curPos = (s as StickerText?)!!.posColorBgText
                adapterColorText!!.selectpos = curPos
                adapterColorText!!.notifyDataSetChanged()
                linearLayoutManagerColor!!.scrollToPositionWithOffset(
                    curPos,
                    textColorRcv!!.width / 2
                )
            } else if (curentStatusinColorvsSizeGround == STATUS_COLORTEXT) {
                txt_rcv_color_mautext!!.setColorFilter(COLOR_HIGHT)
                val curpos = (s as StickerText?)!!.colorPositionText
                Log.d("TAG", "updateStatusMenu: $curpos")
                adapterColorText!!.selectpos = curpos
                adapterColorText!!.notifyDataSetChanged()
                linearLayoutManagerColor!!.scrollToPositionWithOffset(
                    curpos,
                    textColorRcv!!.width / 2
                )
            } else {
                //donothing >>hien dang an
            }
            //4 TH ca biet Edittext
            //thoi diem an hien >> ro rang

            //5 TH font
            val curentFontpos = (s as StickerText?)!!.fontIndex
            linearLayoutManagerFont!!.scrollToPositionWithOffset(
                curentFontpos,
                textFontRcv!!.width / 2
            )
            adapterFontText?.selected(curentFontpos)
            adapterFontText?.notifyDataSetChanged()
        }
    }

    fun hideKeyboard() {
        val imm =
            activity!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
    }

    fun showKeyboard() {
        etText?.requestFocus()
//        val inputMethodManager: InputMethodManager =
//            activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }
}