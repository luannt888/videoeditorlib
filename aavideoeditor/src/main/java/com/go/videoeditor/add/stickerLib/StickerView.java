package com.go.videoeditor.add.stickerLib;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;

import com.go.videoeditor.R;

import java.io.File;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.go.videoeditor.add.stickerLib.Convert.dpToPx;
import static java.lang.Thread.sleep;


public class StickerView extends FrameLayout {

    //<editor-fold desc="Attribute ">
    private boolean showIcons;
    private boolean showBorder;
    private boolean bringToFrontCurrentSticker;
    private int colorBG = Color.TRANSPARENT;
    private int indexList = 0;

    private static final int DEFAULT_MIN_CLICK_DELAY_TIME = 500;
    public static final int FLIP_HORIZONTALLY = 1;
    public static final int FLIP_VERTICALLY = 1 << 1;

    private List<Sticker> stickers = new ArrayList<>();
    private List<StickerBitmapIcon> icons = new ArrayList<>(4);

    public void setStickerList(List<Sticker> stickerList) {
        stickers = stickerList;
    }

    public List<Sticker> getStickerViewList() {
        return stickers;
    }

    private Paint stickerLinePaint = new Paint();
    private Paint stickerIconPaint;
    private Paint iconResizePaint = new Paint();
    private RectF stickerRect = new RectF();

    private Matrix sizeMatrix = new Matrix();
    private Matrix downMatrix = new Matrix();
    private Matrix moveMatrix = new Matrix();

    // region storing variables
    private float[] bitmapPoints = new float[8];
    private float[] bounds = new float[8];
    private float[] point = new float[2];
    private PointF currentCenterPoint = new PointF();
    private float[] tmp = new float[2];
    private PointF midPoint = new PointF();
    // endregion
    private int touchSlop;
    private StickerBitmapIcon currentIcon;
    //the first point down position
    private float downX;
    private float downY;

    private float oldDistance = 0f;
    private float oldRotation = 0f;

    @ActionMode
    private int currentMode = ActionMode.NONE;

    public Sticker handlingSticker;

    private boolean lockStickerView;
    private boolean constrained;

    private EventSticker onStickerOperationListener;

    private long lastClickTime = 0;
    private int minClickDelayTime = DEFAULT_MIN_CLICK_DELAY_TIME;
    private static final String TAG = "StickerView";

    //</editor-fold>


    //<editor-fold desc="Create Grib">
    //keep in track >> isDown
    private Boolean isDown = false;
    private float[] mGridplus;
    private float[] gridMain;
    private Paint paintGrib;

    //</editor-fold>

    public static int PADDING_LINE_DP = 0;
    public static int PADDING_ICON_DP = dpToPx(8);// khong cach tu tam hcn >> tam icon
    private static final int SIZE_ICON_DP = dpToPx(9);
    public static final int SIZE_CIRCLE_RADIUS_DP = dpToPx(8);
    public static int STICKER_COLOR_LINE_VS_ICON;


    private static final int SIZE_ICON2 = 7;
    private static final int GRID_COUNT = 6;
    private static final int SIZE_LINE_BOUND = 1;

    private Context ct;
    private boolean isMoving = false;


    @IntDef({ActionMode.NONE, ActionMode.DRAG, ActionMode.ZOOM_WITH_TWO_FINGER, ActionMode.ICON, ActionMode.CLICK})
    @Retention(RetentionPolicy.SOURCE)
    protected @interface ActionMode {
        int NONE = 0;
        int DRAG = 1;
        int ZOOM_WITH_TWO_FINGER = 2;
        int ICON = 3;
        int CLICK = 4;
    }

    @IntDef(flag = true, value = {FLIP_HORIZONTALLY, FLIP_VERTICALLY})
    @Retention(RetentionPolicy.SOURCE)
    protected @interface Flip {
    }

    public StickerView(Context context) {
        this(context, null);
    }

    public StickerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }


    public StickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        ct = context;
        touchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        TypedArray a = null;
        try {
            a = context.obtainStyledAttributes(attrs, R.styleable.StickerView);
            showIcons = a.getBoolean(R.styleable.StickerView_showIcons, false);
            showBorder = a.getBoolean(R.styleable.StickerView_showBorder, false);
            bringToFrontCurrentSticker =
                    a.getBoolean(R.styleable.StickerView_bringToFrontCurrentSticker, false);

            STICKER_COLOR_LINE_VS_ICON = ct.getResources().getColor(R.color.red);
            stickerLinePaint.setAntiAlias(true);
            stickerLinePaint.setFilterBitmap(true);
            stickerLinePaint.setDither(true);
            stickerLinePaint.setColor(a.getColor(R.styleable.StickerView_borderColor, STICKER_COLOR_LINE_VS_ICON));
            stickerLinePaint.setAlpha(a.getInteger(R.styleable.StickerView_borderAlpha, 255));
            stickerLinePaint.setStrokeWidth(SIZE_LINE_BOUND);

            stickerIconPaint = new Paint(stickerLinePaint);

            iconResizePaint.setAntiAlias(true);
            iconResizePaint.setFilterBitmap(true);
            iconResizePaint.setDither(true);
            iconResizePaint.setColor(Color.TRANSPARENT);
            iconResizePaint.setAlpha(0);

            //set icon
            configDefaultIcons();
        } finally {
            if (a != null) {
                a.recycle();
            }
        }

        setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        initGrib();

    }

    private void initGrib() {
        mGridplus = new float[((GRID_COUNT + 1) * 4) + ((GRID_COUNT + 1) * 4)];
        gridMain = new float[8];

        paintGrib = new Paint();
        paintGrib.setColor(Color.TRANSPARENT);
        paintGrib.setStyle(Paint.Style.FILL);
        paintGrib.setDither(true);
        paintGrib.setStrokeJoin(Paint.Join.ROUND);
        paintGrib.setPathEffect(new DashPathEffect(new float[]{10f, 5f}, 0));
        paintGrib.setAntiAlias(true);
        paintGrib.setFilterBitmap(true);
        paintGrib.setStrokeWidth(2f);

    }

    private void setGrid(int wiPa, int hepa) {
        //
        gridMain[0] = wiPa / 2f;
        gridMain[1] = 0f;
        gridMain[2] = wiPa / 2f;
        gridMain[3] = hepa;

        gridMain[4] = 0f;
        gridMain[5] = hepa / 2f;
        gridMain[6] = wiPa;
        gridMain[7] = hepa / 2f;
        //
        int index = 0;
        for (int i = 0; i <= GRID_COUNT; i++) {
            mGridplus[index++] = 0;
            mGridplus[index++] = (hepa * (((float) i) / (float) (GRID_COUNT)));
            mGridplus[index++] = wiPa;
            mGridplus[index++] = (hepa * (((float) i) / (float) (GRID_COUNT)));

        }
        for (int i = 0; i < GRID_COUNT + 1; i++) {
            mGridplus[index++] = (wiPa * (((float) i) / (float) (GRID_COUNT)));
            mGridplus[index++] = 0;
            mGridplus[index++] = (wiPa * (((float) i) / (float) (GRID_COUNT)));
            mGridplus[index++] = hepa;

        }
    }


    private Drawable resize(Drawable image) {
        Bitmap b = ((BitmapDrawable) image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, SIZE_ICON_DP, SIZE_ICON_DP, true);
        return new BitmapDrawable(getResources(), bitmapResized);
    }

    private Drawable resize2(Drawable image) {
        Bitmap b = ((BitmapDrawable) image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, (SIZE_ICON2), (SIZE_ICON2), true);
        return new BitmapDrawable(getResources(), bitmapResized);
    }

    public void configDefaultIcons() {
        StickerBitmapIcon deleteIcon = new StickerBitmapIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_sticker_del), StickerBitmapIcon.LEFT_TOP, ct);
        deleteIcon.setWidthDrawable((SIZE_ICON_DP));
        deleteIcon.setHeightDrawable((SIZE_ICON_DP));
        deleteIcon.setIconEvent(new EventDeleteIcon());

        StickerBitmapIcon zoomIcon = new StickerBitmapIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_sticker_expand), StickerBitmapIcon.RIGHT_BOTOM, ct);
        zoomIcon.setIconEvent(new EventZoomIcon());
        zoomIcon.setWidthDrawable((SIZE_ICON_DP));
        zoomIcon.setHeightDrawable((SIZE_ICON_DP));

        StickerBitmapIcon rotateIcon = new StickerBitmapIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_sticker_rotate), StickerBitmapIcon.RIGHT_TOP, ct);
        rotateIcon.setIconEvent(new EventRotateIcon());
        rotateIcon.setWidthDrawable((SIZE_ICON_DP));
        rotateIcon.setHeightDrawable((SIZE_ICON_DP));

        StickerBitmapIcon bringToFrontIcon = new StickerBitmapIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_sticker_overlap), StickerBitmapIcon.LEFT_BOTTOM, ct);
        bringToFrontIcon.setIconEvent(new EventBringtoFront());
        bringToFrontIcon.setWidthDrawable((SIZE_ICON_DP));
        bringToFrontIcon.setHeightDrawable((SIZE_ICON_DP));

        icons.clear();
        icons.add(deleteIcon);
        icons.add(zoomIcon);
        icons.add(rotateIcon);
        icons.add(bringToFrontIcon);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (changed) {
            stickerRect.left = left;
            stickerRect.top = top;
            stickerRect.right = right;
            stickerRect.bottom = bottom;
        }
    }

    //ham ve cac view con
    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        drawStickers(canvas);
    }

    private long atTimeMs;

    //PAINT stickers
    protected void drawStickers(Canvas canvas) {
        canvas.drawColor(colorBG);
        //1 >> sticker
        for (int i = 0; i < stickers.size(); i++) {
            Sticker sticker = stickers.get(i);
            if (sticker != null) {
                sticker.setShow(atTimeMs);
                sticker.draw(canvas);
            }
        }

        //2 >> border
        //an border vs StickerDrawable init ban dau
        if (handlingSticker != null && !lockStickerView && (showBorder || showIcons)) {
            getStickerPointsLine(handlingSticker, bitmapPoints);
//if(khong ve >>xoa data icon >>
            float x1 = bitmapPoints[0];
            float y1 = bitmapPoints[1];
            float x2 = bitmapPoints[2];
            float y2 = bitmapPoints[3];
            float x3 = bitmapPoints[4];
            float y3 = bitmapPoints[5];
            float x4 = bitmapPoints[6];
            float y4 = bitmapPoints[7];

            Log.d(TAG, "getDistan2BottomCurentSticker: >>" + getDistan2BottomCurentSticker());

            //ve line >> !drawable not lock all
            if (showBorder && (!((handlingSticker instanceof StickerDrawable) &&
                    ((StickerDrawable) handlingSticker).isLockStickerAll()))) {
                canvas.drawLine(x1, y1, x2, y2, stickerLinePaint);
                canvas.drawLine(x1, y1, x3, y3, stickerLinePaint);
                canvas.drawLine(x2, y2, x4, y4, stickerLinePaint);
                canvas.drawLine(x4, y4, x3, y3, stickerLinePaint);
            }

            //3 ve icon >> text || (drawable not lock all) || (khong di chuyen)
            if (showIcons && !isMoving && (!((handlingSticker instanceof StickerDrawable) &&
                    ((StickerDrawable) handlingSticker).getLockMoveOneSticker()))) {

                getStickerPointsIcon(handlingSticker, bitmapPoints);

                x1 = bitmapPoints[0];
                y1 = bitmapPoints[1];
                x2 = bitmapPoints[2];
                y2 = bitmapPoints[3];
                x3 = bitmapPoints[4];
                y3 = bitmapPoints[5];
                x4 = bitmapPoints[6];
                y4 = bitmapPoints[7];

                float rotation = calculateRotation(x4, y4, x3, y3);
                for (int i = 0; i < icons.size(); i++) {
                    StickerBitmapIcon icon = icons.get(i);
                    switch (icon.getPosition()) {
                        case StickerBitmapIcon.LEFT_TOP:
                            configIconMatrix(icon, x1, y1, rotation);
                            icon.draw(canvas, stickerIconPaint);
                            break;

                        case StickerBitmapIcon.RIGHT_TOP:
                            configIconMatrix(icon, x2, y2, rotation);
                            icon.draw(canvas, stickerIconPaint);
                            break;

                        case StickerBitmapIcon.LEFT_BOTTOM:
                            configIconMatrix(icon, x3, y3, rotation);
                            icon.draw(canvas, stickerIconPaint);
                            break;

                        case StickerBitmapIcon.RIGHT_BOTOM:
                            configIconMatrix(icon, x4, y4, rotation);
                            icon.draw(canvas, stickerIconPaint);
                            break;
                    }
                }
            }
        }

        //3 >> draw grib
        //
        setGrid(getWidth(), getHeight());
        if (isShowGlid()) {
            paintGrib.setStrokeWidth(1f);
            canvas.drawLines(mGridplus, paintGrib);
            paintGrib.setStrokeWidth(3f);
            canvas.drawLines(gridMain, paintGrib);
        }

    }

    private boolean isShowGlid() {
        Sticker s = getCurrentSticker();
        if (isDown //dang cham tay xuong
                && s != null) { // dang focus to sticker
            if (s instanceof StickerDrawable && ((StickerDrawable) s).getLockMoveOneSticker()) { // static sticker
                return false;
            } else {
                return true;
            }
        } else return false;
    }

    protected void configIconMatrix(@NonNull StickerBitmapIcon icon, float x, float y, float rotation) {

        icon.setX(x);
        icon.setY(y);
        icon.getMatrix().reset();

        icon.getMatrix().postRotate(rotation, icon.getWidth() / 2, icon.getHeight() / 2);
        icon.getMatrix().postTranslate(x - icon.getWidth() / 2, y - icon.getHeight() / 2);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (lockStickerView) return super.onInterceptTouchEvent(ev);

        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = ev.getX();
                downY = ev.getY();

                return findCurrentIconTouched() != null || findHandlingSticker() != null;
        }

        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (lockStickerView) {
            return super.onTouchEvent(event);
        }

        int action = event.getActionMasked();
        ;

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                isDown = true;
                isMoving = false;
                if (!onTouchDown(event)) {
                    return false;
                }
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                isDown = true;
                isMoving = false;
                oldDistance = calculateDistance(event);
                oldRotation = calculateRotation(event);
                midPoint = calculateMidPoint(event);
                if (handlingSticker != null && isInStickerArea(handlingSticker, event.getX(1),
                        event.getY(1)) && findCurrentIconTouched() == null) {
                    currentMode = ActionMode.ZOOM_WITH_TWO_FINGER;
                }
                invalidate();
                break;

            case MotionEvent.ACTION_MOVE:
                isMoving = true;
                //TODO >> lock Sticker
                if (handlingSticker instanceof StickerDrawable) {
                    if (((StickerDrawable) handlingSticker).getLockMoveOneSticker()) {
                        break;
                    }
                }

                handleCurrentMode(event);
                invalidate();
                break;

            case MotionEvent.ACTION_UP:
                isMoving = false;
                isDown = false;
                onTouchUp(event);
                //
//                displaymenu();
                invalidate();
                break;

            case MotionEvent.ACTION_POINTER_UP:
                isMoving = false;
                isDown = false;
                if (currentMode == ActionMode.ZOOM_WITH_TWO_FINGER && handlingSticker != null) {
                    if (onStickerOperationListener != null) {
                        onStickerOperationListener.onStickerZoomFinished(handlingSticker);
                    }
                }
                currentMode = ActionMode.NONE;
//                displaymenu();
                invalidate();
                break;
        }

        return true;
    }

//    private void displaymenu() {
//        //
//        if (!((MainActivity) ct).drawingView.getBrushDrawingMode() && !((MainActivity) ct).isFilterMode) {
//            if (handlingSticker instanceof StickerText) {
//                ((MainActivity) ct).textVg.bringToFront();
//            } else if (handlingSticker instanceof StickerDrawable) {
//                ((MainActivity) ct).stickersAddjustVg.bringToFront();
//            } else if (handlingSticker == null) {
//                ((MainActivity) ct).MenuMainVg.bringToFront();
//            }
//        }
//    }

    protected boolean onTouchDown(@NonNull MotionEvent event) {

        currentMode = ActionMode.DRAG;

        downX = event.getX();
        downY = event.getY();

        midPoint = calculateMidPoint();
        oldDistance = calculateDistance(midPoint.x, midPoint.y, downX, downY);
        oldRotation = calculateRotation(midPoint.x, midPoint.y, downX, downY);

        currentIcon = findCurrentIconTouched();
        if (currentIcon != null) {
            currentMode = ActionMode.ICON;
            currentIcon.onActionDown(this, event);
        } else {
            handlingSticker = findHandlingSticker();
        }
        if (handlingSticker != null) {
            downMatrix.set(handlingSticker.getMatrix());
            if (bringToFrontCurrentSticker) {
                stickers.remove(handlingSticker);
                stickers.add(handlingSticker);
            }
            if (onStickerOperationListener != null) {
                onStickerOperationListener.onStickerTouchedDown(handlingSticker);
            }
        } else {
            if (onStickerOperationListener != null) {
                onStickerOperationListener.onClickOutSticker();
            }
        }

        if (currentIcon == null && handlingSticker == null) {
            return false;
        }

        invalidate();
        return true;
    }

    public boolean isLockIcon() {
        //sticker
        if (handlingSticker instanceof StickerDrawable
                && ((StickerDrawable) handlingSticker).getLockMoveOneSticker()) {
            return true;
        }
        return false;
        //

    }


    protected void onTouchUp(@NonNull MotionEvent event) {
        long currentTime = SystemClock.uptimeMillis();
        if (currentMode == ActionMode.ICON && currentIcon != null && handlingSticker != null && !isLockIcon()) {
            currentIcon.onActionUp(this, event);
        }

        if (currentMode == ActionMode.DRAG
                && Math.abs(event.getX() - downX) < touchSlop
                && Math.abs(event.getY() - downY) < touchSlop
                && handlingSticker != null) {
            currentMode = ActionMode.CLICK;
            if (onStickerOperationListener != null) {
                onStickerOperationListener.onStickerClicked(handlingSticker);
            }
            if (currentTime - lastClickTime < minClickDelayTime) {
                if (onStickerOperationListener != null) {
                    onStickerOperationListener.onStickerDoubleTapped(handlingSticker);
                }
            }
        }

        if (currentMode == ActionMode.DRAG && handlingSticker != null) {
            if (onStickerOperationListener != null) {
                onStickerOperationListener.onStickerDragFinished(handlingSticker);
            }
        }

        currentMode = ActionMode.NONE;
        lastClickTime = currentTime;
    }

    protected void handleCurrentMode(@NonNull MotionEvent event) {
        switch (currentMode) {
            case ActionMode.NONE:
            case ActionMode.CLICK:
                break;
            case ActionMode.DRAG:
                if (handlingSticker != null) {
                    moveMatrix.set(downMatrix);
                    moveMatrix.postTranslate(event.getX() - downX, event.getY() - downY);
                    handlingSticker.setMatrix(moveMatrix);
                    if (constrained) {
                        constrainSticker(handlingSticker);
                    }
                }
                break;
            case ActionMode.ZOOM_WITH_TWO_FINGER:
                if (handlingSticker != null) {
                    float newDistance = calculateDistance(event);
                    float newRotation = calculateRotation(event);

                    moveMatrix.set(downMatrix);
                    moveMatrix.postScale(newDistance / oldDistance, newDistance / oldDistance, midPoint.x,
                            midPoint.y);
                    moveMatrix.postRotate(newRotation - oldRotation, midPoint.x, midPoint.y);
                    handlingSticker.setMatrix(moveMatrix);
                    if (onStickerOperationListener != null) {
                        onStickerOperationListener.onZoomandrotateSticker(handlingSticker);
                    }
                }
                break;

            case ActionMode.ICON:
                if (handlingSticker != null && currentIcon != null) {
                    currentIcon.onActionMove(this, event);
                }
                break;
        }
    }

    public void zoomCurrentSticker(@NonNull MotionEvent event) {
        zoomSticker(handlingSticker, event);
    }

    public void zoomSticker(@Nullable Sticker sticker, @NonNull MotionEvent event) {
        if (sticker != null && !isLockIcon()) {
            float newDistance = calculateDistance(midPoint.x, midPoint.y, event.getX(), event.getY());
            moveMatrix.set(downMatrix);
            moveMatrix.postScale(newDistance / oldDistance, newDistance / oldDistance, midPoint.x, midPoint.y);
            handlingSticker.setMatrix(moveMatrix);
            if (onStickerOperationListener != null) {
                onStickerOperationListener.onZoomandrotateSticker(handlingSticker);
            }
        }
    }

    public void rotateCurrentSticker(@NonNull MotionEvent event) {
        rotateSticker(handlingSticker, event);
    }

    public void rotateSticker(@Nullable Sticker sticker, @NonNull MotionEvent event) {
        if (sticker != null && !isLockIcon()) {
            float newRotation = calculateRotation(midPoint.x, midPoint.y, event.getX(), event.getY());
            moveMatrix.set(downMatrix);
            moveMatrix.postRotate(newRotation - oldRotation, midPoint.x, midPoint.y);
            handlingSticker.setMatrix(moveMatrix);
            if (onStickerOperationListener != null) {
                onStickerOperationListener.onZoomandrotateSticker(handlingSticker);
            }
        }
    }

    public void mRotateSticker(Sticker sticker, float angle) {//angle: 0>>360
        if (sticker != null) {
            sticker.getMatrix().postRotate(angle,
                    (sticker.getMappedCenterPoint().x - (sticker.getWidth()) / 2f),
                    (sticker.getMappedCenterPoint().y - (sticker.getHeight()) / 2f));
        }
    }

    public void mScaleSticker(Sticker sticker, float scaleFactor) {
        Log.d(TAG, "mScaleSticker: " + sticker.getCurrentScale() + ">>" + scaleFactor);
        if (sticker != null) {
            sticker.getMatrix().postScale(scaleFactor, scaleFactor, 0, 0);
        }
    }

    public void mRotateStickerMid(Sticker sticker, float angle) {//angle: 0>>360
        if (sticker != null) {
            sticker.getMatrix().postRotate(angle, sticker.getMappedCenterPoint().x, sticker.getMappedCenterPoint().y);
        }
    }

    public void bringStickertoFront() {
        if (handlingSticker != null && !isLockIcon()) {
            stickers.remove(handlingSticker);
            stickers.add(handlingSticker);
        }
    }

    public void zoomOXCurrentSticker(@NonNull MotionEvent event) {
        zoomOXSticker(handlingSticker, event);
    }

    public void zoomOXSticker(@Nullable Sticker sticker, @NonNull MotionEvent event) {
        if (sticker != null) {
            sticker.getBoundPoints();
            getStickerPoints(handlingSticker, bitmapPoints);
            float x1 = bitmapPoints[0];
            float y1 = bitmapPoints[1];
            float x3 = bitmapPoints[4];
            float y3 = bitmapPoints[5];

            float newRotation = calculateRotation((x1 + x3) / 2f, (y1 + y3) / 2f, event.getX(), event.getY());
            if (-90 < newRotation && newRotation < 90) {
                //flip horizaltal
                // sticker.setFlippedHorizontally(true);
            } else {
                float newDistance = calculateDistance((x1 + x3) / 2f, (y1 + y3) / 2f, event.getX(), event.getY());
                float trueNewDistance = newDistance / (sticker.getCurrentScale());
                if (sticker instanceof StickerDrawable) {
                    ((StickerDrawable) sticker).setWidthDrawable((int) trueNewDistance);
                } else {
                    ((StickerText) sticker).setWidthTextBox((int) trueNewDistance);
                }
            }
        }
    }

    protected void constrainSticker(@NonNull Sticker sticker) {
        float moveX = 0;
        float moveY = 0;
        int width = getWidth();
        int height = getHeight();
        sticker.getMappedCenterPoint(currentCenterPoint, point, tmp);
        if (currentCenterPoint.x < 0) {
            moveX = -currentCenterPoint.x;
        }

        if (currentCenterPoint.x > width) {
            moveX = width - currentCenterPoint.x;
        }

        if (currentCenterPoint.y < 0) {
            moveY = -currentCenterPoint.y;
        }

        if (currentCenterPoint.y > height) {
            moveY = height - currentCenterPoint.y;
        }

        sticker.getMatrix().postTranslate(moveX, moveY);
    }

    @Nullable
    protected StickerBitmapIcon findCurrentIconTouched() {
        for (StickerBitmapIcon icon : icons) {
            float x = icon.getX() - downX;
            float y = icon.getY() - downY;
            float distance_pow_2 = x * x + y * y;
            if (distance_pow_2 <= Math.pow(icon.getIconRadius() + icon.getIconRadius(), 2)) {
                return icon;
            }
        }

        return null;
    }


    /**
     * find the touched Sticker
     **/
    @Nullable
    protected Sticker findHandlingSticker() {
        for (int i = stickers.size() - 1; i >= 0; i--) {
            if (isInStickerArea(stickers.get(i), downX, downY) && !isStickerLock(stickers.get(i))) {
                return stickers.get(i);
            }
        }
        return null;
    }

    private boolean isStickerLock(Sticker sticker) {
        if (sticker instanceof StickerDrawable) {
            if (((StickerDrawable) sticker).isLockStickerAll()) return true;
        }
        return false;
    }

    protected boolean isInStickerArea(@NonNull Sticker sticker, float downX, float downY) {
        tmp[0] = downX;
        tmp[1] = downY;
        return sticker.contains(tmp);
    }

    @NonNull
    protected PointF calculateMidPoint(@Nullable MotionEvent event) {
        if (event == null || event.getPointerCount() < 2) {
            midPoint.set(0, 0);
            return midPoint;
        }
        float x = (event.getX(0) + event.getX(1)) / 2;
        float y = (event.getY(0) + event.getY(1)) / 2;
        midPoint.set(x, y);
        return midPoint;
    }

    @NonNull
    protected PointF calculateMidPoint() {
        if (handlingSticker == null) {
            midPoint.set(0, 0);
            return midPoint;
        }
        handlingSticker.getMappedCenterPoint(midPoint, point, tmp);
        return midPoint;
    }

    /**
     * calculate rotation in line with two fingers and x-axis
     **/
    protected float calculateRotation(@Nullable MotionEvent event) {
        if (event == null || event.getPointerCount() < 2) {
            return 0f;
        }
        return calculateRotation(event.getX(0), event.getY(0), event.getX(1), event.getY(1));
    }

    protected float calculateRotation(float x1, float y1, float x2, float y2) {
        double x = x1 - x2;
        double y = y1 - y2;
        double radians = Math.atan2(y, x);
        return (float) Math.toDegrees(radians);
    }

    /**
     * calculate Distance in two fingers
     **/
    protected float calculateDistance(@Nullable MotionEvent event) {
        if (event == null || event.getPointerCount() < 2) {
            return 0f;
        }
        return calculateDistance(event.getX(0), event.getY(0), event.getX(1), event.getY(1));
    }

    protected float calculateDistance(float x1, float y1, float x2, float y2) {
        double x = x1 - x2;
        double y = y1 - y2;

        return (float) Math.sqrt(x * x + y * y);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldW, int oldH) {
        super.onSizeChanged(w, h, oldW, oldH);
        for (int i = 0; i < stickers.size(); i++) {
            Sticker sticker = stickers.get(i);
            if (sticker != null) {
                transformSticker(sticker);
            }
        }
    }

    /**
     * Sticker's drawable will be too bigger or smaller
     * This method is to transform it to fit
     * step 1：let the center of the sticker image is coincident with the center of the View.
     * step 2：Calculate the zoom and zoom
     **/
    protected void transformSticker(@Nullable Sticker sticker) {
        if (sticker == null) {
            Log.e(TAG, "transformSticker: the bitmapSticker is null or the bitmapSticker bitmap is null");
            return;
        }

        sizeMatrix.reset();

        float width = getWidth();
        float height = getHeight();
        float stickerWidth = sticker.getWidth();
        float stickerHeight = sticker.getHeight();
        //step 1
        float offsetX = (width - stickerWidth) / 2;
        float offsetY = (height - stickerHeight) / 2;

        sizeMatrix.postTranslate(offsetX, offsetY);

        //step 2
        float scaleFactor;
        if (width < height) {
            scaleFactor = width / stickerWidth;
        } else {
            scaleFactor = height / stickerHeight;
        }

        sizeMatrix.postScale(scaleFactor / 2f, scaleFactor / 2f, width / 2f, height / 2f);

        sticker.getMatrix().reset();
        sticker.setMatrix(sizeMatrix);

        invalidate();
    }

    public void flipCurrentSticker(int direction) {
        flip(handlingSticker, direction);
    }

    public void flip(@Nullable Sticker sticker, @Flip int direction) {
        if (sticker != null) {
            sticker.getCenterPoint(midPoint);
            if ((direction & FLIP_HORIZONTALLY) > 0) {
                sticker.getMatrix().preScale(-1, 1, midPoint.x, midPoint.y);
                sticker.setFlippedHorizontally(!sticker.isFlippedHorizontally());
            }
            if ((direction & FLIP_VERTICALLY) > 0) {
                sticker.getMatrix().preScale(1, -1, midPoint.x, midPoint.y);
                sticker.setFlippedVertically(!sticker.isFlippedVertically());
            }

            if (onStickerOperationListener != null) {
                onStickerOperationListener.onStickerFlipped(sticker);
            }

            invalidate();
        }
    }

    public boolean replace(@Nullable Sticker sticker) {
        return replace(sticker, true);
    }

    public boolean replace(@Nullable Sticker sticker, boolean needStayState) {
        if (handlingSticker != null && sticker != null) {
            float width = getWidth();
            float height = getHeight();
            if (needStayState) {
                sticker.setMatrix(handlingSticker.getMatrix());
                sticker.setFlippedVertically(handlingSticker.isFlippedVertically());
                sticker.setFlippedHorizontally(handlingSticker.isFlippedHorizontally());
            } else {
                handlingSticker.getMatrix().reset();
                // reset scale, angle, and put it in center
                float offsetX = (width - handlingSticker.getWidth()) / 2f;
                float offsetY = (height - handlingSticker.getHeight()) / 2f;
                sticker.getMatrix().postTranslate(offsetX, offsetY);

                float scaleFactor;
                if (width < height) {
                    scaleFactor = width / handlingSticker.getDrawable().getIntrinsicWidth();
                } else {
                    scaleFactor = height / handlingSticker.getDrawable().getIntrinsicHeight();
                }
                sticker.getMatrix().postScale(scaleFactor / 2f, scaleFactor / 2f, width / 2f, height / 2f);
            }
            int index = stickers.indexOf(handlingSticker);
            stickers.set(index, sticker);
            handlingSticker = sticker;

            invalidate();
            return true;
        } else {
            return false;
        }
    }

    public boolean remove(@Nullable Sticker sticker) {
        if (stickers.contains(sticker)) {
            stickers.remove(sticker);
            if (onStickerOperationListener != null) {
                onStickerOperationListener.onStickerDeleted(sticker);
            }
            if (handlingSticker == sticker) {
                handlingSticker = null;
            }

            invalidate();
            return true;
        } else {
            Log.d(TAG, "remove: the sticker is not in this StickerView");

            return false;
        }
    }

    public boolean removeCurrentSticker() {
        return remove(handlingSticker);
    }

    public void removeAllStickers() {
        stickers.clear();
        if (handlingSticker != null) {
            handlingSticker.release();
            handlingSticker = null;
        }
        invalidate();
    }

    @NonNull
    public StickerView addSticker(@NonNull Sticker sticker) {
        return addSticker(sticker, Sticker.Position.CENTER);
    }

    public StickerView addSticker(@NonNull final Sticker sticker,
                                  final @Sticker.Position int position) {
        if (ViewCompat.isLaidOut(this)) {
            addStickerImmediately(sticker, position);
        } else {
            post(new Runnable() {
                @Override
                public void run() {
                    addStickerImmediately(sticker, position);
                }
            });
        }
        return this;
    }

    private float half;

    protected void addStickerImmediately(@NonNull Sticker sticker, @Sticker.Position int position) {
        setStickerPosition(sticker, position);
        //neu sticker nho hon thi centerInsize()
        float scaleFactor, widthScaleFactor, heightScaleFactor;
        widthScaleFactor = (float) getWidth() / sticker.getDrawable().getIntrinsicWidth();
        heightScaleFactor = (float) getHeight() / sticker.getDrawable().getIntrinsicHeight();
        scaleFactor = widthScaleFactor > heightScaleFactor ? heightScaleFactor : widthScaleFactor;
        sticker.getMatrix()
                .postScale(scaleFactor / 2, scaleFactor / 2, getWidth() / 2, getHeight() / 2);

        handlingSticker = sticker;
        stickers.add(sticker);
        if (onStickerOperationListener != null) {
            onStickerOperationListener.onStickerAdded(sticker);
        }

        //<editor-fold desc="Make Sticke Move from top >>center">
        half = getHeight() / 2f;
        handlingSticker.getMatrix().postTranslate(0, -half);

        new Thread(new Runnable() {
            @Override
            public void run() {
                int time = 30;
                float dy = half / time;
                for (int i = 0; i < time; i++) {
                    if (handlingSticker != null)
                        handlingSticker.getMatrix().postTranslate(0, dy);
                    try {
                        sleep(10);
                        ((Activity) ct).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                invalidate();
                            }
                        });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
        //</editor-fold>
        invalidate();

    }

    public void mAddStickertoSpeceficPosition(Sticker sticker, float xPo, float yPo) {
        sticker.getMatrix().postTranslate(xPo, yPo);
        //handlingSticker = sticker;
        handlingSticker = null;
        stickers.add(sticker);
        if (onStickerOperationListener != null) {
            onStickerOperationListener.onStickerAdded(sticker);
        }
        invalidate();
    }


    public void setStickerPosition(@NonNull Sticker sticker, @Sticker.Position int position) {
        float width = getWidth();
        float height = getHeight();
        float offsetX = width - sticker.getWidth();
        float offsetY = height - sticker.getHeight();

        //offsetY
        if ((position & Sticker.Position.TOP) > 0) {
            offsetY /= 4f;
        } else if ((position & Sticker.Position.BOTTOM) > 0) {
            offsetY *= 3f / 4f;
        } else {//Center
            offsetY /= 2f;
        }

        ////offsetX
        if ((position & Sticker.Position.LEFT) > 0) {
            offsetX /= 4f;
        } else if ((position & Sticker.Position.RIGHT) > 0) {
            offsetX *= 3f / 4f;
        } else {//Center
            offsetX /= 2f;
        }

        sticker.getMatrix().postTranslate(offsetX, offsetY);


    }

    @NonNull
    public float[] getStickerPoints(@Nullable Sticker sticker) {
        float[] points = new float[8];
        getStickerPoints(sticker, points);
        return points;
    }

    public void getStickerPoints(@Nullable Sticker sticker, @NonNull float[] dst) {
        if (sticker == null) {
            Arrays.fill(dst, 0);
            return;
        }
        sticker.getBoundPoints(bounds, dpToPx(0));
        sticker.getMappedPoints(dst, bounds);
    }

    //toa do cac diem line
    public void getStickerPointsLine(@Nullable Sticker sticker, @NonNull float[] dst) {
        if (sticker == null) {
            Arrays.fill(dst, 0);
            return;
        }
        sticker.getBoundPoints(bounds, dpToPx(PADDING_LINE_DP));
        sticker.getMappedPoints(dst, bounds);
    }

    //toan do cac diem icon
    public void getStickerPointsIcon(@Nullable Sticker sticker, @NonNull float[] dst) {
        if (sticker == null) {
            Arrays.fill(dst, 0);
            return;
        }
        //chong scale cua khoang cach
        sticker.getBoundPoints(bounds, (int) (PADDING_ICON_DP / sticker.getCurrentScale()));
        sticker.getMappedPoints(dst, bounds);
    }

    public void saveStickerViewtoFile(Bitmap mBitmap, File file, int trueHeightPx, int trueWidthPx) {
        Bitmap newImageBitmap = createBitmapStickerView(mBitmap, trueHeightPx, trueWidthPx);
        UtilsSticker.saveImageToGallery(file, newImageBitmap);
        UtilsSticker.notifySystemGallery(getContext(), file);
    }

    @NonNull
    public Bitmap createBitmapStickerView(Bitmap bgBitmap, int trueHeightPx, int trueWidthPx) throws OutOfMemoryError {
        handlingSticker = null;
        Bitmap bm = Bitmap.createScaledBitmap(bgBitmap, trueWidthPx, trueHeightPx, true);
        Log.d(TAG, "createBitmap: " + bgBitmap.getHeight() + "x" + bgBitmap.getWidth() + "    " + bgBitmap.getDensity() + " " + bgBitmap.getByteCount() + "byte");
        Canvas canvas = new Canvas(bm);
        canvas.save();
        float scX = (float) bm.getWidth() / getWidth();
        float scY = (float) bm.getHeight() / getHeight();
        canvas.scale(scX, scY);

        for (int i = 0; i < stickers.size(); i++) {
            Sticker sticker = stickers.get(i);
            //draw sticker
            if (sticker != null) {
                sticker.draw(canvas);
            }
        }
        canvas.restore();
        return bm;
    }

    //FIXME >>SIZE and quality is demo
    public Bitmap createBitmapfromStickerView() {
        handlingSticker = null;
        Bitmap resultBm = Bitmap.createBitmap(getStickerWi(), getStickerHe(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(resultBm);
        draw(canvas);
        return resultBm;
    }

    public Bitmap createBitmapfromStickerView(int width, int height) {
        handlingSticker = null;
        Bitmap bm = createBitmapfromStickerView();
        Bitmap resultBm = createBitmapStickerView(bm, height, width);
        return resultBm;
    }

    public int getStickerCount() {
        return stickers.size();
    }

    public boolean isLockStickerView() {
        return lockStickerView;
    }

    @NonNull
    public StickerView setLockStickerView(boolean lockStickerView) {
        this.lockStickerView = lockStickerView;
        invalidate();
        return this;
    }

    @NonNull
    public StickerView setMinClickDelayTime(int minClickDelayTime) {
        this.minClickDelayTime = minClickDelayTime;
        return this;
    }

    public int getMinClickDelayTime() {
        return minClickDelayTime;
    }

    public boolean isConstrained() {
        return constrained;
    }

    @NonNull
    public StickerView setConstrained(boolean constrained) {
        this.constrained = constrained;
        postInvalidate();
        return this;
    }

    @NonNull
    public StickerView setOnStickerOperationListener(@Nullable EventSticker onStickerOperationListener) {
        this.onStickerOperationListener = onStickerOperationListener;
        return this;
    }

    @Nullable
    public EventSticker getOnStickerOperationListener() {
        return onStickerOperationListener;
    }

    @Nullable
    public Sticker getCurrentSticker() {
        return handlingSticker;
    }

    @NonNull
    public List<StickerBitmapIcon> getIcons() {
        return icons;
    }

    public void setIcons(@NonNull List<StickerBitmapIcon> icons) {
        this.icons.clear();
        this.icons.addAll(icons);
        invalidate();
    }

    public void centerHozAlign() {
        if (handlingSticker != null) {
            Matrix curMa = handlingSticker.getMatrix();
            float[] points = getStickerPoints(handlingSticker);
            float midHoz = (points[0] + points[6]) / 2f;
            float midHozStickerView = getWidth() / 2f;
            Log.d(TAG, "centerHozAlign: " + points[2] + "__" + points[0] + "____" + getWidth());

            float trans = midHozStickerView - midHoz;

            curMa.postTranslate(trans, 0f);
            handlingSticker.setMatrix(curMa);
            invalidate();
        }
    }

    public void centerVertAlign() {
        if (handlingSticker != null) {
            Matrix curMa = handlingSticker.getMatrix();
            float[] points = getStickerPoints(handlingSticker);
            float midVer = (points[1] + points[7]) / 2f;
            float midVerSV = getHeight() / 2f;

            float trans = midVerSV - midVer;
            curMa.postTranslate(0f, trans);
            handlingSticker.setMatrix(curMa);
            invalidate();
        }
    }

    public int getColorBG() {
        return colorBG;
    }

    public void setColorBG(int colorBG, int indexList) {
        this.colorBG = colorBG;
        this.indexList = indexList;
    }

    public int getindexBG() {
        return indexList;
    }

    public int getDistan2BottomCurentSticker() {
        if (handlingSticker != null) {
            getStickerPointsLine(handlingSticker, bitmapPoints);
            float y3 = bitmapPoints[5];
            return (int) (getHeight() - y3);
        }
        return getHeight();
    }

    //get Position of Sticker
    public float getStickerleft(Sticker sticker) {
        if (sticker != null) {
            getStickerPointsLine(sticker, bitmapPoints);
            return bitmapPoints[0];
        }
        return 0;
    }

    public float getStickertop(Sticker sticker) {
        if (sticker != null) {
            getStickerPointsLine(sticker, bitmapPoints);
            return bitmapPoints[1];
        }
        return 0;
    }

    //region SET SIZE OLD PJOECT
    public int getStickerWi() {
        if (getWidth() != 0) {
            return getWidth();
        } else {
            return oldProjecWi;
        }
    }

    public int getStickerHe() {
        if (getHeight() != 0) {
            return getHeight();
        } else {
            return oldProjecHe;
        }
    }

    private int oldProjecWi = 0;
    private int oldProjecHe = 0;

    public int getOldProjecWi() {
        return oldProjecWi;
    }

    public void setOldProjecWi(int oldProjecWi) {
        this.oldProjecWi = oldProjecWi;
    }

    public int getOldProjecHe() {
        return oldProjecHe;
    }

    public void setOldProjecHe(int oldProjecHe) {
        this.oldProjecHe = oldProjecHe;
    }

    //endregion
    public void showAtTime(long atTimeMs) {
        this.atTimeMs = atTimeMs;
        invalidate();
    }

    public void clearSelectSticker() {
        handlingSticker = null;
        invalidate();
    }
}
