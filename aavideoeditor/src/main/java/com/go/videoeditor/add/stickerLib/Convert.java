package com.go.videoeditor.add.stickerLib;

import android.content.res.Resources;
import android.util.DisplayMetrics;

public class Convert {
    public static int dpToPx(float dp) {
        DisplayMetrics dm = Resources.getSystem().getDisplayMetrics();
        return Math.round(dm.density * dp);
    }


}
