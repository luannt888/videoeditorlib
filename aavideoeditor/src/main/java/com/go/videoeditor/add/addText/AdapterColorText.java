package com.go.videoeditor.add.addText;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.go.videoeditor.R;

import java.util.ArrayList;
import java.util.List;


public class AdapterColorText extends RecyclerView.Adapter<AdapterColorText.MViewHolder> {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<Integer> listColor;
    private int selectpos = -1;
    private ClickItemColorText clickItemColorText;

    public AdapterColorText(Context context, ClickItemColorText colorTextOnclick ) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.clickItemColorText = colorTextOnclick;
        listColor = new ArrayList<>();
        //tao data
        TypedArray typedArray = context.getResources().obtainTypedArray(R.array.origil_colors);
        for (int i = 0; i < typedArray.length(); i++) {
            listColor.add(typedArray.getColor(i, 0));
        }
        typedArray.recycle();


    }

    @NonNull
    @Override
    public AdapterColorText.MViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_color_text_ve, parent, false);
        return new AdapterColorText.MViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterColorText.MViewHolder holder, int position) {
        //getData
        int color = listColor.get(position);
        holder.color.setColorFilter(color, PorterDuff.Mode.SRC);
        if (selectpos == position) {
            holder.stroke.setVisibility(View.VISIBLE);
        }else {
            holder.stroke.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return listColor.size();
    }

    public ClickItemColorText getClickItemColorText() {
        return clickItemColorText;
    }

    public void setClickItemColorText(ClickItemColorText clickItemColorText) {
        this.clickItemColorText = clickItemColorText;
    }

    public class MViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ImageView color;
        private ImageView stroke;

        public MViewHolder(@NonNull View itemView) {
            super(itemView);
            color = itemView.findViewById(R.id.color_text);
            stroke = itemView.findViewById(R.id.color_text_Select);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            try {
                int pos = getAdapterPosition();
                selectpos = pos;
                if (getClickItemColorText() != null) {
                    getClickItemColorText().onClickColorText(listColor.get(pos), pos);
                }
                notifyDataSetChanged();
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }

        }
    }

    public interface ClickItemColorText {
        void onClickColorText(int color, int pos);
    }



    public int getSelectpos() {
        return selectpos;
    }

    public void setSelectpos(int selectpos) {
        this.selectpos = selectpos;
    }

}
