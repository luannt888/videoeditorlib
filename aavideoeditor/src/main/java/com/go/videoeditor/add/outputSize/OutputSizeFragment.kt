/*
 *
 *  Created by Optisol on Aug 2019.
 *  Copyright © 2019 Optisol Business Solutions pvt ltd. All rights reserved.
 *
 */

package com.go.videoeditor.add.outputSize

import android.app.Dialog
import android.os.Bundle
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.activity.VideoEditorActivity
import com.go.videoeditor.app.Constant
import com.go.videoeditor.model.ItemCmd
import com.go.videoeditor.transcoder.mp4Compose.filter.GlFilter
import com.go.videoeditor.utils.VideoEditUtils
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_add_filter.view.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*

class OutputSizeFragment : BottomSheetDialogFragment() {
    private var tagName: String = OutputSizeFragment::class.java.simpleName
    private lateinit var rootView: View
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var rvFilter: RecyclerView
    private lateinit var ivClose: ImageView
    private lateinit var ivDone: ImageView
    private var videoFilePath: String? = null
    private lateinit var adapterRcv: AdapterRcvSize
    private var glFilter: GlFilter? = null
    private var list: ArrayList<Size>? = ArrayList()
    private var listener: AdapterRcvSize.OnClickItemListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_output_size_ve, container, false)
        return rootView
    }

    /*override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog: Dialog = super.onCreateDialog(savedInstanceState)
        if (dialog.getWindow() != null) {
            dialog.getWindow()?.setBackgroundDrawableResource(android.R.color.transparent)
            dialog.getWindow()?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        }
        return dialog
    }*/

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
//            window?.setDimAmount(0f)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvFilter = rootView.rcv_filter
        ivClose = rootView.findViewById(R.id.buttonClose)
        ivDone = rootView.findViewById(R.id.buttonDone)
        initView()
        initData()
        intSizeRcv()
        intControl()
    }

    fun initData() {
        videoFilePath = arguments?.getString(Constant.FILE_PATH, null)
        if (videoFilePath.isNullOrEmpty()) {
            (activity as? BaseActivity)?.showToast("File not found")
            dismiss()
            return
        }
        initSize()

    }

    private fun initSize() {
        list!!.clear()
        var originSize = VideoEditUtils.getVideoResolution(videoFilePath)
        val ratio = originSize.width.toFloat() / originSize.height.toFloat()
        var size1 = Size(360, getHeightRepair(360, ratio))
        var size2 = Size(640, getHeightRepair(640, ratio))
        var size3 = Size(720, getHeightRepair(720, ratio))
        var size4 = Size(1080, getHeightRepair(1080, ratio))
        var size5 = Size(1280, getHeightRepair(1280, ratio))
        var size6 = Size(1920, getHeightRepair(1920, ratio))
        var size7 = Size(2560, getHeightRepair(2560, ratio))
        list!!.add(originSize)
        addSizeList(list!!, originSize, size1)
        addSizeList(list!!, originSize, size2)
        addSizeList(list!!, originSize, size3)
        addSizeList(list!!, originSize, size4)
        addSizeList(list!!, originSize, size5)
        addSizeList(list!!, originSize, size6)
        addSizeList(list!!, originSize, size7)
    }

    private fun getHeightRepair(width: Int, ratio: Float): Int {
        var newHeight: Int = (width / ratio).toInt()
        if (newHeight % 2 != 0) {
            newHeight--
        }
        return newHeight
    }

    private fun addSizeList(mList: ArrayList<Size>, originSize: Size, size1: Size) {
        if (size1.width <= 1.3f * originSize.width && size1.width != originSize.width) {
//        if ( size1.width != originSize.width) {
            list!!.add(size1)
        }
    }

    private fun intControl() {
        ivClose.setOnClickListener {
            dismiss()
        }

        ivDone.setOnClickListener {
            //add cmd at her
            (activity as VideoEditorActivity).glFilterPlayer = glFilter
            if (glFilter != null) {
                val cmd = "add Filter"
                (activity as VideoEditorActivity).addCmd(ItemCmd(Constant.KEY_ADD_EFFECT, cmd))
            }
            dismiss()
        }
    }

    private fun intSizeRcv() {
        linearLayoutManager =
            LinearLayoutManager(activity!!.applicationContext)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        rvFilter.layoutManager = linearLayoutManager
        adapterRcv = AdapterRcvSize(context, list, { size, position ->
            listener?.onClick(size, position)
            dismiss()
        })

        rvFilter.adapter = adapterRcv
        adapterRcv.notifyDataSetChanged()
    }


    private fun initView() {
        textHeaderTitle.text = getString(R.string.item_select_output_size)
        buttonDone.visibility = View.GONE
    }

    fun setlistener(mListener: AdapterRcvSize.OnClickItemListener) {
        this.listener = mListener
    }

}