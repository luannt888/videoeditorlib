/*
 *
 *  Created by Optisol on Aug 2019.
 *  Copyright © 2019 Optisol Business Solutions pvt ltd. All rights reserved.
 *
 */

package com.go.videoeditor.add.addFilter

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.go.videoeditor.R
import com.go.videoeditor.activity.VideoEditorActivity
import com.go.videoeditor.app.Constant
import com.go.videoeditor.model.ItemCmd
import com.go.videoeditor.transcoder.mp4Compose.filter.FilterType
import com.go.videoeditor.transcoder.mp4Compose.filter.GlFilter
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_add_filter.view.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*
import java.io.File

class AddFilterFragment : BottomSheetDialogFragment() {
    private var tagName: String = AddFilterFragment::class.java.simpleName
    private lateinit var rootView: View
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var rvFilter: RecyclerView
    private lateinit var ivClose: ImageView
    private lateinit var ivDone: ImageView
    private var videoFile: File? = null
    private lateinit var adapterRcv: AdapterRcvFilter
    private var glFilter: GlFilter? = null
    private var filterType: FilterType? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_add_filter, container, false)
        return rootView
    }

    /*override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog: Dialog = super.onCreateDialog(savedInstanceState)
        if (dialog.getWindow() != null) {
            dialog.getWindow()?.setBackgroundDrawableResource(android.R.color.transparent)
            dialog.getWindow()?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        }
        return dialog
    }*/

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvFilter = rootView.rcv_filter
        ivClose = rootView.findViewById(R.id.buttonClose)
        ivDone = rootView.findViewById(R.id.buttonDone)
        initView()
        intFilterRcv()
        intControl()
    }

    private fun intControl() {
        ivClose.setOnClickListener {
            dismiss()
        }

        ivDone.setOnClickListener {
            //add cmd at her
            if (activity is VideoEditorActivity) {
                (activity as VideoEditorActivity).glFilterPlayer =
                    FilterType.createGlFilter(filterType, context)
                (activity as VideoEditorActivity).glFilterDefault =
                    FilterType.createGlFilter(filterType, context)
                (activity as VideoEditorActivity).addRangeSeekBar((activity as VideoEditorActivity).glFilterPlayer!!)
                if (glFilter != null) {
                    val cmd = "add Filter"
                    (activity as VideoEditorActivity).addCmd(ItemCmd(Constant.KEY_ADD_EFFECT, cmd))
                }
            }
            dismiss()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        if (activity is VideoEditorActivity) {
            (activity as VideoEditorActivity).setFilterPlayerDefault()
        }
        super.onDismiss(dialog)
    }

    private fun intFilterRcv() {
        linearLayoutManager =
            LinearLayoutManager(requireActivity().applicationContext)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        rvFilter.layoutManager = linearLayoutManager
        var list = FilterType.createFilterList()
        adapterRcv =
            AdapterRcvFilter(context, list) { filterType, position ->
                this.filterType = filterType
                glFilter = FilterType.createGlFilter(filterType, context)
                if (activity is VideoEditorActivity) {
                    (activity as VideoEditorActivity).setFilterPlayer(glFilter!!)
                    (activity as VideoEditorActivity).filterIndex = position
                }
            }
        rvFilter.adapter = adapterRcv
        adapterRcv.notifyDataSetChanged()
        if (activity is VideoEditorActivity) {
            if ((activity as VideoEditorActivity).filterIndex != -1) {
                adapterRcv.setSelectedPosition((activity as VideoEditorActivity).filterIndex)
//                rvFilter.smoothScrollToPosition((activity as VideoEditorActivity).filterIndex)
            }
        }
    }

    fun setFilePathFromSource(file: File) {
        videoFile = file
    }

    private fun initView() {
        textHeaderTitle.text = getString(R.string.item_add_effect)
    }


}