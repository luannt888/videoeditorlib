package com.go.videoeditor.add.addMusic

import android.content.DialogInterface
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.listener.OnDismissListener
import com.go.videoeditor.utils.BackgroundExecutor
import com.go.videoeditor.utils.OptiCommonMethods
import com.go.videoeditor.utils.UiThreadExecutor
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_merge_ve.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class AddMusicFragment : BottomSheetDialogFragment() {
    lateinit var adapter: SortMusicAdapter
    lateinit var touchHelper: ItemTouchHelper
    var itemList: ArrayList<ItemMusic> = ArrayList()
    var fileList: ArrayList<String>? = null
    var onDismissListener: OnDismissListener? = null
    var isSuccess = false
    var outVideoPath: String? = null
    var videoPath: String? = null
    var numberVideoGetted: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_merge_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog.behavior.skipCollapsed = true
            bottomSheetDialog.behavior.isDraggable = false
            bottomSheetDialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        initData()
        initControl()
    }

    fun initData() {
        fileList = arguments?.getStringArrayList(Constant.DATA) as ArrayList<String>
        if (fileList == null || fileList!!.size == 0) {
            dismiss()
            return
        }
        videoPath = fileList!!.get(0)
        fileList!!.remove(fileList!!.get(0))
        textHeaderTitle.text = getString(R.string.item_add_music)
        initAdapter()
        getVideoInfo()
    }

    fun initAdapter() {
        for (i in 0 until fileList!!.size) {
            val filePath = fileList!!.get(i)
            val itemMusic = ItemMusic(filePath, "File $i", null, 0)
            itemList.add(itemMusic)
        }

        val style = Constant.STYLE_HORIZONTAL
        adapter = SortMusicAdapter(activity!!, itemList, style, 5)
        val callback: ItemTouchHelper.Callback = ItemMusicMoveCallbackListener(adapter)
        touchHelper = ItemTouchHelper(callback)
        touchHelper.attachToRecyclerView(recyclerView)
        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = layoutManager
//        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = adapter
        adapter.startDragMusicListener = object : OnStartDragMusicListener {
            override fun onStartDrag(viewHolder: RecyclerView.ViewHolder) {
                touchHelper.startDrag(viewHolder)
            }
        }

        adapter.onItemClickListener = object : SortMusicAdapter.OnItemClickListener {
            override fun onClick(itemObject: ItemMusic, position: Int, holder: RecyclerView.ViewHolder) {
                Loggers.e("onItemClickListener", "filePath = ${itemObject.filePath}")
            }
        }
    }

    fun getVideoInfo() {
        for (i in 0 until itemList.size) {
            val itemObj = itemList.get(i)
            val idName = "id${Calendar.getInstance().timeInMillis}"
            BackgroundExecutor.execute(object : BackgroundExecutor.Task(idName, 0L, idName) {
                override fun execute() {
                    try {
                        val mediaMetadataRetriever = MediaMetadataRetriever()
                        val videoUri = Uri.parse(itemList.get(i).filePath)
                        mediaMetadataRetriever.setDataSource(context, videoUri)
//                        var bitmap: Bitmap? =
//                            mediaMetadataRetriever.getFrameAtTime(
//                                1000,
//                                MediaMetadataRetriever.OPTION_CLOSEST_SYNC
//                            )
//                        bitmap = Bitmap.createScaledBitmap(
//                            bitmap!!,
//                            VideoEditUtils.THUMB_WIDTH,
//                            VideoEditUtils.THUMB_HEIGHT,
//                            false
//                        )
//                        itemObj.bitmap = bitmap
                        val videoFile = File(itemList.get(i).filePath!!)
                        val timeInMillis = OptiCommonMethods.getVideoDuration(activity!!, videoFile)
                        val second = TimeUnit.MILLISECONDS.toSeconds(timeInMillis)
                        itemObj.duration = second
                        itemObj.title = videoFile.nameWithoutExtension
                        mediaMetadataRetriever.release()
                        UiThreadExecutor.runTask(idName, Runnable {
                            adapter.notifyDataSetChanged()
                        }, 0L)
                        numberVideoGetted++
                    } catch (e: Throwable) {
                        Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e)
                    }
                }
            })
        }
    }

    fun initControl() {
        buttonClose.setOnClickListener {
            isSuccess = false
            dismiss()
        }

        buttonDone.setOnClickListener {
            if (numberVideoGetted != itemList.size) {
                (activity as? BaseActivity)?.showDialog(getString(R.string.msg_getting_video_info))
                return@setOnClickListener
            }
            isSuccess = true
            for (i in 0 until fileList!!.size) {
                val title = fileList!!.get(i)
                Loggers.e("buttonDone_A_$i", "$title")
            }
//            mergeAudios()
            dismiss()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        onDismissListener?.onDismiss(isSuccess, outVideoPath, null)
        super.onDismiss(dialog)
    }

    override fun onCancel(dialog: DialogInterface) {
        isSuccess = false
        onDismissListener?.onDismiss(isSuccess, "", null)
        super.onCancel(dialog)
    }
}