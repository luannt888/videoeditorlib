package com.go.videoeditor.add.addMusic

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Bitmap
import android.graphics.Color
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.activity.VideoEditorActivity
import com.go.videoeditor.app.Constant
import com.go.videoeditor.fragment.PlayerFragment
import com.go.videoeditor.listener.SingleCallback
import com.go.videoeditor.timeLine.FrameTimeLineAdapter
import com.go.videoeditor.trim.ItemFrame
import com.go.videoeditor.trim.RangeSeekBarView
import com.go.videoeditor.utils.*
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_trim_audio_ve.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*
import java.io.File

class TrimAudioFragment : BottomSheetDialogFragment() {
    private var screenType: Int? = -1
    private var aMiliSecondToPx: Float = 0F
    val TAG_NAME = this::class.simpleName
    var filePath: String? = null
    var trimData: String? = null
    var videoFile: File? = null
    var player: ExoPlayer? = null

    var mDuration: Long = 0
    var mThumbsTotalCount: Int = 0
    var isOverScaledTouchSlop = false
    var mRangeSeekBarView: RangeSeekBarView? = null
    var mLeftProgressPos: Long = 0
    var mRightProgressPos: Long = 0
    var lastScrollX = 0
    var VIDEO_MAX_TIME = 50
    var MAX_COUNT_RANGE = 10 //seekBar
    var MAX_SHOOT_DURATION = VIDEO_MAX_TIME * 1000L
    var mScaledTouchSlop = 0
    var isSeeking = false
    var mAverageMsPx: Float = 0f
    var averagePxMs: Float = 0f
    var mMaxWidth = VideoEditUtils.VIDEO_FRAMES_WIDTH
    var RECYCLER_VIEW_PADDING = UnitConverter.dpToPx(35)

    lateinit var adapter: FrameTimeLineAdapter
    var itemList: ArrayList<ItemFrame> = ArrayList()
    var onDismissListener: OnDismissListener? = null
    var isSuccess = false
    var cmd = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_trim_audio_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog.behavior.skipCollapsed = true
            bottomSheetDialog.behavior.isDraggable = false
            bottomSheetDialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        initData()
        initControl()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }

    var playerFragment: PlayerFragment? = null
    fun initData() {
        filePath = arguments?.getString(Constant.FILE_PATH, null)
        screenType = arguments?.getInt(Constant.SCREEN_TYPE) as Int
        mLeftProgressPos = arguments?.getLong(Constant.START, -1)!!
        mRightProgressPos = arguments?.getLong(Constant.END, -1)!!
        if (filePath.isNullOrEmpty()) {
            (activity as? BaseActivity)?.showToast("File not found")
            dismiss()
            return
        }
        if (screenType != Constant.TYPE_REPLACE_MUSIC && screenType != Constant.TYPE_INSERT_VIDEO) {
            dismiss()
            return
        }
        videoFile = File(filePath!!)
        mDuration = OptiCommonMethods.getVideoDuration(activity!!, videoFile!!)
        if (mLeftProgressPos <= 0) {
            mLeftProgressPos = 0
        }
        if (mRightProgressPos <= 0) {
            mRightProgressPos = mDuration
        }
        Log.e("TrimAudioFragment", "filePath = $filePath")
        if (videoFile == null) {
            (activity as? BaseActivity)?.showToast("File not found")
            dismiss()
            return
        }
        initPlayView()
        trimData = arguments?.getString(Constant.DATA, null)
        textHeaderTitle.text = getString(R.string.item_trim)
        initRangeSeekBarView()
        if (!trimData.isNullOrEmpty()) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
//                val trimArr = trimData!!.split(Constant.KEY_SPLIT_TRIM)
//                mLeftProgressPos = trimArr[0].toLong()
//                mRightProgressPos = trimArr[1].toLong()
//                mRangeSeekBarView!!.setSelectedMinValue(mLeftProgressPos)
//                mRangeSeekBarView!!.setSelectedMaxValue(mRightProgressPos)
                if (player == null) {
                    player = playerFragment!!.player!!
                }
//                mRangeSeekBarView!!.setStartEndTime(mLeftProgressPos, mRightProgressPos)
//                player?.seekTo(mLeftProgressPos)
            }, 1000)
        }
        if (screenType == Constant.TYPE_INSERT_VIDEO) {
            initAdapter()
            val mSourceUri = Uri.fromFile(videoFile)
            startShootVideoThumbs(activity!!, mSourceUri, mThumbsTotalCount, 0, mDuration)
        }
    }

    private fun initPlayView() {
        var layoutParams = frameLayoutPlayerView.layoutParams
        layoutParams.height = ScreenSize(activity!!).width * 9 / 16
        playerFragment = PlayerFragment()
        val bundle = Bundle()
        bundle.putString(Constant.FILE_PATH, filePath)
        playerFragment?.setPlayerView(true, true)
        playerFragment?.autoPlay = false
        playerFragment!!.arguments = bundle
        val fm = childFragmentManager
        val transaction = childFragmentManager.beginTransaction()
        transaction.add(R.id.frameLayoutPlayerView, playerFragment!!)
        transaction.commit()
    }

    private fun startShootVideoThumbs(
        context: Context, videoUri: Uri,
        totalThumbsCount: Int, startPosition: Long, endPosition: Long
    ) {
        VideoEditUtils.shootVideoThumbInBackground(
            context,
            videoUri,
            totalThumbsCount,
            startPosition,
            endPosition,
            object : SingleCallback<Bitmap, Int> {
                override fun onSingleCallback(bitmap: Bitmap, interval: Int) {
                    UiThreadExecutor.runTask("", {
//                    mVideoThumbAdapter.addBitmaps(bitmap)
                        val position = itemList.size
//                        Log.e(TAG_NAME, "position = $position _ interval = $interval")
                        itemList.add(ItemFrame(position, bitmap))
//                    adapter.notifyItemChanged(position)
                        adapter.notifyDataSetChanged()
                    }, 0L)
                }
            })
    }

    fun initAdapter() {
        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(filePath)
        val style = Constant.STYLE_HORIZONTAL
        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.setHasFixedSize(true)
        adapter = FrameTimeLineAdapter(activity!!, itemList, style, Constant.WIDTH_FRAME_DP)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    private fun initRangeSeekBarView() {
        if (mRangeSeekBarView != null) return
        mThumbsTotalCount = VideoEditUtils.getThumbTotalCount(mDuration)
        val widthRangeSeekbar = mThumbsTotalCount * UnitConverter.dpToPx(Constant.WIDTH_FRAME_DP)
        aMiliSecondToPx = VideoEditUtils.getPixelAMilisSecond(mDuration)
        mRangeSeekBarView = RangeSeekBarView(activity!!, 0, mDuration)
        val layoutParams =
            ViewGroup.LayoutParams(widthRangeSeekbar, ViewGroup.LayoutParams.MATCH_PARENT)
        mRangeSeekBarView?.layoutParams = layoutParams
        mRangeSeekBarView?.setSelectedMinValue(mLeftProgressPos)
        mRangeSeekBarView?.setSelectedMaxValue(mRightProgressPos)
        mRangeSeekBarView?.setStartEndTime(mLeftProgressPos, mRightProgressPos)
        mRangeSeekBarView?.setNotifyWhileDragging(true)
        if (screenType == Constant.TYPE_REPLACE_MUSIC) {
            mRangeSeekBarView?.backgroundRangebar = Color.parseColor("#0f3073")
            mRangeSeekBarView?.title = "Audio"
        }
        mRangeSeekBarView!!.setOnRangeSeekBarChangeListener(object :
            RangeSeekBarView.OnRangeSeekBarChangeListener {
            override fun onRangeSeekBarValuesChanged(
                bar: RangeSeekBarView?,
                minValue: Long,
                maxValue: Long,
                action: Int,
                isMin: Boolean,
                pressedThumb: RangeSeekBarView.Thumb?
            ) {
//                Log.e(TAG_NAME, "-----minValue----->>>>>>$minValue")
//                Log.e(TAG_NAME, "-----maxValue----->>>>>>$maxValue")
                mLeftProgressPos = minValue
                mRightProgressPos = maxValue
//                Log.d(TAG_NAME, "-----mLeftProgressPos----->>>>>>$mLeftProgressPos")
//                Log.d(
//                    TAG_NAME,
//                    "-----mRightProgressPos----->>>>>>$mRightProgressPos"
//                )
                when (action) {
                    MotionEvent.ACTION_DOWN -> isSeeking = false
                    MotionEvent.ACTION_MOVE -> {
                        isSeeking = true
                        seekTo(if (pressedThumb === RangeSeekBarView.Thumb.MIN) mLeftProgressPos else mRightProgressPos)
                    }
                    MotionEvent.ACTION_UP -> {
                        isSeeking = false
                        seekTo(mLeftProgressPos)
                    }
                }
                mRangeSeekBarView!!.setStartEndTime(mLeftProgressPos, mRightProgressPos)
            }
        })
        seekBarLayout.addView(mRangeSeekBarView)
    }

    private fun seekTo(msec: Long) {
        if (player == null) {
            player = playerFragment!!.player!!
        }
        if (player == null) {
            return
        }
        player?.seekTo(msec)
        Log.e(TAG_NAME, "seekTo = $msec")
    }

    fun onVideoPause() {
        if (player!!.isPlaying()) {
            seekTo(mLeftProgressPos) //复位
            player!!.playWhenReady = false
            setPlayPauseViewIcon(false)
        }
    }

    private fun playVideoOrPause() {
        if (player == null) {
            player = playerFragment!!.player!!
        }
        if (player == null) {
            return
        }
        if (player!!.isPlaying()) {
            player!!.playWhenReady = false
        } else {
            player!!.playWhenReady = true
        }
        setPlayPauseViewIcon(player!!.isPlaying())
    }

    private fun setPlayPauseViewIcon(isPlaying: Boolean) {
        buttonPause.setImageResource(if (isPlaying) R.drawable.exo_icon_pause else R.drawable.exo_icon_play)
    }


    private fun calcScrollXDistance(): Int {
        val layoutManager = recyclerView.getLayoutManager() as LinearLayoutManager
        val position = layoutManager.findFirstVisibleItemPosition()
        val firstVisibleChildView = layoutManager.findViewByPosition(position)
        val itemWidth = firstVisibleChildView!!.width
        return position * itemWidth - firstVisibleChildView.left
    }

    fun initControl() {
        buttonClose.setOnClickListener {
            isSuccess = false
            cmd = ""
            dismiss()
        }

        buttonDone.setOnClickListener {
            isSuccess = true
            dismiss()
        }

        buttonPause.setOnClickListener {
            playVideoOrPause()
        }
    }

    override fun dismiss() {
        (activity as VideoEditorActivity).showButtonPlay(true)
        onDismissListener?.onDismiss(
            isSuccess,
            mLeftProgressPos,
            mRightProgressPos,
            mDuration,
            filePath
        )
        super.dismiss()
    }

    override fun onCancel(dialog: DialogInterface) {
        isSuccess = false
        cmd = ""
        onDismissListener?.onDismiss(
            isSuccess,
            mLeftProgressPos,
            mRightProgressPos,
            mDuration,
            filePath
        )
        super.onCancel(dialog)
    }

    override fun onDestroy() {
        BackgroundExecutor.cancelAll("", true)
        UiThreadExecutor.cancelAll("")
        super.onDestroy()
    }

    interface OnDismissListener {
        fun onDismiss(
            isSuccess: Boolean,
            startPostition: Long,
            endPosition: Long,
            duration: Long,
            filePath: String?
        )
    }
}