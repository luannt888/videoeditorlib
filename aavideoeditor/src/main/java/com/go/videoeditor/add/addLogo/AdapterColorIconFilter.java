package com.go.videoeditor.add.addLogo;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.go.videoeditor.R;

import java.util.List;


public class AdapterColorIconFilter extends RecyclerView.Adapter<AdapterColorIconFilter.IViewHolder>{

    private Context context;
    private List<Integer> listColor;
    private int selectpos = -1;
    private ColorFilerListener colorFilerListener;


    public int getPos() {
        return selectpos;
    }

    public void setPos(int pos) {
        selectpos = pos;
    }

    public interface ColorFilerListener {
        void onClickColorFilterListener(int color, int pos);
    }

    public AdapterColorIconFilter(Context ct, ColorFilerListener colorFilerListener, List<Integer> listColor) {

        this.context = ct;
        this.colorFilerListener = colorFilerListener;
        this.listColor = listColor;
    }

    @NonNull
    @Override
    public IViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_color_filter_ve, parent, false);
        return new AdapterColorIconFilter.IViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final IViewHolder holder, int position) {
        int color = listColor.get(position);
        holder.color.setColorFilter(color, PorterDuff.Mode.SRC);
        if (selectpos == position) {
            holder.stroke.setVisibility(View.VISIBLE);
        }else {
            holder.stroke.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return listColor.size();
    }

    public class IViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {
        private ImageView color;
        private ImageView stroke;
        public IViewHolder(@NonNull View itemView) {
            super(itemView);
            color = itemView.findViewById(R.id.color_text);
            stroke = itemView.findViewById(R.id.color_text_Select);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            try {
                int pos = getAdapterPosition();
                selectpos = pos;
                colorFilerListener.onClickColorFilterListener(listColor.get(pos), pos);
                notifyDataSetChanged();
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    }
}
