package com.go.videoeditor.add.cropVideo.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;

import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoSize;

@SuppressLint("ViewConstructor")
public class PlayerTextureView extends TextureView implements TextureView.SurfaceTextureListener, Player.Listener {

    private final static String TAG = PlayerTextureView.class.getSimpleName();

    protected static final float DEFAULT_ASPECT = -1f;
    private final ExoPlayer player;
    protected float videoAspect = DEFAULT_ASPECT;

    public PlayerTextureView(Context context, String path) {
        super(context, null, 0);

        DefaultHttpDataSource.Factory dataSourceFactory = new DefaultHttpDataSource.Factory();
        dataSourceFactory.setUserAgent(Util.getUserAgent(context, context.getPackageName()));

        Uri uri = Uri.parse(path);
        MediaItem mediaItem = new MediaItem.Builder().setUri(uri).build();
        MediaSource videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(mediaItem);


        // Produces DataSource instances through which media data is loaded.
//        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context, Util.getUserAgent(context, "yourApplicationName"));

        // This is the MediaSource representing the media to be played.
//        MediaSource videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
//                .createMediaSource(Uri.parse(path));
//        LoopingMediaSource loopingMediaSource = new LoopingMediaSource(videoSource);

        // ExoPlayer
//        player = ExoPlayerFactory.newSimpleInstance(context);
        AdaptiveTrackSelection.Factory adaptiveTrackSelectionFactory = new AdaptiveTrackSelection.Factory();
        DefaultTrackSelector trackSelector = new DefaultTrackSelector(context, adaptiveTrackSelectionFactory);
        DefaultRenderersFactory renderersFactory = new DefaultRenderersFactory(context);
        player = new ExoPlayer.Builder(context, renderersFactory).setTrackSelector(trackSelector).build();
        player.setMediaSource(videoSource);
//        player.setRepeatMode(Player.REPEAT_MODE_ALL);
        // Prepare the player with the source.
        player.prepare();
        player.addListener(this);

        setSurfaceTextureListener(this);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (videoAspect == DEFAULT_ASPECT) return;

        int measuredWidth = getMeasuredWidth();
        int viewHeight = (int) (measuredWidth / videoAspect);
        Log.d(TAG, "onMeasure videoAspect = " + videoAspect);
        Log.d(TAG, "onMeasure viewWidth = " + measuredWidth + " viewHeight = " + viewHeight);

        setMeasuredDimension(measuredWidth, viewHeight);
    }


    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        Log.d(TAG, "onSurfaceTextureAvailable width = " + width + " height = " + height);

        //3. bind the player to the view
        player.setVideoSurface(new Surface(surface));
        player.setPlayWhenReady(true);
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        player.stop();
        player.release();
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    @Override
    public void onVideoSizeChanged(VideoSize videoSize) {
        int width = videoSize.width;
        int height = videoSize.height;
        Log.d(TAG, "width = " + width + " height = " + height + " unappliedRotationDegrees = " + videoSize.unappliedRotationDegrees + " pixelWidthHeightRatio = " + videoSize.pixelWidthHeightRatio);
        videoAspect = ((float) width / height) * videoSize.pixelWidthHeightRatio;
        Log.d(TAG, "videoAspect = " + videoAspect);
        // Log.d(TAG, "videoAspect = " + videoAspect);
        requestLayout();
    }

    public void play() {
        player.setPlayWhenReady(true);
    }

    public void pause() {
        player.setPlayWhenReady(false);
    }

}
