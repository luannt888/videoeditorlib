package com.go.videoeditor.add.stickerLib;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.RectF;
import android.net.Uri;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static java.lang.Math.round;

class UtilsSticker {
    private static final String TAG = "StickerView";

    public static File saveImageToGallery(@NonNull File file, @NonNull Bitmap bmp) {
        try {
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static void notifySystemGallery(@NonNull Context context, @NonNull File file) {
        //->loi dulicate file
        //        try {
        //            if (file == null || !file.exists()) {
        //                throw new IllegalArgumentException("bmp should not be null");
        //            }
        //            MediaStore.Images.Media.insertImage(context.getContentResolver(), file.getAbsolutePath(),
        //                    file.getName(), null);
        //            Log.d("name", file.getAbsolutePath() + "   " + file.getName());
        //        } catch (FileNotFoundException e) {
        //            throw new IllegalStateException("File couldn't be found");
        //        }
        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
    }

    @NonNull
    public static RectF trapToRect(@NonNull float[] array) {
        RectF r = new RectF();
        trapToRect(r, array);
        return r;
    }

    public static void trapToRect(@NonNull RectF r, @NonNull float[] array) {
        r.set(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY, Float.NEGATIVE_INFINITY,
                Float.NEGATIVE_INFINITY);
        for (int i = 1; i < array.length; i += 2) {
            float x = round(array[i - 1] * 10) / 10.f;
            float y = round(array[i] * 10) / 10.f;
            r.left = (x < r.left) ? x : r.left;
            r.top = (y < r.top) ? y : r.top;
            r.right = (x > r.right) ? x : r.right;
            r.bottom = (y > r.bottom) ? y : r.bottom;
        }
        r.sort();
    }
}
