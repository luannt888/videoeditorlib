package com.go.videoeditor.add.stickerLib;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;

import java.util.List;

import static com.go.videoeditor.add.stickerLib.StickerInfor.DRAWABLE_NONE;
import static com.go.videoeditor.add.stickerLib.StickerInfor.SHAPE_CIRCLE;
import static com.go.videoeditor.add.stickerLib.StickerInfor.SHAPE_DACBIET_TEMPLATE;
import static com.go.videoeditor.add.stickerLib.StickerInfor.SHAPE_DEFAUT;


public class StickerDrawable extends Sticker {
    //TODO van de xoa Uri >> o luu Draf
    private Uri curUri = null;
    private String uriPath;

    private boolean lockStickerApart = false;//lock apart >> move vs show icon
    private boolean lockStickerAll = false;//lock all sticker >> tounch
    private int alpha = 255;
    private int colorSticker = Color.WHITE;
    private int posColor = 0;
    private Drawable drawable;
    private int DrawableType = DRAWABLE_NONE; //voi loai shape co dinh >>
    private Rect realBounds;
    private Paint bitmapPaint;
    private Paint shapePaint;
    private RectF rectFShape;
    private boolean isAddImage;

    public boolean isAddImage() {
        return isAddImage;
    }

    public void setAddImage(boolean addImage) {
        isAddImage = addImage;
    }

    private int widthDrawable;
    private int heightDrawable;
    private int shape = SHAPE_DEFAUT;
    private Drawable shapeCircle;
    private Context ct;

    private List<Float> speceficPAth;
    private Path paraPath;
    private float paraK;
    private boolean isIcon = false;
    private int poIcon = 0;

    public StickerDrawable(Drawable drawable, Context context) {
        ct = context;
        this.drawable = drawable;
        widthDrawable = drawable.getIntrinsicWidth();
        heightDrawable = drawable.getIntrinsicHeight();

        bitmapPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        bitmapPaint.setFilterBitmap(true);
        bitmapPaint.setAntiAlias(true);
        bitmapPaint.setDither(true);
        bitmapPaint.setStyle(Paint.Style.FILL);
        bitmapPaint.setStrokeJoin(Paint.Join.ROUND);

        shapePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        shapePaint.setFilterBitmap(true);
        shapePaint.setAntiAlias(true);
        shapePaint.setDither(true);
        shapePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        //path >> ve hinh binh hanh
        paraPath = new Path();

    }

    Bitmap bitmap;

    @Override
    public void draw(@NonNull Canvas canvas) {
        if (!isShow()) {
            return;
        }
        canvas.save();
        canvas.concat(getMatrix());
        realBounds = new Rect(0, 0, getWidthDrawable(), getHeightDrawable());

        if (shape == SHAPE_CIRCLE) {
            Bitmap shapeBitmap = Bitmap.createBitmap(getWidth(), getWidth(), Bitmap.Config.ARGB_8888);
            Canvas canvas1 = new Canvas(shapeBitmap);

            canvas1.drawARGB(0, 0, 0, 0);
            int R = getWidth() / 2;
//            ShapCrop >> CirCle
            canvas1.drawCircle(R, R, R, bitmapPaint);

            Bitmap stickerBitmap = ConvertImageType.drawableToBitmap(drawable, getWidthDrawable(), getHeightDrawable());
            canvas1.drawBitmap(stickerBitmap, 0, 0, shapePaint);
            bitmapPaint.setAlpha(alpha);
            bitmapPaint.setColorFilter(new PorterDuffColorFilter(colorSticker, PorterDuff.Mode.MULTIPLY));
            canvas.drawBitmap(shapeBitmap, 0, 0, bitmapPaint);
            shapeBitmap.recycle();

        } else if (shape == SHAPE_DACBIET_TEMPLATE) {
            // chuyen drawable vao, o ve o day
            Bitmap shapeBitmap = Bitmap.createBitmap(getWidthDrawable(), getHeightDrawable(), Bitmap.Config.ARGB_8888);
            Canvas canvasTemp = new Canvas(shapeBitmap);
            canvasTemp.drawARGB(0, 0, 0, 0);
            if (speceficPAth != null) {
                paraPath.reset();
                for (int i = 0; i < speceficPAth.size(); i = i + 2) {
                    if (i == 0) {
                        paraPath.moveTo(speceficPAth.get(i) * getWidth(), speceficPAth.get(i + 1) * getHeight());
                    } else {

                        paraPath.lineTo(speceficPAth.get(i) * getWidth(), speceficPAth.get(i + 1) * getHeight());
                    }
                }
                bitmapPaint.setColor(Color.BLUE);
                canvasTemp.drawPath(paraPath, bitmapPaint);

                //ve hinh
                Bitmap stickerBitmap = ConvertImageType.drawableToBitmap(drawable, getWidthDrawable(), getHeightDrawable());
                canvasTemp.drawBitmap(stickerBitmap, 0, 0, shapePaint);

                //
                bitmapPaint.setAlpha(alpha);
                bitmapPaint.setColorFilter(new PorterDuffColorFilter(colorSticker, PorterDuff.Mode.MULTIPLY));
                canvas.drawBitmap(shapeBitmap, 0, 0, bitmapPaint);

            }

        } else {//shape == HCN
            drawable.setBounds(realBounds);
            drawable.setFilterBitmap(true);
            drawable.setDither(true);
            //color
            drawable.setColorFilter(new PorterDuffColorFilter(colorSticker, PorterDuff.Mode.MULTIPLY));
            //alpha
            drawable.setAlpha(alpha);

            drawable.draw(canvas);

//            c2
//            bitmapSticker =ConvertUtils.drawableToBitmap(drawable);
//            bitmapPaint.setAlpha(alpha);
//            canvas.drawBitmap(bitmapSticker, null, realBounds, bitmapPaint);
        }

        canvas.restore();
    }

    @NonNull
    @Override
    public Drawable getDrawable() {
        return drawable;
    }

    @Override
    public StickerDrawable setDrawable(@NonNull Drawable drawable) {
        this.drawable = drawable;
        return this;
    }

    @NonNull
    @Override
    public StickerDrawable setAlpha(@IntRange(from = 0, to = 255) int alpha) {
        this.alpha = alpha;
        return this;
    }

    public int getAlpha() {
        return alpha;
    }

    @Override
    public int getWidth() {
        return widthDrawable;
    }

    @Override
    public int getHeight() {
        return heightDrawable;
    }

    @Override
    public void release() {
        super.release();
        if (drawable != null) {
            drawable = null;
        }
    }


    public int getWidthDrawable() {
        return widthDrawable;
    }

    public int getColorSticker() {
        return colorSticker;
    }

    public int getPosColor() {
        return posColor;
    }


    public void setColorSticker(int colorSticker, int posColor) {
        this.colorSticker = colorSticker;
        this.posColor = posColor;
    }

    public void setWidthDrawable(int widthDrawable) {
        this.widthDrawable = widthDrawable;
    }

    public int getHeightDrawable() {
        return heightDrawable;
    }

    public void setHeightDrawable(int heightDrawable) {
        this.heightDrawable = heightDrawable;
    }

    public int getShape() {
        return shape;
    }

    public void setShape(int shape) {
        this.shape = shape;
    }

    public Path getShapeParaPath() {
        return paraPath;
    }

    public void setShapeParaPath(int paraK) {
        this.paraK = paraK;
    }

    public List<Float> getSpeceficPAth() {
        return speceficPAth;
    }

    public void setSpeceficPAth(List<Float> speceficPAth) {
        this.speceficPAth = speceficPAth;
    }

    public boolean isLockStickerAll() {
        return lockStickerAll;
    }

    public void setLockStickerAll(boolean lockStickerAll) {
        this.lockStickerAll = lockStickerAll;
    }

    public boolean getLockMoveOneSticker() {
        return lockStickerApart;
    }

    public void setLockMoveOneSticker(boolean lockMove) {
        this.lockStickerApart = lockMove;
    }

    public Uri getCurUri() {
        return curUri;
    }

    public void setCurUri(Uri curUri) {
        this.curUri = curUri;
    }

    public int getDrawableType() {
        return DrawableType;
    }

    public void setDrawableType(int drawableType) {
        DrawableType = drawableType;
    }

    public boolean getIsIcon() {
        return isIcon;
    }

    public int getPosIcon() {
        return poIcon;
    }

    public void setIsIcon(int posIcon, boolean icon) {
        isIcon = icon;
        this.poIcon = posIcon;
    }

    public String getUriPath() {
        return uriPath;
    }

    public void setUriPath(String uriPath) {
        this.uriPath = uriPath;
    }
}
