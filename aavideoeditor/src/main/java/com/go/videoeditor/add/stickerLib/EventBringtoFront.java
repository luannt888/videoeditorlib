package com.go.videoeditor.add.stickerLib;

import android.view.MotionEvent;

public class EventBringtoFront implements EventStickerIcon {
    @Override
    public void onActionDown(StickerView stickerView, MotionEvent event) {
        //bringtofront
        stickerView.bringStickertoFront();
    }

    @Override
    public void onActionMove(StickerView stickerView, MotionEvent event) {

    }

    @Override
    public void onActionUp(StickerView stickerView, MotionEvent event) {

    }
}
