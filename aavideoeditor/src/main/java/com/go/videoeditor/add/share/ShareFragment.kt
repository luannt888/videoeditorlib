package com.go.videoeditor.add.share

import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.FileProvider
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.app.Constant
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_share_ve.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*
import java.io.File

class ShareFragment : BottomSheetDialogFragment() {

    var url: String? = null
    private val Facebook = "com.facebook.katana"
    private val Twitter = "com.twitter.android"
    private val Instagram = "com.instagram.android"
    private val More = "more"
    private var listStoryFile: ArrayList<File> = ArrayList()
    private var listShareUri: ArrayList<Uri> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_share_ve, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog.behavior.skipCollapsed = true
            bottomSheetDialog.behavior.isDraggable = true
            bottomSheetDialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        initView()
        initData()
        initControl()
    }

    private fun initView() {
        textHeaderTitle.text = getString(R.string.item_share)
        buttonDone.visibility = View.GONE
    }

    var onClickDownloadListenner: View.OnClickListener? = null
    private fun initControl() {
        btnShareFacebook.setOnClickListener {
            share(Facebook)
        }
        btnShareInsta.setOnClickListener {
            share(Instagram)
        }
        btnShareTwiter.setOnClickListener {
            share(Twitter)
        }
        btnShareMore.setOnClickListener {
            share(More)
        }
        buttonClose.setOnClickListener {
            dismiss()
        }

    }

    private fun share(shareType: String) {
        if (shareType.equals(More)) {
            shareFiles(shareType)
        } else {
            try {
                checkAppInstall(shareType)
                shareFiles(shareType)
            } catch (e: PackageManager.NameNotFoundException) {
                Toast.makeText(activity!!, "You need install app first!", Toast.LENGTH_SHORT).show()
                downloadApp(shareType)
            }
        }
    }

    fun initData() {
        val filePath = arguments?.getString(Constant.FILE_PATH, null)
        if (filePath.isNullOrEmpty()) {
            (activity as? BaseActivity)?.showToast("Không tìm thấy file")
            dismiss()
            return
        }
        listStoryFile.add(File(filePath))

        for (i in listStoryFile.indices) {
            val uri1 = FileProvider.getUriForFile(
                activity!!, activity!!.applicationContext.packageName.toString() + ".provider",
                listStoryFile[i]
            )
            listShareUri.add(uri1)
        }
    }

    @Throws(PackageManager.NameNotFoundException::class)
    private fun checkAppInstall(uri: String) {
        val pm: PackageManager = activity!!.getPackageManager()
        pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
    }


    fun shareFiles(shareType: String) {
//vs 1 PHOTO
        if (listStoryFile.size > 0) {
            //1
            val shareIntent = Intent()
            shareIntent.type = "image/*"

            //2
            if (listStoryFile.size == 1) { //photo
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.putExtra(Intent.EXTRA_STREAM, listShareUri.get(0))
            } else { //story
                shareIntent.action = Intent.ACTION_SEND_MULTIPLE
                shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, listShareUri)
            }

            //3
            if (shareType == More) {
                try {
                    startActivity(Intent.createChooser(shareIntent, "Share image"))
                } catch (e: Exception) {
                    Toast.makeText(activity!!, "Some thing error!!", Toast.LENGTH_SHORT).show()
                }
            } else {
                try {
                    shareIntent.setPackage(shareType)
                    startActivity(shareIntent)
                } catch (e: Exception) {
                    //This app support uploading more than two images.>>twitter vs instagram
                    Toast.makeText(activity!!, "This app not support uploading more than two files!", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    fun downloadApp(pkNam: String) {
        try {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$pkNam")))
        } catch (anfe: ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$pkNam")))
        }
    }

}