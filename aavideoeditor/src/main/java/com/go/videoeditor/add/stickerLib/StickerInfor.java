package com.go.videoeditor.add.stickerLib;

import android.graphics.Color;
import android.graphics.drawable.Drawable;

import java.util.List;

public class StickerInfor {

    public static final String TYPE_STICKER_IMG = "IMG";
    public static final String TYPE_STICKER_TXT = "TXT";
    public static final int DEFAUT_COLOR = Color.BLACK;
    public static final int DEFAUT_ANGLE = 0;

    public static final int SHAPE_DEFAUT = 0; //hinh chu nhat
    public static final int SHAPE_CIRCLE = 1;
    public static final int SHAPE_DACBIET_TEMPLATE = 3; //hinh khac

    public static final int DRAWABLE_NONE = 10; //hinh khac
    public static final int DRAWABLE_TYPE1 = 11; //hinh khac
    public static final int DRAWABLE_TYPE2 = 12; //hinh khac
    public static final int DRAWABLE_TYPE3 = 13; //hinh khac
    public static final int DRAWABLE_TYPE4 = 14; //hinh khac
    public static final int DRAWABLE_TYPE5 = 15; //hinh khac
    public static final int DRAWABLE_TYPE6 = 16; //hinh khac
    public static final int DRAWABLE_TYPE7 = 17; //hinh khac
    public static final int DRAWABLE_TYPE8 = 18; //hinh khac

    //region Generall
    private String type;

    private Constraint constraints;

    private String textContent;

    private int rotate ;

    private int textColor ;
    private int posTextColor = 2;

    private Drawable drawableStatic = null;
    private int typeDrawableStatic = DRAWABLE_NONE;

    private int alpha;

    private int alig;

    private int bgColor;
    private int bgColorindex = 0;

    private int shape ;

    private boolean isBold = false;
    private boolean isU = false;
    private boolean isI = false;

    private boolean isLockAll = false;
    private boolean isLockApart = false;

    private int colorIconFilter = Color.WHITE;
    private int colorIconpos = 0;

    private boolean isIcon = false;

    private List<Float> PathPercent;

    private int font; //phai khop vs font index


    //endregion

    //
    private String uriPath;

    //region Add with StickerText

    public StickerInfor(String type, Constraint constraints, String textContent) {
        this.type = type;
        this.constraints = constraints;
        this.textContent = textContent;
        this.textColor = DEFAUT_COLOR;
        this.rotate = DEFAUT_ANGLE;
        this.shape = SHAPE_DEFAUT;
        this.bgColor = -1;
        this.font = 0;
        isLockAll = false;

    }
    //endregion

    //endregion

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Constraint getConstraints() {
        return constraints;
    }

    public void setConstraints(Constraint constraints) {
        this.constraints = constraints;
    }

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        this.textContent = textContent;
    }

    public int getRotate() {
        return rotate;
    }

    public void setRotate(int rotate) {
        this.rotate = rotate;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor,int pos) {
        this.setPosTextColor(pos);
        this.textColor = textColor;
    }

    public Drawable getDrawableStatic() {
        return drawableStatic;
    }

    public int getTypeDrawableStatic() {
        return this.typeDrawableStatic;
    }

    public void setTypeDrawableStatic(int typeDrawableStatic) {
        this.typeDrawableStatic = typeDrawableStatic;
    }

    public void setDrawableStatic(Drawable drawableStatic, int typeDrawableStatic) {
        this.drawableStatic = drawableStatic;
        this.typeDrawableStatic = typeDrawableStatic;
    }

    public int getAlpha() {
        return alpha;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public int getShape() {
        return shape;
    }

    public void setShape(int shape) {
        this.shape = shape;
    }

    public List<Float> getPathPercent() {
        return PathPercent;
    }

    public void setPathPercent(List<Float> pathPercent) {
        PathPercent = pathPercent;
    }

    public int getBgColor() {
        return bgColor;
    }

    public void setBgColor(int bgColor, int bgColorIndex) {

        this.bgColor = bgColor;
        this.bgColorindex = bgColorIndex;

    }

    public int getFont() {
        return font;
    }

    public void setFont(int font) {
        this.font = font;
    }

    public boolean isBold() {
        return isBold;
    }

    public void setBold(boolean bold) {
        isBold = bold;
    }

    public boolean getIsLockAll() {
        return isLockAll;
    }

    public void setIsLockAll(boolean lockAll) {
        isLockAll = lockAll;
    }

    public int getPosTextColor() {
        return posTextColor;
    }

    public void setPosTextColor(int posTextColor) {
        this.posTextColor = posTextColor;
    }

    public int getBgColorindex() {
        return bgColorindex;
    }

    public void setBgColorindex(int bgColorindex) {
        this.bgColorindex = bgColorindex;
    }

    public String getUriPath() {
        return uriPath;
    }

    public void setUriPath(String uriPath) {
        this.uriPath = uriPath;
    }

    public boolean isU() {
        return isU;
    }

    public void setU(boolean u) {
        isU = u;
    }

    public boolean isI() {
        return isI;
    }

    public void setI(boolean i) {
        isI = i;
    }

    //
    private float scale = 1f;
    //
    private float textSize = 20f;
    private float mutiLine = 1f;
    private float spaceLetter = 0f;

    public float getTextSize() {
        return textSize;
    }

    public void setTextSize(float textSize) {
        this.textSize = textSize;
    }

    public float getMutiLine() {
        return mutiLine;
    }

    public void setMutiLine(float mutiLine) {
        this.mutiLine = mutiLine;
    }

    public float getSpaceLetter() {
        return spaceLetter;
    }

    public void setSpaceLetter(float spaceLetter) {
        this.spaceLetter = spaceLetter;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }


    public int getAlig() {
        return alig;
    }

    public void setAlig(int alig) {
        this.alig = alig;
    }

    public boolean getIsIcon() {
        return isIcon;
    }

    public int getposICon() {
        return posIcon;
    }

    private int posIcon;
    public void setIsIcon(boolean icon, int posIcon) {
        isIcon = icon;
        this.posIcon = posIcon;
    }

    public boolean isLockApart() {
        return isLockApart;
    }

    public void setLockApart(boolean lockApart) {
        isLockApart = lockApart;
    }

    public void setIConFilter(int colorIconFilter, int colorIconpos) {
        this.colorIconFilter = colorIconFilter;
        this.colorIconpos = colorIconpos;
    }

    public int getColorIconFilter() {
        return this.colorIconFilter;
    }

    public int getColorIconpos() {
        return this.colorIconpos;
    }
}

