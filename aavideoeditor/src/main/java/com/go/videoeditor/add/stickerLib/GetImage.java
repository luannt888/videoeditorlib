package com.go.videoeditor.add.stickerLib;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import com.go.videoeditor.utils.AppUtils;

import java.io.File;

public class GetImage {
    public static File openCamera(int request, Activity act) {
        Intent t = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (t.resolveActivity(act.getPackageManager()) != null) {
            File mImageFile = null;
            //                mImageFile = createRandomnameTempImageFile(act);
            mImageFile = AppUtils.Companion.createImageTempFile(act);

            Uri photoUri;
            if(Build.VERSION.SDK_INT < 24) {
                //loi camera has stop
                photoUri = Uri.fromFile(mImageFile);

            }else{
                //loi FileUriExposedException
                photoUri = FileProvider.getUriForFile(act, act.getApplicationContext().getPackageName() + ".provider", mImageFile);
            }
            t.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            try {
                act.startActivityForResult(t, request);
                act.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            } catch (SecurityException e) {
                Toast.makeText(act, "Permission not gain!", Toast.LENGTH_SHORT).show();
            }

            return mImageFile;
        }
        return null;
    }

    public static void openGellary(int request, Activity act) {
//        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);//c1
//        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);//c2
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI);//c2
//        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        act.startActivityForResult(intent, request);
        act.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public static void openGellaryOnlyGifImage(int request, Activity act) {
//        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);//c1
//        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);//c2
//        Intent intent = new Intent( Intent.ACTION_PICK);//c2
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI);//c2
//        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        String[] mimeTypes = {"image/gif"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        act.startActivityForResult(intent, request);
        act.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
