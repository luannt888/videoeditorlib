package com.go.videoeditor.add.stickerLib;

public class Dimen {


    private float percentage;
    private float constant;

    public Dimen(float percentage, float constant) {
        this.percentage = percentage;
        this.constant = constant;
    }

    public float getPercentage() {
        return percentage;
    }

    public void setPercentage(float percentage) {
        this.percentage = percentage;
    }

    public float getConstant() {
        return constant;
    }

    public void setConstant(float constant) {
        this.constant = constant;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [constant = "+constant+", percentage = "+percentage+"]";
    }

    public float caculateDime(float oldParent, float newParent) {
        return (constant / oldParent) * newParent;
    }
}
