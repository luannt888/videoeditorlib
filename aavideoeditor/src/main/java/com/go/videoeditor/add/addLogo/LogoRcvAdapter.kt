/*
 *
 *  Created by Optisol on Aug 2019.
 *  Copyright © 2019 Optisol Business Solutions pvt ltd. All rights reserved.
 *
 */

package com.go.videoeditor.add.addLogo

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.go.videoeditor.R
import com.go.videoeditor.add.stickerLib.ManagerAssets

class LogoRcvAdapter(list: ArrayList<String>, val context: Context, onSelectLogoListener: OnSelectLogoListener) :
    RecyclerView.Adapter<LogoRcvAdapter.MyPostViewHolder>() {

    private var tagName: String = LogoRcvAdapter::class.java.simpleName
    private var list = list
    private var selectLogoListener = onSelectLogoListener
    private var selectedPosition: Int = -1
    private var selectedFilePath: String? = null

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyPostViewHolder {
        return MyPostViewHolder(LayoutInflater.from(context).inflate(R.layout.item_add_logo_ve, p0, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyPostViewHolder, position: Int) {

        Glide.with(context).load(
            ManagerAssets.ACSSESPATH + list.get(position)
        )
            .thumbnail(0.2f)
            .apply(RequestOptions().encodeQuality(80))
            .centerCrop().into(holder.tvClipArt)
        holder.tvClipArt.isSelected = selectedPosition == position

        holder.tvClipArt.setOnClickListener {
            //selected clip art will be saved here
            selectedPosition = position
            selectedFilePath = list[holder.adapterPosition]
            selectLogoListener.onSelectedLogo(selectedFilePath!!)
            notifyDataSetChanged()
        }
    }

    class MyPostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvClipArt: ImageView = itemView.findViewById(R.id.tv_clip_art)
    }

    interface OnSelectLogoListener {
        fun onSelectedLogo(path: String)
    }
}