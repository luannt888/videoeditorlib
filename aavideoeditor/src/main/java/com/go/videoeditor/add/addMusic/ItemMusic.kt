package com.go.videoeditor.add.addMusic

import android.graphics.Bitmap
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemMusic(var filePath: String?, var title: String, var bitmap: Bitmap?, var duration: Long?) : Parcelable