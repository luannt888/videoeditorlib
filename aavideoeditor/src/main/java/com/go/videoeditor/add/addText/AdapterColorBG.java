package com.go.videoeditor.add.addText;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.go.videoeditor.R;

import java.util.ArrayList;
import java.util.List;


public class AdapterColorBG extends RecyclerView.Adapter<AdapterColorBG.MHolder> {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<Integer> listColor;
    private int selectpos = -1;
    private ListItemListener listItemListener;

    public List<Integer> getListColor() {
        return listColor;
    }

    public AdapterColorBG(Context context, ListItemListener listener) {
        this.listItemListener = listener;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        listColor = new ArrayList<>();
        //tao data
        TypedArray typedArray = context.getResources().obtainTypedArray(R.array.origil_colors);
        //bo mau tranparent
        for (int i = 1; i < typedArray.length(); i++) {
            listColor.add(typedArray.getColor(i, 0));
        }
        typedArray.recycle();


    }

    @NonNull
    @Override
    public MHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_color_bg_ve, parent, false);
        return new MHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MHolder holder, int position) {
        //getData
        int color = listColor.get(position);
        holder.color.setColorFilter(color, PorterDuff.Mode.SRC);
        if (selectpos == position) {
            holder.stroke.setVisibility(View.VISIBLE);
        } else {
            holder.stroke.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return listColor.size();
    }

    public class MHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView color;
        private ImageView stroke;

        public MHolder(@NonNull View itemView) {
            super(itemView);
            color = itemView.findViewById(R.id.color_bt);
            stroke = itemView.findViewById(R.id.color_bt_Select);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            try {
                int clickedPosition = getAdapterPosition();
                selectpos = clickedPosition;
                if (listItemListener != null) {
                    listItemListener.onListItemClick(clickedPosition);
                }
                notifyDataSetChanged();
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    }

    public int getSelected() {
        return selectpos;
    }

    public void setSelected(int pos) {
        selectpos = pos;
    }
}
