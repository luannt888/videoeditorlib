package com.go.videoeditor.merge

import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.go.videoeditor.R
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.utils.VideoEditUtils
import kotlinx.android.synthetic.main.item_sort_video_ve.view.*
import java.util.*

class SortVideoAdapter(
    val activity: Context, val itemList: ArrayList<ItemVideo>, val type: Int, val style: Int, val
    numberColumn: Int
) : RecyclerView.Adapter<ViewHolder>(), ItemMoveCallbackListener.Listener {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0
    var startDragListener: OnStartDragListener? = null
    var placeHolder: Int = 0

    init {
        placeHolder = if (type == Constant.TYPE_REPLACE_MUSIC) R.mipmap.ic_music_loading else R.drawable.img_loading
    }

    override fun getItemViewType(position: Int): Int {
        val viewType = style
        return viewType
    }

    fun initViewSize(viewType: Int) {
//        val screenSize = ScreenSize(activity)
//        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
//            screenSize.width
        val height: Int = activity.resources.getDimensionPixelSize(R.dimen.dimen_150)
        when (viewType) {
            Constant.STYLE_HORIZONTAL -> {
                imageWidth = height * 2 / 3
                imageHeight = height
            }
        }
//        Loggers.e("initViewSize: $viewType", "$imageWidth x $imageHeight")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initViewSize(viewType)
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_sort_video_ve,
            parent, false
        )
        return FrameViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemObj: ItemVideo = itemList.get(position)
        if (holder is FrameViewHolder) {
            bindDataFrameView(holder, itemObj, position)
        }
    }

    fun bindDataFrameView(holder: FrameViewHolder, itemObj: ItemVideo, position: Int) {
//        holder.imageThumbnail.setImageBitmap(itemObj.bitmap)
        if (itemObj.bitmap != null) {
            MyApplication.getInstance()?.loadImage(activity, holder.imageThumbnail, itemObj.bitmap!!)
        } else {
            MyApplication.getInstance()?.loadImage(activity, holder.imageThumbnail, placeHolder)
        }
        holder.textTitle.text = itemObj.title
        holder.textDuration.text = VideoEditUtils.convertMiliSecondsToTime(itemObj.duration!!)

        holder.layoutRoot.setOnClickListener {
//            onItemClickListener?.onClick(itemObj, position, holder)
        }
        holder.itemView.buttonReorder.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                startDragListener?.onStartDrag(holder)
            }
            return@setOnTouchListener true
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class FrameViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imageThumbnail = itemView.imageThumbnail
        val layoutImage = itemView.layoutImage
        val textTitle = itemView.textTitle
        val textDuration = itemView.textDuration
        val buttonReorder = itemView.buttonReorder

        /*init {
            val lp: LinearLayout.LayoutParams = layoutImage.layoutParams as LinearLayout
            .LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
//            Log.e("FrameViewHolder", "$imageWidth x $imageHeight")
            layoutImage.layoutParams = lp
        }*/
    }

    interface OnItemClickListener {
        fun onClick(itemObject: ItemVideo, position: Int, holder: ViewHolder)
    }

    override fun onRowMoved(fromPosition: Int, toPosition: Int) {
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(itemList, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(itemList, i, i - 1)
            }
        }
        notifyItemMoved(fromPosition, toPosition)
    }

    override fun onRowSelected(itemViewHolder: DragDropRecyclerAdapter.ItemViewHolder) {
        TODO("Not yet implemented")
    }

    override fun onRowClear(itemViewHolder: DragDropRecyclerAdapter.ItemViewHolder) {
        TODO("Not yet implemented")
    }
}