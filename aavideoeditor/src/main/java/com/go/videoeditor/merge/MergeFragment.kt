package com.go.videoeditor.merge

import android.content.DialogInterface
import android.graphics.Bitmap
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.activity.VideoEditorActivity
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.listener.OnDismissListener
import com.go.videoeditor.transcoder.Transcoder
import com.go.videoeditor.transcoder.TranscoderOptions
import com.go.videoeditor.utils.AppUtils
import com.go.videoeditor.utils.BackgroundExecutor
import com.go.videoeditor.utils.OptiCommonMethods
import com.go.videoeditor.utils.UiThreadExecutor
import com.go.videoeditor.utils.VideoEditUtils
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_merge_ve.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class MergeFragment : BottomSheetDialogFragment() {
    var screenType: Int = -1
    lateinit var adapter: SortVideoAdapter
    lateinit var touchHelper: ItemTouchHelper
    var itemList: ArrayList<ItemVideo> = ArrayList()
    var fileList: ArrayList<String>? = null
    var onDismissListener: OnDismissListener? = null
    var isSuccess = false
    var videoPath: String? = null
    val idBackgroundThreadList: ArrayList<String> = ArrayList()
    var numberVideoGetted: Int = 0
    var outVideoPath: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_merge_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog.behavior.skipCollapsed = true
            bottomSheetDialog.behavior.isDraggable = false
            bottomSheetDialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        initData()
        initControl()
    }

    fun initData() {
        screenType = arguments?.getInt(Constant.SCREEN_TYPE) as Int
        if (screenType != Constant.TYPE_MERGE_VIDEO && screenType != Constant.TYPE_REPLACE_MUSIC && screenType != Constant.TYPE_INSERT_VIDEO) {
            dismiss()
            return
        }
        fileList = arguments?.getStringArrayList(Constant.DATA) as ArrayList<String>
        if (fileList == null || fileList!!.size == 0) {
            dismiss()
            return
        }
        var title = ""
        if (screenType == Constant.TYPE_REPLACE_MUSIC) {
            title = getString(R.string.item_add_music)
            fileList!!.removeAt(0)
        } else if (screenType == Constant.TYPE_INSERT_VIDEO) {
            title = getString(R.string.item_insert_video)
            fileList!!.removeAt(0)
        } else {
            title = getString(R.string.item_merge)
        }
        textHeaderTitle.text = title
        initAdapter()
        getVideoInfo()
    }

    fun initAdapter() {
        for (i in 0 until fileList!!.size) {
            val filePath = fileList!!.get(i)
            val itemVideo = ItemVideo("", "", filePath, "File ${i + 1}", null, 0)
            itemList.add(itemVideo)
        }

        val style = Constant.STYLE_HORIZONTAL
        adapter = SortVideoAdapter(activity!!, itemList, screenType, style, 1)
        val callback: ItemTouchHelper.Callback = ItemMoveCallbackListener(adapter)
        touchHelper = ItemTouchHelper(callback)
        touchHelper.attachToRecyclerView(recyclerView)
        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = layoutManager
//        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = adapter
        adapter.startDragListener = object : OnStartDragListener {
            override fun onStartDrag(viewHolder: RecyclerView.ViewHolder) {
                touchHelper.startDrag(viewHolder)
            }
        }

        adapter.onItemClickListener = object : SortVideoAdapter.OnItemClickListener {
            override fun onClick(itemObject: ItemVideo, position: Int, holder: RecyclerView.ViewHolder) {
                Loggers.e("onItemClickListener", "filePath = ${itemObject.filePath}")
            }
        }
    }

    fun getVideoInfo() {
        for (i in 0 until itemList.size) {
            val itemObj = itemList.get(i)
            val idName = "id${Calendar.getInstance().timeInMillis}"
            idBackgroundThreadList.add(idName)
            BackgroundExecutor.execute(object : BackgroundExecutor.Task(idName, 0L, idName) {
                override fun execute() {
                    if (isDetached) {
                        return
                    }
                    try {
                        val videoUri = Uri.parse(itemObj.filePath)
                        if (screenType == Constant.TYPE_MERGE_VIDEO) {
                            val mediaMetadataRetriever = MediaMetadataRetriever()
                            mediaMetadataRetriever.setDataSource(activity, videoUri)
                            var bitmap: Bitmap? =
                                mediaMetadataRetriever.getFrameAtTime(
                                    1000,
                                    MediaMetadataRetriever.OPTION_CLOSEST_SYNC
                                )
                            bitmap = Bitmap.createScaledBitmap(
                                bitmap!!,
                                VideoEditUtils.THUMB_WIDTH,
                                VideoEditUtils.THUMB_HEIGHT,
                                false
                            )
                            itemObj.bitmap = bitmap
                            mediaMetadataRetriever.release()
                        }
                        val videoFile = File(itemObj.filePath!!)
                        val timeInMillis = OptiCommonMethods.getVideoDuration(activity!!, videoFile)
                        itemObj.duration = timeInMillis
                        itemObj.title = videoFile.nameWithoutExtension
                        UiThreadExecutor.runTask(idName, Runnable {
                            adapter.notifyDataSetChanged()
                        }, 0L)
                        numberVideoGetted++
                    } catch (e: Throwable) {
                        e.printStackTrace()
                    }
                }
            })
        }
    }

    fun mergeVideos() {
//        val exportFilePath = OptiCommonMethods.getVideoFilePath(activity!!)
        val exportFilePath = AppUtils.createVideoFileEditedPath(activity!!)
        val builder: TranscoderOptions.Builder = Transcoder.into(exportFilePath)

        for (i in 0 until itemList.size) {
            val item = itemList.get(i)
            for (j in 0 until fileList!!.size) {
                val fPath = fileList!!.get(j)
                if (item.filePath.equals(fPath)) {
                    val uri = Uri.parse(fPath)
                    builder.addDataSource(activity!!, uri)
                    break
                }
            }
        }
        (activity as? VideoEditorActivity)?.startConvertInThread(exportFilePath,builder)
    }

    fun initControl() {
        buttonClose.setOnClickListener {
            isSuccess = false
            dismiss()
        }

        buttonDone.setOnClickListener {
            if (numberVideoGetted != itemList.size) {
                (activity as? BaseActivity)?.showDialog(getString(R.string.msg_getting_video_info))
                return@setOnClickListener
            }
            if (screenType == Constant.TYPE_MERGE_VIDEO) {
                mergeVideos()
            }
            isSuccess = true
            dismiss()
            //TYPE ADD MUSIC
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        for (i in 0 until idBackgroundThreadList.size) {
            BackgroundExecutor.cancelAll(idBackgroundThreadList.get(i), true)
        }
        onDismissListener?.onDismiss(isSuccess, videoPath, itemList)
        super.onDismiss(dialog)
    }

    override fun onCancel(dialog: DialogInterface) {
        isSuccess = false
        onDismissListener?.onDismiss(isSuccess, "", null)
        super.onCancel(dialog)
    }
}