package com.go.videoeditor.merge

import android.graphics.Bitmap
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemVideo(val key: String, val editData: String, var filePath: String?, var title: String, var bitmap: Bitmap?, var duration: Long?) : Parcelable