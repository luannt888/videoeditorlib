package com.go.videoeditor.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import com.go.videoeditor.stream.GoLivestreamFragment
import com.go.videoeditor.R
import com.go.videoeditor.listener.OnDialogButtonListener

class LivestreamActivity : BaseActivity() {
    var goLivestreamFragment: GoLivestreamFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_livestream_ve)
        init()
    }

    fun init() {
        goLivestreamFragment = GoLivestreamFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayoutActivity, goLivestreamFragment!!)
        fragmentTransaction.commit()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        goLivestreamFragment?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (goLivestreamFragment != null && goLivestreamFragment!!.isPopupMenuFilterShowing()) {
                return true
            }
            if (goLivestreamFragment != null && goLivestreamFragment!!.isStreaming) {
                showDialog(
                    true,
                    R.string.notification,
                    R.string.msg_stop_stream,
                    R.string.stop_stream,
                    R.string.close,
                    object : OnDialogButtonListener {
                        override fun onLeftButtonClick() {
                            finish()
                        }

                        override fun onRightButtonClick() {
                        }

                    })
                return true
            }
        }
        return super.onKeyUp(keyCode, event)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_CANCELED) return
        val fragments = supportFragmentManager.fragments
        for (i in 0 until fragments.size) {
            fragments.get(i).onActivityResult(requestCode, resultCode, data)
            /*if (fragments.get(i) is AddLogoFragment) {
                fragments.get(i).onActivityResult(requestCode, resultCode, data)
            }*/
        }
    }

    override fun finish() {
        super.finish()
    }
}