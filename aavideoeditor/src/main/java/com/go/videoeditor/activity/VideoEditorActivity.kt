package com.go.videoeditor.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.TypedArray
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.util.Log
import android.util.Size
import android.view.Gravity
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.go.videoeditor.R
import com.go.videoeditor.add.addFilter.AddFilterFragment
import com.go.videoeditor.add.addLogo.AddLogoFragment
import com.go.videoeditor.add.addMusic.TrimAudioFragment
import com.go.videoeditor.add.addText.AddTextFragment
import com.go.videoeditor.add.outputSize.AdapterRcvSize
import com.go.videoeditor.add.outputSize.OutputSizeFragment
import com.go.videoeditor.add.share.ShareFragment
import com.go.videoeditor.add.stickerLib.EventSticker
import com.go.videoeditor.add.stickerLib.Sticker
import com.go.videoeditor.add.stickerLib.StickerDrawable
import com.go.videoeditor.add.stickerLib.StickerText
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.convert.ConvertManager
import com.go.videoeditor.convert.ItemConvertVideo
import com.go.videoeditor.convert.TaskBackgroundBottomDialogFragment
import com.go.videoeditor.downloadVideo.DownloadedVideoFragment
import com.go.videoeditor.fragment.PlayerFragment
import com.go.videoeditor.fragment.SaveProjectFragment
import com.go.videoeditor.listener.OnDialogButtonListener
import com.go.videoeditor.listener.OnDismissListener
import com.go.videoeditor.listener.OnPlayerStateChangeListener
import com.go.videoeditor.listener.SingleCallback
import com.go.videoeditor.merge.ItemVideo
import com.go.videoeditor.merge.MergeFragment
import com.go.videoeditor.model.ItemCmd
import com.go.videoeditor.model.ItemVideoTimeLine
import com.go.videoeditor.timeLine.FrameTimeLineAdapter
import com.go.videoeditor.timescale.TimeScaleFragment
import com.go.videoeditor.transcoder.Transcoder
import com.go.videoeditor.transcoder.TranscoderListener
import com.go.videoeditor.transcoder.TranscoderOptions
import com.go.videoeditor.transcoder.engine.TrackType
import com.go.videoeditor.transcoder.mp4Compose.composer.Mp4Composer
import com.go.videoeditor.transcoder.mp4Compose.filter.GlBitmapOverlaySampleFilter
import com.go.videoeditor.transcoder.mp4Compose.filter.GlFilter
import com.go.videoeditor.transcoder.mp4Compose.filter.GlFilterGroup
import com.go.videoeditor.transcoder.mp4Compose.tranform.FillMode
import com.go.videoeditor.transcoder.mp4Compose.tranform.FillModeCustomItem
import com.go.videoeditor.transcoder.source.ClipDataSource
import com.go.videoeditor.transcoder.source.DataSource
import com.go.videoeditor.transcoder.source.FilePathDataSource
import com.go.videoeditor.transcoder.strategy.DefaultVideoStrategy
import com.go.videoeditor.trim.ItemFrame
import com.go.videoeditor.trim.RangeSeekBarView
import com.go.videoeditor.trim.TrimFragment
import com.go.videoeditor.upload.UploadFragment
import com.go.videoeditor.utils.*
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.PlaybackParameters
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.skydoves.powermenu.*
import kotlinx.android.synthetic.main.activity_video_editor.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class VideoEditorActivity : BaseActivity() {
    val TAG_NAME = this::class.simpleName
    private var videoFile: File? = null
    private var duration: Long = 0L
    private var mPlayer: ExoPlayer? = null
    var filePath: String? = null
    var playerFragment: PlayerFragment? = null
    var cmdList: ArrayList<ItemCmd> = ArrayList()
    var filterIndex: Int = -1
    var projectKey: String? = null
    var editData: String? = null
    var projectName: String? = null
    var popupMenuSeekbarTimeLine: PowerMenu? = null
    var accessToken:String? = null

    fun addCmd(itemObj: ItemCmd) {
        if (itemObj.cmd.isEmpty()) {
            removeCmd(itemObj)
            return
        }
        for (obj in cmdList) {
            if (obj.key.equals(itemObj.key)) {
                obj.cmd = itemObj.cmd
                return
            }
        }
        cmdList.add(itemObj)
    }

    fun removeCmd(itemObj: ItemCmd) {
        for (obj in cmdList) {
            if (obj.key.equals(itemObj.key)) {
                cmdList.remove(obj)
                return
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (MyApplication.sContext == null) {
            MyApplication.init(this)
        }
        setContentView(R.layout.activity_video_editor)
//        val ca = MediaCodecInfo.CodecCapabilities().maxSupportedInstances
//        Loggers.e("MediaCodecInfo", "" + ca);

        init()
    }

    private fun init() {
        initData()
        if (filePath.isNullOrEmpty()) {
            return
        }
        initTimeLine()
        initStickerView()
        initControl()
    }

    fun initData() {
        val bundle = intent.extras
        bundle?.let {accessToken = it.getString(Constant.ACCESS_TOKEN)
        }
        if (filePath.isNullOrEmpty()) {
            bundle?.let {
                filePath = it.getString(Constant.FILE_PATH, null)
                projectKey = it.getString(Constant.KEY, null)
                editData =it.getString(Constant.DATA, null)
            }
            if (filePath.isNullOrEmpty()) {
//            showToast(R.string.msg_video_not_found)
//            finish()
                Handler(Looper.getMainLooper()).postDelayed({
                    showSelectVideoPopup()
                }, 500)
                return
            }
        }
        if (!projectKey.isNullOrEmpty()) {
            buttonDelete.visibility = View.VISIBLE
        }
        videoFile = File(filePath!!)
        val screenSize = ScreenSize(this)
        val lp: RelativeLayout.LayoutParams =
            frameLayout.layoutParams as RelativeLayout.LayoutParams
        lp.height = screenSize.width * 9 / 16
        initPlayerFragment()
        if (!editData.isNullOrEmpty()) {
            showLoadingDialog(false, null)
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                initEditData()
                hideLoadingDialog()
            }, 1000)
        }
        /*Handler(Looper.getMainLooper()).postDelayed(Runnable {
            playerFragment?.play()
        }, 4000)*/
    }

    fun initEditData() {
        try {
            val dataArr = JSONArray(editData)
            for (j in 0 until dataArr.length()) {
                val itemObj = dataArr.getJSONObject(j)
                val keys = itemObj.keys()
                while (keys.hasNext()) {
                    val key = keys.next()
                    val value = itemObj.get(key) as String
                    val itemCmd = ItemCmd(key, value)
                    addCmd(itemCmd)
                    when (key) {
                        Constant.PROJECT_NAME -> {
                            projectName = value
                        }
                        Constant.KEY_TRIM -> {
                            setPlayerTimeRange(value)
                        }
                        Constant.KEY_ADD_LOGO, Constant.KEY_ADD_TEXT, Constant.KEY_ADD_EFFECT -> {
                        }
                        Constant.KEY_ROTATE -> {//pausing
                        }
                        Constant.KEY_TIME_SCALE -> {
                            val param = PlaybackParameters(value.toFloat())
                            playerFragment?.player?.setPlaybackParameters(param)
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun initPlayerFragment() {
        playerFragment = PlayerFragment()
        val bundle = Bundle()
        bundle.putString(Constant.FILE_PATH, filePath)
        playerFragment!!.arguments = bundle
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayout, playerFragment!!)
        fragmentTransaction.commit()
        playerFragment!!.onPlayerStateChangeListener = object : OnPlayerStateChangeListener {
            override fun playerState(isPlaying: Boolean) {
                val icon = if (isPlaying) R.drawable.exo_icon_pause else R.drawable.exo_icon_play
                buttonPause.setImageResource(icon)
                if (isPlaying) {
                    startTimeLineTracking()
                } else {
                    stopTimlineTracking()
                }
            }

            override fun endVideo() {
//                buttonPlay.setImageResource(R.drawable.exo_icon_play)
                //                startPosition = 0;
                stopTimlineTracking()
            }

            override fun onStateReady(player: ExoPlayer) {
                mPlayer = player
                if (player.playWhenReady) {
                    startTimeLineTracking()
                }
            }

            override fun onBuffering() {
            }
        }
    }

    fun initControl() {
        buttonPause.setOnClickListener {
            if (playerFragment == null) {
                return@setOnClickListener
            }
            val player = playerFragment!!.player
            val isPlaying = player!!.playWhenReady
//            val icon = if (isPlaying) R.drawable.exo_icon_play else R.drawable.exo_icon_pause
//            buttonPlay.setImageResource(icon)
            if (isPlaying) {
                playerFragment?.pause()
            } else {
                playerFragment?.play()
            }
        }
        /*buttonPlay.setOnLongClickListener(object : View.OnLongClickListener {
            override fun onLongClick(p0: View?): Boolean {
                playerFragment?.toggleMute()
                return true
            }
        })*/
        buttonBack.setOnClickListener {
            finish()
        }
        buttonDelete.setOnClickListener {
            showDialog(
                true,
                R.string.delete,
                R.string.msg_delete_project,
                R.string.delete,
                R.string.close,
                object : OnDialogButtonListener {
                    override fun onLeftButtonClick() {
                        showLoadingDialog(false, R.string.deleting, null)
                        SharedPreferencesManager.deleteVideo(this@VideoEditorActivity, projectKey!!)
                        hideLoadingDialog()
                        finish()
                    }

                    override fun onRightButtonClick() {
                    }
                })
        }
        buttonSave.setOnClickListener {
            turnOffCrop()
            save()
        }
        buttonSaveDraft.setOnClickListener {
            turnOffCrop()
            saveDraft()
        }
        buttonUpLoad.setOnClickListener {
            turnOffCrop()
            showUploadFragment()
        }
//        buttonShare.setOnClickListener {
//            turnOffCrop()
//            shareVideo()
//        }
        buttonEdited.setOnClickListener {
            openEditedFolder()
        }
        buttonTask.setOnClickListener {
            turnOffCrop()
            showTaskBackground()
        }
        buttonTrim.setOnClickListener {
            turnOffCrop()
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                showButtonPlay(false)
                val fragment = TrimFragment()
                val bundle = Bundle()
                bundle.putString(Constant.FILE_PATH, filePath)
                for (i in 0 until cmdList.size) {
                    val itemCmd = cmdList.get(i)
                    if (itemCmd.key.equals(Constant.KEY_TRIM)) {
                        bundle.putString(Constant.DATA, itemCmd.cmd)
                        break
                    }
                }
                fragment.arguments = bundle
                fragment.onDismissListener = object : OnDismissListener {
                    override fun onDismiss(
                        isSuccess: Boolean,
                        data: String?,
                        list: ArrayList<ItemVideo>?
                    ) {
                        if (!isSuccess || data.isNullOrEmpty()) {
                            return
                        }
                        setPlayerTimeRange(data)
                        playerFragment?.play()
//                        playerFragment?.playLink()
                    }
                }
                showBottomSheetDialog(fragment)
            }, 200)
        }
        buttonAddText.setOnClickListener {
            turnOffCrop()
            resizeStickerView()
            openAddTextFragment()
        }
        buttonAddLogo.setOnClickListener {
            turnOffCrop()
            resizeStickerView()
            openAddLogoFragment()
        }
        buttonAddEffect.setOnClickListener {
            turnOffCrop()
            clearFocusStickerview()
            openAddEffectFragment()
        }
        buttonMerge.setOnClickListener {
            turnOffCrop()
            playerFragment?.pause()
            selectMultipleVideoForMerge()
        }
        buttonRotate.setOnClickListener {
            turnOffCrop()
            playerFragment?.updateRotatePlayerView()
//            stickerView.rotation = playerFragment!!.getRotate().toFloat()
            val cmd = "" + playerFragment?.getRotate()
            addCmd(ItemCmd(Constant.KEY_ROTATE, cmd))
        }
        buttonAddCrop.setOnClickListener {
            openCrop()
            val cmd = "" + playerFragment?.ePlayerView!!.scaleX
            addCmd(ItemCmd(Constant.KEY_CROP, cmd))
        }
        buttonAddMusic.setOnClickListener {
            turnOffCrop()
            playerFragment?.pause()
            isNeedReplace = false
            selectMultipleMusicToAdd()
        }
        buttonTimeScale.setOnClickListener {
            turnOffCrop()
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                showButtonPlay(false)
                val fragment = TimeScaleFragment()
                val bundle = Bundle()
                bundle.putString(Constant.FILE_PATH, filePath)
                fragment.arguments = bundle
                showBottomSheetDialog(fragment)
            }, 200)
        }

        buttonInsertVideo.setOnClickListener {
            turnOffCrop()
            playerFragment?.pause()
            selectVideoToInsert()

        }
    }

    private fun openEditedFolder() {
        turnOffCrop()
        val fragment = DownloadedVideoFragment.newInstance(AppUtils.FOLDER_EDITED, accessToken)
        showBottomSheetDialog(fragment)
    }

    private fun shareVideo() {
        val fragment = ShareFragment()
        val bundle = Bundle()
        bundle.putString(Constant.FILE_PATH, filePath)
        fragment.arguments = bundle
        showBottomSheetDialog(fragment)
    }



    private fun showUploadFragment() {
        if(accessToken.isNullOrEmpty()|| filePath.isNullOrEmpty()){
            return
        }
        val fragment = UploadFragment.newInstance(accessToken!!,filePath!!)
        showBottomSheetDialog(fragment)
    }

    fun setPlayerTimeRange(cmd: String) {
        val trimArr = cmd.split(Constant.KEY_SPLIT_TRIM)
        if (trimArr.size != 2) {
            return
        }
        val startPosition = trimArr[0].toLong()
        val endPosition = trimArr[1].toLong()
        playerFragment?.startPosition = startPosition
        playerFragment?.endPosition = endPosition
//        playerFragment?.player?.seekTo(startPosition)
        playerFragment?.seekTo(startPosition)
//        trimRangeSeekBar.setRangStart(startPosition)
//        trimRangeSeekBar.setRangEnd(endPosition)
//        trimRangeSeekBar.invalidate()
//        playerFragment?.play()
    }

    private fun save() {
        outSize = null
        turnOffCrop()
        val fragment = OutputSizeFragment()
        val bundle = Bundle()
        bundle.putString(Constant.FILE_PATH, filePath)
        fragment.arguments = bundle
        fragment.setlistener(AdapterRcvSize.OnClickItemListener { size, position ->
            outSize = size
            convertVideo()
        })
        showBottomSheetDialog(fragment)
    }

    private fun selectMultipleMusicToAdd() {
//        val i = Intent(Intent.ACTION_GET_CONTENT)
//        i.type = "audio/*"
        val i = Intent(
            Intent.ACTION_PICK,
            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        )
//        i.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("audio/*", "video/*"))
        i.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("audio/*"))
//        i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        startActivityForResult(i, Constant.REQUEST_CODE_GALLERY_SELECT_MUSIC)
    }

    fun selectMultipleVideoForMerge() {
        val i = Intent(
            Intent.ACTION_PICK,
            MediaStore.Video.Media.EXTERNAL_CONTENT_URI
        )
        i.type = "video/*"
        i.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("video/*"))
        i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        startActivityForResult(i, Constant.REQUEST_CODE_GALLERY_MULTIPLE_SELECT_VIDEO)
    }

    fun selectVideoToInsert() {
        val i = Intent(
            Intent.ACTION_PICK,
            MediaStore.Video.Media.EXTERNAL_CONTENT_URI
        )
        i.type = "video/*"
        i.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("video/*"))
//        i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        startActivityForResult(i, Constant.REQUEST_CODE_GALLERY_SELECT_VIDEO_TO_INSERT)
    }

    var isInitCrop = false
    private fun openCrop() {
        playerFragment?.setLockGesture(true)
        val ratio =
            playerFragment?.ePlayerView!!.width.toFloat() / playerFragment?.ePlayerView!!.height.toFloat()
        overlayView.setTargetAspectRatio(ratio)
        overlayView.visibility = View.VISIBLE
        if (!isInitCrop) {
            isInitCrop = true
            overlayView.isFreestyleCropEnabled = false
        }
        val a: TypedArray = obtainStyledAttributes(null, R.styleable.ucrop_UCropView)
        overlayView.processStyledAttributes(a)
        overlayView.setShowCropGrid(true)
        overlayView.setShowCropFrame(true)
    }

    private fun turnOffCrop() {
        playerFragment?.setLockGesture(false)
        overlayView.setShowCropGrid(false)
        overlayView.setShowCropFrame(false)
        overlayView.setDimmedColor(Color.BLACK)
        overlayView.invalidate()
        mFragment?.dismiss()
    }

    private fun clearFocusStickerview() {
        stickerView?.handlingSticker = null
        stickerView?.invalidate()
    }
var currentFragment:BottomSheetDialogFragment? =null
    fun showBottomSheetDialog(fragment: BottomSheetDialogFragment) {
        currentFragment?.dismiss()
        playerFragment?.player?.playWhenReady = false
        fragment.show(supportFragmentManager, fragment.tag)
        currentFragment = fragment
    }

    private fun initStickerView() {
        frameLayoutSticker.layoutParams = frameLayout.layoutParams
        stickerView.setOnStickerOperationListener(object : EventSticker {
            override fun onStickerZoomFinished(sticker: Sticker) {
            }

            override fun onStickerClicked(sticker: Sticker) {
                if (sticker is StickerText) {
                    openAddTextFragment()
                } else if (sticker is StickerDrawable) {
                    openAddLogoFragment()
                }
            }

            override fun onStickerTouchedDown(sticker: Sticker) {

            }

            override fun onStickerDoubleTapped(sticker: Sticker) {

            }

            override fun onStickerDragFinished(sticker: Sticker) {
            }

            override fun onStickerFlipped(sticker: Sticker) {
            }

            override fun onZoomandrotateSticker(sticker: Sticker) {
            }

            override fun onClickOutSticker() {
                mFragment?.dismiss()
            }

            override fun onStickerDeleted(sticker: Sticker) {
                removeRangeSeekBar(sticker)
                mFragment?.dismiss()
            }

            override fun onStickerAdded(sticker: Sticker) {
                addRangeSeekBar(sticker)
            }
        })
    }

    private var mFragment: BottomSheetDialogFragment? = null
    private fun openAddTextFragment() {
        if (mFragment !is AddTextFragment) {
            mFragment?.dismiss()
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                val fragment = AddTextFragment()
                fragment.stickerView = stickerView
                if (stickerView?.currentSticker is StickerText) {
                    fragment.setText((stickerView?.currentSticker!! as StickerText).text)
                }
                mFragment = fragment
                showBottomSheetDialog(fragment)
            }, 0)
        } else {
            if (!mFragment!!.isVisible) {
                showBottomSheetDialog(mFragment!!)
            }
        }
    }

    private fun openAddLogoFragment() {
        if (mFragment !is AddLogoFragment) {
            mFragment?.dismiss()
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                val fragment = AddLogoFragment()
                fragment.stickerView = stickerView
                mFragment = fragment
                showBottomSheetDialog(fragment)
            }, 0)
        } else {
            if (!mFragment!!.isVisible) {
                showBottomSheetDialog(mFragment!!)
            }
        }
    }

    private var addFilterFragment: AddFilterFragment? = null
    private fun openAddEffectFragment() {
        if (!(mFragment is AddFilterFragment)) {
            mFragment?.dismiss()
        }
        if (addFilterFragment == null) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                val fragment = AddFilterFragment()
                mFragment = fragment
                addFilterFragment = fragment
                showBottomSheetDialog(fragment)
            }, 0)
        } else {
            showBottomSheetDialog(addFilterFragment!!)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_CANCELED) return
        if (requestCode == Constant.REQUEST_CODE_GALLERY_MULTIPLE_SELECT_VIDEO || requestCode == Constant.REQUEST_CODE_GALLERY_SELECT_VIDEO_TO_INSERT || requestCode == Constant.REQUEST_CODE_GALLERY_SELECT_MUSIC) {
            if (data == null) {
                return
            }
            handlePickMultipleVideos(data, requestCode)
            return
        }
        val fragments = supportFragmentManager.fragments
        for (i in 0 until fragments.size) {
            if (fragments.get(i) is AddLogoFragment) {
                fragments.get(i).onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    val videoUriList: ArrayList<String> = ArrayList()
    fun handlePickMultipleVideos(data: Intent, requestCode: Int) {
        videoUriList.clear()
        val clipData = data.clipData
        if (clipData != null && clipData.itemCount > 0) {
            for (i in 0 until clipData.itemCount) {
                val videoItem = clipData.getItemAt(i)
                val videoURI = videoItem.uri
                val filePath = OptiCommonMethods.getRealPathFromUri(this, videoURI)
                if (filePath.isNullOrEmpty()) {
                    continue
                }
                Log.e("handlePickMultiple_A_$i", "videoURI = $videoURI")
                Log.e("handlePickMultiple_A_$i", "filePath = $filePath")
                videoUriList.add(filePath)
            }
        } else {
            val videoURI = data.data
            if (videoURI == null) {
                return
            }
            val filePath = OptiCommonMethods.getRealPathFromUri(this, videoURI)
            Log.e("handlePickMultiple_B", "videoURI = $videoURI")
            Log.e("handlePickMultiple_B", "filePath = $filePath")
            if (filePath.isNullOrEmpty()) {
                return
            }
            videoUriList.add(filePath)
        }
        if (videoUriList.size < 1) {
            return
        }
        videoUriList.add(0, filePath!!)
        val screenType =
            if (requestCode == Constant.REQUEST_CODE_GALLERY_MULTIPLE_SELECT_VIDEO) Constant.TYPE_MERGE_VIDEO else if (requestCode == Constant.REQUEST_CODE_GALLERY_SELECT_MUSIC) Constant.TYPE_REPLACE_MUSIC else Constant.TYPE_INSERT_VIDEO

        if (requestCode != Constant.REQUEST_CODE_GALLERY_MULTIPLE_SELECT_VIDEO) {
            showTrimVideoInsertFragment(videoUriList.get(1), screenType, 0, 0)
            return
        }
        Handler(Looper.getMainLooper()).postDelayed(
            Runnable
            {
                val fragment = MergeFragment()
                val bundle = Bundle()
                bundle.putInt(Constant.SCREEN_TYPE, screenType)
                bundle.putStringArrayList(Constant.DATA, videoUriList)
                fragment.arguments = bundle
                /*fragment.onDismissListener = object : OnDismissListener {
                    override fun onDismiss(
                        isSuccess: Boolean,
                        data: String?,
                        list: ArrayList<ItemVideo>?
                    ) {
//                    Loggers.e("onSelected_$isSuccess", "size = ${list?.size}")
                        if (!isSuccess) {
                            return
                        }
                        when (screenType) {
                            Constant.TYPE_MERGE_VIDEO -> {
                                videoUriList.clear()
                                videoOutPath = data
                                refreshGallery(videoOutPath!!)
                                val uri = Uri.parse(videoOutPath)
                                playerFragment?.fileUri = uri
                                playerFragment?.playLink()
                            }

                        }
                    }
                }*/
                showBottomSheetDialog(fragment)
            }, 200
        )
    }

    private fun showTrimVideoInsertFragment(
        filePath: String,
        screenType: Int,
        startPosition: Long,
        endPosition: Long
    ) {
        val fragment = TrimAudioFragment()
        val bundle = Bundle()
        bundle.putString(Constant.FILE_PATH, filePath)
        bundle.putInt(Constant.SCREEN_TYPE, screenType)
        bundle.putLong(Constant.START, startPosition)
        bundle.putLong(Constant.END, endPosition)
        fragment.arguments = bundle
        fragment.onDismissListener = object : TrimAudioFragment.OnDismissListener {
            override fun onDismiss(
                isSuccess: Boolean,
                startPostition: Long,
                endPosition: Long, duration: Long,
                filePath: String?
            ) {
                if (!isSuccess) {
                    return
                }
                if (filePath.isNullOrEmpty()) {
                    videoUriList.clear()
                    return
                }
                //                                var itemObj: ItemVideoTimeLine
                val itemObj = ItemVideoTimeLine(
                    "${Calendar.getInstance().timeInMillis}",
                    filePath,
                    0,//milisecond
                    endPosition - startPostition //milisecond
                )
                if (itemObj.endPosition > mDuration) {
                    itemObj.endPosition = mDuration
                }
                itemObj.duration = duration
                itemObj.originStartPosition = startPostition
                itemObj.originEndPosition = endPosition
                val originMaxRange =
                    itemObj.duration - itemObj.originStartPosition
                val maxRange =
                    if (originMaxRange > mDuration) mDuration else originMaxRange
                itemObj.maxRange = maxRange

                if (isNeedReplace && rangeSeekBarViewNeedReplace != null && objNeedReplace is ItemVideoTimeLine) {
                    itemObj.key = (objNeedReplace as ItemVideoTimeLine).key
                    itemObj.type = (objNeedReplace as ItemVideoTimeLine).type
                    itemObj.startPosition = rangeSeekBarViewNeedReplace!!.selectedMinValue
                    itemObj.endPosition =
                        itemObj.endPosition + rangeSeekBarViewNeedReplace!!.selectedMinValue
                    if (itemObj.endPosition > mDuration) {
                        itemObj.endPosition = mDuration
                    }
                    playerFragment?.updateItemVideoTimeLine(itemObj)
                    rangeSeekBarViewNeedReplace?.obj = itemObj

//                    rangeSeekBarViewNeedReplace!!.maxShootTime = maxRange
//                    rangeSeekBarViewNeedReplace!!.setRangStart(0)
//                    rangeSeekBarViewNeedReplace!!.setRangEnd(mDuration)
//                            rangeSeekBarViewNeedReplace!!.setSelectedMinValue(0)
                    rangeSeekBarViewNeedReplace!!.setSelectedMaxValue(
                        itemObj.endPosition
                    )
                    rangeSeekBarViewNeedReplace!!.setStartEndTime(
                        rangeSeekBarViewNeedReplace!!.selectedMinValue, itemObj.endPosition
                    )
                    rangeSeekBarViewNeedReplace!!.setNotifyWhileDragging(true)
//                    rangeSeekBarViewNeedReplace!!.notifyRangeSeek()
                } else {
                    when (screenType) {
                        Constant.TYPE_REPLACE_MUSIC -> {
                            itemObj.type = Constant.TYPE_AUDIO
                        }
                        Constant.TYPE_INSERT_VIDEO -> {
                            itemObj.type = Constant.TYPE_VIDEO
                        }
                    }
                    playerFragment?.addItemVideoTimeLine(itemObj)
                    addRangeSeekBar(itemObj)
                }
                videoUriList.clear()
                playerFragment?.playLink()
//                playerFragment?.play()
            }
        }
        showBottomSheetDialog(fragment)
    }

    /*showDialog(
        true,
        getString(R.string.notification),
        getString(R.string.msg_need_merge_videos),
        getString(R.string.close),
        getString(R.string.yes),
        object : OnDialogButtonListener {
            override fun onLeftButtonClick() {
                videoUriList.clear()
            }

            override fun onRightButtonClick() {
                mergeVideos()
            }
        })*/
    var outSize: Size? = null

    private var mp4Composer: Mp4Composer? = null
    private var videoOutPath: String? = null
    private fun convertVideo() {
        playerFragment?.pause()
//        if (cmdList.size == 0) {
//            showToast(R.string.msg_video_not_edit)
//            return
//        }
        if (outSize == null) {
            outSize = VideoEditUtils.getVideoResolution(filePath)
        }

//        videoOutPath = OptiCommonMethods.getVideoFilePath(this)
        videoOutPath = AppUtils.createVideoFileEditedPath(this)

        listFilter = null
        trimStartVideo = 0
        trimEndVideo = Long.MAX_VALUE
        var dataSource: DataSource? = null
        dataSource = FilePathDataSource(filePath!!)
//        convertByMp4()
        val builder = Transcoder.into(videoOutPath!!)
//            .setVideoTrackStrategy(DefaultVideoStrategy.exact(outSize!!.width, outSize!!.height).build())
            .setVideoTrackStrategy(
                DefaultVideoStrategy.fraction(
                    outSize!!.width.toFloat() / (VideoEditUtils.getVideoResolution(
                        filePath
                    ).width.toFloat())
                ).build()
            )
        if ((stickerView.stickerCount > 0) || (glFilterDefault != null && !glFilterDefault!!.javaClass.simpleName.equals(
                GlFilter::class.java.simpleName
            ))
        ) {
            getFilter()
            listFilter?.let {
                builder.setListFilter(it)
            }
        }
        for (i in 0 until cmdList.size) {
            val item = cmdList.get(i)
            when (item.key) {
                Constant.KEY_TRIM -> {
                    val array = item.cmd.split(Constant.KEY_SPLIT_TRIM)
                    trimStartVideo = array.get(0).trim().toLong()
                    trimEndVideo = array.get(1).trim().toLong()
                    dataSource =
                        ClipDataSource(
                            dataSource!!,
                            array.get(0).trim().toLong() * 1000,
                            array.get(1).trim().toLong() * 1000
                        )
                }
//                Constant.KEY_ADD_LOGO, Constant.KEY_ADD_TEXT, Constant.KEY_ADD_EFFECT -> {
//                    if (listFilter == null) {
//                        getFilter()
//                        listFilter?.let {
//                            builder.setListFilter(it)
//                        }
//                    } else {
//                        continue
//                    }
//                }
                Constant.KEY_ROTATE -> {//pausing
                    if (playerFragment?.getRotate() != 0) {
//                        val rotate = Rotation.fromInt(playerFragment?.getRotate()!!)
//                        mp4Composer?.rotation(rotate)
                    }
                }
                Constant.KEY_TIME_SCALE -> {
                    builder.setSpeed(item.cmd.toFloat())
                }
                Constant.KEY_CROP -> {
                    builder.setFillMode(FillMode.CUSTOM)
                        .setFillModeCustomItem(getFillModeCrop())
                }
            }
        }
        addDataSource(builder)
        startConvertInThread(videoOutPath, builder)
    }

    fun startConvertInThread(
        videoOutPath: String?,
        builder: TranscoderOptions.Builder
    ) {
        val id = ConvertManager.getInstance()!!.getUniqueId(videoOutPath!!)
        val item = ItemConvertVideo(id, videoOutPath)
        item.name = FileUltis.getVideoNameFileUrlOrPath(videoOutPath)
        item.title = FileUltis.getVideoNameFileUrlOrPath(videoOutPath)
        val imageFile = FileUltis.getThumbnailFile(this, filePath)
        item.thumbnail = imageFile.absolutePath
        ConvertManager.getInstance()?.putItemConvertVideo(item)
        startExitService()
        builder.setListener(object : TranscoderListener {
            override fun onTranscodeCompleted(successCode: Int) {
                ConvertManager.getInstance()
                    ?.getItemConvertVideo(id)?.listener?.onTranscodeCompleted(successCode)
                AppUtils.addToGalleryRunBackground(
                    this@VideoEditorActivity,
                    videoOutPath,
                    AppUtils.FOLDER_EDITED
                )
                if(!isDestroyed && !isFinishing){
                    openEditedFolder()
//                    filePath = videoOutPath
//                    resetUI()
//                    init()
                }
            }

            override fun onTranscodeProgress(progress: Double) {
                ConvertManager.getInstance()
                    ?.getItemConvertVideo(id)?.listener?.onTranscodeProgress(progress)
            }

            override fun onTranscodeCanceled() {
                val file = File(videoOutPath)
                if (file.exists()) {
                    file.delete()
                }
                ConvertManager.getInstance()
                    ?.getItemConvertVideo(id)?.listener?.onTranscodeCanceled()
            }

            override fun onTranscodeFailed(exception: Throwable) {
                val file = File(videoOutPath)
                if (file.exists()) {
                    file.delete()
                }
                ConvertManager.getInstance()
                    ?.getItemConvertVideo(id)?.listener?.onTranscodeFailed(
                        exception
                    )
            }
        })

        val trancodeTask = builder.transcode()
        item.task = trancodeTask
        showTaskBackground()
    }

//    private fun convertByMp4() {
//        mp4Composer = Mp4Composer(filePath!!, videoPath!!)
//            .size(outSize!!.width, outSize!!.height)
//            .fillMode(FillMode.CUSTOM)
//            .customFillMode(getFillModeCrop())
//            .listener(mp4ComposerListener)
//        for (i in 0 until cmdList.size) {
//            val item = cmdList.get(i)
//            when (item.key) {
//                Constant.KEY_TRIM -> {
//                    val array = item.cmd.split(Constant.KEY_SPLIT_TRIM)
//                    mp4Composer?.trim(array.get(0).trim().toLong(), array.get(1).trim().toLong())
//                }
//                Constant.KEY_ADD_LOGO, Constant.KEY_ADD_TEXT, Constant.KEY_ADD_EFFECT -> {
//                    if (listFilter == null) {
//                        getFilter()
//                        listFilter?.let {
//                            mp4Composer?.filters(it)
//                        }
//                    } else {
//                        continue
//                    }
//                }
//                //                Constant.KEY_ROTATE -> {
//                //                    if (playerFragment?.getRotate() != 0) {
//                //                        val rotate = Rotation.fromInt(playerFragment?.getRotate()!!)
//                //                        mp4Composer?.rotation(rotate)
//                //                    }
//                Constant.KEY_ROTATE -> {//pausing
//                    if (playerFragment?.getRotate() != 0) {
//                        //                        val rotate = Rotation.fromInt(playerFragment?.getRotate()!!)
//                        //                        mp4Composer?.rotation(rotate)
//                    }
//                }
//                Constant.KEY_TIME_SCALE -> {
//                    mp4Composer?.timeScale(item.cmd.toFloat())
//                }
//            }
//        }
//
//        playerFragment?.pause()
//        showLoadingDialog(false, R.string.saving, null)
//        mp4Composer?.start()
//    }

//    private val mp4ComposerListener = object : Mp4Composer.Listener {
//        override fun onProgress(progress: Double) {
//            runOnUiThread {
//                setTextLoadingDialog((progress * 100).toInt().toString() + " %")
//            }
//        }
//
//        override fun onCompleted() {
//            Log.e("onTranscodeCompleted", "onCompleted() " + videoPath)
//            runOnUiThread {
//                cmdList.clear()
//                hideLoadingDialog()
//                filePath = videoPath
//                refreshGallery(videoPath!!)
//                isResize = false
//                val uri = Uri.parse(videoPath)
//                playerFragment!!.fileUri = uri
//                playerFragment!!.playLink()
//                glFilterDefault = null
//                resetUI()
//            }
//        }
//
//        override fun onCanceled() {
//            hideLoadingDialog()
//        }
//
//        override fun onFailed(exception: Exception) {
//            exception.printStackTrace()
//            runOnUiThread {
////                        progressBar.visibility = View.GONE
//                hideLoadingDialog()
//            }
//            Log.e("onTranscodeCompleted", "onFailed() _1 " + exception.cause)
//            Log.e("onTranscodeCompleted", "onFailed() _2 " + exception.localizedMessage)
//            Log.e("onTranscodeCompleted", "onFailed() _3" + exception.message)
//        }
//    }

    fun refreshGallery(filePath: String) {
        FileUltis.refreshGalery(applicationContext, filePath)
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//            val values = ContentValues(2)
//            values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4")
//            values.put(MediaStore.Video.Media.DATA, filePath)
//            contentResolver.insert(
//                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
//                values
//            )
//        } else {
//            val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
//            val f = File(filePath)
//            val contentUri = Uri.fromFile(f)
//            mediaScanIntent.data = contentUri
//            sendBroadcast(mediaScanIntent)
//        }
    }

    private var listFilter: ArrayList<GlFilter>? = null
    private var glFilter: GlFilter? = null
    var glFilterPlayer: GlFilter? = null
    var glFilterDefault: GlFilter? = null
    private fun getFilter() {
        stickerView.visibility = View.INVISIBLE
        listFilter = ArrayList()
        var glFilterOverlay: GlFilter? = null
        val listPointTrim = java.util.ArrayList<Long>(getPointsTrim())

        for (i in 0 until listPointTrim.size - 1) {
            var timeStart = listPointTrim.get(i)
            var timeEnd = listPointTrim.get(i + 1)
            val timeAverage = timeStart + ((timeEnd - timeStart) / 2)
            if (stickerView.stickerCount > 0) {
                stickerView.showAtTime(timeAverage)
                val bitmap = if (outSize == null) stickerView.createBitmapfromStickerView() else
                    stickerView.createBitmapfromStickerView(outSize!!.width, outSize!!.height)
                glFilterOverlay = GlBitmapOverlaySampleFilter(bitmap)
                glFilter =
                    if (glFilterPlayer == null || !glFilterPlayer!!.isShowFilterAtTime(
                            timeAverage
                        )
                    ) glFilterOverlay else GlFilterGroup(
                        glFilterDefault,
                        glFilterOverlay
                    )
            } else {
                glFilter =
                    if (glFilterPlayer == null || !glFilterPlayer!!.isShowFilterAtTime(
                            timeAverage
                        )
                    ) GlFilter() else glFilterDefault
            }
            timeStart = timeStart - trimStartVideo
            timeEnd = timeEnd - trimStartVideo
            glFilter?.setTrimRangeMs(timeStart, timeEnd)
            listFilter?.add(glFilter!!)
        }
    }

    fun setFilterPlayerDefault() {
        setFilterPlayer(glFilterPlayer)
    }

    fun setFilterPlayer(glFilter: GlFilter?) {
        playerFragment?.setFilter(glFilter)
    }

    private var isResize = false
    fun resizeStickerView() {
        if (!isResize) {
            isResize = true
            val ePlayerView = playerFragment?.ePlayerView
            val layoutParam = stickerView.layoutParams
            layoutParam.width = ePlayerView!!.measuredWidth
            layoutParam.height = ePlayerView.measuredHeight
            stickerView.layoutParams = layoutParam
        }
    }

    private fun getFillModeCrop(): FillModeCustomItem {
        val resolution: Size = VideoEditUtils.getVideoResolution(filePath)
        val ePlayerView = playerFragment!!.ePlayerView
        Log.e(
            "CROP",
            "" + ePlayerView!!.getScaleX() + "/" + ePlayerView.getRotation() + "/" + ePlayerView.getTranslationX() + "/" + ePlayerView.getTranslationY() + "/" + resolution.width + "/" + resolution.height
        )
        val fillModeCustomItem = FillModeCustomItem(
            ePlayerView.getScaleX(),
            ePlayerView.getRotation(),
            (ePlayerView.getTranslationX() / ePlayerView.width * 2f),
            (ePlayerView.getTranslationY() / ePlayerView.height * 2f),
            resolution.width.toFloat(),
            resolution.height.toFloat()
        )
        return fillModeCustomItem
    }

    fun showButtonPlay(isShow: Boolean) {
        buttonPause.visibility = if (isShow) View.VISIBLE else View.INVISIBLE
    }

    fun saveDraft() {
        if (cmdList.size == 0) {
            showDialog(getString(R.string.msg_video_not_edit))
            return
        }

        val dataArray = JSONArray()
        for (i in 0 until cmdList.size) {
            val itemCmd = cmdList.get(i)
            val itemObj = JSONObject()
            var value = ""
            when (itemCmd.key) {
                Constant.KEY_TRIM -> {
                    value = itemCmd.cmd
                }
                Constant.KEY_ADD_LOGO, Constant.KEY_ADD_TEXT, Constant.KEY_ADD_EFFECT -> {
                    value = "logo, text, effect"
                }
                Constant.KEY_ROTATE -> {
                    value = "${playerFragment!!.getRotate()}"
                }
                Constant.KEY_TIME_SCALE -> {
                    value = itemCmd.cmd
                }
            }
            if (!value.isEmpty()) {
                itemObj.put(itemCmd.key, value)
                dataArray.put(itemObj)
            }
        }
        if (dataArray.length() == 0) {
            showDialog(getString(R.string.msg_video_not_edit))
            return
        }
        val itemObj = JSONObject()
        itemObj.put(Constant.FILE_PATH, filePath)
        dataArray.put(itemObj)

        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            showButtonPlay(false)
            val fragment = SaveProjectFragment()
            if (!projectName.isNullOrEmpty()) {
                val bundle = Bundle()
                bundle.putString(Constant.PROJECT_NAME, projectName)
                fragment.arguments = bundle
            }
            showBottomSheetDialog(fragment)
            fragment.onDismissListener = object : OnDismissListener {
                override fun onDismiss(
                    isSuccess: Boolean,
                    data: String?,
                    list: ArrayList<ItemVideo>?
                ) {
                    if (isSuccess) {
                        val itemObjName = JSONObject()
                        itemObjName.put(Constant.PROJECT_NAME, data)
                        dataArray.put(itemObjName)
                        Loggers.e("saveDraft", dataArray.toString())
                        val key =
                            if (projectKey.isNullOrEmpty()) "${Calendar.getInstance().timeInMillis}" else projectKey
                        val editData = EncryptUtil.base64Encode(dataArray.toString())
                        SharedPreferencesManager.saveVideo(
                            this@VideoEditorActivity,
                            key!!,
                            editData
                        )
                        finish()
                    }
                }
            }
        }, 200)
    }

    override fun onDestroy() {
        mp4Composer?.cancel()
        super.onDestroy()
    }

    var mDuration: Long = 0
    var mThumbsTotalCount: Int = 0

    private val TIME_DELAY_MS = 50L
    private var mHandler: Handler? = null
    private fun startTimeLineTracking() {
        if (mHandler == null) {
            mHandler = Handler(Looper.getMainLooper())
        }
        mHandler!!.postDelayed(trackingProgress, TIME_DELAY_MS)
    }

    private fun stopTimlineTracking() {
        if (mHandler != null) {
            mHandler!!.removeCallbacks(trackingProgress)
        }
    }

    private val trackingProgress: Runnable = object : Runnable {
        override fun run() {
            if (mPlayer == null) {
                return
            }
            Loggers.e("TRACKINGTIMELINE", "pos: " + playerFragment!!.getCurrentRealPosition()!!)
            stickerView.showAtTime(playerFragment!!.getCurrentRealPosition()!!)
            if (!isDragTimeline) {
                scrollViewTimeLine.smoothScrollTo(
                    (playerFragment!!.getCurrentRealPosition()!! * aMiliSecondToPx).toInt(),
                    0
                )
            }
            if (addFilterFragment == null || !addFilterFragment!!.isVisible) {
                if (glFilterPlayer != null && glFilterPlayer!!.isShowFilterAtTime(playerFragment!!.getCurrentRealPosition()!!)) {
                    setFilterPlayerDefault()
                } else {
                    setFilterPlayer(GlFilter())
                }
            }
            if (mHandler == null) {
                mHandler = Handler(Looper.getMainLooper())
            }
            mHandler!!.postDelayed(this, TIME_DELAY_MS)
        }
    }

    /*private val trackingProgress: Runnable = object : Runnable {
        override fun run() {
            if (mPlayer == null) {
                return
            }
            stickerView.showAtTime(mPlayer!!.currentPosition)
            if (!isDragTimeline) {
                scrollViewTimeLine.smoothScrollTo(
                    (mPlayer!!.currentPosition * aMiliSecondToPx).toInt(),
                    0
                )
            }
            if (addFilterFragment == null || !addFilterFragment!!.isVisible) {
                if (glFilterPlayer != null && glFilterPlayer!!.isShowFilterAtTime(mPlayer!!.currentPosition)) {
                    setFilterPlayerDefault()
                } else {
                    setFilterPlayer(GlFilter())
                }
            }
            if (mHandler == null) {
                mHandler = Handler(Looper.getMainLooper())
            }
            mHandler!!.postDelayed(this, TIME_DELAY_MS)
        }
    }*/
    var aMiliSecondToPx = 0F
    lateinit var adapter: FrameTimeLineAdapter
    var itemList: ArrayList<ItemFrame> = ArrayList()

    private fun initTimeLine() {
        initTimeLineView()
        initFrameAdapter()
        initRangeSeekBar(trimRangeSeekBar, null)
        trimRangeSeekBar.setTouchDown(true)
        initScrollViewTimeLine()
    }

    fun initFrameAdapter() {
        Loggers.e("initFrameAdapter", "filePath = $filePath")
        val retriever = MediaMetadataRetriever()
        val videoFile = File(filePath)
        retriever.setDataSource(this, Uri.fromFile(videoFile))
        mDuration = OptiCommonMethods.getVideoDuration(this, videoFile!!)
        val style = Constant.STYLE_HORIZONTAL
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.setHasFixedSize(true)
        mThumbsTotalCount = VideoEditUtils.getThumbTotalCount(mDuration)
        aMiliSecondToPx = VideoEditUtils.getPixelAMilisSecond(mDuration)

        //initTrimRangeSeekbar
        val widthRangeSeekbar =
            mThumbsTotalCount * UnitConverter.dpToPx(Constant.WIDTH_FRAME_DP)
        trimRangeSeekBar!!.layoutParams?.width = widthRangeSeekbar
        //initTrimRangeSeekbar
        adapter = FrameTimeLineAdapter(this, itemList, style, Constant.WIDTH_FRAME_DP)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
        val mSourceUri = Uri.fromFile(videoFile)
        itemList.clear()
        startShootVideoThumbs(this, mSourceUri, mThumbsTotalCount, 0, mDuration)
    }

    private fun initTimeLineView() {
        val leftPadding = ScreenSize(this).width / 2
        layoutTimeLine.setPadding(
            leftPadding,
            layoutTimeLine.paddingTop,
            leftPadding,
            layoutTimeLine.paddingBottom
        )
    }

    private fun startShootVideoThumbs(
        context: Context, videoUri: Uri,
        totalThumbsCount: Int, startPosition: Long, endPosition: Long
    ) {
        VideoEditUtils.shootVideoThumbInBackground(
            context,
            videoUri,
            totalThumbsCount,
            startPosition,
            endPosition,
            object : SingleCallback<Bitmap, Int> {
                override fun onSingleCallback(bitmap: Bitmap, interval: Int) {
                    UiThreadExecutor.runTask("", {
//                    mVideoThumbAdapter.addBitmaps(bitmap)
                        val position = itemList.size
                        itemList.add(ItemFrame(position, bitmap))
//                    adapter.notifyItemChanged(position)
                        adapter.notifyDataSetChanged()
                    }, 0L)
                }
            })
    }

    private var isDragTimeline = false;
    private fun initScrollViewTimeLine() {
        scrollViewTimeLine.setOnMyScrollListener { isUser, view, scrollX, scrollY, oldScrollX, oldScrollY ->
            val position = ((scrollX.toFloat() / aMiliSecondToPx)).toLong()
            tvCurrentTime.setText(VideoEditUtils.convertSecondsToTime(position / 1000))
            isDragTimeline = isUser
            if (isUser) {
                playerFragment?.seekTo(position)
            }
        }
        addScrollListener()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun addScrollListener() {
        scrollViewTimeLine.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_UP || event.action == MotionEvent.ACTION_CANCEL) {
                isDragTimeline = false
            }
            return@setOnTouchListener scrollViewTimeLine.onTouchEvent(event)
        }
    }

    fun addRangeSeekBar(obj: Any) {
        if (obj is GlFilter && isExitTimeLineBarFilter(obj)) {
            return
        }
        val mRangeSeekBarView = RangeSeekBarView(this, 0, mDuration)
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            UnitConverter.dpToPx(50)
        )
        mRangeSeekBarView.layoutParams = params
        layoutRangeSeekBar.addView(mRangeSeekBarView)
        initRangeSeekBar(mRangeSeekBarView, obj)
    }

    fun isExitTimeLineBarFilter(glFilter: GlFilter?): Boolean {
        for (i in 0 until listRangeSeekBarView.size) {
            val view = listRangeSeekBarView.get(i)
            if (view.obj is GlFilter) {
                glFilter?.setTrimRangeMs(view.selectedMinValue, view.selectedMaxValue)
                return true
            }
        }
        return false
    }

    var currentPositionActionDown = 0L
    var currentMinPositionRangeActionDown = 0L
    var currentMaxPositionRangeActionDown = 0L

    private fun initRangeSeekBar(rangeSeekBarView: RangeSeekBarView, obj: Any?) {
        var maxRange = mDuration
        var maxValue: Long = mDuration
        if (obj is StickerDrawable) {
            rangeSeekBarView.backgroundRangebar = Color.RED
            rangeSeekBarView.title = "Logo"
        } else if (obj is StickerText) {
            rangeSeekBarView.backgroundRangebar = Color.DKGRAY
            rangeSeekBarView.title = obj.text
        } else if (obj is GlFilter) {
            rangeSeekBarView.backgroundRangebar = Color.CYAN
            rangeSeekBarView.title = "Hiệu ứng"
        } else if (obj is ItemVideoTimeLine) {
            if (obj.type == Constant.TYPE_AUDIO) {
                rangeSeekBarView.backgroundRangebar = Color.parseColor("#0f3073")
                rangeSeekBarView.title = "Audio"
            } else {
                rangeSeekBarView.backgroundRangebar = Color.parseColor("#091d47")
                rangeSeekBarView.title = "Video"
            }
            val originMaxRange = obj.duration - obj.originStartPosition
            maxRange = if (originMaxRange > mDuration) mDuration else originMaxRange
//            Loggers.e("Seekbar", "mDuration = $mDuration, endPosition = ${obj.endPosition}, maxRange = $maxRange")
//            rangeSeekBarView.maxShootTime = maxRange
            maxValue = obj.endPosition
        }
        listRangeSeekBarView.add(rangeSeekBarView)
        rangeSeekBarView.obj = obj
        rangeSeekBarView.setRangStart(0)
        rangeSeekBarView.setRangEnd(mDuration)
//        mRangeSeekBarView.setMinShootTime(VideoEditUtils.MIN_SHOOT_DURATION)
        rangeSeekBarView.setSelectedMinValue(0)
        rangeSeekBarView.setSelectedMaxValue(maxValue)
        rangeSeekBarView.setStartEndTime(0, maxValue)
        rangeSeekBarView.setNotifyWhileDragging(true)
//        rangeSeekBarView.notifyRangeSeek()
        rangeSeekBarView.setOnRangeSeekBarChangeListener(object :
            RangeSeekBarView.OnRangeSeekBarChangeListener {
            override fun onRangeSeekBarValuesChanged(
                bar: RangeSeekBarView?,
                minValue: Long,
                maxValue: Long,
                action: Int,
                isMin: Boolean,
                pressedThumb: RangeSeekBarView.Thumb?
            ) {
                val mLeftProgressPos = minValue
                val mRightProgressPos = maxValue
                rangeSeekBarView.setStartEndTime(mLeftProgressPos, mRightProgressPos)
                if (rangeSeekBarView.obj is Sticker) {
                    (rangeSeekBarView.obj as Sticker).setTrimMsRange(
                        mLeftProgressPos,
                        mRightProgressPos
                    )
                    handleRangeSeekBarOnClicked(rangeSeekBarView, action)
                } else if (rangeSeekBarView.obj is GlFilter) {
                    (rangeSeekBarView.obj as GlFilter).setTrimRangeMs(
                        mLeftProgressPos,
                        mRightProgressPos
                    )
                    handleRangeSeekBarOnClicked(rangeSeekBarView, action)
                } else if (rangeSeekBarView.obj is ItemVideoTimeLine) {
                    handleRangeSeekBarOnClicked(rangeSeekBarView, action)
                    var minSelected = rangeSeekBarView.selectedMinValue
                    var maxSelected = rangeSeekBarView.selectedMaxValue
                    if (currentMinPositionRangeActionDown == minSelected && currentMaxPositionRangeActionDown == maxSelected) {
                        return
                    }
                    val itemVideoTimeLine = rangeSeekBarView.obj as ItemVideoTimeLine

                    if (pressedThumb == RangeSeekBarView.Thumb.MIN && ((maxSelected - minSelected) > itemVideoTimeLine.maxRange)) {
                        maxSelected = minSelected + itemVideoTimeLine.maxRange

                    }
                    if (pressedThumb == RangeSeekBarView.Thumb.MAX && ((maxSelected - minSelected) > itemVideoTimeLine.maxRange)) {
                        minSelected = maxSelected - itemVideoTimeLine.maxRange
                    }
                    rangeSeekBarView.selectedMinValue = minSelected
                    rangeSeekBarView.selectedMaxValue = maxSelected
                    rangeSeekBarView.setStartEndTime(minSelected, maxSelected)

                    val currentPos = playerFragment?.getCurrentRealPosition()
                    playerFragment?.saveAndStartCurPos()
                    when (action) {
                        MotionEvent.ACTION_UP -> {
//                            val currentPos = playerFragment?.player?.currentPosition
                            if (currentPos == null) {
                                return
                            }
                            /*if (currentPositionActionDown == currentPos) {
                                return
                            }*/
//                            var minSelected = rangeSeekBarView.selectedMinValue
//                            var maxSelected = rangeSeekBarView.selectedMaxValue
//                            if (currentMinPositionRangeActionDown == minSelected && currentMaxPositionRangeActionDown == maxSelected) {
//                                return
//                            }
//                            val itemVideoTimeLine = rangeSeekBarView.obj as ItemVideoTimeLine
//
//                            if(pressedThumb == RangeSeekBarView.Thumb.MIN && ((maxSelected - minSelected)> itemVideoTimeLine.maxRange)){
//                                maxSelected = minSelected +  itemVideoTimeLine.maxRange
//
//                            }
//                            if(pressedThumb == RangeSeekBarView.Thumb.MAX && ((maxSelected - minSelected)> itemVideoTimeLine.maxRange)){
//                                minSelected = maxSelected - itemVideoTimeLine.maxRange
//                            }
//                            rangeSeekBarView.selectedMinValue = minSelected
//                            rangeSeekBarView.selectedMaxValue = maxSelected
//                            rangeSeekBarView.setStartEndTime(minSelected, maxSelected)
                            itemVideoTimeLine.startPosition = minValue
                            itemVideoTimeLine.endPosition = maxValue
                            playerFragment?.updateItemVideoTimeLine(itemVideoTimeLine)
                            stopTimlineTracking()
                            playerFragment?.playLink()
//                            Handler(Looper.getMainLooper()).postDelayed({
//                            playerFragment?.seekTo(currentPos)},100)
//                            playerFragment?.play()
                        }
                    }
                }
            }
        })
    }

    fun handleRangeSeekBarOnClicked(rangeSeekBarView: RangeSeekBarView, action: Int) {
        when (action) {
            MotionEvent.ACTION_DOWN -> {
                currentPositionActionDown = playerFragment?.player?.currentPosition!!
                currentMinPositionRangeActionDown = rangeSeekBarView.selectedMinValue
                currentMaxPositionRangeActionDown = rangeSeekBarView.selectedMaxValue
            }
            MotionEvent.ACTION_UP -> {
                val currentPos = playerFragment?.player?.currentPosition
                if (currentPos == null) {
                    return
                }
                val minSelected = rangeSeekBarView.selectedMinValue
                val maxSelected = rangeSeekBarView.selectedMaxValue
                if (currentMinPositionRangeActionDown == minSelected && currentMaxPositionRangeActionDown == maxSelected) {
                    showPopupMenuSeekbarTimeLine(rangeSeekBarView)
                    return
                }
            }
        }
    }

    private var listRangeSeekBarView: ArrayList<RangeSeekBarView> = ArrayList()
    private fun removeRangeSeekBar(obj: Any) {
        for (i in 0 until listRangeSeekBarView.size) {
            val view = listRangeSeekBarView.get(i)
            if (obj == view.obj) {
                layoutRangeSeekBar.removeView(view)
                listRangeSeekBarView.removeAt(i)
                return
            }
        }
    }

    private fun removeAllRangeSeekBar() {
        for (i in 0 until listRangeSeekBarView.size) {
            val view = listRangeSeekBarView.get(i)
            if (view != trimRangeSeekBar) {
                layoutRangeSeekBar.removeView(view)
            }
        }
        listRangeSeekBarView.clear()
    }

    private fun getPointsTrim(): TreeSet<Long>? {
        val treeSetPoint = TreeSet<Long>()
        for (i in 0 until listRangeSeekBarView.size) {
            val obj = listRangeSeekBarView.get(i).obj
            if (obj is Sticker || obj == null || obj is GlFilter) {
                val item = listRangeSeekBarView.get(i)
                treeSetPoint.add(item.getSelectedMinValue())
                treeSetPoint.add(item.getSelectedMaxValue())
            }
        }
        return treeSetPoint
    }

    private fun addDataSource(builder: TranscoderOptions.Builder) {
        val treeSetPoint = TreeSet<Long>()
        for (i in 0 until listRangeSeekBarView.size) {
            val obj = listRangeSeekBarView.get(i).obj
            if (obj is ItemVideoTimeLine || obj == null) {
                val item = listRangeSeekBarView.get(i)
                if (item.getSelectedMinValue() >= trimStartVideo) {
                    treeSetPoint.add(item.getSelectedMinValue())
                }
                if (item.getSelectedMaxValue() <= trimEndVideo) {
                    treeSetPoint.add(item.getSelectedMaxValue())
                }
            }
        }

        val listPointTrim = java.util.ArrayList<Long>(treeSetPoint)
        for (j in 0 until listPointTrim.size - 1) {
            var isAddVideo = false
            var isAddAudio = false
            for (k in listRangeSeekBarView.size - 1 downTo 0) {
                if (isAddAudio && isAddVideo) {
                    break
                }
                val obj = listRangeSeekBarView.get(k).obj
                if ((obj is ItemVideoTimeLine) && (obj.type == Constant.TYPE_AUDIO) && !isAddAudio && (listPointTrim.get(
                        j
                    ) >= obj.startPosition) && ((listPointTrim.get(
                        j + 1
                    ) <= obj.endPosition))
                ) {
                    isAddAudio = true
                    val audioSource = FilePathDataSource(obj.filePath)
                    val clipAudioSource = ClipDataSource(
                        audioSource,
                        (obj.originStartPosition) * 1000,
                        (Math.min(
                            ((listPointTrim.get(j + 1) - listPointTrim.get(j)) + obj.originStartPosition),
                            (obj.duration - obj.originStartPosition)
                        )) * 1000
                    )
                    builder.addDataSource(TrackType.AUDIO, clipAudioSource)
                }
                if ((obj is ItemVideoTimeLine) && obj.type == Constant.TYPE_VIDEO && !isAddVideo && (listPointTrim.get(
                        j
                    ) >= obj.startPosition) && (listPointTrim.get(
                        j + 1
                    ) <= obj.endPosition)
                ) {
                    isAddVideo = true
                    val videoSource = FilePathDataSource(obj.filePath)
                    val clipVideoSource = ClipDataSource(
                        videoSource,
                        (obj.originStartPosition) * 1000,
                        (Math.min(
                            ((listPointTrim.get(j + 1) - listPointTrim.get(j)) + obj.originStartPosition),
                            (obj.duration - obj.originStartPosition)
                        )) * 1000
                    )
                    builder.addDataSource(TrackType.VIDEO, clipVideoSource)


                    isAddAudio = true
                    val audioSource = FilePathDataSource(obj.filePath)
                    val clipAudioSource = ClipDataSource(
                        audioSource,
                        (obj.originStartPosition) * 1000,
                        (Math.min(
                            ((listPointTrim.get(j + 1) - listPointTrim.get(j)) + obj.originStartPosition),
                            (obj.duration - obj.originStartPosition)
                        )) * 1000
                    )
                    builder.addDataSource(TrackType.AUDIO, clipAudioSource)
                }
            }
            if (!isAddVideo) {
                val dataSource = FilePathDataSource(filePath!!)
                val clipDataSource = ClipDataSource(
                    dataSource,
                    listPointTrim.get(j) * 1000,
                    (listPointTrim.get(j + 1)) * 1000
                )
                builder.addDataSource(TrackType.VIDEO, clipDataSource)
            }
            if (!isAddAudio) {
                val dataSource = FilePathDataSource(filePath!!)
                val clipDataSource = ClipDataSource(
                    dataSource,
                    listPointTrim.get(j) * 1000,
                    (listPointTrim.get(j + 1)) * 1000
                )
                builder.addDataSource(TrackType.AUDIO, clipDataSource)
            }
        }
    }

    private var trimStartVideo: Long = 0;
    private var trimEndVideo: Long = Long.MAX_VALUE
    private fun resetUI() {
        playerFragment?.ePlayerView?.resetLayout()
        stickerView.removeAllStickers()
        removeAllRangeSeekBar()
        stickerView.visibility = View.VISIBLE
//        initTimeLine()
    }

    fun showPopupMenuSeekbarTimeLine(rangeSeekBarView: RangeSeekBarView) {
        playerFragment?.pause()
        popupMenuSeekbarTimeLine = null
        val itemList: ArrayList<PowerMenuItem> = ArrayList()
        itemList.add(PowerMenuItem(getString(R.string.item_delete), R.mipmap.ic_delete_ve))
        itemList.add(PowerMenuItem(getString(R.string.item_replace), R.mipmap.ic_replace_ve))
        if (rangeSeekBarView.obj is ItemVideoTimeLine) {
            itemList.add(PowerMenuItem(getString(R.string.item_edit_power), R.mipmap.ic_edit_ve))
        }
        val screenSize = ScreenSize(this)
        val popupWidth = screenSize.width * 2 / 3
        popupMenuSeekbarTimeLine = PowerMenu.Builder(this)
            .addItemList(itemList)
            .setAnimation(MenuAnimation.SHOWUP_BOTTOM_LEFT)
            .setShowBackground(true)
            .setWidth(popupWidth)
//            .setHeight(popupHeight)
            .setMenuRadius(10f)
            .setMenuShadow(10f)
            .setDividerHeight(1)
            .setDivider(ColorDrawable(ContextCompat.getColor(this, R.color.gray2)))
            .setCircularEffect(CircularEffect.BODY)
            .setTextSize(16)
            .setTextGravity(Gravity.START)
            .setTextColor(ContextCompat.getColor(this, R.color.white))
            .setTextTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL))
            .setSelectedTextColor(ContextCompat.getColor(this, R.color.white))
            .setMenuColor(Color.parseColor("#000000"))
            .setSelectedMenuColor(Color.parseColor("#000000"))
            .setOnMenuItemClickListener(object : OnMenuItemClickListener<PowerMenuItem> {
                override fun onItemClick(position: Int, item: PowerMenuItem?) {
                    popupMenuSeekbarTimeLine!!.dismiss()
                    val obj = rangeSeekBarView.obj
                    handleOnMenuItemClick(obj, item, rangeSeekBarView)
                }
            })
            .build()
        val pointView = rangeSeekBarView.getLocationOnScreen()
        val menuHeight = resources.getDimensionPixelSize(R.dimen.dimen_40) * itemList.size
        val xOff = screenSize.width / 2 - popupWidth / 2
        val yOff = pointView.y - menuHeight - resources.getDimensionPixelSize(R.dimen.dimen_20)
        popupMenuSeekbarTimeLine!!.showAtLocation(rangeSeekBarView, xOff, yOff)
    }

    var rangeSeekBarViewNeedReplace: RangeSeekBarView? = null
    var objNeedReplace: Any? = null
    var isNeedReplace: Boolean = false
    fun handleOnMenuItemClick(
        obj: Any,
        item: PowerMenuItem?,
        rangeSeekBarView: RangeSeekBarView
    ) {
        when (item?.title) {
            getString(R.string.item_delete) -> {
                removeRangeSeekBar(obj)
                if (obj is ItemVideoTimeLine) {
                    playerFragment?.removeItemVideoTimeLine(obj.key)
                    playerFragment?.playLink()
                } else if (obj is Sticker) {
                    stickerView.remove(obj)
                } else if (obj is GlFilter) {
                    glFilterDefault = null
                    glFilterPlayer = null
                    playerFragment?.setFilter(null)
                }

            }
            getString(R.string.item_replace) -> {
                if (obj is ItemVideoTimeLine) {
//                    val oldIt1 = rangeSeekBarView.obj as ItemVideoTimeLine
//                    Loggers.e("Popup_replace_AA", "${oldIt1.key}")
                    isNeedReplace = true
                    rangeSeekBarViewNeedReplace = rangeSeekBarView
                    objNeedReplace = obj
                    selectMultipleMusicToAdd()
//                    val it: ItemVideoTimeLine = rangeSeekBarViewNeedReplace!!.obj as ItemVideoTimeLine
//                    it.key = "abc"
//                    Loggers.e("Popup_replace_AB", "${(rangeSeekBarViewNeedReplace!!.obj as ItemVideoTimeLine).key}")
//
//                    val oldIt2 = rangeSeekBarView.obj as ItemVideoTimeLine
//                    Loggers.e("Popup_replace_AC", "${oldIt2.key}")
                } else if (obj is StickerText) {
                    stickerView?.handlingSticker = obj
                    openAddTextFragment()
                } else if (obj is StickerDrawable) {
                    stickerView?.handlingSticker = obj
                    openAddLogoFragment()
                } else if (obj is GlFilter) {
                    openAddEffectFragment()
                }
            }
            getString(R.string.item_edit_power) -> {
                if (obj is ItemVideoTimeLine) {
//                    val oldIt1 = rangeSeekBarView.obj as ItemVideoTimeLine
//                    Loggers.e("Popup_replace_AA", "${oldIt1.key}")
                    isNeedReplace = true
                    rangeSeekBarViewNeedReplace = rangeSeekBarView
                    objNeedReplace = obj
                    val screenType =
                        if (obj.type == Constant.TYPE_AUDIO) Constant.TYPE_REPLACE_MUSIC else Constant.TYPE_INSERT_VIDEO
                    showTrimVideoInsertFragment(
                        obj.filePath,
                        screenType,
                        obj.originStartPosition,
                        obj.originStartPosition + (rangeSeekBarView.selectedMaxValue - rangeSeekBarView.selectedMinValue)
                    )
                }
            }
        }
    }

    var popupSelectVideo: PowerMenu? = null
    fun showSelectVideoPopup() {
        val itemList: java.util.ArrayList<PowerMenuItem> = java.util.ArrayList()
        itemList.add(PowerMenuItem(getString(R.string.select_from_gallery)))
        itemList.add(PowerMenuItem(getString(R.string.select_from_camera)))
        if (popupSelectVideo != null) {
            popupSelectVideo?.showAtCenter(layoutRoot)
            return
        }
        val textView = TextView(this)
        textView.textSize = 17f
        textView.setTextColor(ContextCompat.getColor(this, R.color.color_primary_dark))
        textView.text = getString(R.string.select_video_title)
        textView.setPadding(25, 15, 15, 15)
        textView.setBackgroundColor(Color.parseColor("#f8f8f8"))
        textView.typeface = Typeface.create("sans-serif-medium", Typeface.BOLD)

        val screenSize = ScreenSize(this)
        val popupWidth = screenSize.width * 2 / 3
//        val popupHeight = screenSize.height / 2
        popupSelectVideo = PowerMenu.Builder(this)
            .addItemList(itemList)
            .setAnimation(MenuAnimation.SHOWUP_TOP_LEFT)
            .setAutoDismiss(false)
            .setShowBackground(true)
            .setWidth(popupWidth)
//                .setHeight(popupHeight)
            .setMenuRadius(10f)
            .setMenuShadow(10f)
            .setDividerHeight(1)
            .setDivider(ColorDrawable(ContextCompat.getColor(this, R.color.dark_text)))
            .setCircularEffect(CircularEffect.BODY)
            .setTextSize(16)
            .setTextGravity(Gravity.START)
            .setTextColor(ContextCompat.getColor(this, R.color.dark_text))
            .setTextTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL))
            .setSelectedTextColor(ContextCompat.getColor(this, R.color.dark_text))
            .setMenuColor(Color.parseColor("#f8f8f8"))
            .setSelectedMenuColor(Color.parseColor("#f8f8f8"))
            .setOnMenuItemClickListener(object : OnMenuItemClickListener<PowerMenuItem> {
                override fun onItemClick(position: Int, item: PowerMenuItem?) {
                    popupSelectVideo!!.dismiss()
                    if (item == null) {
                        return
                    }
                    val title = item.title
                    if (title.equals(getString(R.string.select_from_gallery))) {
                        openGallery()
                        return
                    }
                    if (title.equals(getString(R.string.select_from_camera))) {
                        openCamera()
                    }
                }

            })
            .setHeaderView(textView)
            .build()
        popupSelectVideo?.showAtCenter(layoutRoot)
    }

    fun openGallery() {
        refreshGalleryAlone()
        val i = Intent(
            Intent.ACTION_PICK,
            MediaStore.Video.Media.EXTERNAL_CONTENT_URI
        )
        i.type = "video/*"
        i.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("video/*"))
        resultLauncherGallery.launch(i)
    }

    var resultLauncherGallery =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                result.data?.let {
                    setFilePath(result.resultCode, it, Constant.REQUEST_CODE_GALLERY)
                }
            }
        }

    var videoCameraFile: File? = null
    fun openCamera() {
        val intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        var videoFile: File? = null
        try {
            videoFile = createFile()
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
        if (videoFile != null) {
            val videoURI: Uri = FileProvider.getUriForFile(
                this, this.packageName + ".provider",
                videoFile
            )
            videoCameraFile = videoFile
            intent.putExtra(MediaStore.EXTRA_OUTPUT, videoURI)
            intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 240) //4 minutes
//                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1)
            resultLauncherCamera.launch(intent)
        }
    }

    var resultLauncherCamera =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                if (videoCameraFile == null) {
                    return@registerForActivityResult
                }
                filePath = videoCameraFile.toString()
                init()
            }
        }

    private fun createFile(): File? {
        val timeStamp = SimpleDateFormat("ddMMyyyyHHmmss").format(Date())
        val mFileName = "MDT_" + timeStamp + "_"
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_MOVIES)
        if (storageDir == null) {
            return null
        }
        return File.createTempFile(mFileName, ".mp4", storageDir)
    }

    fun refreshGalleryAlone() {
        try {
            val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
            sendBroadcast(mediaScanIntent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setFilePath(resultCode: Int, data: Intent, requestCode: Int) {
        if (resultCode == AppCompatActivity.RESULT_OK) {
            try {
                val selectedVideo = data.data
                val filePath = OptiCommonMethods.getRealPathFromUri(this, selectedVideo)
                if (filePath.isNullOrEmpty()) {
                    return
                }
                Log.e("TrimVideo", "filePath: $filePath")
                if (requestCode == Constant.REQUEST_CODE_GALLERY) {
                    val videoFile = File(filePath)
                    val extension = OptiCommonMethods.getFileExtension(videoFile.absolutePath)
                    val timeInMillis =
                        OptiCommonMethods.getVideoDuration(this, videoFile)
                    Log.e("TrimVideo", "timeInMillis: $timeInMillis")
                    val duration = OptiCommonMethods.convertDurationInMin(timeInMillis)
                    Log.e("TrimVideo", "duration: $duration")
                    val second = TimeUnit.MILLISECONDS.toSeconds(timeInMillis)
                    Log.e("TrimVideo", "second: $second")

                    //check if video is more than 4 minutes
                    if (duration < Constant.VIDEO_LIMIT) {
                        //check video format before playing into exoplayer
                        if (extension == Constant.AVI_FORMAT) {
//                                convertAviToMp4() //avi format is not supported in exoplayer
                            showToast(R.string.msg_file_note_support)
                        } else {
                            this.filePath = filePath
                            init()
                        }
                    } else {
                        val msg = String.format(
                            Locale.ENGLISH,
                            getString(R.string.msg_max_duration),
                            Constant.VIDEO_LIMIT
                        )
                        showToast(msg)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun isPopupMenuSeekbarTimeLineShowing(): Boolean {
        if (popupMenuSeekbarTimeLine == null || !popupMenuSeekbarTimeLine!!.isShowing()) {
            return false
        }
        popupMenuSeekbarTimeLine!!.dismiss()
        return true
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isPopupMenuSeekbarTimeLineShowing()) {
                return true
            }
        }
        return super.onKeyUp(keyCode, event)
    }
}