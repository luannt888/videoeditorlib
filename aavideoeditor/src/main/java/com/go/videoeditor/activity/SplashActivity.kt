package com.go.videoeditor.activity

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.TranslateAnimation
import android.widget.LinearLayout
import com.go.videoeditor.R
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.htextview.AnimationListener
import com.go.videoeditor.htextview.HTextView
import com.go.videoeditor.listener.OnDialogButtonListener
import com.go.videoeditor.model.ItemAppConfig
import com.go.videoeditor.model.ItemVersion
import com.go.videoeditor.service.MyHttpRequest
import com.go.videoeditor.service.RequestParams
import com.go.videoeditor.service.ServiceUtil
import com.go.videoeditor.user.OnLoginFormListener
import com.go.videoeditor.user.UserLoginActivity
import com.go.videoeditor.utils.JsonParser
import com.go.videoeditor.utils.ScreenSize
import kotlinx.android.synthetic.main.activity_splash_ve.*


class SplashActivity : BaseActivity() {
    var myHttpRequest: MyHttpRequest? = null
    var currentBgLaunch: String? = null
    var currentLogo: String? = null
    var isEndAnimation = false
    var isStopSloganAnimation = false
    var currentSloganPosition = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_ve)
        if(MyApplication.sContext == null){
            MyApplication.init(this)
        }
        MyApplication.getInstance()?.clearDataManager()
        initUI()
        initData()
    }

    fun initUI() {
        MyApplication.getInstance()?.loadDrawableImage(this, R.drawable.bg_splash, imageBackground)
        val sharedPreferences = getSharedPreferences(Constant.CONFIG, MODE_PRIVATE)
        /*currentBgLaunch = sharedPreferences.getString(Constant.IMG_LAUNCH, null)
        currentLogo = sharedPreferences.getString(Constant.LOGO, null)*/
        if (!currentBgLaunch.isNullOrEmpty()) {
            MyApplication.getInstance()
                ?.loadImageNoPlaceHolder(this, imageBackgroundServer, currentBgLaunch)
            imageBackgroundServer.visibility = View.GONE
        }
        if (!currentLogo.isNullOrEmpty()) {
            MyApplication.getInstance()?.loadImageBitmapListener(
                this,
                currentLogo,
                object : MyApplication.OnLoadImageBitmapListener {
                    override fun onResourceReady(resource: Bitmap?) {
                        initAnimation(resource)
                    }
                })
        } else {
            MyApplication.getInstance()?.loadImageBitmapListener(
                this,
                R.drawable.ic_logo_splash,
                object : MyApplication.OnLoadImageBitmapListener {
                    override fun onResourceReady(resource: Bitmap?) {
                        initAnimation(resource)
                    }
                })
        }
    }

    fun initAnimation(resource: Bitmap?) {
        if (resource == null) {
            isEndAnimation = true
            return
        }
        val screenSize = ScreenSize(this)
//        val layoutParamsParent: RelativeLayout.LayoutParams = imageLogoActionbarCenter.layoutParams as RelativeLayout.LayoutParams
//        layoutParamsParent.width = screenSize.width / 2

        val imageWidth = resource.width
        val imageHeight = resource.height
        val widthRatio: Float = imageWidth * 1f / imageHeight

        val imageCenterWidth = screenSize.width / 4
        val imageCenterHeight = (imageCenterWidth / widthRatio).toInt()
        val lpLogoActionbarCenter: LinearLayout.LayoutParams =
            imageLogoActionbarCenter.layoutParams as LinearLayout.LayoutParams
        lpLogoActionbarCenter.width = imageCenterWidth
        lpLogoActionbarCenter.height = imageCenterHeight
//        imageLogoActionbarCenter.setImageBitmap(resource)
        imageLogoActionbarCenter.setImageBitmap(
            Bitmap.createScaledBitmap(
                resource,
                imageCenterWidth,
                imageCenterHeight,
                false
            )
        )

        val anim = AnimationUtils.loadAnimation(this, R.anim.zoom_in)
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(p0: Animation?) {
            }

            override fun onAnimationEnd(p0: Animation?) {
                animLogoSlideLeft()
                /*val width1 = textSloganTemp.width
                Loggers.e("textSloganTemp", "width1 = $width1")
                textSloganTemp.post(Runnable {
                    val width = textSloganTemp.width
                    Loggers.e("textSloganTemp", "width2 = $width")
                })

                textSlogan.setAnimationListener(object : AnimationListener {
                    override fun onAnimationEnd(hTextView: HTextView?) {
                        Handler(Looper.getMainLooper()).postDelayed(Runnable {
                            if (isDestroyed || isFinishing || isStopSloganAnimation) {
                                return@Runnable
                            }
//                            isEndAnimation = true
                        }, 2000)
                    }
                })
                textSlogan.animateText(getString(R.string.app_name))
                currentSloganPosition++*/
            }

            override fun onAnimationRepeat(p0: Animation?) {
            }
        })
        imageLogoActionbarCenter.startAnimation(anim)
    }

    fun animLogoSlideLeft() {
        if (isStopSloganAnimation || isDestroyed || isFinishing) {
            return
        }
        imageLogoActionbarCenter.pivotX = 0f
        imageLogoActionbarCenter.pivotY = 0f
        val posX = textSloganTemp.width / -2f
        val animate = TranslateAnimation(0f, posX, 0f, 0f)
        animate.duration = 600
        animate.fillAfter = false
        animate.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(p0: Animation?) {
            }

            override fun onAnimationEnd(p0: Animation?) {
                textSlogan.setAnimationListener(object : AnimationListener {
                    override fun onAnimationEnd(hTextView: HTextView?) {
                        if (isStopSloganAnimation || isDestroyed || isFinishing) {
                            return
                        }
                        isEndAnimation = true
                        animSloganZoomInOut(true)
                    }
                })
                textSlogan.animateText(getString(R.string.app_name))
            }

            override fun onAnimationRepeat(p0: Animation?) {
            }
        })
        imageLogoActionbarCenter.startAnimation(animate)

        /*imageLogoActionbarCenter.animate()
            .x(posX)
            .setInterpolator(DecelerateInterpolator())
            .setDuration(800)
            .withEndAction {
                textSlogan.animateText(getString(R.string.app_name))
            }
            .setStartDelay(500L)
            .start()*/
    }

    fun animSloganZoomInOut(isZoomIn: Boolean) {
        val animId = if (isZoomIn) R.anim.zoom_in_slogan else R.anim.zoom_out_slogan
        val anim = AnimationUtils.loadAnimation(this, animId)
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(p0: Animation?) {
            }

            override fun onAnimationEnd(p0: Animation?) {
                if (isStopSloganAnimation || isDestroyed || isFinishing) {
                    return
                }
                animSloganZoomInOut(!isZoomIn)
            }

            override fun onAnimationRepeat(p0: Animation?) {
            }
        })
        textSlogan.startAnimation(anim)
    }

    fun initData() {
        getData()
    }

    val TYPE_GET_CONFIG = 2
    var type: Int = TYPE_GET_CONFIG

    fun getData() {
        getConfig()
    }

    fun getConfig() {
        if (!MyApplication.getInstance()!!.isNetworkConnect()) {
            runOnUiThread {
                showErrorNetwork()
            }
            return
        }
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(this)
        } else {
            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        requestParams.put("type", "android")
        var api: String? = null
        if (type == TYPE_GET_CONFIG) {
            api = Constant.API_CONFIG
        }
        myHttpRequest!!.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isFinishing || isDestroyed) {
                    return
                }
                runOnUiThread { showErrorNetwork() }
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isFinishing || isDestroyed) {
                    return
                }
                handleData(responseString)
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            runOnUiThread { showErrorNetwork() }
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            runOnUiThread { showErrorNetwork() }
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            runOnUiThread { showDialog(message) }
            return
        }

        if (type == TYPE_GET_CONFIG) {
            val resultObj = JsonParser.getJsonObject(jsonObject, "result")
            val itemAppConfig = JsonParser.parseItemAppConfig(resultObj)
            if (itemAppConfig == null) {
                runOnUiThread { showErrorNetwork() }
                return
            }
            val itemVersion: ItemVersion? = JsonParser.parseItemVersion(resultObj)
            if (itemVersion == null) {
                runOnUiThread { showErrorNetwork() }
                return
            }
            val currentVersion: Int = MyApplication.getInstance()!!.getVersionCode(this)
            if (currentVersion < itemVersion.versionCode!! && !itemVersion.url.isNullOrEmpty()) {
                runOnUiThread {
                    val leftButton = if (itemVersion.isUpdate!!) null else getString(R.string.close)
                    showDialog(
                        false,
                        itemVersion.title,
                        itemVersion.message,
                        leftButton,
                        getString(R.string.update),
                        object : OnDialogButtonListener {
                            override fun onLeftButtonClick() {
                                showWaitingScreen(itemAppConfig)
                            }

                            override fun onRightButtonClick() {
                                openLink(itemVersion.url)
                            }
                        })
                }
                return
            }
            showWaitingScreen(itemAppConfig)
        }
    }

    fun showWaitingScreen(itemAppConfig: ItemAppConfig) {
        val ignoreBgLaunch = true
        if (ignoreBgLaunch) {
            autoLogin()
            return
        }
        if (!itemAppConfig.bgLaunch.isNullOrEmpty()) {
            runOnUiThread { imageBackgroundServer.visibility = View.VISIBLE }
            val sharedPreferences = getSharedPreferences(Constant.CONFIG, MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            if (currentBgLaunch.isNullOrEmpty() || (!currentBgLaunch.isNullOrEmpty() && !currentBgLaunch.equals(
                    itemAppConfig.bgLaunch
                ))
            ) {
                runOnUiThread {
                    MyApplication.getInstance()?.loadImageNoPlaceHolder(
                        this,
                        imageBackgroundServer,
                        itemAppConfig.bgLaunch
                    )
                }
                editor.putString(Constant.IMG_LAUNCH, itemAppConfig.bgLaunch)
            }

            if (currentLogo.isNullOrEmpty() || (!currentLogo.isNullOrEmpty() && !currentLogo.equals(
                    itemAppConfig.logo
                ))
            ) {
                currentLogo = itemAppConfig.logo
                if (!isEndAnimation) {
                    runOnUiThread {
                        imageLogoActionbarCenter.clearAnimation()
                    }
                    if (!currentLogo.isNullOrEmpty()) {
                        MyApplication.getInstance()?.loadImageBitmapListener(
                            this,
                            currentLogo,
                            object : MyApplication.OnLoadImageBitmapListener {
                                override fun onResourceReady(resource: Bitmap?) {
                                    runOnUiThread {
                                        initAnimation(resource)
                                    }
                                }
                            })
                    } else {
                        isEndAnimation = true
                    }
                }
                editor.putString(Constant.LOGO, itemAppConfig.logo)
            }
            editor.apply()
        }
        autoLogin()
    }

    private fun autoLogin() {
        ServiceUtil.autoLogin(this, object : OnLoginFormListener {
            override fun onLogin(hasLogin: Boolean) {
                if (isFinishing || isDestroyed) {
                    return
                }
                if (hasLogin) {
                    goMainActivity()
                } else {
//                    getData()
                    startActivityForResult(
                        Intent(
                            this@SplashActivity,
                            UserLoginActivity::class.java
                        ), Constant.CODE_LOGIN
                    )
                }
            }
        })
    }

    fun goMainActivity() {
        if (!isEndAnimation) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                if (isDestroyed || isFinishing) {
                    return@Runnable
                }
                goMainActivity()
            }, 200)
            return
        }
        isStopSloganAnimation = true
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            if (isDestroyed || isFinishing) {
                return@Runnable
            }
            var bundle = intent.extras
//            gotoActivity(MainActivity::class.java, bundle)
            val intent = Intent(this, MainVideoEditorActivity::class.java)
            if (bundle == null) {
                bundle = Bundle()
            }
            val imageLogoActionbarCenterLocation = IntArray(2)
            imageLogoActionbarCenter.getLocationOnScreen(imageLogoActionbarCenterLocation)

            bundle.putIntArray(Constant.LOCATION, imageLogoActionbarCenterLocation)
            intent.putExtras(bundle)

            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }, 1000)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RESULT_OK && requestCode == Constant.CODE_LOGIN) {
            goMainActivity()
        }
    }

    fun showErrorNetwork() {
        showDialog(
            false,
            R.string.notification,
            R.string.msg_network_error,
            R.string.close,
            R.string.try_again,
            object : OnDialogButtonListener {
                override fun onLeftButtonClick() {
                    finish()
                }

                override fun onRightButtonClick() {
                    getData()
                }
            })
    }

    override fun finish() {
        myHttpRequest?.cancel()
        super.finish()
    }
}