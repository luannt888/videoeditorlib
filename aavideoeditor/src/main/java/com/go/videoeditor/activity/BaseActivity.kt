package com.go.videoeditor.activity

import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.graphics.Outline
import android.graphics.Point
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.method.LinkMovementMethod
import android.util.TypedValue
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ShareCompat
import androidx.fragment.app.Fragment
import com.go.videoeditor.R
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.ExitService
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.app.MyApplication.Companion.getInstance
import com.go.videoeditor.convert.TaskBackgroundBottomDialogFragment
import com.go.videoeditor.listener.OnCancelListener
import com.go.videoeditor.listener.OnDialogButtonListener
import com.go.videoeditor.listener.OnKeyboardVisibilityListener
import com.go.videoeditor.model.ItemAppConfig
import com.go.videoeditor.utils.MyDialog
import com.go.videoeditor.utils.ScreenSize
import com.go.videoeditor.utils.SharedPreferencesManager.Companion.getToken
import com.go.videoeditor.utils.TextUtil
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.activity_main_ve.*
import kotlinx.android.synthetic.main.layout_loading_ve.*
import java.io.IOException

open class BaseActivity : AppCompatActivity() {
    var loadingDialog: Dialog? = null

    fun showLoadingDialog(cancelable: Boolean, onCancelListener: OnCancelListener?) {
        showLoadingDialog(cancelable, R.string.please_waiting, onCancelListener)
    }

    fun showLoadingDialog(cancelable: Boolean, message: Int, onCancelListener: OnCancelListener?) {
        showLoadingDialog(cancelable, getString(message), onCancelListener)
    }

    fun showLoadingDialog(
        cancelable: Boolean,
        message: String?,
        onCancelListener: OnCancelListener?
    ) {
        if (isFinishing || isDestroyed) {
            return
        }
        if (loadingDialog != null) {
            loadingDialog!!.dismiss()
            loadingDialog!!.setCancelable(cancelable)
            loadingDialog!!.setCanceledOnTouchOutside(false)
            val textMessage = loadingDialog!!.textMessage
            if (message.isNullOrEmpty()) {
                textMessage?.visibility = View.GONE
            } else {
                textMessage?.text = message
                textMessage?.visibility = View.VISIBLE
            }
            loadingDialog?.show()
            return
        }
        loadingDialog = Dialog(this)
        if (loadingDialog!!.window != null) {
            loadingDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        loadingDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        loadingDialog!!.setCancelable(cancelable)
        loadingDialog!!.setCanceledOnTouchOutside(false)
        loadingDialog!!.setContentView(R.layout.layout_loading_ve)
        val textMessage = loadingDialog!!.textMessage
        if (message.isNullOrEmpty()) {
            textMessage?.visibility = View.GONE
        } else {
            textMessage?.text = message
            textMessage?.visibility = View.VISIBLE
        }
        loadingDialog?.setOnCancelListener {
            if (onCancelListener != null) {
                onCancelListener.onCancel(true)
            }
        }
        loadingDialog?.show()
    }

    fun hideLoadingDialog() {
        loadingDialog?.dismiss()
        loadingDialog?.buttonDownload?.visibility = View.GONE
    }

    fun setTextLoadingDialog(msg: String) {
        loadingDialog?.textMessage?.text = msg
    }

    fun setButtonCancelClick(onClickListener: View.OnClickListener) {
        loadingDialog?.buttonDownload?.visibility = View.VISIBLE
        loadingDialog?.buttonDownload?.setOnClickListener(onClickListener)
    }

    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun showToast(messageId: Int) {
        Toast.makeText(this, messageId, Toast.LENGTH_SHORT).show()
    }

    fun showDialog(message: String?) {
        showDialog(
            true,
            getString(R.string.notification),
            message,
            getString(R.string.close),
            null,
            null
        )
    }

    fun showDialog(message: String?, onDialogButtonListener: OnDialogButtonListener?) {
        showDialog(
            true,
            getString(R.string.notification),
            message,
            getString(R.string.close),
            null,
            onDialogButtonListener
        )
    }

    fun showDialog(
        cancelable: Boolean,
        message: String?,
        onDialogButtonListener: OnDialogButtonListener?
    ) {
        showDialog(
            cancelable,
            getString(R.string.notification),
            message,
            getString(R.string.close),
            null,
            onDialogButtonListener
        )
    }

    fun showDialog(
        cancelable: Boolean, titleId: Int, messageId: Int,
        leftButtonTitleId: Int, rightButtonTitleId: Int,
        dialogButtonListener: OnDialogButtonListener?
    ) {
        var message: String? = null
        if (messageId != 0) {
            message = getString(messageId)
        }
        showDialog(
            cancelable,
            titleId,
            message,
            leftButtonTitleId,
            rightButtonTitleId,
            dialogButtonListener
        )
    }

    fun showDialog(
        cancelable: Boolean, titleId: Int, message: String?,
        leftButtonTitleId: Int, rightButtonTitleId: Int,
        dialogButtonListener: OnDialogButtonListener?
    ) {
        var title: String? = null
        var leftButtonTitle: String? = null
        var rightButtonTitle: String? = null
        if (titleId != 0) {
            title = getString(titleId)
        }
        if (leftButtonTitleId != 0) {
            leftButtonTitle = getString(leftButtonTitleId)
        }
        if (rightButtonTitleId != 0) {
            rightButtonTitle = getString(rightButtonTitleId)
        }
        showDialog(
            cancelable,
            title,
            message,
            leftButtonTitle,
            rightButtonTitle,
            dialogButtonListener
        )
    }

    fun showDialog(
        cancelable: Boolean, title: String?, message: String?,
        leftButtonTitle: String?, rightButtonTitle: String?,
        onDialogButtonListener: OnDialogButtonListener?
    ): MyDialog? {
        return showDialog(
            cancelable,
            false,
            title,
            message,
            leftButtonTitle,
            rightButtonTitle,
            onDialogButtonListener
        )
    }

    fun showDialog(
        cancelable: Boolean, cancelableOutside: Boolean, title: String?, message: String?,
        leftButtonTitle: String?, rightButtonTitle: String?,
        onDialogButtonListener: OnDialogButtonListener?
    ): MyDialog? {
        if (isFinishing || isDestroyed) {
            return null
        }
        val dialog = MyDialog(this)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val lp = WindowManager.LayoutParams()
        //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimationLeftRight
        dialog.window?.attributes = lp
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(cancelable)
        dialog.setCanceledOnTouchOutside(cancelableOutside)
        dialog.setContentView(R.layout.layout_dialog_ve)
        val screenSize = ScreenSize(this)
        dialog.window?.setLayout(screenSize.width * 8 / 10, WindowManager.LayoutParams.WRAP_CONTENT)
        val layoutRoot: View = dialog.findViewById(R.id.layoutRoot)
        val textTitle: TextView = dialog.findViewById(R.id.textTitle)
        val textMessage: TextView = dialog.findViewById(R.id.textMessage)
        val buttonLeft: TextView = dialog.findViewById(R.id.buttonLeft)
        val buttonRight: TextView = dialog.findViewById(R.id.buttonRight)
        //        View viewDivider = dialog.findViewById(R.id.viewDivider);
        layoutRoot.clipToOutline = true
        layoutRoot.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                val cornerRadiusDP = 15f
                outline.setRoundRect(0, 0, view.width, view.height, cornerRadiusDP)
            }
        }
        if (!title.isNullOrEmpty()) {
            textTitle.text = title
        }
        if (!message.isNullOrEmpty()) {
//            textMessage.setText(message);
            TextUtil.setHtmlTextView(message, textMessage)
            textMessage.movementMethod = LinkMovementMethod.getInstance()
        }
        if (leftButtonTitle.isNullOrEmpty()) {
            buttonLeft.visibility = View.GONE
            //            viewDivider.setVisibility(View.GONE);
        } else {
            buttonLeft.text = leftButtonTitle
            buttonLeft.visibility = View.VISIBLE
        }
        if (rightButtonTitle.isNullOrEmpty()) {
            buttonRight.visibility = View.GONE
            //            viewDivider.setVisibility(View.GONE);
        } else {
            buttonRight.text = rightButtonTitle
            buttonRight.visibility = View.VISIBLE
        }
        buttonLeft.setOnClickListener {
            dialog.dismiss()
            if (onDialogButtonListener != null) {
                onDialogButtonListener.onLeftButtonClick()
            }
        }
        buttonRight.setOnClickListener {
            dialog.dismiss()
            if (onDialogButtonListener != null) {
                onDialogButtonListener.onRightButtonClick()
            }
        }
        try {
            dialog.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return dialog
    }

    fun showKeyboardFocus(editText: EditText?) {
        editText?.requestFocus()
        val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
    }

    fun hideKeyboard(editText: EditText?) {
        val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(
            editText?.windowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }

    fun View.getLocationOnScreen(): Point {
        val location = IntArray(2)
        this.getLocationOnScreen(location)
        return Point(location[0], location[1])
    }

    fun showDialogUser(message: String?) {
        showDialogUser(
            true,
            getString(R.string.notification),
            message,
            getString(R.string.yes),
            null,
            null
        )
    }

    fun showDialogUser(
        cancelable: Boolean,
        title: String?,
        message: String?,
        leftButtonTitle: String?,
        rightButtonTitle: String?,
        onDialogButtonListener: OnDialogButtonListener?
    ): Dialog? {
        if (isFinishing || isDestroyed) {
            return null
        }
        val dialog = MyDialog(this)
        if (dialog.window != null) {
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        val lp = WindowManager.LayoutParams()
        //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimationLeftRight
        dialog.window!!.attributes = lp
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(cancelable)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.layout_dialog_user_ve)
        val screenSize = ScreenSize(this)
        dialog.window!!.setLayout(
            screenSize.width * 9 / 10,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        val layoutRoot: View = dialog.findViewById(R.id.layoutRoot)
        val textTitle: TextView = dialog.findViewById(R.id.textTitle)
        val textMessage: TextView = dialog.findViewById(R.id.textMessage)
        val buttonLeft: Button = dialog.findViewById(R.id.buttonLeft)
        val buttonRight: Button = dialog.findViewById(R.id.buttonRight)
        val viewDivider: View = dialog.findViewById(R.id.viewDivider)
        layoutRoot.clipToOutline = true
        layoutRoot.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                val cornerRadiusDP = 15f
                outline.setRoundRect(0, 0, view.width, view.height, cornerRadiusDP)
            }
        }
        if (!title.isNullOrEmpty()) {
            textTitle.text = title
        }
        if (!message.isNullOrEmpty()) {
            textMessage.text = message
        }
        if (leftButtonTitle.isNullOrEmpty()) {
            buttonLeft.visibility = View.GONE
            viewDivider.visibility = View.GONE
        } else {
            buttonLeft.text = leftButtonTitle
            buttonLeft.visibility = View.VISIBLE
        }
        if (rightButtonTitle.isNullOrEmpty()) {
            buttonRight.visibility = View.GONE
            viewDivider.visibility = View.GONE
        } else {
            buttonRight.text = rightButtonTitle
            buttonRight.visibility = View.VISIBLE
        }
        buttonLeft.setOnClickListener {
            dialog.dismiss()
            onDialogButtonListener?.onLeftButtonClick()
        }
        buttonRight.setOnClickListener {
            dialog.dismiss()
            onDialogButtonListener?.onRightButtonClick()
        }
        try {
            dialog.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return dialog
    }

    fun setFullStatusTransparent() {
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.decorView.systemUiVisibility = (
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
        } else {
            window.setDecorFitsSystemWindows(false)
            window.insetsController!!.systemBarsBehavior =
                WindowInsetsController.BEHAVIOR_SHOW_BARS_BY_TOUCH
        }
        window.statusBarColor = Color.TRANSPARENT
//        setBackgroudColor(layoutActionbar, "#00000000")
//
        val statusBarHeight = getStatusBarHeight()
        if (layoutActionbar?.getLayoutParams() is ConstraintLayout.LayoutParams) {
            val params = layoutActionbar?.getLayoutParams() as ConstraintLayout.LayoutParams
            params.topMargin = statusBarHeight
            layoutActionbar?.setLayoutParams(params)
        }
    }

    fun setBackgroudColor(view: View?, bgColor: String) {
        view?.setBackgroundColor(Color.parseColor(bgColor))
    }

    fun getStatusBarHeight(): Int {
        val res = resources.getIdentifier("status_bar_height", "dimen", "android")
        var statusBarHeight = 0
        if (res != 0) {
            statusBarHeight = resources.getDimensionPixelSize(res)
        }
        return statusBarHeight
    }

    fun openLink(link: String?) {
        if (link.isNullOrEmpty()) {
            return
        }
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun gotoActivity(intent: Intent?) {
        if (intent != null) {
            startActivity(intent)
        }
    }

    fun gotoActivity(cla: Class<*>?) {
        if (cla == null) {
            return
        }
        val intent = Intent(this, cla)
        startActivity(intent)
    }

    fun gotoActivity(cla: Class<*>?, bundle: Bundle?) {
        if (cla == null) {
            return
        }
        val intent = Intent(this, cla)
        if (bundle != null) {
            intent.putExtras(bundle)
        }
        startActivity(intent)
    }

    fun gotoActivityForResult(cla: Class<*>?, bundle: Bundle?, requestCode: Int) {
        if (cla == null) {
            return
        }
        val intent = Intent(this, cla)
        if (bundle != null) {
            intent.putExtras(bundle)
        }
        startActivityForResult(intent, requestCode)
    }

    fun gotoActivityForResult(cla: Class<*>?, requestCode: Int) {
        if (cla == null) {
            return
        }
        val intent = Intent(this, cla)
        startActivityForResult(intent, requestCode)
    }

    fun shareLinkApp() {
        val itemAppConfig: ItemAppConfig? =
            MyApplication.getInstance()!!.getDataManager()!!.itemAppConfig
        if (itemAppConfig?.linkShareApp.isNullOrEmpty()) {
            showToast(R.string.link_share_empty)
            return
        }
//        ServiceUtil.addShareLinkApp(this)
        shareApp(itemAppConfig?.linkShareApp)
    }

    fun shareApp(url: String?) {
        if (url == null || url.isEmpty()) {
            showToast(R.string.msg_empty_data)
            return
        }
        ShareCompat.IntentBuilder.from(this)
            .setType("text/plain")
            .setText(url)
            .setChooserTitle(getString(R.string.select_share_app))
            .startChooser()
    }

    open fun setOnKeyboardVisibilityListener(onKeyboardVisibilityListener: OnKeyboardVisibilityListener) {
        val parentView = (findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0)
        parentView.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            private var alreadyOpen = false
            private val defaultKeyboardHeightDP = 100
            private val EstimatedKeyboardDP =
                defaultKeyboardHeightDP + if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) 48 else 0
            private val rect = Rect()
            override fun onGlobalLayout() {
                val estimatedKeyboardHeight = TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    EstimatedKeyboardDP.toFloat(),
                    parentView.resources.displayMetrics
                ).toInt()
                parentView.getWindowVisibleDisplayFrame(rect)
                val heightDiff = parentView.rootView.height - (rect.bottom - rect.top)
                val isShown = heightDiff >= estimatedKeyboardHeight
                if (isShown == alreadyOpen) {
                    Loggers.e("Keyboard state", "Ignoring global layout change...")
                    return
                }
                alreadyOpen = isShown
                onKeyboardVisibilityListener.onVisibilityChanged(isShown)
            }
        })
    }

    fun showBottomSheetDialog(fragment: Fragment, bundle: Bundle? = null) {
        if (!(fragment is BottomSheetDialogFragment)) {
            return
        }
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            if (bundle != null) {
                fragment.arguments = bundle
            }
            fragment.show(supportFragmentManager, fragment.tag)
        }, Constant.DELAY_HANDLE)
    }

    fun startExitService() {
        if (!ExitService.hasConnect) {
            ExitService.hasConnect = true
            val intent = Intent(this, ExitService::class.java)
            startService(intent)
        }
    }

    fun checkExitApp() {
        if (!hasLogin()) {
            exitApp()
        }
    }

    private fun exitApp() {
        finishAffinity()
    }

    fun hasLogin(): Boolean {
        val token = getToken(this)
        return !getInstance()!!.isEmpty(token)
    }

    fun goVideoEditorActivity(filePath: String) {
        try {
            val bundle = Bundle()
            bundle.putString(
                Constant.FILE_PATH,
                filePath
            )
            gotoActivity(VideoEditorActivity::class.java, bundle)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
     fun showTaskBackground() {
        val fragment = TaskBackgroundBottomDialogFragment()
        showBottomSheetDialog(fragment)
    }
    fun showBottomSheetDialogFragment(fragment: Fragment) {
        if (!(fragment is BottomSheetDialogFragment)) {
            return
        }
        fragment.show(supportFragmentManager, fragment.tag)
    }

}