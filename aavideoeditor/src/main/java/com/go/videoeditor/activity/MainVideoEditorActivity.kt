package com.go.videoeditor.activity

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.provider.Settings
import android.view.KeyEvent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.downloader.PRDownloader
import com.go.videoeditor.R
import com.go.videoeditor.adapter.ViewPagerAdapter
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.convert.TaskBackgroundFragment
import com.go.videoeditor.downloadVideo.DownloadVideoFragment
import com.go.videoeditor.fragment.TabHomeFragment
import com.go.videoeditor.listener.OnDialogButtonListener
import com.go.videoeditor.upload.UploadManager
import com.go.videoeditor.user.TabPersonFragment
import com.go.videoeditor.utils.AppUtils
import com.go.videoeditor.utils.FileUltis
import com.mukesh.permissions.EasyPermissions
import com.mukesh.permissions.OnPermissionListener
import kotlinx.android.synthetic.main.activity_main_ve.*
import java.io.File
import java.io.IOException

class MainVideoEditorActivity : BaseActivity() {
    var fragmentList: ArrayList<Fragment> = ArrayList()
    var easyPermission: EasyPermissions? = null
    var isHandling = false
    var accessToken: String? = null
    private var statusBarMargin: Boolean = false

    companion object {
        fun show(
            context: Context?,
            accessToken: String?,
            statusBarMargin: Boolean? = false,
            resId: Int? = 0
        ) {
            if (context == null || accessToken.isNullOrEmpty()) {
                return
            }
            val intent = Intent(context!!, MainVideoEditorActivity::class.java)
            val bundle = Bundle()
            bundle.putString(Constant.ACCESS_TOKEN, accessToken)
            bundle.putBoolean(Constant.STATUS_BAR_MARGIN, statusBarMargin ?: false)
            resId?.let { bundle.putInt(Constant.IMAGE, it) }
            intent.putExtras(bundle)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (MyApplication.sContext == null) {
            MyApplication.init(this)
        }
        setContentView(R.layout.activity_main_ve)
        initUI()
        initData()
        initControl()
    }

    private fun initData() {
        AppUtils.removeTempDownloadFolderPath(this)
    }

    private fun initUI() {
        intent?.extras?.let {
            accessToken = it.getString(Constant.ACCESS_TOKEN)
            val resBG = it.getInt(Constant.IMAGE, 0)
            statusBarMargin = it.getBoolean(Constant.STATUS_BAR_MARGIN)
            if (0 != resBG) {
                imageBgMain.setBackgroundResource(resBG)
            }
        }
        if (statusBarMargin) {
            setFullStatusTransparent()
            AppUtils.setFullStatusTransparent(this, layoutActionbar)
            AppUtils.setStatusBarColor(this, true, null)
        }
//        tabMain.setTabEnabledById(R.id.tabLive, false)

        initPager()
    }

    fun initPager() {
        val tabHomeFragment = TabHomeFragment.newInstance(accessToken, true)
        fragmentList.add(tabHomeFragment)

        /* val videoFragment = DownloadVideoFragment()
         fragmentList.add(videoFragment)

         val blankFragment = Fragment()
         fragmentList.add(blankFragment)

         val taskFragment = TaskBackgroundFragment()
         fragmentList.add(taskFragment)

         val personalFragment = TabPersonFragment()
         fragmentList.add(personalFragment)*/

        val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager, fragmentList, null)
        viewPager.adapter = viewPagerAdapter
        viewPager.offscreenPageLimit = fragmentList.size
        /*   tabMain.setOnTabSelectListener(object : AnimatedBottomBar.OnTabSelectListener {
               override fun onTabSelected(
                   lastIndex: Int,
                   lastTab: AnimatedBottomBar.Tab?,
                   newIndex: Int,
                   newTab: AnimatedBottomBar.Tab
               ) {
                   handleOnPageSelected(newIndex)
               }

           })
           tabMain.setupWithViewPager(viewPager)*/
        handleOnPageSelected(0)
    }

    fun handleOnPageSelected(position: Int) {
        if (position == 2) {
            return
        }
        val fragment = fragmentList.get(position)
        if (fragment is TabHomeFragment) {
            fragment.checkRefresh()
        } else if (fragment is DownloadVideoFragment) {
            fragment.checkRefresh()
        } else if (fragment is TaskBackgroundFragment) {
            fragment.checkRefresh()
        } else if (fragment is TabPersonFragment) {
            fragment.checkRefresh()
        }
    }

    fun initControl() {
        buttonLive.setOnClickListener {
            if (isHandling) {
                return@setOnClickListener
            }
            isHandling = true
            loadingBottom.visibility = View.VISIBLE

            if (!checkPermission()) {
                isHandling = false
                loadingBottom.visibility = View.GONE
                return@setOnClickListener
            }
            isHandling = true
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                gotoActivity(LivestreamActivity::class.java)
                Handler(Looper.getMainLooper()).postDelayed(Runnable {
                    isHandling = false
                    loadingBottom.visibility = View.GONE
                }, 1000)
            }, Constant.DELAY_HANDLE)
        }
        buttonBack.setOnClickListener {
            finish()
        }
    }

    fun openGallery() {
        refreshGalleryAlone()
        val i = Intent(
            Intent.ACTION_PICK,
            MediaStore.Video.Media.EXTERNAL_CONTENT_URI
        )
        i.type = "video/*"
        i.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("video/*"))
        startActivityForResult(i, Constant.REQUEST_CODE_GALLERY)
    }

    fun refreshGalleryAlone() {
        try {
            val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
            sendBroadcast(mediaScanIntent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    var videoFile: File? = null
    fun openCamera() {
        val intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        if (intent.resolveActivity(packageManager) != null) {
            var videoFile: File? = null
            try {
                videoFile = createFile()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            if (videoFile != null) {
                val videoURI: Uri = FileProvider.getUriForFile(
                    this, packageName + ".provider",
                    videoFile
                )
                this.videoFile = videoFile
                intent.putExtra(MediaStore.EXTRA_OUTPUT, videoURI)
                intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 600) //4 minutes
//                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1)
                startActivityForResult(intent, Constant.REQUEST_CODE_CAMERA)
            }
        }
    }

    private fun createFile(): File? {
        return FileUltis.createVideoRecordFile(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, intent)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        val fragments = supportFragmentManager.fragments
        for (i in 0 until fragments.size) {
            fragments.get(i).onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    val ALL_PERMISSIONS = arrayOf(
        Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.RECORD_AUDIO
    )

    fun initCheckPermission() {
        easyPermission = EasyPermissions.Builder()
            .with(this)
            .listener(object : OnPermissionListener {
                override fun onAllPermissionsGranted(permissions: MutableList<String>) {
                }

                override fun onPermissionsGranted(permissions: MutableList<String>) {
                    /*if (permissions.size == ALL_PERMISSIONS.size) {
                        return
                    }
                    if (easyPermission?.hasPermission(*ALL_PERMISSIONS)!!) {
                    }*/
                }

                override fun onPermissionsDenied(permissions: MutableList<String>) {
                    for (i in 0 until permissions.size) {
                        Loggers.e("A_Denied_$i", "${permissions.get(i)}")
                    }
                    runOnUiThread { showToast(R.string.msg_accept_permission) }
                    checkPermission()
                }
            }).build()
    }

    fun checkPermission(): Boolean {
        if (easyPermission == null) {
            initCheckPermission()
        }
        if (easyPermission?.hasPermission(*ALL_PERMISSIONS)!!) {
            return true
        }
        Loggers.e("A_checkPermission", "---------")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED && !shouldShowRequestPermissionRationale(
                    Manifest.permission.CAMERA
                )
            ) {
                Loggers.e("A_checkPermission_check2", "Open setting")
            }
        }
        easyPermission?.request(*ALL_PERMISSIONS)
        return false
    }

    fun callPermissionSettings() {
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.fromParts("package", applicationContext.packageName, null)
        intent.data = uri
        startActivityForResult(intent, 300)
    }

    override fun onDestroy() {
        PRDownloader.cancelAll()
        PRDownloader.shutDown()
        UploadManager.getInstance()?.cancelAll()
        AppUtils.removeTempDownloadFolderPath(this)
        super.onDestroy()
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            showDialog(
                true,
                R.string.notification,
                R.string.exit_msg,
                R.string.close,
                R.string.exit,
                object : OnDialogButtonListener {
                    override fun onLeftButtonClick() {
                    }

                    override fun onRightButtonClick() {
                        finish()
                    }

                })
            return true
        }
        return super.onKeyUp(keyCode, event)
    }

}