package com.go.videoeditor.activity

import android.os.Bundle
import com.go.videoeditor.R
import com.go.videoeditor.fragment.MediaStorageFragment

class MediaStorageActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_media_storage_ve)
        init()
    }

    fun init() {
        val fragment = MediaStorageFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayoutActivity, fragment)
        fragmentTransaction.commit()
    }

}