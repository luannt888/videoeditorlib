package com.go.videoeditor.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.core.view.GravityCompat
import com.downloader.PRDownloader
import com.go.videoeditor.R
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.convert.TaskBackgroundBottomDialogFragment
import com.go.videoeditor.downloadVideo.DownloadVideoFragment
import com.go.videoeditor.downloadVideo.DownloadedVideoFragment
import com.go.videoeditor.downloadVideo.EditedVideoFragment
import com.go.videoeditor.downloadVideo.UploadedVideoFragment
import com.go.videoeditor.fragment.DraftSavedFragment
import com.go.videoeditor.utils.AppUtils
import com.go.videoeditor.upload.UploadManager
import com.go.videoeditor.user.TabPersonFragment
import com.go.videoeditor.utils.OptiCommonMethods
import com.go.videoeditor.utils.ScreenSize
import com.go.videoeditor.utils.SharedPreferencesManager
import kotlinx.android.synthetic.main.activity_main_old.*
import kotlinx.android.synthetic.main.layout_home_actionbar_ve.*
import kotlinx.android.synthetic.main.layout_main_menu_left_ve.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class MainActivityOld : BaseActivity() {
    var drawerToggle: ActionBarDrawerToggle? = null
    val TAG_NAME = this::class.simpleName
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_old)
        AppUtils.removeTempDownloadFolderPath(this)
//        ViewCompat.setTransitionName(findViewById(R.id.textLogo), "12345")
//        val transition = window.sharedElementEnterTransition
//        transition.duration = 500
//        transition.addListener(object : Transition.TransitionListener {
//            override fun onTransitionStart(transition: Transition?) {
//            }
//
//            override fun onTransitionEnd(transition: Transition?) {
//
//                initUI()
//                initData()
//                initControl()
//            }
//
//            override fun onTransitionCancel(transition: Transition?) {
//            }
//
//            override fun onTransitionPause(transition: Transition?) {
//            }
//
//            override fun onTransitionResume(transition: Transition?) {
//            }
//        })
        initUI()
        initData()
        initControl()
    }

    private fun initData() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            requestPermissions(
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                Constant.REQUEST_WRITE_EXTERNAL_STORAGE
            )
        }
        val pInfo = packageManager.getPackageInfo(packageName, 0)
        textVersion.text = pInfo.versionName ?: ""
    }

    private fun initUI() {
        drawerToggle = object :
            ActionBarDrawerToggle(this, drawerLayout, R.string.app_name, R.string.app_name) {
            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
            }

            override fun onDrawerClosed(drawerView: View) {
                super.onDrawerClosed(drawerView)
            }
        }
        drawerLayout?.addDrawerListener(drawerToggle as ActionBarDrawerToggle)
        drawerToggle?.syncState()
        setLayoutWidthLeftMenu()
    }

    private fun setLayoutWidthLeftMenu() {
        val screenSize = ScreenSize(this)
        val param = layoutMainLeft.layoutParams
        val leftWidth = screenSize.width * 3 / 4
        param.width = leftWidth
//        layoutMainLeft.layoutParams = param
//        MyApplication.getInstance()?.loadDrawableImageNow(this, R.drawable.bg_main, imageBgLeftMenu)

        val paramImageLogo: LinearLayout.LayoutParams =
            imageLogoLeftMenu.layoutParams as LinearLayout.LayoutParams
        val imageWidth = leftWidth * 3 / 5
        paramImageLogo.width = imageWidth
        paramImageLogo.height = imageWidth / 3
//        imageBgLeftMenu.layoutParams = paramImageLogo
    }

    fun initControl() {
        buttonLivestream.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                gotoActivity(LivestreamActivity::class.java)
            }, Constant.DELAY_HANDLE)
        }
        buttonGallery.setOnClickListener {
            openGallery()
        }
        buttonCamera.setOnClickListener {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                requestPermissions(
                    arrayOf(Manifest.permission.CAMERA),
                    Constant.REQUEST_CODE_CAMERA
                )
                return@setOnClickListener
            }
            openCamera()
        }
        buttonDraft.setOnClickListener {
            showDragSaveFragment()
        }
        buttonDownload.setOnClickListener {
            buttonDownloadClick()
        }
        buttonUploaded.setOnClickListener {
            buttonUploadedClick()
        }
        buttonTask?.setOnClickListener {
            showTaskBackground()
        }

        buttonDownloaded?.setOnClickListener {
            buttonDownloadedClick()
        }
        buttonEdited?.setOnClickListener {
            buttonEditedClick()
        }
        buttonMenu?.setOnClickListener {
            toogleLeftMenu()
        }
        buttonPerson?.setOnClickListener {
            if (drawerLayout!!.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            }
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                showPersonFragment()
            }, 100)
        }
        buttonNotification?.setOnClickListener {
//            Handler(Looper.getMainLooper()).postDelayed(Runnable {
//                val fragment = NotificationFragment()
//                showBottomSheetDialog(fragment)
//            }, 200)
//            val screenSize = ScreenSize(this)
//            val size = "${screenSize.width} x ${screenSize.height}"
//            showToast("Comming soon... $size")
        }
        buttonSearch?.setOnClickListener {
//            Handler(Looper.getMainLooper()).postDelayed(Runnable {
//                val fragment = SearchFragment()
//                showBottomSheetDialog(fragment)
//            }, 200)
        }

        buttonMenuShare?.setOnClickListener {
            drawerLayout.closeDrawers()
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                shareLinkApp()
            }, 200)
        }

        buttonMenuAbout?.setOnClickListener {

        }
    }

    private fun showDragSaveFragment() {
        val videoList = SharedPreferencesManager.getVideoList(this)
        if (videoList == null || videoList.size == 0) {
            showDialog(getString(R.string.msg_not_video_draft))
            return
        }
        val fragment = DraftSavedFragment()
        fragment.show(supportFragmentManager, fragment.tag)
    }


    fun buttonDownloadClick() {
        val fragment = DownloadVideoFragment()
        showBottomSheetDialog(fragment, null)
    }

    fun buttonUploadedClick() {
        val fragment = UploadedVideoFragment()
        showBottomSheetDialog(fragment, null)
    }

    fun buttonDownloadedClick() {
        /* val folderFile = FileUltis.getDownloadFolderPath()
         if (!folderFile.exists() || folderFile.listFiles() == null || folderFile.listFiles().size == 0) {
             showToast("Hiện chưa có video nào được chỉnh sửa")
             return
         }*/
        val fragment = DownloadedVideoFragment()
        showBottomSheetDialog(fragment, null)
    }

    fun buttonEditedClick() {
        /*val folderFile = FileUltis.getEditedFolderPath()
        if (!folderFile.exists() || folderFile.listFiles() == null || folderFile.listFiles().size == 0) {
            showToast("Hiện chưa có video nào được chỉnh sửa")
            return
        }*/
        val fragment = EditedVideoFragment()
        showBottomSheetDialog(fragment, null)
    }

    fun toogleLeftMenu() {
        if (drawerLayout!!.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers()
        } else {
            drawerLayout.openDrawer(GravityCompat.START)
        }
    }

    fun openGallery() {
        refreshGalleryAlone()
        val i = Intent(
            Intent.ACTION_PICK,
            MediaStore.Video.Media.EXTERNAL_CONTENT_URI
        )
        i.type = "video/*"
        i.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("video/*"))
        startActivityForResult(i, Constant.REQUEST_CODE_GALLERY)
    }

    var videoFile: File? = null
    fun openCamera() {
        val intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        if (intent.resolveActivity(packageManager) != null) {
            var videoFile: File? = null
            try {
                videoFile = createFile()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            if (videoFile != null) {
                val videoURI: Uri = FileProvider.getUriForFile(
                    this, packageName + ".provider",
                    videoFile
                )
                this.videoFile = videoFile
                intent.putExtra(MediaStore.EXTRA_OUTPUT, videoURI)
                intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 240) //4 minutes
//                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1)
                startActivityForResult(intent, Constant.REQUEST_CODE_CAMERA)
            }
        }
    }

    private fun createFile(): File? {
        val timeStamp = SimpleDateFormat("ddMMyyyyHHmmss").format(Date())
        val mFileName = "MDT_" + timeStamp + "_"
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_MOVIES)
        if (storageDir == null) {
            return null
        }
        return File.createTempFile(mFileName, ".mp4", storageDir)
    }

    fun callPermissionSettings() {
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.fromParts("package", applicationContext.packageName, null)
        intent.data = uri
        startActivityForResult(intent, 300)
    }

    fun refreshGalleryAlone() {
        try {
            val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
            sendBroadcast(mediaScanIntent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setFilePath(resultCode: Int, data: Intent, requestCode: Int) {
        if (resultCode == RESULT_OK) {
            try {
                val selectedVideo = data.data
                val filePath = OptiCommonMethods.getRealPathFromUri(this, selectedVideo)
                if (filePath.isNullOrEmpty()) {
                    return
                }
                Log.e("TrimVideo", "filePath: $filePath")
                if (requestCode == Constant.REQUEST_CODE_GALLERY) {
                    val videoFile = File(filePath)
                    val extension = OptiCommonMethods.getFileExtension(videoFile.absolutePath)
                    val timeInMillis =
                        OptiCommonMethods.getVideoDuration(this, videoFile)
                    Log.e("TrimVideo", "timeInMillis: $timeInMillis")
                    val duration = OptiCommonMethods.convertDurationInMin(timeInMillis)
                    Log.e("TrimVideo", "duration: $duration")
                    val second = TimeUnit.MILLISECONDS.toSeconds(timeInMillis)
                    Log.e("TrimVideo", "second: $second")

                    //check if video is more than 4 minutes
                    if (duration < Constant.VIDEO_LIMIT) {
                        //check video format before playing into exoplayer
                        if (extension == Constant.AVI_FORMAT) {
//                                convertAviToMp4() //avi format is not supported in exoplayer
                            showToast("Định dạng file này chưa được hỗ trợ")
                        } else {
                            /*playbackPosition = 0
                            currentWindow = 0
                            initializePlayer()*/
//                                getAllFrameVideo()
                            val intent = Intent(this, VideoEditorActivity::class.java)
                            val bundle = Bundle()
                            bundle.putString(Constant.FILE_PATH, filePath)
                            intent.putExtras(bundle)
                            Loggers.e("START", "MainActivity")
                            startActivity(intent)
                        }
                    } else {
                        Toast.makeText(
                            this, "Độ dài video tối đa là 4 phút",
                            Toast.LENGTH_SHORT
                        ).show()

                        /* isLargeVideo = true
                         val uri = Uri.fromFile(masterVideoFile)
                         val intent = Intent(context, OptiTrimmerActivity::class.java)
                         intent.putExtra("VideoPath", filePath)
                         intent.putExtra(
                             "VideoDuration",
                             OptiCommonMethods.getMediaDuration(context, uri)
                         )
                         startActivityForResult(intent, OptiConstant.MAIN_VIDEO_TRIM)*/
                    }
                }


                /*val filePathColumn = arrayOf(MediaStore.MediaColumns.DATA)
                val cursor = contentResolver
                    .query(selectedVideo!!, filePathColumn, null, null, null)
                if (cursor != null) {
                    cursor.moveToFirst()
                    val columnIndex = cursor
                        .getColumnIndex(filePathColumn[0])
                    val filePath = cursor.getString(columnIndex)
                    cursor.close()

                }*/
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            Constant.REQUEST_WRITE_EXTERNAL_STORAGE, Constant.REQUEST_CODE_CAMERA -> {
                for (permission in permissions) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                        Toast.makeText(this, R.string.msg_accept_permission, Toast.LENGTH_SHORT)
                            .show()
                        if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            finish()
                        }
                        break
                    } else {
                        if (ActivityCompat.checkSelfPermission(
                                this as Activity,
                                permission
                            ) == PackageManager.PERMISSION_GRANTED
                        ) {
                            if (requestCode == Constant.REQUEST_CODE_CAMERA) {
                                openCamera()
                            }
                        } else {
                            callPermissionSettings()
                        }
                    }
                }
                return
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_CANCELED) return
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, intent)
        }
        when (requestCode) {
            Constant.REQUEST_CODE_GALLERY -> {
                data?.let {
                    setFilePath(resultCode, it, Constant.REQUEST_CODE_GALLERY)
                }
            }
            Constant.REQUEST_CODE_CAMERA -> {
                if (videoFile == null) {
                    return
                }
                /*var newIntent = data
                if (newIntent == null) {
                    newIntent = Intent()
                }
                val uri = Uri.fromFile(videoFile)
                newIntent.data = uri
                setFilePath(resultCode, newIntent, requestCode)*/

                val intent = Intent(this, VideoEditorActivity::class.java)
                val bundle = Bundle()
                bundle.putString(Constant.FILE_PATH, videoFile.toString())
                intent.putExtras(bundle)
                startActivity(intent)
            }
        }
    }

    var tabPersonFragment: TabPersonFragment? = null
    var isTabPersonFragmentShowing = false
    fun showPersonFragment() {
        if (isTabPersonFragmentShowing) {
            return
        }
        isTabPersonFragmentShowing = true
        tabPersonFragment = TabPersonFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.fragment_drop_down, R.anim.fragment_drop_up)
        fragmentTransaction.add(R.id.frameLayoutDialog, tabPersonFragment!!)
        fragmentTransaction.commit()
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            tabPersonFragment!!.checkRefresh()
        }, 200)
    }

    fun refreshTabPersonFragment() {
        if (isTabPersonFragmentShowing && tabPersonFragment != null) {
            runOnUiThread { tabPersonFragment!!.initLogin() }
        }
        /*for (fragment in fragmentList) {
            if (fragment is TabPersonFragment) {
                val childFragment = fragment as TabPersonFragment
                runOnUiThread { childFragment.initLogin() }
                break
            }
        }*/
    }

    fun removePersonFragment(): Boolean {
        if (isTabPersonFragmentShowing && tabPersonFragment != null) {
            if (tabPersonFragment!!.isPopupMenuChangeAvatarShowing()) {
                return true
            }
            if (tabPersonFragment!!.isPopupMenuChangeGenderShowing()) {
                return true
            }
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(
                R.anim.fragment_drop_down,
                R.anim.fragment_drop_up
            )
            fragmentTransaction.remove(tabPersonFragment!!)
            fragmentTransaction.commit()
            isTabPersonFragmentShowing = false
            tabPersonFragment = null
            return true
        }
        return false
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isTabPersonFragmentShowing) {
                checkExitApp()
                removePersonFragment()
                return true
            }
        }
        return super.onKeyUp(keyCode, event)
    }

    override fun onDestroy() {
        PRDownloader.cancelAll()
        PRDownloader.shutDown()
        UploadManager.getInstance()?.cancelAll()
        AppUtils.removeTempDownloadFolderPath(this)
        super.onDestroy()
    }

    override fun onBackPressed() {
        if (isTabPersonFragmentShowing) {
            checkExitApp()
            removePersonFragment()
            return
        }
        super.onBackPressed()
    }

}