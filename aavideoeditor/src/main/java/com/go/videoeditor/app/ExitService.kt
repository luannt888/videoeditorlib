package com.go.videoeditor.app

import android.app.Service
import android.content.Intent
import android.os.IBinder
import androidx.annotation.Nullable
import com.downloader.PRDownloader
import com.go.videoeditor.convert.ConvertManager
import com.go.videoeditor.upload.UploadManager

class ExitService : Service() {
    companion object{
        var hasConnect:Boolean = false
    }
    @Nullable
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        println("onTaskRemoved called")
        super.onTaskRemoved(rootIntent)
        Loggers.e("ExitService", "onTaskRemoved")
        PRDownloader.cancelAll()
        PRDownloader.shutDown()
        ConvertManager.getInstance()?.cancelAll()
        UploadManager.getInstance()!!.cancelAll()
        //do something you want before app closes.
        //stop service
        this.stopSelf()
    }
}
