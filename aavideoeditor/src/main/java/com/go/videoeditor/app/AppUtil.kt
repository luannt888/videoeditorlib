package com.go.videoeditor.app

import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.os.Build
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.fragment.app.FragmentActivity
import androidx.multidex.MultiDexApplication
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.go.videoeditor.R
import java.io.File

class AppUtil {
    companion object{
        fun loadImage(context: Context, imageView: ImageView, bitmap: Bitmap) {
            val options: RequestOptions = RequestOptions()
                .priority(Priority.IMMEDIATE)
                .placeholder(R.drawable.img_loading)
                .error(R.drawable.img_loading)
            Glide.with(context).load(bitmap).apply(options).into(imageView)
        }

        fun loadImage(context: Context, imageView: ImageView, drawableId: Int) {
            val options: RequestOptions = RequestOptions()
                .priority(Priority.IMMEDIATE)
                .placeholder(R.drawable.img_loading)
                .error(R.drawable.img_loading)
            Glide.with(context).load(drawableId).apply(options).into(imageView)
        }
        fun isNetworkConnect(context: Context): Boolean {
            val connMgr =
                context.getSystemService(MultiDexApplication.CONNECTIVITY_SERVICE) as ConnectivityManager
                    ?: return true
            val networkInfo = connMgr.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnected
        }
        fun isEmpty(data: String?): Boolean {
            return data == null || data.trim { it <= ' ' }.isEmpty()
        }
        private var baseApiUrl = "http://videoeditor.mediatech.vn/api/"

        fun getBaseApiUrl(): String? {
            return baseApiUrl
        }

        fun setBaseApiUrl(baseApiUrl: String?) {
            this.baseApiUrl = baseApiUrl!!
        }

        fun loadDrawableLayout(context: Context?, drawableId: Int, layout: RelativeLayout) {
            val options = RequestOptions()
                .priority(Priority.NORMAL)
            Glide.with(context!!).load(drawableId).apply(options)
                .into(object : CustomTarget<Drawable?>() {
                    override fun onResourceReady(
                        resource: Drawable,
                        transition: Transition<in Drawable?>?
                    ) {
                        layout.background = resource
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                    }
                })
        }

        fun loadDrawableLayout(context: Context?, url: String?, layout: RelativeLayout) {
            val options = RequestOptions()
                .priority(Priority.NORMAL)
            Glide.with(context!!).load(url).apply(options).into(object : CustomTarget<Drawable?>() {
                override fun onResourceReady(
                    resource: Drawable,
                    transition: Transition<in Drawable?>?
                ) {
                    layout.background = resource
                }

                override fun onLoadCleared(placeholder: Drawable?) {
                }
            })
        }

        fun loadDrawableImage(context: Context?, drawableId: Int, imageView: ImageView?) {
            if (imageView == null) {
                return
            }
            val options = RequestOptions()
                .priority(Priority.HIGH)
            Glide.with(context!!).load(drawableId).apply(options).into(imageView)
        }

        fun loadDrawableImageNow(context: Context?, drawableId: Int, imageView: ImageView?) {
            if (imageView == null) {
                return
            }
            val options = RequestOptions()
                .priority(Priority.IMMEDIATE)
            Glide.with(context!!).load(drawableId).apply(options).into(imageView)
        }

        fun loadImageDrawable(context: Context?, imageView: ImageView?, drawableId: Int) {
            if (imageView == null) {
                return
            }
            val options = RequestOptions()
                .priority(Priority.HIGH)
            Glide.with(context!!).load(drawableId).apply(options).into(imageView)
        }

        fun loadImage(context: Context?, imageView: ImageView?, url: String?) {
            if (imageView == null || isEmpty(url)) {
                return
            }
            val options = RequestOptions()
                .priority(Priority.IMMEDIATE)
                .placeholder(R.drawable.img_loading)
                .error(R.drawable.img_loading)
            Glide.with(context!!).load(url).apply(options).into(imageView)
        }

        fun loadImage(context: Context?, imageView: ImageView?, file: File?, isCircle: Boolean) {
            var options = RequestOptions()
                .priority(Priority.IMMEDIATE)
            if (isCircle) {
                options = options.transform(CircleCrop())
            }
            Glide.with(context!!).load(file).apply(options).into(imageView!!)
        }

        fun loadImage(context: Context?, imageView: ImageView?, bitmap: Bitmap?, isCircle: Boolean) {
            var options = RequestOptions()
                .priority(Priority.IMMEDIATE)
            if (isCircle) {
                options = options.transform(CircleCrop())
            }
            Glide.with(context!!).load(bitmap).apply(options).into(imageView!!)
        }

        fun loadImage(
            context: Context?, imageView: ImageView?, url: String?,
            isRoundedCorner: Boolean
        ) {
            if (imageView == null || isEmpty(url)) {
                return
            }
            if (!isRoundedCorner) {
                loadImage(context, imageView, url)
                return
            }
            val options = RequestOptions().transform(
                CenterCrop(),
                RoundedCorners(7)
            )
                .priority(Priority.IMMEDIATE)
                .placeholder(R.drawable.img_loading)
                .error(R.drawable.img_loading)
            Glide.with(context!!).load(url).apply(options).into(imageView)
        }

        fun loadImageNoPlaceHolder(context: Context?, view: View?, drawableId: Int) {
            if (view == null || drawableId < 1) {
                return
            }
            val options = RequestOptions()
                .priority(Priority.NORMAL)
            Glide.with(context!!).load(drawableId).apply(options)
                .into(object : CustomTarget<Drawable?>() {
                    override fun onResourceReady(
                        resource: Drawable,
                        transition: Transition<in Drawable?>?
                    ) {
                        view.background = resource
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                    }
                })
        }

//    fun loadImageNoPlaceHolder(context: Context?, imageView: ImageView?, url: String?) {
//        if (imageView == null || isEmpty(url)) {
//            return
//        }
//        if (url != null && url.endsWith(".svg")) {
//            loadImageSVGNoPlaceHolder(context, imageView, url)
//            return
//        }
//        val options = RequestOptions()
//            .priority(Priority.IMMEDIATE)
//        Glide.with(context!!).load(url).apply(options).into(imageView)
//    }

        fun loadImageNoPlaceHolder(context: Context?, imageView: ImageView?, drawableId: Int) {
            if (imageView == null) {
                return
            }
            val options = RequestOptions()
                .priority(Priority.IMMEDIATE)
            Glide.with(context!!).load(drawableId).apply(options).into(imageView)
        }

        fun loadImageNoPlaceHolder(context: Context?, imageView: ImageView?, url: String?) {
            if (imageView == null) {
                return
            }
            val options = RequestOptions()
                .priority(Priority.IMMEDIATE)
            Glide.with(context!!).load(url).apply(options).into(imageView)
        }

//    fun loadImageSVGNoPlaceHolder(context: Context?, imageView: ImageView?, url: String?) {
//        val requestBuilder: RequestBuilder<PictureDrawable>
//        requestBuilder = Glide.with(context!!)
//            .`as`(PictureDrawable::class.java)
//            .transition(DrawableTransitionOptions.withCrossFade())
//            .listener(SvgSoftwareLayerSetter())
//        val uri = Uri.parse(url)
//        requestBuilder.load(uri).into(imageView!!)
//    }

        fun loadImage(
            context: Context?, imageView: ImageView?, url: String?,
            placeholderDrawableId: Int
        ) {
            val options = RequestOptions()
                .placeholder(placeholderDrawableId)
                .error(placeholderDrawableId)
                .priority(Priority.NORMAL)
            Glide.with(context!!).load(url).apply(options).into(imageView!!)
        }

        fun loadImageCircle(context: Context?, imageView: ImageView?, url: String?) {
            loadImageCircle(context, imageView, url, 0)
        }

        fun loadImageCircle(
            context: Context?, imageView: ImageView?, url: String?,
            placeholderDrawableId: Int
        ) {
            val options: RequestOptions
            options = if (placeholderDrawableId <= 0) {
                RequestOptions().transform(CircleCrop())
                    .priority(Priority.NORMAL)
            } else {
                RequestOptions().transform(CircleCrop())
                    .placeholder(placeholderDrawableId)
                    .error(placeholderDrawableId)
                    .priority(Priority.NORMAL)
            }
            Glide.with(context!!).load(url).apply(options).into(imageView!!)
        }

        fun loadDrawableId(
            context: Context?, drawableId: Int,
            onLoadImageListener: OnLoadImageListener?
        ) {
            val options = RequestOptions()
                .priority(Priority.HIGH)
            Glide.with(context!!).load(drawableId).apply(options)
                .into(object : CustomTarget<Drawable?>() {
                    override fun onResourceReady(
                        resource: Drawable,
                        transition: Transition<in Drawable?>?
                    ) {
                        onLoadImageListener?.onResourceReady(resource)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                    }
                })
        }

        fun loadImageListener(
            context: Context?, url: String?,
            onLoadImageListener: OnLoadImageListener?
        ) {
            val options = RequestOptions()
                .placeholder(R.drawable.img_loading)
                .error(R.drawable.img_loading)
                .priority(Priority.NORMAL)
            Glide.with(context!!).load(url).apply(options).into(object : CustomTarget<Drawable?>() {
                override fun onResourceReady(
                    resource: Drawable,
                    transition: Transition<in Drawable?>?
                ) {
                    onLoadImageListener?.onResourceReady(resource)
                }

                override fun onLoadCleared(placeholder: Drawable?) {
                }
            })
        }

        fun loadImageBitmapListener(
            context: Context?, url: String?,
            onLoadImageBitmapListener: OnLoadImageBitmapListener?
        ) {
            val options = RequestOptions()
                .priority(Priority.HIGH)
            Glide.with(context!!).asBitmap().load(url).apply(options)
                .into(object : CustomTarget<Bitmap?>() {
                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: Transition<in Bitmap?>?
                    ) {
                        onLoadImageBitmapListener?.onResourceReady(resource)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                    }

                })
        }

        fun loadImageBitmapListener(
            context: Context?, drawableId: Int,
            onLoadImageBitmapListener: OnLoadImageBitmapListener?
        ) {
            val options = RequestOptions()
                .priority(Priority.HIGH)
            Glide.with(context!!).asBitmap().load(drawableId).apply(options)
                .into(object : CustomTarget<Bitmap?>() {
                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: Transition<in Bitmap?>?
                    ) {
                        onLoadImageBitmapListener?.onResourceReady(resource)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                    }

                })
        }

//    fun loadImage(context: Context?, imageView: SimpleDraweeView, url: String?) {
//        if (isEmpty(url)) {
//            return
//        }
//        val uri = Uri.parse(url)
//        imageView.setImageURI(uri)
//    }

        interface OnLoadImageListener {
            fun onResourceReady(resource: Drawable?)
        }

        interface OnLoadImageBitmapListener {
            fun onResourceReady(resource: Bitmap?)
        }

        private var versionCode = 0

        fun getVersionCode(context: Context): Int {
            if (versionCode > 0) {
                return versionCode
            }
            versionCode = 0
            val packageInfo: PackageInfo?
            try {
                packageInfo = context.packageManager.getPackageInfo(
                    context.packageName, 0
                )
                if (packageInfo == null) {
                    return versionCode
                }
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
                    versionCode = packageInfo.longVersionCode.toInt()
                } else {
                    versionCode = packageInfo.versionCode
                }
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
                return versionCode
            }
            return versionCode
        }
    }
    fun setFullStatusTransparent(activity: FragmentActivity, layout: View?) {
        try {
            activity.window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            activity.window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            activity.window.decorView.systemUiVisibility = (
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
            activity.window.statusBarColor = Color.TRANSPARENT
        } catch (e: Exception) {
            e.printStackTrace()
        }
//        val bgColor = "#00000000"
//        setBackgroudColor(layout, bgColor)
        if (layout == null) {
            return
        }
        val statusBarHeight: Int = getStatusBarHeight(activity)
        if (layout.layoutParams is RelativeLayout.LayoutParams) {
            val params = layout.layoutParams as RelativeLayout.LayoutParams
            params.topMargin = statusBarHeight
            layout.layoutParams = params
        } else if (layout.layoutParams is LinearLayout.LayoutParams) {
            val params = layout.layoutParams as LinearLayout.LayoutParams
            params.topMargin = statusBarHeight
            layout.layoutParams = params
        }
    }

    private fun getStatusBarHeight(activity: FragmentActivity): Int {
        val res = activity.resources.getIdentifier("status_bar_height", "dimen", "android")
        var statusBarHeight = 0
        if (res != 0) {
            statusBarHeight = activity.resources.getDimensionPixelSize(res)
        }
        return statusBarHeight
    }

    fun setStatusBarColor(activity: FragmentActivity, isTextLight: Boolean, bgColor: String?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            if (!bgColor.isNullOrEmpty()) {
                activity.window.statusBarColor = Color.parseColor(bgColor)
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity.window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                try {
                    if (isTextLight) {
                        //                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                        activity.window.decorView.systemUiVisibility =
                            activity.window.decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv() //set status text  light
                    } else {
                        activity.window.decorView.systemUiVisibility =
                            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }
}