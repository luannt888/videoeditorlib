package com.go.videoeditor.app

class Constant {
    companion object {
        const val STATUS_BAR_MARGIN: String ="STATUS_BAR_MARGIN"
        const val AUTO_PLAY= "autoPlay"
        const val APP_NAME = "VideoEditor"
        const val DATA = "DATA"
        const val REQUEST_CODE_GALLERY = 100
        const val REQUEST_CODE_CAMERA = 101
        const val REQUEST_CODE_GALLERY_MULTIPLE_SELECT_VIDEO = 102
        const val REQUEST_CODE_GALLERY_SELECT_VIDEO_TO_INSERT = 104
        const val REQUEST_CODE_GALLERY_SELECT_MUSIC = 103
        const val REQUEST_DOWNLOAD = 105
        const val REQUEST_UPLOADED = 106
        const val REQUEST_WRITE_EXTERNAL_STORAGE = 108
        const val VIDEO_LIMIT = 100 //4 minutes
        const val DATE_FORMAT = "yyyyMMdd_HHmmss"
        const val VIDEO_FORMAT = ".mp4"
        const val AUDIO_FORMAT = ".mp3"
        const val AVI_FORMAT = ".avi"
        const val STYLE_HORIZONTAL = 1
        const val FILE_PATH = "FILE_PATH"
        const val PROJECT_NAME = "PROJECT_NAME"
        const val KEY = "KEY"
        const val WEB_USER_AGENT =
            "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.2.3986.149 Safari/517.32"

        const val KEY_TRIM = "KEY_TRIM"
        const val KEY_ADD_TEXT = "KEY_ADD_TEXT"
        const val KEY_ADD_LOGO = "KEY_ADD_LOGO"
        const val KEY_ADD_EFFECT = "KEY_ADD_EFFECT"
        const val KEY_ADD_MUSIC = "KEY_ADD_MUSIC"
        const val KEY_ROTATE = "KEY_ROTATE"
        const val KEY_CROP = "KEY_CROP"
        const val KEY_TIME_SCALE = "KEY_TIME_SCALE"

        const val KEY_SPLIT_TRIM = "_"
        const val KEY_SPLIT_DRAFT_VIDEO_ITEM_CONTENT = "__"
        const val KEY_SPLIT_DRAFT_VIDEO_ITEM = ","
        const val START = "start"
        const val END = "end"

        //Minimum crop time is 3s
        val minSelection = 3000 //3s

        // The maximum cutting time is 5min
        val maxSelection: Long = 5 * 60000 // up to 5 min
        var MSG_UPDATE = 1

        const val SECOND_A_FRAME = 5
        const val WIDTH_FRAME_DP = 50
        const val FRAME_COUNT_MAX = 20
        const val FRAME_COUNT_MIN = 10
        const val DURATION_MIN_SECOND = 25

        const val SCREEN_TYPE = "SCREEN_TYPE"
        const val TYPE_MERGE_VIDEO = 1
        const val TYPE_REPLACE_MUSIC = 2
        const val TYPE_INSERT_VIDEO = 3
        const val TYPE_PLAY_URL = 1
        const val TYPE_PLAY_FILE = -1
        const val TYPE_AUDIO = 2
        const val TYPE_VIDEO = 1

        const val TYPE_REGISTER_FORM = "form"
        const val TYPE_REGISTER_FACEBOOK = "facebook"
        const val TYPE_REGISTER_GOOGLE = "google"
        const val TYPE_FORGOT_PASSWORD = "forgot_password"
        const val TYPE_RESEND_OTP = "resend_otp"

//        const val API_CONFIG = "http://mdtv.mediatech.vn/api/config?app_id=Mj6kG4gJRW51E5L"
        const val API_CONFIG = "http://videoeditor.mediatech.vn/api/v1/public/config"
        const val API_SHARE = "v1/article/share"
        const val API_LIKE = "v1/article/like"
        const val API_USER_AUTO_LOGIN = "v1/oauth/auto"
        const val API_USER_LOGIN = "v1/oauth/login"
        const val API_USER_REGISTER = "v1/oauth/register"
        const val API_USER_LOGIN_SOCIAL = "v1/oauth/social"
        const val API_USER_FORGET_PASSWORD = "v1/oauth/forget"
        const val API_USER_SEND_OTP = "v1/oauth/sendotp"
        const val API_USER_VERIFY_OTP = "v1/oauth/verifyotp"
        const val API_USER_CHANGE_PASSSWORD = "v1/user/changePassword"
        const val API_USER_CHANGE_AVATAR = "v1/user/avatar"
        const val API_USER_UPDATE_PROFILE = "v1/user/update"
        const val API_MY_COMMENT = "v1/user/comment"
        const val API_PROVINCE = "v1/public/group"
        const val API_VIDEO_LIST = "v1/post/index"
//        const val API_UPLOADED_VIDEO_LIST = "v1/post/video"
        const val API_UPLOADED_VIDEO_LIST = "v1/article/list?"
//        const val API_UPLOAD_VIDEO = "v1/post/create"
        const val API_UPLOAD_VIDEO = "v1/article/create"
        const val API_CATEGORY = "v1/category"
        const val API_LIVE_CONFIG = "v1/channel/config"

        const val CONFIG = "CONFIG"
        const val USERNAME = "USERNAME"
        const val PASSWORD = "PASSWORD"
        const val USER_ID = "USER_ID"
        const val IMAGE = "IMAGE"
        const val IMG_LAUNCH = "IMG_LAUNCH"
        const val LOGO = "LOGO"
        const val LOCATION = "LOCATION"
        const val TIME_DELAY_LOAD_TAB = 200
        const val ACCESS_TOKEN = "ACCESS_TOKEN"
        const val LOGIN_TYPE = "LOGIN_TYPE"
        const val LOGIN_TYPE_NORMAL = 1
        const val LOGIN_TYPE_FACEBOOK = 2
        const val LOGIN_TYPE_GOOGLE = 3
        const val REQUEST_CODE_LOGIN = 100
        const val REQUEST_CODE_LOGIN_GOOGLE = 103
        const val REQUEST_CODE_NOTIFICATION = 188
        const val REQUEST_CODE_NOTIFY_LIVE = 189

        const val CODE_USER_LOGIN = 190
        const val CODE_VERIFY_OTP = 100
        const val CODE_ENTER_PHONE = 203
        const val CODE_REGISTER_FORM = 204
        const val CODE_RE_LOGIN = 401
        const val CODE_UPDATE_PROFILE = 205
        const val CODE_LOGIN = 401


        const val CODE_REQUIRE_LOGIN = -1010
        const val REQUEST_CODE_SPEAK = 101
        const val REQUEST_CODE_DETAIL_PRODUCT = 105
        const val IS_INTANCE_NEWS_FRAGMENT = "IS_INTANCE_NEWS_FRAGMENT"
        const val TAB_POSITION = "TAB_POSITION"
        const val TAB_TAG = "TAB_TAG"
        const val YOUTUBE_URL = "YOUTUBE_URL"
        const val IS_REGISTER = "IS_REGISTER"
        const val IS_SHOW_TOP_SPACE = "IS_SHOW_TOP_SPACE"
        const val IS_CHANGE_PASSWORD_SCREEN = "IS_CHANGE_PASSWORD_SCREEN"

        const val POST = 1
        const val GET = 2
        const val SUCCESS = 200
        const val CODE = "errCode"
        const val MESSAGE = "errMsg"
        const val RESULT = "result"
        const val NOTIFY = "notify"
        const val TAB_TYPE = "tab_type"
        const val TYPE = "type"

        const val DELAY_HANDLE = 150L
        const val STREAM_URL = "rtmp://103.90.223.2:1935/mdtvlive/group4KT4?token=1585212410-673118a3f1554f60d9ea911c77f63089"
        const val VIDEO_QUALITY_DEFAULT = "720P"
        const val AUDIO_QUALITY_DEFAULT = "HIGHT"

        const val FILE_TYPE_UNDEFINED = 0
        const val FILE_TYPE_VIDEO = 1
        const val FILE_TYPE_IMAGE = 2
        const val FILE_TYPE_AUDIO = 3
    }
}