package com.go.videoeditor.app;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

public class Loggers {
    private static final boolean DEBUG_MODE = true;

    public static void e(String TAG, String msg) {
        if (DEBUG_MODE) {
            Log.e(TAG, msg + "");
        }
    }
    public static void e(String TAG, int msg) {
        if (DEBUG_MODE) {
            Log.e(TAG, msg + "");
        }
    }
}