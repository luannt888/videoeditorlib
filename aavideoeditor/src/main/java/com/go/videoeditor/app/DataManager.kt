package com.go.videoeditor.app

//import vn.mediatech.istudiocafe.model.ItemNews
import com.go.videoeditor.model.ItemAppConfig
import com.go.videoeditor.user.ItemProvince
import com.go.videoeditor.user.ItemUser

class DataManager {
    var itemAppConfig: ItemAppConfig? = null
    var itemUserTemp: ItemUser? = null
    var itemUser: ItemUser? = null
    var isNeedRefreshLeftMenu: Boolean = false
    var itemProvinceList:ArrayList<ItemProvince>? = null
//    var itemCartList: ArrayList<ItemNews> = ArrayList()
}