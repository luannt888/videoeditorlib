package com.go.videoeditor.service

import okhttp3.MediaType
import okhttp3.RequestBody
import okio.BufferedSink
import okio.Source
import okio.source
import java.io.File
import java.io.IOException


class FileRequestBody(var mediaType: MediaType, val file: File, val fileRequestBodyHttpListener: FileRequestBodyHttpListener) :
    RequestBody() {
    override fun contentType(): MediaType? {
        return mediaType
    }

    private val SEGMENT_SIZE = 2048L

    @Throws(IOException::class)
    override fun contentLength(): Long {
        return if (file != null || file.exists()) {
            file.length()
        } else {
            0
        }
    }

    @Throws(IOException::class)
    override fun writeTo(sink: BufferedSink) {
        var source: Source? = null
        try {
            source = file.source()
            var total: Long = 0
            var read: Long
            while (source.read(sink.buffer, SEGMENT_SIZE).also { read = it } != -1L) {
                total += read
                sink.flush()
                fileRequestBodyHttpListener?.onProgress(total, contentLength())
            }
        } finally {
            source?.close()
        }
    }

    interface FileRequestBodyHttpListener {
        fun onProgress(readSize: Long, totalSixe: Long)
    }
}