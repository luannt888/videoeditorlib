package com.go.videoeditor.service;

import android.content.Context;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;

import com.go.videoeditor.app.Constant;
import com.go.videoeditor.app.Loggers;
import com.go.videoeditor.app.MyApplication;
import com.go.videoeditor.user.ItemUser;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class MyHttpRequest {
    private Context context;
    private OkHttpClient okHttpClient;
    private boolean hasLoaded;
    private String url;

    public MyHttpRequest(Context context) {
        this.context = context;
        getOkHttpClient();
    }

    public boolean isHasLoaded() {
        return hasLoaded;
    }

    private void getOkHttpClient() {
        /*okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();*/
        okHttpClient = getUnsafeOkHttpClient()
                .connectTimeout(200, TimeUnit.SECONDS)
                .writeTimeout(200, TimeUnit.SECONDS)
                .readTimeout(200, TimeUnit.SECONDS)
                .build();
    }

    public static OkHttpClient.Builder getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain
                                , String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain
                                , String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };
            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            return builder;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void cancel() {
        if (hasLoaded) {
            return;
        }
        try {
            okHttpClient.dispatcher().cancelAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void uploadImage(String url, String imagePath,
                            final ResponseListener responseListener) {
        Loggers.e("imagePath", imagePath);
        File file = new File(imagePath);
        Loggers.e("MyHttpRequest_url", "file name = " + file.getName());

        /*BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;
        Loggers.e("MyCheck_HttpRequest_uploadImage", imageWidth + " x " + imageHeight);*/

        ItemUser itemUser = MyApplication.Companion.getInstance().getDataManager().getItemUser();
        if (itemUser == null) {
            if (responseListener != null) {
                responseListener.onFailure(-1001);
            }
            return;
        }
        this.url = url;
        if (MyApplication.Companion.getInstance().isEmpty(url) || !url.startsWith("http")) {
            if (responseListener != null) {
                responseListener.onFailure(-100);
            }
            return;
        }
        if (okHttpClient == null) {
            okHttpClient = new OkHttpClient();
        }

        String mimeType = getMimeType(file);
        if (mimeType == null || mimeType.isEmpty()) {
            mimeType = "*/*";
        }
        RequestBody body = RequestBody.create(file, MediaType.parse(mimeType));

//        RequestBody image = RequestBody.create(file, MediaType.parse("image/*"));
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("image", file.getName(), body)
                .addFormDataPart("access_token", itemUser.getAccessToken())
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        final String finalUrl = url;
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Loggers.e("MyHttpRequest_url onFailure", "url = " + finalUrl);
                e.printStackTrace();
                if (responseListener != null) {
                    responseListener.onFailure(-101);
                }
                hasLoaded = true;
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (responseListener != null) {
                    if (response.isSuccessful() && response.body() != null) {
                        try {
                            String result = response.body().string();
                            Loggers.e("MyHttpRequest_url", finalUrl);
                            Loggers.e("MyHttpRequest_result", result);
                            Loggers.e("MyHttpRequest_result", "----------------");
                            responseListener.onSuccess(response.code(), result);
                        } catch (IOException e) {
                            e.printStackTrace();
                            responseListener.onFailure(response.code());
                        }
                    } else {
                        responseListener.onFailure(response.code());
                    }
                }
                hasLoaded = true;
            }
        });
    }

    public void uploadVideo(String url, String imagePath, String videoPath, String title, String nameFile, String content,
                            final ResponseListener responseListener, FileRequestBody.FileRequestBodyHttpListener fileRequestBodyHttpListener, String accessToken) {
        Loggers.e("imagePath", imagePath);
        File imageFile = new File(imagePath);
        File videoFile = new File(videoPath);
        Loggers.e("MyHttpRequest_url", "file name = " + imageFile.getName());
        String token = accessToken;
        if (accessToken == null || accessToken.isEmpty()) {
            ItemUser itemUser = MyApplication.Companion.getInstance().getDataManager().getItemUser();
            if (itemUser == null) {
                if (responseListener != null) {
                    responseListener.onFailure(-1001);
                }
                return;
            }
            token = itemUser.getAccessToken();
        }
        this.url = url;
        if (MyApplication.Companion.getInstance().isEmpty(url) || !url.startsWith("http")) {
            if (responseListener != null) {
                responseListener.onFailure(-100);
            }
            return;
        }
        if (okHttpClient == null) {
            okHttpClient = new OkHttpClient();
            okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(200, TimeUnit.SECONDS)
                    .writeTimeout(200, TimeUnit.SECONDS)
                    .readTimeout(200, TimeUnit.SECONDS)
                    .build();
        }

        String mimeType = getMimeType(imageFile);
        if (mimeType == null || mimeType.isEmpty()) {
            mimeType = "*/*";
        }
        RequestBody body = RequestBody.create(imageFile, MediaType.parse(mimeType));

//        RequestBody image = RequestBody.create(imageFile, MediaType.parse("image/*"));
//        RequestBody video = RequestBody.create(MediaType.parse("video/*"), videoFile);
        RequestBody video = new FileRequestBody(MediaType.parse("video/*"), videoFile, fileRequestBodyHttpListener);
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("image", imageFile.getName(), body)
                .addFormDataPart("file_link", videoFile.getName(), video)
                .addFormDataPart("access_token", token)
                .addFormDataPart("content_type", "video")
                .addFormDataPart("name", title)
                .addFormDataPart("content", content)
                .addFormDataPart("name_file", nameFile)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        final String finalUrl = url;
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Loggers.e("MyHttpRequest_url onFailure", "url = " + finalUrl);
                e.printStackTrace();
                if (responseListener != null) {
                    responseListener.onFailure(-101);
                }
                hasLoaded = true;
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (responseListener != null) {
                    if (response.isSuccessful() && response.body() != null) {
                        try {
                            String result = response.body().string();
                            Loggers.e("MyHttpRequest_url", finalUrl);
                            Loggers.e("MyHttpRequest_result", result);
                            Loggers.e("MyHttpRequest_result", "----------------");
                            responseListener.onSuccess(response.code(), result);
                        } catch (IOException e) {
                            e.printStackTrace();
                            responseListener.onFailure(response.code());
                        }
                    } else {
                        responseListener.onFailure(response.code());
                    }
                }
                hasLoaded = true;
            }
        });
    }

    public void request(boolean isPostMethod, String url, RequestParams requestParams,
                        boolean isOtherSite, final ResponseListener responseListener) {
        request(isPostMethod, url, requestParams, isOtherSite ? configHeaderYoutube() : null,
                responseListener);
    }

    public void request(boolean isPostMethod, String url, RequestParams requestParams,
                        final ResponseListener responseListener) {
        request(isPostMethod, url, requestParams, null, responseListener);
    }

    public void request(boolean isPostMethod, String url, RequestParams requestParams,
                        Headers headers, final ResponseListener responseListener) {
        this.url = url;
//        Loggers.e("My_check_request", "url = " + url);
        if (MyApplication.Companion.getInstance().isEmpty(url) || !url.startsWith("http")) {
            if (responseListener != null) {
                responseListener.onFailure(-100);
            }
            return;
        }
        if (okHttpClient == null) {
            okHttpClient = new OkHttpClient();
        }
        Request request;
        Request.Builder builder = new Request.Builder().headers(headers == null ? configHeader()
                : headers);
        requestParams = headers == null ? addDefaultRequestParam(requestParams) : null;

        boolean isFormData = true;
        if (!isPostMethod) {
            url = generateUrlByParam(url, requestParams);
            request = builder
                    .url(url)
                    .build();
        } else {
            if (!isFormData) {
                request = builder
                        .url(url)
                        .post(getParam(requestParams))
                        .build();
            } else {
                request = new Request.Builder()
                        .url(url)
                        .post(getParamFormData(requestParams))
                        .build();
            }
        }
        Loggers.e("MyHttpRequest_url_first_isPost = " + isPostMethod, url);

        final String finalUrl = url;
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull final IOException e) {
                Loggers.e("MyHttpRequest_url onFailure", "url = " + finalUrl);
                e.printStackTrace();
                if (responseListener != null) {
                    responseListener.onFailure(-101);
                }
                hasLoaded = true;
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull final Response response) throws IOException {
                if (responseListener != null) {
                    if (response.isSuccessful() && response.body() != null) {
                        try {
                            String result = response.body().string();
                            Loggers.e("MyHttpRequest_url", finalUrl);
                            Loggers.e("MyHttpRequest_result", result);
                            Loggers.e("MyHttpRequest_result", "----------------");
                            responseListener.onSuccess(response.code(), result);
                        } catch (IOException e) {
                            Loggers.e("MyHttpRequest_Exception", "IOException");
                            e.printStackTrace();
                            responseListener.onFailure(response.code());
                        }
                    } else {
                        Loggers.e("MyHttpRequest_onFailure_last", "response.isSuccessful() = " + response.isSuccessful());
                        responseListener.onFailure(response.code());
                    }
                }
                hasLoaded = true;
            }
        });
    }

    private RequestBody getParam(RequestParams requestParams) {
        if (requestParams == null) {
            return new FormBody.Builder().build();
        }
        ConcurrentHashMap<String, String> urlParams = requestParams.getUrlParams();
        FormBody.Builder builder = new FormBody.Builder();
        String pr = "";
        for (Map.Entry<String, String> entry : urlParams.entrySet()) {
            builder.add(entry.getKey(), entry.getValue());
            if (!pr.isEmpty()) {
                pr += " \n ";
            }
            pr += "key = " + entry.getKey() + " _value = " + entry.getValue();
        }
        Loggers.e("MyHttpRequest getParam", pr);
        return builder.build();
    }

    private RequestBody getParamFormData(RequestParams requestParams) {
        if (requestParams == null) {
            return new MultipartBody.Builder().build();
        }
        ConcurrentHashMap<String, String> urlParams = requestParams.getUrlParams();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        String pr = "";
        for (Map.Entry<String, String> entry : urlParams.entrySet()) {
            if (!pr.isEmpty()) {
                pr += " \n ";
            }

            String filePattern = "_file";
            if (!entry.getKey().endsWith(filePattern)) {
                builder.addFormDataPart(entry.getKey(), entry.getValue());
                pr += "key = " + entry.getKey() + " _value = " + entry.getValue();
            } else {
                String filePath = entry.getValue();
                if (filePath == null || filePath.isEmpty()) {
                    continue;
                }
                String fileName = entry.getKey().split(filePattern)[0];
                File file = new File(filePath);
                if (!file.exists()) {
                    Loggers.e("MyHttpRequest_file", "file not exists");
                    continue;
                }
                String mimeType = getMimeType(file);
                if (mimeType == null || mimeType.isEmpty()) {
                    mimeType = "*/*";
                }
//                RequestBody image = RequestBody.create(MediaType.parse("image/*"), file);
                RequestBody body = RequestBody.create(file, MediaType.parse(mimeType));
                pr += "key = " + fileName + " _value = " + entry.getValue();
                builder.addFormDataPart(fileName, file.getName(), body);
            }
        }
        Loggers.e("MyHttpRequest getParam", pr);
        return builder.build();
    }

    private String getExtension(String fileName) {
        char[] arrayOfFilename = fileName.toCharArray();
        for (int i = arrayOfFilename.length - 1; i > 0; i--) {
            if (arrayOfFilename[i] == '.') {
                return fileName.substring(i + 1, fileName.length());
            }
        }
        return "";
    }

    public String getMimeType(File file) {
        String mimeType = "";
        String extension = getExtension(file.getName());
        if (MimeTypeMap.getSingleton().hasExtension(extension)) {
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return mimeType;
    }

    private Headers configHeader() {
        /*HashMap<String, String> headers = new HashMap<>();
        String appId = MyApplication.getInstance()?.getAppId();
        String time = String.valueOf(Calendar.getInstance(Locale.ENGLISH).getTimeInMillis());
        String secretKey = MyApplication.getInstance()?.getSecretKey();
        String secretKeyEncrypt = md5(time + secretKey + appId);
        headers.put("Time", time);
        headers.put("Secret-Key", secretKeyEncrypt);
        return Headers.of(headers);*/
        HashMap<String, String> headers = new HashMap<>();
        headers.put("User-Agent", System.getProperty("http.agent"));
        return Headers.of(headers);
    }

    private Headers configHeaderYoutube() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("User-Agent", Constant.WEB_USER_AGENT);
        headers.put("Accept", "*/*");
        headers.put("Sec-Fetch-Site", "same-origin");
        headers.put("Sec-Fetch-Mode", "cors");
        headers.put("Accept-Language", "vi-VN,vi;q=0.9,fr-FR;q=0.8,fr;q=0.7,en-US;q=0.6,en;q=0.5");
        return Headers.of(headers);
    }

    private RequestParams addDefaultRequestParam(RequestParams requestParams) {
        if (requestParams == null) {
            requestParams = new RequestParams();
        }
        /*String appId = MyApplication.getInstance()?.getAppId();
        int versionCode = MyApplication.getInstance()?.getVersionCode(context);
        if (!MyApplication.getInstance()?.isEmpty(appId)) {
            requestParams.put("app_id", appId);
            requestParams.put("version_code", String.valueOf(versionCode));
        }
        requestParams.put("os_type", String.valueOf(Constant.OS_TYPE));*/
        return requestParams;
    }

    private String generateUrlByParam(String url, RequestParams requestParams) {
        if (url == null || url.isEmpty()) {
            return "";
        }
        if (requestParams == null) {
            return url;
        }
        ConcurrentHashMap<String, String> urlParams = requestParams.getUrlParams();
        String params = "";
        boolean flag = false;
        for (Map.Entry<String, String> entry : urlParams.entrySet()) {
            if (flag) {
                params += "&";
            }
            params += entry.getKey() + "=" + entry.getValue();
            flag = true;
        }
        if (!params.isEmpty()) {
            if (!url.contains("?")) {
                url += "?" + params;
            } else {
                url += "&" + params;
            }
        }
        return url;
    }

    private String md5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes(StandardCharsets.UTF_8));
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < array.length; ++i) {
                stringBuilder.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,
                        3));
            }
            return stringBuilder.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public interface ResponseListener {
        void onFailure(int statusCode);

        void onSuccess(int statusCode, String responseString);
    }

    public interface ResponseBodyHttpListener {
        void onFailure(int statusCode);

        void onSuccess(int statusCode, ResponseBody responseBody);
    }
}