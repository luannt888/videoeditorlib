package com.go.videoeditor.service

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.downloader.*
import com.downloader.request.DownloadRequest
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.activity.VideoEditorActivity
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.downloadVideo.ItemDownloadVideo
import com.go.videoeditor.listener.OnBaseRequestListener
import com.go.videoeditor.listener.OnDialogButtonListener
import com.go.videoeditor.listener.OnGetInfoListener
import com.go.videoeditor.model.ItemFile
import com.go.videoeditor.service.FileRequestBody.FileRequestBodyHttpListener
import com.go.videoeditor.service.MyHttpRequest.ResponseListener
import com.go.videoeditor.utils.AppUtils
import com.go.videoeditor.upload.ItemUploadVideo
import com.go.videoeditor.upload.UploadManager
import com.go.videoeditor.user.OnGetProvinceDataListener
import com.go.videoeditor.user.OnLoginFormListener
import com.go.videoeditor.utils.BackgroundExecutor
import com.go.videoeditor.utils.FileUltis
import com.go.videoeditor.utils.JsonParser.Companion.getInt
import com.go.videoeditor.utils.JsonParser.Companion.getItemProvinceList
import com.go.videoeditor.utils.JsonParser.Companion.getJsonArray
import com.go.videoeditor.utils.JsonParser.Companion.getJsonObject
import com.go.videoeditor.utils.JsonParser.Companion.getString
import com.go.videoeditor.utils.JsonParser.Companion.parseItemUserLogin
import com.go.videoeditor.utils.SharedPreferencesManager.Companion.getToken
import com.go.videoeditor.utils.SharedPreferencesManager.Companion.saveUser
import com.go.videoeditor.utils.VideoEditUtils
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

object ServiceUtil {
    fun addLike(context: Context?, id: String?) {
        if (MyApplication.getInstance()!!.isEmpty(id)) {
            return
        }
        val requestParams = RequestParams()
        requestParams.put("article_id", id)
        val myHttpRequest = MyHttpRequest(context)
        myHttpRequest.request(true, validAPI(Constant.API_LIKE), requestParams,
            object : ResponseListener {
                override fun onFailure(statusCode: Int) {}
                override fun onSuccess(statusCode: Int, responseString: String) {}
            })
    }

    fun addShare(context: Context?, id: String?) {
        if (MyApplication.getInstance()!!.isEmpty(id)) {
            return
        }
        val requestParams = RequestParams()
        requestParams.put("id", id)
        val myHttpRequest = MyHttpRequest(context)
        myHttpRequest.request(false, validAPI(Constant.API_SHARE), requestParams,
            object : ResponseListener {
                override fun onFailure(statusCode: Int) {}
                override fun onSuccess(statusCode: Int, responseString: String) {}
            })
    }

    fun addShareLinkApp(context: Context?) {
        val requestParams = RequestParams()
        requestParams.put("type", "share_app")
        requestParams.put("article_id", "-1")
        val myHttpRequest = MyHttpRequest(context)
        myHttpRequest.request(false, validAPI(Constant.API_SHARE), requestParams,
            object : ResponseListener {
                override fun onFailure(statusCode: Int) {}
                override fun onSuccess(statusCode: Int, responseString: String) {}
            })
    }

    fun addLikeLinkApp(context: Context?) {
        val requestParams = RequestParams()
        requestParams.put("type", "like")
        requestParams.put("article_id", "-1")
        val myHttpRequest = MyHttpRequest(context)
        myHttpRequest.request(false, validAPI(Constant.API_SHARE), requestParams,
            object : ResponseListener {
                override fun onFailure(statusCode: Int) {}
                override fun onSuccess(statusCode: Int, responseString: String) {}
            })
    }

    @JvmStatic
    fun validAPI(api: String): String {
        val baseUrl = MyApplication.getInstance()!!.getBaseApiUrl()
        return baseUrl + api
    }

    fun autoLogin(
        context: Context,
        loginFormListener: OnLoginFormListener?
    ) {
        val token = getToken(context)
        if (MyApplication.getInstance()!!.isEmpty(token)) {
            loginFormListener?.onLogin(false)
            return
        }
        val myHttpRequest = MyHttpRequest(context)
        val requestParams = RequestParams()
        requestParams.put("access_token", token)
        myHttpRequest.request(false,
            validAPI(Constant.API_USER_AUTO_LOGIN),
            requestParams,
            object : ResponseListener {
                override fun onFailure(statusCode: Int) {
                    loginFormListener?.onLogin(false)
                }

                override fun onSuccess(statusCode: Int, responseString: String) {
                    handleDataLogin(context, responseString, loginFormListener)
                }
            })
    }

    private fun handleDataLogin(
        context: Context, responseString: String,
        loginFormListener: OnLoginFormListener?
    ) {
        var responseString = responseString
        if (MyApplication.getInstance()!!.isEmpty(responseString)) {
            loginFormListener?.onLogin(false)
            return
        }
        responseString = responseString.trim { it <= ' ' }
        val jsonObject = getJsonObject(responseString)
        if (jsonObject == null) {
            loginFormListener?.onLogin(false)
            return
        }
        val errorCode = getInt(jsonObject, Constant.CODE)!!
        if (errorCode != Constant.SUCCESS) {
            saveUser(context, null, null)
            loginFormListener?.onLogin(false)
            return
        }
        val resultObj = getJsonObject(jsonObject, "result")
        val itemUser = parseItemUserLogin(resultObj)
        if (itemUser == null) {
            loginFormListener?.onLogin(false)
            return
        }
        saveUser(context, itemUser.phone, itemUser.accessToken)
        loginFormListener?.onLogin(true)
    }

    fun getNameFileUrl(url: String): String {
        val name: String
        name = url.substring(url.lastIndexOf("/"))
        return name
    }

    fun getVideoNameFileUrl(url: String): String {
        var name = getNameFileUrl(url)
        if (!name.endsWith(".mp4")) {
            name = "$name.mp4"
        }
        return name
    }

    fun updateUserProfile(
        activity: Activity, requestParams: RequestParams?,
        onBaseRequestListener: OnBaseRequestListener?
    ) {
        val myHttpRequest = MyHttpRequest(activity)
        myHttpRequest.request(true, validAPI(Constant.API_USER_UPDATE_PROFILE),
            requestParams,
            object : ResponseListener {
                override fun onFailure(statusCode: Int) {
                    onBaseRequestListener?.onResponse(
                        false,
                        activity.getString(R.string.msg_network_error), null
                    )
                }

                override fun onSuccess(statusCode: Int, responseString: String) {
                    handleDataUpdateUserProfile(
                        activity, responseString,
                        onBaseRequestListener
                    )
                }
            })
    }

    private fun handleDataUpdateUserProfile(
        activity: Activity,
        responseString: String,
        onBaseRequestListener: OnBaseRequestListener?
    ) {
        var responseString = responseString
        responseString = responseString.trim { it <= ' ' }
        val jsonObject = getJsonObject(responseString)
        if (jsonObject == null) {
            onBaseRequestListener?.onResponse(
                false,
                activity.getString(R.string.msg_empty_data), responseString
            )
            return
        }
        val errorCode = getInt(jsonObject, Constant.CODE)!!
        val message = getString(jsonObject, Constant.MESSAGE)
        if (errorCode != Constant.SUCCESS) {
            onBaseRequestListener?.onResponse(false, message, null)
            return
        }
        //        JSONObject resultObj = JsonParser.Companion.getJsonObject(jsonObject, "result");
//        JSONArray dataArr = JsonParser.Companion.getJsonArray(resultObj, "data");
//        ArrayList<ItemNews> itemList = JsonParser.Companion.parseItemNewsList(activity, dataArr);
        onBaseRequestListener?.onResponse(true, message, responseString)
    }

    @JvmStatic
    fun getDataProvince(
        context: Context,
        onGetProvinceDataListener: OnGetProvinceDataListener?
    ) {
        if (!MyApplication.getInstance()!!.isNetworkConnect()) {
            onGetProvinceDataListener?.onResponse(false)
            return
        }
        val myHttpRequest = MyHttpRequest(context)
        val requestParams = RequestParams()
        myHttpRequest.request(
            false,
            validAPI(Constant.API_PROVINCE),
            requestParams,
            object : ResponseListener {
                override fun onFailure(statusCode: Int) {
                    onGetProvinceDataListener?.onResponse(false)
                }

                override fun onSuccess(statusCode: Int, responseString: String) {
                    handleDataProvince(context, responseString, onGetProvinceDataListener)
                }
            })
    }

    private fun handleDataProvince(
        context: Context, responseString: String,
        onGetProvinceDataListener: OnGetProvinceDataListener?
    ) {
        var responseString = responseString
        if (MyApplication.getInstance()!!.isEmpty(responseString)) {
            onGetProvinceDataListener?.onResponse(false)
            return
        }
        responseString = responseString.trim { it <= ' ' }
        val jsonObject = getJsonObject(responseString)
        if (jsonObject == null) {
            onGetProvinceDataListener?.onResponse(false)
            return
        }
        val errorCode = getInt(jsonObject, Constant.CODE)!!
        if (errorCode != Constant.SUCCESS) {
            saveUser(context, null, null)
            onGetProvinceDataListener?.onResponse(false)
            return
        }
        val jsonArray = getJsonArray(jsonObject, "result")
        val itemList = getItemProvinceList(jsonArray)
        if (itemList == null || itemList.size == 0) {
            onGetProvinceDataListener?.onResponse(false)
            return
        }
        onGetProvinceDataListener?.onResponse(true)
        MyApplication.getInstance()?.getDataManager()?.itemProvinceList = itemList
    }

    val idBackgroundThreadList = ArrayList<String>()
    fun cancelAllGetInfo() {
        for (i in 0 until idBackgroundThreadList.size) {
            BackgroundExecutor.cancelAll(idBackgroundThreadList.get(i), true)
        }
        idBackgroundThreadList.clear()
    }

    fun getVideoInfo(
        itemList: ArrayList<ItemDownloadVideo>,
        onGetInfoListener: OnGetInfoListener?
    ) {
        for (i in itemList.indices) {
            val itemObj = itemList[i]
            val idName = "id_${Calendar.getInstance().timeInMillis}"
            idBackgroundThreadList.add(idName)
            BackgroundExecutor.execute(object : BackgroundExecutor.Task(idName, 0L, idName) {
                override fun execute() {
                    Loggers.e("getVideoInfo_$i", "execute:_ ${itemObj.title}")
                    if (itemObj.bitmap == null && (itemObj.duration == null || itemObj.duration == 0L)) {
                        try {
                            val mediaMetadataRetriever =
                                VideoEditUtils.getMediaMetadataRetriever(itemObj.url)
//                            itemObj.bitmap =
//                                VideoEditUtils.getBitmapThumbnail(mediaMetadataRetriever)
                            val timeInMillis =
                                VideoEditUtils.getDurationMsVideo(mediaMetadataRetriever)
                            mediaMetadataRetriever.release()
                            Loggers.e(
                                "getVideoInfo_ $i",
                                "executed:_ ${itemObj.title} _ $timeInMillis"
                            )
                            itemObj.duration = timeInMillis
                            if (onGetInfoListener != null) {
                                onGetInfoListener.onGet(i)
                                if (i == itemList.size - 1) {
                                    onGetInfoListener.onFinish()
                                }
                            }


//                        itemObj.title = videoFile.nameWithoutExtension
//                        UiThreadExecutor.runTask(idName, Runnable {
//                            videoServerListFragment?.notifyDataSetChanged()
//                        }, 0L)
                        } catch (e: Throwable) {
                            e.printStackTrace()
                        }
                    }
                }
            })
        }
    }

    val idBackgroundThreadVideoInfoList = ArrayList<String>()
    fun cancelAllGetVideoInfo() {
        for (i in 0 until idBackgroundThreadVideoInfoList.size) {
            BackgroundExecutor.cancelAll(idBackgroundThreadVideoInfoList.get(i), true)
        }
        idBackgroundThreadVideoInfoList.clear()
    }

    fun getVideoInfo(
        context: Context,
        itemList: ArrayList<ItemFile>,
        onGetInfoListener: OnGetInfoListener?
    ) {
        for (i in itemList.indices) {
            val itemObj = itemList[i]
            val idName = "id_${Calendar.getInstance().timeInMillis}"
            idBackgroundThreadVideoInfoList.add(idName)
            BackgroundExecutor.execute(object : BackgroundExecutor.Task(idName, 0L, idName) {
                override fun execute() {
                    Loggers.e("getVideoInfo_\$i", "execute done")
                    if (itemObj.bitmap == null && (itemObj.duration == null || itemObj.duration == 0L)) {
                        try {
                            val mediaMetadataRetriever =
                                VideoEditUtils.getMediaMetadataRetriever(context, itemObj.filePath)
//                            itemObj.bitmap =
//                                VideoEditUtils.getBitmapThumbnail(mediaMetadataRetriever)
                            val timeInMillis =
                                VideoEditUtils.getDurationMsVideo(mediaMetadataRetriever)
                            mediaMetadataRetriever.release()
                            itemObj.duration = timeInMillis
                            if (onGetInfoListener != null) {
                                onGetInfoListener.onGet(i)
                                if (i == itemList.size - 1) {
                                    onGetInfoListener.onFinish()
                                }
                            }


//                        itemObj.title = videoFile.nameWithoutExtension
//                        UiThreadExecutor.runTask(idName, Runnable {
//                            videoServerListFragment?.notifyDataSetChanged()
//                        }, 0L)
                        } catch (e: Throwable) {
                            e.printStackTrace()
                        }
                    }
                }
            })
        }
    }

    private var hasDownloaded = false

    /*@Throws(Exception::class)
    fun download(
        activity: Activity,
        url: String?,
        listener: OnDownloadListener?,
        onProgressListener: OnProgressListener?
    ): Int {
        val config = PRDownloaderConfig.newBuilder() //            .setDatabaseEnabled(true)
            .setReadTimeout(30000)
            .setConnectTimeout(30000)
            .build()
        PRDownloader.initialize(activity.applicationContext, config)
        val dirPath = FileUltis.getDownloadFolderPath().absolutePath
        val fileName = FileUltis.getVideoNameFileUrlOrPath(url)
        if (AppUtils.checkVideoDownloadFileExist(activity, fileName)) {
            listener?.onDownloadComplete()
            return -1
        }
        val downloadId = PRDownloader.download(url, dirPath, fileName)
            .build()
            .setOnCancelListener {
                if (listener != null) {
                    val error = Error()
                    error.serverErrorMessage = "Cancel download"
                    listener.onError(error)
                }
            }
            .setOnProgressListener(onProgressListener)
            .start(listener)
        if (!hasDownloaded) {
            hasDownloaded = true
            val intent = Intent(activity, ExitService::class.java)
            activity.startService(intent)
        }
        return downloadId
    }*/

    @Throws(Exception::class)
    fun downloadRequest(
        activity: Activity,
        url: String?, fileName: String?,
        listener: OnDownloadListener?,
        onProgressListener: OnProgressListener?
    ): DownloadRequest? {
        val config = PRDownloaderConfig.newBuilder()
            .setDatabaseEnabled(true)
            .setReadTimeout(30000)
            .setConnectTimeout(30000)
            .build()
        PRDownloader.initialize(activity.applicationContext, config)
//        val dirPath = FileUltis.getDownloadFolderPath().absolutePath
        val dirPath = AppUtils.getDownloadFolderPath(activity).absolutePath
        if (AppUtils.checkVideoDownloadFileExist(activity, fileName)) {
            listener?.onDownloadComplete()
            return null
        }
        val downloadRequest = PRDownloader.download(url, dirPath, fileName)
            .build()
        downloadRequest.setOnCancelListener {
            if (listener != null) {
                val error = Error()
                error.serverErrorMessage = "Cancel download"
                listener.onError(error)
            }
        }
            .setOnProgressListener(onProgressListener)
            .start(listener)
        (activity as? BaseActivity)?.startExitService()
        return downloadRequest
    }

    fun showDownloadSuccess(baseActivity: BaseActivity, fileName: String) {
        if (baseActivity.isDestroyed || baseActivity.isFinishing) {
            return
        }
        baseActivity.showDialog(true, R.string.notification,
            "Video \"$fileName\" đã được tải về thành công. Bắt đầu chỉnh sửa ?",
            R.string.cancel,
            R.string.ok, object : OnDialogButtonListener {
                override fun onLeftButtonClick() {}
                override fun onRightButtonClick() {
                    try {
                        val intent = Intent(baseActivity, VideoEditorActivity::class.java)
                        val bundle = Bundle()
                        bundle.putString(
                            Constant.FILE_PATH,
                            AppUtils.getVideoDownloadFile(baseActivity, fileName).absolutePath
//                            FileUltis.getVideoDownloadFile(fileName).toString()
                        )
                        intent.putExtras(bundle)
                        baseActivity.startActivity(intent)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        )
    }

    @Throws(IOException::class)
    fun uploadVideo(item: ItemUploadVideo, activity: Context) {
        val imageFile = FileUltis.getThumbnailFile(activity, item.filePath)
        item.thumbnail = imageFile.absolutePath
        val myHttpRequest = MyHttpRequest(activity)
        item.myHttpRequest = myHttpRequest
        Loggers.e("Upload", "onStart ")
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val handler = Handler(Looper.getMainLooper())
        (activity as? BaseActivity)?.startExitService()
        myHttpRequest.uploadVideo(
            validAPI(Constant.API_UPLOAD_VIDEO),
            imageFile.absolutePath,
            item.filePath, item.title, item.name, item.content, object : ResponseListener {
                override fun onFailure(statusCode: Int) {
                    handler.post {
                        UploadManager.getInstance()?.getItemUpload(item.id)?.listener?.onFailure(
                            statusCode
                        )
                    }
                }

                override fun onSuccess(statusCode: Int, responseString: String) {
                    handler.post {
                        UploadManager.getInstance()!!.getItemUpload(item.id)?.listener?.onSuccess(
                            statusCode,
                            responseString
                        )
                    }
                }
            }, object : FileRequestBodyHttpListener {
                override fun onProgress(readSize: Long, totalSixe: Long) {
                    handler.post {
                        UploadManager.getInstance()
                            ?.getItemUpload(item.id)?.progressListener?.onProgress(
                                readSize,
                                totalSixe
                            )
                    }
                }
            }, null
        )
    }
    @Throws(IOException::class)
    fun uploadVideo(item: ItemUploadVideo, activity: Context,accessToken: String?) {
        if(accessToken.isNullOrEmpty()){
            return
        }
        val imageFile = FileUltis.getThumbnailFile(activity, item.filePath)
        item.thumbnail = imageFile.absolutePath
        val myHttpRequest = MyHttpRequest(activity)
        item.myHttpRequest = myHttpRequest
        Loggers.e("Upload", "onStart ")
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val handler = Handler(Looper.getMainLooper())
        (activity as? BaseActivity)?.startExitService()
        myHttpRequest.uploadVideo(
            validAPI(Constant.API_UPLOAD_VIDEO),
            imageFile.absolutePath,
            item.filePath, item.title, item.name, item.content, object : ResponseListener {
                override fun onFailure(statusCode: Int) {
                    handler.post {
                        UploadManager.getInstance()?.getItemUpload(item.id)?.listener?.onFailure(
                            statusCode
                        )
                    }
                }

                override fun onSuccess(statusCode: Int, responseString: String) {
                    handler.post {
                        UploadManager.getInstance()!!.getItemUpload(item.id)?.listener?.onSuccess(
                            statusCode,
                            responseString
                        )
                    }
                }
            }, object : FileRequestBodyHttpListener {
                override fun onProgress(readSize: Long, totalSixe: Long) {
                    handler.post {
                        UploadManager.getInstance()
                            ?.getItemUpload(item.id)?.progressListener?.onProgress(
                                readSize,
                                totalSixe
                            )
                    }
                }
            }, accessToken)
        (activity as? BaseActivity)?.showTaskBackground()
    }
}