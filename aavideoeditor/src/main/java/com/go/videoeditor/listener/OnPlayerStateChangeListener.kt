package com.go.videoeditor.listener

import com.google.android.exoplayer2.ExoPlayer

interface OnPlayerStateChangeListener {
    fun playerState(isPlaying: Boolean)
    fun endVideo()
    fun onStateReady(player: ExoPlayer)
    fun onBuffering()
}