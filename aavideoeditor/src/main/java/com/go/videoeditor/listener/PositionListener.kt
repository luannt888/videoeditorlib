package com.go.videoeditor.listener

interface PositionListener {
    fun progress(position: Long)
}