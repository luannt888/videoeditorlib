package com.go.videoeditor.listener

import com.go.videoeditor.merge.ItemVideo

interface OnDismissListener {
    fun onDismiss(isSuccess: Boolean, data: String?, list: ArrayList<ItemVideo>?)
}