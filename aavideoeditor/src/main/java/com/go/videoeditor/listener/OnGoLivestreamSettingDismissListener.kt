package com.go.videoeditor.listener

import com.go.videoeditor.stream.ItemLivestreamConfig

interface OnGoLivestreamSettingDismissListener {
    fun onDismiss(itemLivestreamConfig: ItemLivestreamConfig?)
}