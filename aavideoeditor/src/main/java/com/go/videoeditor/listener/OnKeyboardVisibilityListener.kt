package com.go.videoeditor.listener

interface OnKeyboardVisibilityListener {
    fun onVisibilityChanged(visible: Boolean)
}