package com.go.videoeditor.listener

interface OnSimpleListviewListener {
    fun onItemClick(position: Int, title: String?)
}