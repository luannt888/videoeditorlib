package com.go.videoeditor.listener

interface OnNotifyDataChangeListener {
    fun onChange()
}