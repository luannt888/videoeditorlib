package com.go.videoeditor.listener

interface SingleCallback<T, V> {
    fun onSingleCallback(t: T, v: V)
}