package com.go.videoeditor.listener

interface OnAsyncTaskListener {
    fun onPreExecute()
    fun doInBackground(result: String)
}