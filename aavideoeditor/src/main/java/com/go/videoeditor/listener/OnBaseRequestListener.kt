package com.go.videoeditor.listener

interface OnBaseRequestListener {
    fun onResponse(isSuccess: Boolean, message: String?, response: String?)
}