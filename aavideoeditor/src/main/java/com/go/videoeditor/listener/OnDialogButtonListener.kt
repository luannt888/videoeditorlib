package com.go.videoeditor.listener

interface OnDialogButtonListener {
    fun onLeftButtonClick()
    fun onRightButtonClick()
}