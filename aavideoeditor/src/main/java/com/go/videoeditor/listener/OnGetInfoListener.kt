package com.go.videoeditor.listener

import com.go.videoeditor.downloadVideo.ItemDownloadVideo

interface OnGetInfoListener {
    fun onGet(position: Int)
    fun onFinish()
}