package com.go.videoeditor.listener

interface OnCancelListener {
    fun onCancel(isCancel: Boolean)
}