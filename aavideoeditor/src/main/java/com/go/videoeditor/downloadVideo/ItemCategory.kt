package com.go.videoeditor.downloadVideo

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemCategory(val id: Int?, val name: String?, val type: String?, val style: Int?) : Parcelable