package com.go.videoeditor.downloadVideo

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.adapter.ViewPagerAdapter
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.listener.OnDismissListener
import com.go.videoeditor.service.MyHttpRequest
import com.go.videoeditor.service.RequestParams
import com.go.videoeditor.service.ServiceUtil
import com.go.videoeditor.utils.JsonParser
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_tab_video_ve.*
import org.json.JSONArray

class VideoTabFragment : BottomSheetDialogFragment() {
    lateinit var adapterDownload: DownloadVideoAdapter
    var onDismissListener: OnDismissListener? = null
    var isSuccess = false
    var videoPath: String? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    val idBackgroundThreadList: ArrayList<String> = ArrayList()

    var isTabSelected: Boolean = false
    var isViewCreated: Boolean = false
    var isLoading: Boolean = false
    var myHttpRequest: MyHttpRequest? = null
    var itemList: ArrayList<ItemCategory>? = null
    val fragmentList: ArrayList<Fragment> = ArrayList()
    var tabType: Int = 0
    var isTabProduct = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tab_video_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = false
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        isViewCreated = true
        checkRefresh()
    }


    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 1000)
            return
        }
        init()
    }

    fun init() {
        if (isTabSelected) {
            return
        }
        isTabSelected = true
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
    }

    fun initData() {
//        tabType = arguments?.getInt(Constant.TYPE, Constant.TAB_TYPE_VIDEO)!!
//        isTabProduct = tabType == Constant.TAB_TYPE_PRODUCT
//        val topSpace = arguments?.getBoolean(Constant.IS_SHOW_TOP_SPACE, false)
//        if (topSpace!!) {
////            val lp: ViewGroup.LayoutParams = layoutFragmentRoot.layoutParams
////            layoutActionbar
//            val topSpacePadding = (activity as MainActivity).topSpace
//            Loggers.e("TabNewsFrg", "topSpacePadding = $topSpacePadding")
//            layoutFragmentRoot.setPadding(0, topSpacePadding, 0, 0)
//        }
        setData()
//        getData()
    }

    private fun setData() {
        itemList = ArrayList()
        for(i in 0 until 5){
            val item = ItemCategory(1,"CachingUp","video", 1)
            itemList?.add(item)
        }
        initPager()
    }


    fun showErrorNetwork(message: String) {
        activity?.runOnUiThread {
            if (itemList == null || itemList!!.size <= 0) {
                textNotify.visibility = View.VISIBLE
                textNotify.text = message
            }
//            progressBar.visibility = View.GONE
        }
    }

    fun getData() {
        if (isLoading) {
            return
        }
        isLoading = true
        if (!MyApplication.getInstance()!!.isNetworkConnect()) {
            isLoading = false
            showErrorNetwork(getString(R.string.msg_network_error))
            return
        }
//        progressBar.visibility = View.VISIBLE
        textNotify.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
        var type = ""
        val requestParams = RequestParams()
//        when (tabType) {
//            Constant.TAB_TYPE_EVENT -> {
//                type = "event"
//            }
//            Constant.TAB_TYPE_VIDEO -> {
//                type = "video"
//            }
//            Constant.TAB_TYPE_GALLERY -> {
//                type = "photo"
//            }
//            Constant.TAB_TYPE_PRODUCT -> {
//                type = "product"
//            }
//        }
        if (!type.isEmpty()) {
            requestParams.put("type", type)
        }
        myHttpRequest!!.request(false, ServiceUtil.validAPI(Constant.API_CATEGORY), requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (activity?.isFinishing!! || activity?.isDestroyed!!) {
                    return
                }
                isLoading = false
                showErrorNetwork(getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (activity?.isFinishing!! || activity?.isDestroyed!!) {
                    return
                }
                handleData(responseString)
                activity?.runOnUiThread {
//                    progressBar.visibility = View.GONE
                }
                isLoading = false
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            activity?.runOnUiThread { (activity as? BaseActivity)?.showDialog(message) }
            return
        }
        val resultObj = JsonParser.getJsonObject(jsonObject, "result")
        val dataArray: JSONArray? = JsonParser.getJsonArray(resultObj, "data")
        itemList = JsonParser.parseItemCategoryList(dataArray)
        if (itemList == null || itemList?.size == 0) {
            activity?.runOnUiThread {
                textNotify.text = getString(R.string.msg_empty_data)
                textNotify.visibility = View.VISIBLE
            }
            return
        }
        initPager()
    }

    fun initPager() {
        val tabTitleList: ArrayList<String> = ArrayList()
        for (i in 0 until itemList!!.size) {
            val itemCategory: ItemCategory = itemList!!.get(i)
            if (itemCategory.name.isNullOrEmpty()) {
                continue
            }
//            val fragment = VideoServerListFragment()
           /* val fragment = (activity!! as MainActivity).downloadClick()
            val bundle = Bundle()
            bundle.putInt(Constant.TYPE, tabType)
            bundle.putParcelable(Constant.DATA, itemCategory)
            fragment?.arguments = bundle
            fragmentList.add(fragment!!)
            tabTitleList.add(itemCategory.name)*/
        }
        activity?.runOnUiThread {
            layoutTab.visibility = View.VISIBLE
            layoutTab.visibility = if (fragmentList.size > 1) View.VISIBLE else View.GONE
            val viewPagerAdapter = ViewPagerAdapter(childFragmentManager, fragmentList, tabTitleList)
            viewPager.adapter = viewPagerAdapter
            tabLayout.setupWithViewPager(viewPager)
            viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                }

                override fun onPageSelected(position: Int) {
                    handleOnPageSelected(position)
                }

                override fun onPageScrollStateChanged(state: Int) {
                }
            })
            handleOnPageSelected(0)
        }
    }

    fun handleOnPageSelected(position: Int) {
        val fragment = fragmentList.get(position)
        (fragment as DownloadVideoFragment).checkRefresh()

    }

    fun initControl() {
        textNotify.setOnClickListener {
            getData()
        }

    }



    override fun onDestroy() {
        myHttpRequest?.cancel()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
    }

}