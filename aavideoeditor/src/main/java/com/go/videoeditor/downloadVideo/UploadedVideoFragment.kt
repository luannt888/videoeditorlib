package com.go.videoeditor.downloadVideo

import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.listener.OnDismissListener
import com.go.videoeditor.service.MyHttpRequest
import com.go.videoeditor.service.RequestParams
import com.go.videoeditor.service.ServiceUtil
import com.go.videoeditor.utils.BackgroundExecutor
import com.go.videoeditor.utils.JsonParser
import com.go.videoeditor.utils.SharedPreferencesManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_download_video_list_ve.*
import kotlinx.android.synthetic.main.fragment_video_list_ve.*
import kotlinx.android.synthetic.main.fragment_video_list_ve.layoutLoadMore
import kotlinx.android.synthetic.main.fragment_video_list_ve.layoutRefresh
import kotlinx.android.synthetic.main.fragment_video_list_ve.progressBar
import kotlinx.android.synthetic.main.fragment_video_list_ve.recyclerView
import kotlinx.android.synthetic.main.fragment_video_list_ve.textNotify
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*

class UploadedVideoFragment : BottomSheetDialogFragment() {
    private var loadMoreAble: Boolean = true
    private var isLoading: Boolean = false
    private var isViewCreated: Boolean = false
    lateinit var adapter: DownloadVideoAdapter
    var itemList: ArrayList<ItemDownloadVideo> = ArrayList()
    var onDismissListener: OnDismissListener? = null
    var isSuccess = false
    var videoPath: String? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    val idBackgroundThreadList: ArrayList<String> = ArrayList()
    var hasLayoutAnimation = false
     var acessToken:String? =null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_video_list_ve, container, false)
    }

    companion object {
        fun show(activity: FragmentActivity?, acessToken: String?): UploadedVideoFragment? {
            if (activity == null || acessToken.isNullOrEmpty()) {
                return null
            }
            val bundle = Bundle()
            bundle.putString(Constant.ACCESS_TOKEN, acessToken)
            val fragment = UploadedVideoFragment()
            fragment.arguments = bundle
            fragment.show(activity.supportFragmentManager, UploadedVideoFragment.toString())
            return fragment
        }

        fun newInstance(acessToken: String?) = UploadedVideoFragment().apply {
            if (!acessToken.isNullOrEmpty()) {
                arguments= Bundle().apply {
                    putString(Constant.ACCESS_TOKEN, acessToken)
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = true
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        isViewCreated = true
        checkRefresh()
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 1000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
        buttonDone.visibility = View.GONE
        textHeaderTitle.text = getString(R.string.list_video_uploaded)
        textHeaderTitle.setTextColor(ContextCompat.getColor(activity!!, R.color.dark_text))
        buttonClose.setImageResource(R.mipmap.ic_close_ve)
        initAdapter()
    }

    fun initData() {
        arguments?.let {
            acessToken = it.getString(Constant.ACCESS_TOKEN)
        }
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            if (isDetached) {
                return@Runnable
            }
            getData()
        }, 200)
    }

    fun initAdapter() {
        val style = Constant.STYLE_HORIZONTAL
        adapter = DownloadVideoAdapter(activity!!, itemList, style, 1)
        adapter.type = Constant.REQUEST_UPLOADED
        val layoutManager = StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
        recyclerView.scheduleLayoutAnimation()
//        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                super.onScrolled(recyclerView, dx, dy)
////                val firstPos = layoutManager.findFirstCompletelyVisibleItemPositions(null)[0]
////                bottomSheetDialog?.behavior?.isDraggable = if (firstPos < 0 || firstPos == 0) true else false
//            }
//        })
        adapter.onItemClickListener = object : DownloadVideoAdapter.OnItemClickListener {
            override fun onEditClick(itemDownloadObject: ItemDownloadVideo, position: Int) {
                dismiss()
            }
        }
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
//                onScrolledListener?.onScrolled(recyclerView, dx, dy)
                if (isLoading || !loadMoreAble) {
                    return
                }
                val firstPos = layoutManager.findFirstCompletelyVisibleItemPositions(null)[0]
                layoutRefresh.isEnabled = firstPos == 0

                val visibleItemCount = layoutManager.childCount
                val firstVisible = layoutManager.findFirstVisibleItemPositions(null)[0]
                val totalItem = layoutManager.itemCount
                val spanCount = layoutManager.spanCount + 1
                if (visibleItemCount + firstVisible >= totalItem - spanCount) {
                    layoutLoadMore.visibility = View.VISIBLE
                    getData()
                }
            }
        })
    }

    fun initControl() {
        buttonClose.setOnClickListener {
            isSuccess = false
            dismiss()
        }
        layoutRefresh.setOnRefreshListener {
            if (isLoading) {
                return@setOnRefreshListener
            }
            publishDate = ""
            itemList.clear()
            layoutLoadMore.visibility = View.GONE
            loadMoreAble = true
            layoutRefresh.isRefreshing = false
            getData()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        for (i in 0 until idBackgroundThreadList.size) {
            BackgroundExecutor.cancelAll(idBackgroundThreadList.get(i), true)
        }
        onDismissListener?.onDismiss(isSuccess, videoPath, null)
        super.onDismiss(dialog)
    }


    var publishDate: String? = null
    var myHttpRequest: MyHttpRequest? = null
    fun getData() {
        if (isLoading || !loadMoreAble) {
            return
        }
        if (!MyApplication.getInstance()!!.isNetworkConnect()) {
            activity?.runOnUiThread {
                hideLoading()
                showErrorNetwork()
            }
            return
        }
        textNotify.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
//        (activity as? BaseActivity)?.showLoadingDialog(false, null)
        val requestParams = RequestParams()
        val token = SharedPreferencesManager.getToken(context!!)
        requestParams.put("access_token", acessToken)
        requestParams.put("update_at", publishDate)
        val api = ServiceUtil.validAPI(Constant.API_UPLOADED_VIDEO_LIST)
//        (activity as? BaseActivity)?.showLoadingDialog(false, null)
        isLoading = true
        myHttpRequest!!.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                activity?.runOnUiThread {
                    hideLoading()
                    showErrorNetwork()
                }
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isDetached) {
                    return
                }
                handleData(responseString)
                activity?.runOnUiThread {
                    hideLoading()
                }
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            activity?.runOnUiThread { showErrorNetwork() }
            return
        }
        val responseString = responseStr.trim()
//        responseString = responseString.substring(
//            responseString.indexOf("{"),
//            responseString.lastIndexOf("}") + 1
//        )
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            activity?.runOnUiThread { showErrorNetwork() }
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            activity?.runOnUiThread { (activity as? BaseActivity)?.showDialog(message) }
            return
        }

        val jsonArray = JsonParser.getJsonArray(jsonObject, "result")
        val list = JsonParser.getItemVideoServerList(jsonArray)
        if (list == null || list.size == 0) {
            activity?.runOnUiThread { showErrorNetwork() }
            return
        }
        if (list.size < 10) {
            loadMoreAble = false;
        }
        if (publishDate.isNullOrEmpty() || publishDate!!.trim().equals("")) {
            itemList.clear()
        }
        publishDate = list.get(list.size - 1).published_at.toString()
        itemList.addAll(list)
        getInfoVideo()
    }

    private fun getInfoVideo() {
        activity?.runOnUiThread {
            hideLoading()
            adapter.notifyDataSetChanged()
            if (!hasLayoutAnimation) {
                hasLayoutAnimation = true
                recyclerView.scheduleLayoutAnimation()
            }
//            synchronized(synObj) {
//                ServiceUtil.getVideoInfo(itemDownloadList, object : OnGetInfoListener {
//                    override fun onGet(position: Int, itemDownload: ItemDownloadVideo) {
//                        Handler(Looper.getMainLooper()).post {
//                            adapter.notifyDataSetChanged()
//                        }
//                    }
//
//                    override fun onFinish() {
//                    }
//                })
//
//            }
        }
    }

    //    private val synObj: Any = Any()
    fun showErrorNetwork() {
        hideLoading()
        textNotify.visibility = View.VISIBLE
//        (activity as? BaseActivity)?.showDialog(
//            false,
//            R.string.notification,
//            R.string.msg_network_error,
//            R.string.close,
//            R.string.try_again,
//            object : OnDialogButtonListener {
//                override fun onLeftButtonClick() {
//                }
//
//                override fun onRightButtonClick() {
//                    getData()
//                }
//            })
    }

    private fun hideLoading() {
        isLoading = false
        layoutLoadMore.visibility = View.GONE
        progressBar.visibility = View.GONE
//        (activity as? BaseActivity)?.hideLoadingDialog()
    }
}