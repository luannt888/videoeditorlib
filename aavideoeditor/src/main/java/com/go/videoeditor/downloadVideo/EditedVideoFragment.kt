package com.go.videoeditor.downloadVideo

import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.listener.OnDialogButtonListener
import com.go.videoeditor.listener.OnDismissListener
import com.go.videoeditor.listener.OnGetInfoListener
import com.go.videoeditor.merge.ItemVideo
import com.go.videoeditor.model.ItemFile
import com.go.videoeditor.service.ServiceUtil
import com.go.videoeditor.upload.UploadFragment
import com.go.videoeditor.utils.AppUtils
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_draft_saved.recyclerView
import kotlinx.android.synthetic.main.fragment_video_list_ve.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*

class EditedVideoFragment : BottomSheetDialogFragment() {
    private var loadMoreAble: Boolean = true
    private var isLoading: Boolean = false
    private var isViewCreated: Boolean = false
    lateinit var adapter: VideoFileAdapter
    var itemList: ArrayList<ItemFile> = ArrayList()
    var onDismissListener: OnDismissListener? = null
    var isSuccess = false
    var videoPath: String? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    val idBackgroundThreadList: ArrayList<String> = ArrayList()
    var accessToken: String? = null

    companion object {
        fun newInstance(accessToken: String?) = EditedVideoFragment().apply {
            arguments = Bundle().apply {
                putString(Constant.ACCESS_TOKEN, accessToken)
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            accessToken = it.getString(Constant.ACCESS_TOKEN)
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_video_list_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = false
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        isViewCreated = true
        checkRefresh()
        return
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
        buttonDone.visibility = View.GONE
        textHeaderTitle.text = getString(R.string.list_video_edited)
        textHeaderTitle.setTextColor(ContextCompat.getColor(activity!!, R.color.dark_text))
        buttonClose.setImageResource(R.mipmap.ic_close_ve)
        layoutRefresh.isEnabled = false
        initAdapter()
    }

    fun initData() {
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            if (isDetached) {
                return@Runnable
            }
            getData()
        }, 1000)
    }

    fun initAdapter() {
        val style = Constant.STYLE_HORIZONTAL
        adapter = VideoFileAdapter(activity!!, itemList, style, 1, 1)
        adapter?.accessToken = accessToken
        val layoutManager = StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
        adapter.onItemClickListener = object : VideoFileAdapter.OnItemClickListener {
            override fun onClick(
                itemObject: ItemFile,
                position: Int
            ) {
                dismiss()
            }

            override fun onDelete(itemObject: ItemFile, position: Int) {
                getData()
            }
        }
//        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                super.onScrolled(recyclerView, dx, dy)
////                val firstPos = layoutManager.findFirstCompletelyVisibleItemPositions(null)[0]
////                bottomSheetDialog?.behavior?.isDraggable = if (firstPos < 0 || firstPos == 0) true else false
//            }
//        })
        /*recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
//                onScrolledListener?.onScrolled(recyclerView, dx, dy)
                if (isLoading || !loadMoreAble) {
                    return
                }
                val firstPos = layoutManager.findFirstCompletelyVisibleItemPositions(null)[0]
                layoutRefresh.isEnabled = firstPos == 0

                val visibleItemCount = layoutManager.childCount
                val firstVisible = layoutManager.findFirstVisibleItemPositions(null)[0]
                val totalItem = layoutManager.itemCount
                val spanCount = layoutManager.spanCount + 1
                if (visibleItemCount + firstVisible >= totalItem - spanCount) {
                    layoutLoadMore.visibility = View.VISIBLE
                    getData()
                }
            }
        })*/
    }

    fun initControl() {
        buttonClose.setOnClickListener {
            isSuccess = false
            dismiss()
        }
        layoutRefresh.setOnRefreshListener {
            if (isLoading) {
                return@setOnRefreshListener
            }
            itemList.clear()
            layoutLoadMore.visibility = View.GONE
            loadMoreAble = true
            layoutRefresh.isRefreshing = false
            getData()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        ServiceUtil.cancelAllGetVideoInfo()
        onDismissListener?.onDismiss(isSuccess, videoPath, null)
        super.onDismiss(dialog)
    }

    fun getData() {
        itemList.clear()
        textNotify.visibility = View.GONE
        val folderFile = AppUtils.getVideoFileEditedPath(activity!!)
        if (!folderFile.exists() || folderFile.listFiles() == null || folderFile.listFiles().size == 0) {
            textNotify.visibility = View.VISIBLE
            hideLoading()
            return
        }
        val fileList = AppUtils.getVideoFileEditedPath(activity!!).listFiles()
        for (i in 0 until fileList!!.size) {
            val file = fileList.get(i)
            val length = file.length()
            if (length <= 0) {
                continue
            }
            val filePath = file.absolutePath
            val fileName = file.name
            Loggers.e("length_ $i", "length = $length")
            val itemVideo = ItemVideo("", "", filePath, fileName, null, 0)
//            itemList.add(itemVideo)
        }
        adapter.notifyDataSetChanged()
        hideLoading()
        getInfoVideo()
    }

    private fun getInfoVideo() {
        ServiceUtil.getVideoInfo(context!!, itemList, object : OnGetInfoListener {
            override fun onGet(position: Int) {
                if (!isDetached) {
                    ServiceUtil.cancelAllGetVideoInfo()
                    return
                }
                Handler(Looper.getMainLooper()).post {
                    adapter.notifyDataSetChanged()
                }
            }

            override fun onFinish() {
            }
        })
    }

    private val synObj: Any = Any()
    fun showErrorNetwork() {
        hideLoading()
        (activity as? BaseActivity)?.showDialog(
            false,
            R.string.notification,
            R.string.msg_network_error,
            R.string.close,
            R.string.try_again,
            object : OnDialogButtonListener {
                override fun onLeftButtonClick() {
                }

                override fun onRightButtonClick() {
                    getData()
                }
            })
    }

    private fun hideLoading() {
        isLoading = false
        layoutLoadMore.visibility = View.GONE
        progressBar.visibility = View.GONE
//        (activity as? BaseActivity)?.hideLoadingDialog()
    }
}