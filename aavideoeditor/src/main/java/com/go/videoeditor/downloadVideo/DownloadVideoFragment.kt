package com.go.videoeditor.downloadVideo

import android.app.Activity
import android.app.DatePickerDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.speech.RecognizerIntent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.listener.OnDismissListener
import com.go.videoeditor.listener.OnGetInfoListener
import com.go.videoeditor.service.MyHttpRequest
import com.go.videoeditor.service.RequestParams
import com.go.videoeditor.service.ServiceUtil
import com.go.videoeditor.utils.BackgroundExecutor
import com.go.videoeditor.utils.JsonParser
import com.go.videoeditor.utils.SharedPreferencesManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_download_video_list_ve.*
import kotlinx.android.synthetic.main.fragment_download_video_list_ve.layoutLoadMore
import kotlinx.android.synthetic.main.fragment_download_video_list_ve.layoutRefresh
import kotlinx.android.synthetic.main.fragment_download_video_list_ve.progressBar
import kotlinx.android.synthetic.main.fragment_download_video_list_ve.recyclerView
import kotlinx.android.synthetic.main.fragment_download_video_list_ve.textNotify
import kotlinx.android.synthetic.main.fragment_video_list_ve.*
import java.util.*
import kotlin.collections.ArrayList

class DownloadVideoFragment : Fragment() {
    var isTabSelected: Boolean = false
    var isViewCreated: Boolean = false
    var loadMoreAble: Boolean = true
    var isLoading: Boolean = false
    lateinit var adapter: DownloadVideoAdapter
    var itemList: ArrayList<ItemDownloadVideo> = ArrayList()
    var onDismissListener: OnDismissListener? = null
    var isSuccess = false
    var videoPath: String? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    val idBackgroundThreadList: ArrayList<String> = ArrayList()
    var hasLayoutAnimation = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_download_video_list_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /*dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = false
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }*/
        isViewCreated = true
//        checkRefresh()
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 1000)
            return
        }
        if (isTabSelected) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                adapter?.notifyDataSetChanged()
            }, 500)
            return
        }
        isTabSelected = true
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
        /*buttonDone.visibility = View.GONE
        viewLineHeader.visibility = View.GONE
        textHeaderTitle.text = getString(R.string.list_video)*/
        initAdapter()
    }

    fun initData() {
        getData()
    }

    fun initAdapter() {
        val style = Constant.STYLE_HORIZONTAL
        adapter = DownloadVideoAdapter(activity!!, itemList, style, 1)
        val layoutManager = StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
        recyclerView.scheduleLayoutAnimation()
//        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                super.onScrolled(recyclerView, dx, dy)
////                val firstPos = layoutManager.findFirstCompletelyVisibleItemPositions(null)[0]
////                bottomSheetDialog?.behavior?.isDraggable = if (firstPos < 0 || firstPos == 0) true else false
//            }
//        })
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
//                onScrolledListener?.onScrolled(recyclerView, dx, dy)
                if (isLoading || !loadMoreAble) {
                    return
                }
                val firstPos = layoutManager.findFirstCompletelyVisibleItemPositions(null)[0]
                layoutRefresh.isEnabled = firstPos == 0

                val visibleItemCount = layoutManager.childCount
                val firstVisible = layoutManager.findFirstVisibleItemPositions(null)[0]
                val totalItem = layoutManager.itemCount
                val spanCount = layoutManager.spanCount + 1
                if (visibleItemCount + firstVisible >= totalItem - spanCount) {
                    layoutLoadMore.visibility = View.VISIBLE
                    getData()
                }
            }
        })
    }

    fun initControl() {
        /* buttonClose.setOnClickListener {
             isSuccess = false
             dismiss()
         }*/
        buttonCalendarSchedule.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed({
                showDatePicker()
            }, 200)
        }
        layoutRefresh.setOnRefreshListener {
            if (isLoading) {
                return@setOnRefreshListener
            }
            publishDate = ""
            itemList.clear()
            adapter.notifyDataSetChanged()
            layoutLoadMore.visibility = View.GONE
            loadMoreAble = true
            layoutRefresh.isRefreshing = false
            progressBar.visibility = View.VISIBLE
            hasLayoutAnimation = false
            ServiceUtil.cancelAllGetInfo()
            getData()
        }
        buttonSpeak.setOnClickListener {
            promptSpeechInput()
        }

        editSearch.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                handleSearch()
                return@OnEditorActionListener true
            }
            false
        })

        try {
            checkVoiceRecognition()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /*override fun onDismiss(dialog: DialogInterface) {
        for (i in 0 until idBackgroundThreadList.size) {
            BackgroundExecutor.cancelAll(idBackgroundThreadList.get(i), true)
        }
        onDismissListener?.onDismiss(isSuccess, videoPath, null)
        super.onDismiss(dialog)
    }*/

    var publishDate: String? = null
    var name: String? = null
    var myHttpRequest: MyHttpRequest? = null
    fun getData() {
        if (isLoading || !loadMoreAble) {
            return
        }
        if (!MyApplication.getInstance()!!.isNetworkConnect()) {
            activity?.runOnUiThread {
                showErrorNetwork()
            }
            return
        }
        textNotify.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        val token = SharedPreferencesManager.getToken(context!!)
        requestParams.put("access_token", token)
        if (!publishDate.isNullOrEmpty()) {
            requestParams.put("update_at", publishDate)
        }
        if (!name.isNullOrEmpty()) {
            requestParams.put("name", name)
        }
        val api = ServiceUtil.validAPI(Constant.API_VIDEO_LIST)
        isLoading = true
        myHttpRequest!!.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                activity?.runOnUiThread {
                    showErrorNetwork()
                }
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isDetached) {
                    return
                }
                handleData(responseString)
                activity?.runOnUiThread {
                    hideLoading()
                }
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            activity?.runOnUiThread { (activity as BaseActivity).showDialog(getString(R.string.msg_empty_data)) }
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            activity?.runOnUiThread { (activity as BaseActivity).showDialog(getString(R.string.msg_empty_data)) }
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            activity?.runOnUiThread { (activity as? BaseActivity)?.showDialog(message) }
            return
        }

        val jsonArray = JsonParser.getJsonArray(jsonObject, "result")
        val list = JsonParser.getItemVideoServerList(jsonArray)
        if (list == null || list.size == 0) {
            activity?.runOnUiThread { (activity as BaseActivity).showDialog(getString(R.string.msg_empty_data)) }
            return
        }

        if (list.size < 10) {
            loadMoreAble = false;
        }
        if (publishDate.isNullOrEmpty() || publishDate!!.trim().equals("")) {
            itemList.clear()
        }
        publishDate = list.get(list.size - 1).published_at.toString()
        itemList.addAll(list)
        activity?.runOnUiThread {
            adapter.notifyDataSetChanged()
            if (!hasLayoutAnimation) {
                hasLayoutAnimation = true
                recyclerView.scheduleLayoutAnimation()
            }
            /*Handler(Looper.getMainLooper()).postDelayed(Runnable {
                getInfoVideo()
            }, 200)*/
        }
    }

    private fun getInfoVideo() {
        ServiceUtil.getVideoInfo(itemList, object : OnGetInfoListener {
            override fun onGet(position: Int) {
                Handler(Looper.getMainLooper()).post {
                    adapter.notifyDataSetChanged()
                }
            }

            override fun onFinish() {
            }
        })
    }

    fun showErrorNetwork() {
        hideLoading()
        textNotify.visibility = View.VISIBLE
//        (activity as? BaseActivity)?.showDialog(
//            false,
//            R.string.notification,
//            R.string.msg_network_error,
//            R.string.close,
//            R.string.try_again,
//            object : OnDialogButtonListener {
//                override fun onLeftButtonClick() {
//                }
//
//                override fun onRightButtonClick() {
//                    getData()
//                }
//            })
    }

    private fun hideLoading() {
        isLoading = false
        layoutLoadMore.visibility = View.GONE
        progressBar.visibility = View.GONE
//        (activity as? BaseActivity)?.hideLoadingDialog()
    }

    private var datePickerDialog: DatePickerDialog? = null

    private fun showDatePicker() {
        if (datePickerDialog != null) {
            datePickerDialog!!.show()
            return
        }
        val calendar = Calendar.getInstance()
        val year = calendar[Calendar.YEAR]
        val month = calendar[Calendar.MONTH]
        val day = calendar[Calendar.DAY_OF_MONTH]
        datePickerDialog = DatePickerDialog(
            activity!!,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                if (!view.isShown) {
                    return@OnDateSetListener
                }
                val calendarSelected = Calendar.getInstance()
                calendarSelected[Calendar.YEAR] = year
                calendarSelected[Calendar.MONTH] = monthOfYear
                calendarSelected[Calendar.DAY_OF_MONTH] = dayOfMonth
                if (itemList != null) {
                    itemList.clear()
                }
                if (adapter != null) {
                    adapter.notifyDataSetChanged()
                }
                publishDate = (calendarSelected.timeInMillis / 1000).toString()
                val keyword = editSearch.text.toString().trim { it <= ' ' }
                (activity as? BaseActivity)?.hideKeyboard(editSearch)
                name = keyword
                itemList.clear()
                progressBar.visibility = View.VISIBLE
                getData()
            }, year, month, day
        )
//        datePickerDialog!!.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(activity!!, R.color.dark_text))
        datePickerDialog!!.show()
    }

    fun checkVoiceRecognition() {
        val pm: PackageManager = activity!!.packageManager
        val activities =
            pm.queryIntentActivities(Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0)
        if (activities.size == 0) {
            buttonSpeak.visibility = View.GONE
        }
    }

    private fun promptSpeechInput() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, (activity as Activity).packageName)
        intent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.msg_speech_hint))
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1)
        try {
            startActivityForResult(intent, Constant.REQUEST_CODE_SPEAK)
        } catch (a: ActivityNotFoundException) {
            Toast.makeText(
                activity,
                getString(R.string.speech_not_supported),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun handleSearch() {
        val keyword = editSearch.text.toString().trim { it <= ' ' }
        (activity as? BaseActivity)?.hideKeyboard(editSearch)
        name = keyword
        publishDate = ""
        itemList.clear()
        adapter?.notifyDataSetChanged()
        progressBar.visibility = View.VISIBLE
        hasLayoutAnimation = false
        getData()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == Constant.REQUEST_CODE_SPEAK && data != null) {
            val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
            if (result != null && result.size > 0) {
                val keyword = result[0]
                editSearch.setText(keyword)
                editSearch.setSelection(keyword.length)
                handleSearch()
            }
        }
    }

    override fun onDestroy() {
        try {
            for (i in 0 until idBackgroundThreadList.size) {
                BackgroundExecutor.cancelAll(idBackgroundThreadList.get(i), true)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        super.onDestroy()
    }
}