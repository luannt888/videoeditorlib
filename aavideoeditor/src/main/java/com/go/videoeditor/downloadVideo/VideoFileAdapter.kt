package com.go.videoeditor.downloadVideo

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.add.share.ShareFragment
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.listener.OnDialogButtonListener
import com.go.videoeditor.model.ItemFile
import com.go.videoeditor.upload.UploadFragment
import com.go.videoeditor.utils.AppUtils
import com.go.videoeditor.utils.ScreenSize
import com.go.videoeditor.utils.VideoEditUtils
import kotlinx.android.synthetic.main.item_video_file_ve.view.*
import java.util.*

class VideoFileAdapter(
    val activity: Context, val itemList: ArrayList<ItemFile>, val type: Int, val style: Int, val
    numberColumn: Int
) : RecyclerView.Adapter<ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0
    var placeHolder: Int = 0
    var accessToken: String? = null

    init {
        placeHolder = R.drawable.img_loading
    }

    override fun getItemViewType(position: Int): Int {
        val viewType = style
        return viewType
    }

    fun initViewSize(viewType: Int) {
        /*val height: Int = activity.resources.getDimensionPixelSize(R.dimen.dimen_150)
        when (viewType) {
            Constant.STYLE_HORIZONTAL -> {
                imageWidth = height * 2 / 3
                imageHeight = height
            }
        }*/
        val screenSize = ScreenSize(activity)
        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
            screenSize.width
        imageWidth = screenWidth / 4
        imageHeight = imageWidth * 7 / 10
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initViewSize(viewType)
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_video_file_ve,
            parent, false
        )
        return FrameViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemObj: ItemFile = itemList.get(position)
        if (holder is FrameViewHolder) {
            bindDataFrameView(holder, itemObj, position)
        }
    }

    fun bindDataFrameView(holder: FrameViewHolder, itemObj: ItemFile, position: Int) {
//        holder.imageThumbnail.setImageBitmap(itemObj.bitmap)
        if (itemObj.bitmap != null) {
            MyApplication.getInstance()
                ?.loadImage(activity, holder.imageThumbnail, itemObj.bitmap!!)
        } else {
            MyApplication.getInstance()
                ?.loadImage(activity, holder.imageThumbnail, itemObj.filePath)
        }
        holder.textTitle.text = itemObj.title
        holder.textDuration.text = VideoEditUtils.convertMiliSecondsToTime(itemObj.duration)
        holder.buttonDelete.setOnClickListener {
            /*val file = File(itemObj.filePath)
            val fileName = file.name*/
            (activity as? BaseActivity)?.showDialog(true, R.string.notification,
                "Bạn có muốn xoá video \"${itemObj.title}\" không ? ",
                R.string.cancel,
                R.string.ok, object : OnDialogButtonListener {
                    override fun onLeftButtonClick() {}
                    override fun onRightButtonClick() {
                        AppUtils.deleteFile(activity, itemObj)
                        onItemClickListener?.onDelete(itemObj, position)
                        itemList.removeAt(position)
                        notifyDataSetChanged()
                        /*if (file.exists()) {
                            val path = file.absolutePath
                            file.delete()
                            FileUltis.refreshGalery(activity.applicationContext, path)
                            (activity as? BaseActivity)?.showToast("Đã xoá video thành công")
                            onItemClickListener?.onDelete(itemObj, position)
                        }*/
                    }
                }
            )
        }
        holder.buttonShare.setOnClickListener {
            val fragment = ShareFragment()
            val bundle = Bundle()
            bundle.putString(Constant.FILE_PATH, itemObj.filePath)
            fragment.arguments = bundle
            (activity as? BaseActivity)?.showBottomSheetDialogFragment(fragment)

        }
        holder.buttonUpLoad.setOnClickListener {
            if(accessToken.isNullOrEmpty()){
                return@setOnClickListener
            }
            val fragment = UploadFragment.newInstance(accessToken!!,itemObj.filePath)
            (activity as? BaseActivity)?.showBottomSheetDialogFragment(fragment)
        }

        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position)
            if (activity is BaseActivity) {
                activity.goVideoEditorActivity(itemObj.filePath!!)
            }
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class FrameViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imageThumbnail = itemView.imageThumbnail
        val layoutImage = itemView.layoutImage
        val textTitle = itemView.textTitle
        val textDuration = itemView.textDuration
        val buttonDelete = itemView.buttonDelete
        val buttonShare = itemView.buttonShare
        val buttonUpLoad = itemView.buttonUpLoad

        init {
//            buttonDelete.setImageResource(R.mipmap.ic_delete)
            /*buttonDelete.setColorFilter(
                ContextCompat.getColor(activity, R.color.dark_text),
                PorterDuff.Mode.SRC_ATOP
            )*/
        }

        /*init {
            val lp: LinearLayout.LayoutParams = layoutImage.layoutParams as LinearLayout
            .LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
//            Log.e("FrameViewHolder", "$imageWidth x $imageHeight")
            layoutImage.layoutParams = lp
        }*/
    }

    interface OnItemClickListener {
        fun onClick(itemObject: ItemFile, position: Int)
        fun onDelete(itemObject: ItemFile, position: Int)
    }
}