package com.go.videoeditor.downloadVideo

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.graphics.PorterDuff
import android.os.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.downloader.*
import com.downloader.request.DownloadRequest
import com.downloader.utils.Utils
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.fragment.PlayPreviewFragment
import com.go.videoeditor.listener.OnNotifyDataChangeListener
import com.go.videoeditor.service.ServiceUtil
import com.go.videoeditor.utils.AppUtils
import com.go.videoeditor.utils.FileUltis
import com.go.videoeditor.utils.VideoEditUtils
import kotlinx.android.synthetic.main.item_download_video_ve.view.*
import java.util.*
import java.util.concurrent.ConcurrentHashMap

class DownloadVideoAdapter(
    val activity: Context, val itemDownloadList: ArrayList<ItemDownloadVideo>, val style: Int,
    val
    numberColumn: Int,
) : RecyclerView.Adapter<ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var onDownloadListener: OnDownloadListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0
    var type = Constant.REQUEST_DOWNLOAD
    var isPlaying: Boolean = false
    var onNotifyDataChangeListener: OnNotifyDataChangeListener? = null

    companion object {
        var currentRequestMap: Map<Int, DownloadRequest>? = null
            get() = if (field == null) ConcurrentHashMap<Int, DownloadRequest>() else field
    }

    override fun getItemViewType(position: Int): Int {
        val viewType = style
        return viewType
    }

    fun initViewSize(viewType: Int) {
//        val screenSize = ScreenSize(activity)
//        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
//            screenSize.width
        val height: Int = activity.resources.getDimensionPixelSize(R.dimen.dimen_150)
        when (viewType) {
            Constant.STYLE_HORIZONTAL -> {
                imageWidth = height * 2 / 3
                imageHeight = height
            }
        }
//        Loggers.e("initViewSize: $viewType", "$imageWidth x $imageHeight")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initViewSize(viewType)
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_download_video_ve,
            parent, false
        )
        return VideoViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemDownloadObj: ItemDownloadVideo = itemDownloadList.get(position)
        if (holder is VideoViewHolder) {
            bindDataFrameView(holder, itemDownloadObj, position)
        }
    }

    fun bindDataFrameView(
        holder: VideoViewHolder,
        itemDownloadObj: ItemDownloadVideo,
        position: Int
    ) {
        if (itemDownloadObj.thumbnail != null) {
            MyApplication.getInstance()
                ?.loadImage(activity, holder.imageThumbnail, itemDownloadObj.thumbnail)
        } else
            if (itemDownloadObj.bitmap != null) {
                MyApplication.getInstance()
                    ?.loadImage(activity, holder.imageThumbnail, itemDownloadObj.bitmap!!)
                holder.layoutImage.visibility = View.VISIBLE
            } else {
//                MyApplication.getInstance()
//                    ?.loadImage(activity, holder.imageThumbnail, itemDownloadObj.url!!)
//                holder.layoutImage.visibility = View.VISIBLE
                holder.layoutImage.visibility = View.GONE
//                MyApplication.getInstance()
//                    ?.loadImage(activity, holder.imageThumbnail, R.drawable.img_loading)
            }
        holder.textTitle.text = itemDownloadObj.title
        if (type == Constant.REQUEST_DOWNLOAD) {
            if (itemDownloadObj.duration!! > 0L) {
                holder.textDuration.text =
                    VideoEditUtils.convertMiliSecondsToTime(itemDownloadObj.duration!!)
            } else {
                holder.textDuration.text =
                    VideoEditUtils.convertTimestampToFormat(itemDownloadObj.published_at!!.toLong())
            }
//            val visibleDuration =
//                if (itemDownloadObj.duration!! == 0L) View.GONE else View.VISIBLE
//            holder.textDuration.visibility = visibleDuration
        } else if (type == Constant.REQUEST_UPLOADED) {
            holder.textDuration.text =
                VideoEditUtils.convertTimestampToFormat(itemDownloadObj.published_at!!.toLong())
            holder.tvMsg.text = ""
        }
//        holder.textDuration.text =
//            VideoEditUtils.convertTimestampToFormat(itemDownloadObj.published_at!!.toLong())
        holder.layoutRoot.setOnClickListener {
//            onItemClickListener?.onClick(itemObj, position, holder)
            if (!isPlaying) {
                isPlaying = true
                Handler(Looper.getMainLooper()).postDelayed(Runnable {
                    clickItem(itemDownloadObj, holder)
                }, Constant.DELAY_HANDLE)
            }
        }
        val fileName = FileUltis.getVideoNameFileFromTitle(itemDownloadObj.title)
        if (AppUtils.checkVideoDownloadFileExist(activity, fileName)) {
            holder.layoutDownloaded.visibility = View.VISIBLE
        } else {
            holder.layoutDownloaded.visibility = View.INVISIBLE
        }
        getDownloadRequest(itemDownloadObj)
        if (itemDownloadObj.downloadRequest != null) {
            val status = PRDownloader.getStatus(itemDownloadObj.downloadRequest!!.downloadId)
            val request = itemDownloadObj.downloadRequest!!
            if (status == Status.RUNNING || status == Status.QUEUED) {
                setListenerDownload(itemDownloadObj, holder)
                holder.layoutDowload.visibility = View.VISIBLE
                holder.buttonDownload.setImageResource(R.drawable.ic_sticker_del)
            } else if (status == Status.PAUSED) {
                holder.layoutDowload.visibility = View.VISIBLE
                holder.buttonDownload.setImageResource(R.drawable.ic_sticker_del)
                holder.buttonPause.setImageResource(R.drawable.reset)
                holder.tvProgress.text = "" + FileUltis.convertByteToMegaByte(
                    request.getDownloadedBytes(),
                    2
                ) + "MB/ " + FileUltis.convertByteToMegaByte(request.totalBytes, 2) + "MB"
                val progress =
                    (request.getDownloadedBytes() * 100 / (request.totalBytes.toFloat())).toInt()
                holder.progress.progress = progress
            } else {
                resetDownload(itemDownloadObj)
                holder.buttonDownload.setImageResource(R.drawable.ic_download)
            }
        } else {
            holder.layoutDowload.visibility = View.INVISIBLE
        }
        holder.buttonDownload.setOnClickListener {
//            onItemClickListener?.onDownloadClick(itemObj, position, holder)
            download(itemDownloadObj, holder)
        }
        holder.buttonPause.setOnClickListener {
//            onItemClickListener?.onPlayClick(itemObj, position, holder)
            if (itemDownloadObj.downloadRequest != null) {
                val status = PRDownloader.getStatus(itemDownloadObj.downloadRequest!!.downloadId)
                if (status == Status.RUNNING || status == Status.QUEUED) {
                    PRDownloader.pause(itemDownloadObj.downloadRequest!!.downloadId)
                } else if (status == Status.PAUSED) {
                    PRDownloader.resume(itemDownloadObj.downloadRequest!!.downloadId)
                }
            }
            notifyDataSetChanged()
        }
        holder.buttonFolder.setOnClickListener {
            AppUtils.showFileManagerBrowseFileDownload(activity)
        }
    }

    private fun download(
        itemDownloadObj: ItemDownloadVideo,
        holder: VideoViewHolder,
    ) {
//        val fileName = FileUltis.getVideoNameFileUrl(itemDownloadObj.url)
        val fileName = FileUltis.getVideoNameFileFromTitle(itemDownloadObj.title)
        if (AppUtils.checkVideoDownloadFileExist(activity, fileName)) {
            if (activity is BaseActivity) {
                ServiceUtil.showDownloadSuccess(activity, fileName)
            }
            return
        }
        if (itemDownloadObj.downloadRequest == null) {
            val downloadRequest =
                ServiceUtil.downloadRequest(
                    activity as Activity,
                    itemDownloadObj.url,
                    fileName,
                    null,
                    null
                )
            downloadRequest?.title = itemDownloadObj.title
            downloadRequest?.published_at = itemDownloadObj.published_at
            downloadRequest?.duration = itemDownloadObj.duration!!
            holder.progress.setIndeterminate(false)
        } else {
            resetDownload(itemDownloadObj)
            holder.buttonDownload.setImageResource(R.drawable.ic_download)
        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return itemDownloadList.size
    }

    inner class VideoViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imageThumbnail = itemView.imageThumbnail
        val layoutImage = itemView.layoutImage
        val textTitle = itemView.textTitle
        val textDuration = itemView.textDuration
        val buttonPause = itemView.buttonPause
        val buttonDownload = itemView.buttonDownload
        val layoutDowload = itemView.layoutDowload
        val buttonFolder = itemView.buttonFolder
        val tvProgress = itemView.tvProgress
        val progress = itemView.progressBar
        val layoutDownloaded = itemView.layoutDownloaded
        val tvMsg = itemView.tvMsg

        init {
            buttonDownload.setColorFilter(
                ContextCompat.getColor(
                    activity,
                    R.color.color_primary_dark
                ), PorterDuff.Mode.SRC_ATOP
            )
        }

        /*init {
            val lp: LinearLayout.LayoutParams = layoutImage.layoutParams as LinearLayout
            .LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
    //            Log.e("FrameViewHolder", "$imageWidth x $imageHeight")
            layoutImage.layoutParams = lp
        }*/
    }

    interface OnItemClickListener {
        //        fun onClick(itemDownloadObject: ItemDownloadVideo, position: Int, holder: ViewHolder)
//        fun onDownloadClick(
//            itemDownloadObject: ItemDownloadVideo,
//            position: Int
//        )
//        fun onPlayClick(itemDownloadObject: ItemDownloadVideo, position: Int)
        fun onEditClick(itemDownloadObject: ItemDownloadVideo, position: Int)
    }

    private fun setListenerDownload(
        itemDownloadObj: ItemDownloadVideo,
        holder: VideoViewHolder
    ) {
        val downloadRequest = itemDownloadObj.downloadRequest
        if (downloadRequest == null) {
            return
        }
        val handler = Handler(Looper.getMainLooper());
        downloadRequest.setOnProgressListener(object : OnProgressListener {
            override fun onProgress(progress: Progress?) {
                handler.post {
                    holder.tvProgress.text = "" + FileUltis.convertByteToMegaByte(
                        progress!!.currentBytes,
                        2
                    ) + "MB/ " + FileUltis.convertByteToMegaByte(progress!!.totalBytes, 2) + "MB"
                    val progress =
                        (progress.currentBytes * 100 / (progress.totalBytes.toFloat())).toInt()
                    holder.progress.progress = progress
                }
            }
        })
        downloadRequest.setOnPauseListener {
            holder.buttonPause.setImageResource(R.drawable.reset)
        }
        downloadRequest.setOnStartOrResumeListener {
            holder.buttonPause.setImageResource(R.drawable.exo_controls_pause)
        }
        downloadRequest.setOnCancelListener {
            onDownloadListener?.onError(null)
            onNotifyDataChangeListener?.onChange()
        }
        downloadRequest.setOnDownloadListener(object : OnDownloadListener {
            override fun onDownloadComplete() {
                holder.layoutDowload.visibility = View.INVISIBLE
                holder.layoutDownloaded.visibility = View.VISIBLE
                holder.buttonDownload.setImageResource(R.drawable.ic_download)
                onDownloadListener?.onDownloadComplete()
                onNotifyDataChangeListener?.onChange()
                resetDownload(itemDownloadObj)
                val filePath = AppUtils.getVideoDownloadFile(
                    activity,
//                        FileUltis.getVideoNameFileUrlOrPath(itemDownloadObj.url)
                    FileUltis.getVideoNameFileFromTitle(itemDownloadObj.title)
                ).toString()
//                FileUltis.refreshGalery(activity.applicationContext, filePath)
                Loggers.e("onDownloadComplete", filePath)
                AppUtils.addToGalleryRunBackground(activity, filePath, AppUtils.FOLDER_DOWNLOAD)
            }

            override fun onError(error: Error?) {
                resetDownload(itemDownloadObj)
                onDownloadListener?.onError(error)
                onNotifyDataChangeListener?.onChange()
            }
        })
    }

    private fun resetDownload(itemDownloadObj: ItemDownloadVideo) {
        if (itemDownloadObj.downloadRequest != null) {
            PRDownloader.cancel(itemDownloadObj.downloadRequest!!.downloadId)
        }
        itemDownloadObj.downloadRequest = null
        itemDownloadObj.downloadId = -1
    }

    private fun getDownloadRequest(itemDownloadObj: ItemDownloadVideo) {
        val url = itemDownloadObj.url
        val folder = AppUtils.getDownloadFolderPath(activity).absolutePath
        val name = FileUltis.getVideoNameFileFromTitle(itemDownloadObj.title)
//        val name = FileUltis.getVideoNameFileUrl(itemDownloadObj.url)
        val downloadId = Utils.getUniqueId(url, folder, name)
        val downloadRequest = PRDownloader.getDowloadRequest(downloadId)
        itemDownloadObj.downloadRequest = downloadRequest
    }

    private fun clickItem(itemDownloadObj: ItemDownloadVideo, holder: VideoViewHolder) {
//        val fileName = FileUltis.getVideoNameFileUrl(itemDownloadObj.url)
        val fileName = FileUltis.getVideoNameFileFromTitle(itemDownloadObj.title)
        if (AppUtils.checkVideoDownloadFileExist(activity, fileName)) {
            if (activity is BaseActivity) {
                onItemClickListener?.onEditClick(itemDownloadObj, holder.adapterPosition)
                val filePath: String
                val childFolder = activity.getString(R.string.app_name).trim()
                    .replace(" ", "-") + "/" + AppUtils.FOLDER_DOWNLOAD
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    val folderPath = Environment.DIRECTORY_MOVIES + "/" + childFolder
                    filePath = "/storage/emulated/0/" + folderPath + "/" + fileName
                } else {
                    val directory = Environment.getExternalStorageDirectory()
                        .getAbsolutePath()
                    val folderPath =
                        directory + "/" + Environment.DIRECTORY_MOVIES + "/" + childFolder
                    filePath = folderPath + "/" + fileName
                }
                activity.goVideoEditorActivity(filePath)
                /*activity.goVideoEditorActivity(
                    AppUtils.getVideoDownloadFile(activity, fileName).absolutePath
                )*/
            }
            return
        }
        isPlaying = true
        val fragment = PlayPreviewFragment()
        val bundle = Bundle()
        bundle.putString(Constant.FILE_PATH, itemDownloadObj.url)
        fragment.setButtonName("Tải video")
        fragment.onClickButtonActionListenner = object : View.OnClickListener {
            override fun onClick(v: View?) {
                fragment.dismiss()
                download(itemDownloadObj, holder)
            }
        }
        fragment.onDiaLogOnDismissedListener = object : DialogInterface {
            override fun cancel() {
                isPlaying = false
            }

            override fun dismiss() {
                isPlaying = false
            }
        }
        if (activity is BaseActivity) {
            activity.showBottomSheetDialog(fragment, bundle)
        }
    }

}