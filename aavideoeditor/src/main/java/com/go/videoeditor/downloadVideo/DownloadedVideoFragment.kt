package com.go.videoeditor.downloadVideo

import android.content.DialogInterface
import android.os.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.go.videoeditor.R
import com.go.videoeditor.activity.VideoEditorActivity
import com.go.videoeditor.app.Constant
import com.go.videoeditor.listener.OnDismissListener
import com.go.videoeditor.listener.OnGetInfoListener
import com.go.videoeditor.merge.ItemVideo
import com.go.videoeditor.model.ItemFile
import com.go.videoeditor.service.ServiceUtil
import com.go.videoeditor.upload.UploadFragment
import com.go.videoeditor.utils.AppUtils
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_draft_saved.recyclerView
import kotlinx.android.synthetic.main.fragment_video_list_ve.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*
import java.util.*
import kotlin.collections.ArrayList

class DownloadedVideoFragment : BottomSheetDialogFragment() {
    private var loadMoreAble: Boolean = true
    private var isLoading: Boolean = false
    private var isViewCreated: Boolean = false
    lateinit var adapter: VideoFileAdapter
    var itemList: ArrayList<ItemFile> = ArrayList()
    var onDismissListener: OnDismissListener? = null
    var isSuccess = false
    var videoPath: String? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    var hasLayoutAnimation = false
    var accessToken: String? = null
    var folderType: String? = null

    companion object {
        fun newInstance(folderType:String?,accessToken: String?) = DownloadedVideoFragment().apply {
            arguments = Bundle().apply {
                putString(Constant.ACCESS_TOKEN, accessToken)
                putString(Constant.DATA, folderType)
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            accessToken = it.getString(Constant.ACCESS_TOKEN)
            folderType = it.getString(Constant.DATA, null)
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_video_list_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = true
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        isViewCreated = true
        checkRefresh()
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 1000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
        buttonDone.visibility = View.GONE
//        textHeaderTitle.text = getString(R.string.list_video_downloaded)
        textHeaderTitle.setTextColor(ContextCompat.getColor(activity!!, R.color.dark_text))
        buttonClose.setImageResource(R.mipmap.ic_close_ve)
        layoutRefresh.isEnabled = false
        initAdapter()
    }

    fun initData() {
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            if (isDetached) {
                return@Runnable
            }
            getData()
        }, 200)
    }

    fun initAdapter() {
        val style = Constant.STYLE_HORIZONTAL
        val act = activity!!
        adapter = VideoFileAdapter(act, itemList, style, 1, 1)
        adapter?.accessToken = accessToken
        val layoutManager = StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
        recyclerView.scheduleLayoutAnimation()
        adapter.onItemClickListener = object : VideoFileAdapter.OnItemClickListener {
            override fun onClick(
                itemObject: ItemFile,
                position: Int
            ) {
                dismiss()
                Handler(Looper.getMainLooper()).postDelayed({
                    if(act is VideoEditorActivity){
                        act.finish()
                    }
                },200)
            }

            override fun onDelete(itemObject: ItemFile, position: Int) {
                getData()
            }
        }
//
    }

    fun getData() {
        textNotify.visibility = View.GONE
        itemList.clear()
        if (folderType.isNullOrEmpty()) {
            progressBar.visibility = View.GONE
            return
        }
        textHeaderTitle.text =
            if (folderType.equals(AppUtils.FOLDER_DOWNLOAD)) getString(R.string.item_main_video_downloaded) else getString(
                R.string.item_main_video_editted
            )
        val childFolder = activity!!.getString(R.string.app_name).trim()
            .replace(" ", "-") + "/" + folderType
        val folderPath: String
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            folderPath = Environment.DIRECTORY_MOVIES + "/" + childFolder
        } else {
            val directory = Environment.getExternalStorageDirectory()
                .getAbsolutePath()
            folderPath = directory + "/" + Environment.DIRECTORY_MOVIES + "/" + childFolder
        }
        val itemListTemp = AppUtils.getFileListFromFolder(activity!!, folderPath)
        if (itemListTemp.size == 0) {
            textNotify.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
            return
        }
        Collections.sort(itemListTemp, object : Comparator<ItemFile> {
            override fun compare(obj1: ItemFile?, obj2: ItemFile?): Int {
                if (obj1 == null || obj2 == null) {
                    return -1
                }
                return obj2.dateLong.compareTo(obj1.dateLong)
            }
        })
        itemList.addAll(itemListTemp)
        hideLoading()
        adapter.notifyDataSetChanged()
        recyclerView.scheduleLayoutAnimation()
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            getInfoVideo()
        }, 900)
    }

    private fun getInfoVideo() {
        if (!isVisible|| isDetached) {
            return
        }
        ServiceUtil.getVideoInfo(context!!, itemList, object : OnGetInfoListener {
            override fun onGet(position: Int) {
                if (!isVisible|| isDetached) {
                    ServiceUtil.cancelAllGetVideoInfo()
                    return
                }
                Handler(Looper.getMainLooper()).post {
                    adapter.notifyDataSetChanged()
                }
            }

            override fun onFinish() {
            }
        })
    }

    private fun hideLoading() {
        isLoading = false
        layoutLoadMore.visibility = View.GONE
        progressBar.visibility = View.GONE
    }

    fun initControl() {
        buttonClose.setOnClickListener {
            isSuccess = false
            dismiss()
        }
        layoutRefresh.setOnRefreshListener {
            if (isLoading) {
                return@setOnRefreshListener
            }
            itemList.clear()
            adapter?.notifyDataSetChanged()
            layoutLoadMore.visibility = View.GONE
            loadMoreAble = true
            layoutRefresh.isRefreshing = false
            hasLayoutAnimation = false
            progressBar.visibility = View.VISIBLE
            getData()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        ServiceUtil.cancelAllGetVideoInfo()
        onDismissListener?.onDismiss(isSuccess, videoPath, null)
        super.onDismiss(dialog)
    }
}