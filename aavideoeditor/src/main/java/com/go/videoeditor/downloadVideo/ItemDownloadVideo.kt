package com.go.videoeditor.downloadVideo

import android.graphics.Bitmap
import com.downloader.request.DownloadRequest

data class ItemDownloadVideo(
    val id: String?,
    var title: String?,
    val url: String?,
    val published_at: String?,
    val thumbnail: String?,
    val alias: String?,
    val content_type: String?,
    var duration: Long?
) {
    var downloadId: Int = -1;
    var downloadRequest: DownloadRequest? = null
    var bitmap: Bitmap? = null
}