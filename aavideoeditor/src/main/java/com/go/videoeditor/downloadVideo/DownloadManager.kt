package com.go.videoeditor.downloadVideo

import com.downloader.PRDownloader

class DownloadManager {
    companion object {
        fun getDownloadingVideoList(): ArrayList<ItemDownloadVideo>? {
            var list: ArrayList<ItemDownloadVideo>? = ArrayList()
            val downloadRequestList = PRDownloader.getDowloadRequestList()
            for (i in 0 until downloadRequestList.size) {
                val downloadRequest = downloadRequestList.get(i)
                var itemDownloadVideo = ItemDownloadVideo(
                    i.toString(),
                    downloadRequest.title,
                    downloadRequest.url,
                    downloadRequest.published_at,
                    null,
                    null,
                    "video",
                    downloadRequest.duration
                )
                if (itemDownloadVideo.title.isNullOrEmpty()) {
                    itemDownloadVideo.title = downloadRequest.fileName!!
                }
                itemDownloadVideo.downloadRequest = downloadRequest
                list?.add(itemDownloadVideo)
            }
            return list
        }

    }
}