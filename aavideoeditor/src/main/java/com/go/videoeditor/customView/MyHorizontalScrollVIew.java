package com.go.videoeditor.customView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;

public class MyHorizontalScrollVIew extends HorizontalScrollView {
    private boolean isUserScroll;

    private MyScrollListener mMyScrollListener;

    public MyHorizontalScrollVIew(Context context) {
        super(context);
    }

    public MyHorizontalScrollVIew(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyHorizontalScrollVIew(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public void setOnMyScrollListener(MyScrollListener myScrollListener) {
        this.mMyScrollListener = myScrollListener;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (mMyScrollListener != null) {
            mMyScrollListener.onScrollChange(isUserScroll, this, l, t, oldl, oldt);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_MOVE: {
                isUserScroll = true;
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                isUserScroll = false;
                break;

        }
        return super.onTouchEvent(ev);
    }

    public interface MyScrollListener {
        void onScrollChange(Boolean isUser, View view, int scrollX, int scrollY, int oldScrollX, int oldScrollY);
    }

    public interface onTouchListener {
        void onTouch(MotionEvent ev);
    }
}
