package com.go.videoeditor.customView;

import android.graphics.Matrix;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import com.go.videoeditor.R;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

/**
 * {@link BottomSheetDialogFragment} with a simple hack to make it able to be noncancelable, or
 * "locked".
 *
 * @author beta
 */
public class MyBottomSheetDialogFragment extends BottomSheetDialogFragment {

    /**
     * Sets whether the dialog is cancelable.
     * If {@code cancelable} is set to {@code false}, the dialog cannot be canceled neither by
     * clicking out of the dialog, nor by swiping down on the dialog. Otherwise, it could be
     * canceled just like a normal {@link BottomSheetDialog}.
     *
     * @param cancelable Whether the dialog is cancelable.
     */
    @Override
    public void setCancelable(boolean cancelable) {
        if (!cancelable) {
            final View touchOutsideView = getDialog().getWindow().getDecorView().findViewById(R.id.touch_outside);
            touchOutsideView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    int actionBarHeight = 0;
                    int[] array = {0, 0};
                    v.getLocationOnScreen(array);
                    // Calculate ActionBar height
                    TypedValue tv = new TypedValue();
                    actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
                    Matrix matrix = new Matrix();
                    matrix.setTranslate(0, actionBarHeight + array[1]);
                    event.transform(matrix);
                    getActivity().dispatchTouchEvent(event);
                    return true;
                }
            });

        } else {
            final View touchOutsideView = getDialog().getWindow().getDecorView().findViewById(R.id.touch_outside);
            touchOutsideView.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return false;
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setCancelable(false);
    }
}
