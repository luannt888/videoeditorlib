package com.go.videoeditor.htextview;

public enum HTextViewType {
    SCALE, EVAPORATE, FALL, PIXELATE, ANVIL, SPARKLE, LINE, TYPER, RAINBOW
}