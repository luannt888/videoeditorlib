package com.go.videoeditor.htextview;

public interface AnimationListener {
    void onAnimationEnd(HTextView hTextView);
}
