package com.go.videoeditor.htextview;

public class CharacterDiffResult {
    public char c;
    public int fromIndex;
    public int moveIndex;
}
