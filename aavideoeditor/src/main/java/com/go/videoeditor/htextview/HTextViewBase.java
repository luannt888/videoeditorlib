package com.go.videoeditor.htextview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.go.videoeditor.R;

public class HTextViewBase extends HTextView {
    private HTextView mIHText;
    private AttributeSet attrs;
    private Context context;
    private int defStyle;
    private int animateType;

    public HTextViewBase(@NonNull Context context) {
        super(context);
        init(context, null, 0);
    }

    public HTextViewBase(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public HTextViewBase(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyle);
    }

    @Override
    public void setAnimationListener(AnimationListener listener) {
//        mIHText.setAnimationListener(listener);
        if (animateType == 0) {
            ((ScaleTextView) mIHText).setAnimationListener(listener);
        } else if (animateType == 1) {
            ((EvaporateTextView) mIHText).setAnimationListener(listener);
        } else {
            ((LineTextView) mIHText).setAnimationListener(listener);
        }
    }

    @Override
    public void setProgress(float progress) {
    }

    @Override
    public void animateText(CharSequence text) {
        if (animateType == 0) {
            ((ScaleTextView) mIHText).animateText(text);
        } else if (animateType == 1) {
            ((EvaporateTextView) mIHText).animateText(text);
        } else {
            ((LineTextView) mIHText).animateText(text);
        }
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        this.context = context;
        this.attrs = attrs;
        this.defStyle = defStyle;

        // Get the attributes array
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.HTextView);
        animateType = typedArray.getInt(R.styleable.HTextView_animateType, 0);
        final String fontAsset = typedArray.getString(R.styleable.HTextView_fontAsset);

        if (!this.isInEditMode()) {
            // Set custom typeface
            if (fontAsset != null && !fontAsset.trim().isEmpty()) {
                setTypeface(Typeface.createFromAsset(getContext().getAssets(), fontAsset));
            }
        }

        if (animateType == 0) {
            mIHText = new ScaleTextView(context, attrs, defStyle);
        } else if (animateType == 1) {
            mIHText = new EvaporateTextView(context, attrs, defStyle);
        } else {
            mIHText = new LineTextView(context, attrs, defStyle);
        }

        typedArray.recycle();
        initHText(attrs, defStyle);
    }

    private void initHText(AttributeSet attrs, int defStyle) {
//        mIHText.invalidate();
    }

    public void setAnimateType(int animateType) {
        if (animateType == 0) {
            mIHText = new ScaleTextView(context, attrs, defStyle);
        } else if (animateType == 1) {
            mIHText = new EvaporateTextView(context, attrs, defStyle);
        } else {
            mIHText = new LineTextView(context, attrs, defStyle);
        }
        initHText(attrs, defStyle);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mIHText == null) {
            return;
        }
//        mIHText.draw(canvas);
        if (animateType == 0) {
            ((ScaleTextView) mIHText).draw(canvas);
        } else if (animateType == 1) {
            ((EvaporateTextView) mIHText).draw(canvas);
        } else {
            ((LineTextView) mIHText).draw(canvas);
        }
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState state = new SavedState(superState);
        state.animateType = animateType;
        return state;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }
        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(state);
        animateType = ss.animateType;
    }

    public static class SavedState extends BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
        int animateType;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        public SavedState(Parcel source) {
            super(source);
            animateType = source.readInt();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(animateType);
        }

        @Override
        public int describeContents() {
            return 0;
        }
    }
}
