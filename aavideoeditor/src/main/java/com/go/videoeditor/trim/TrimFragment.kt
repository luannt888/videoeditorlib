package com.go.videoeditor.trim

import android.animation.ValueAnimator
import android.animation.ValueAnimator.AnimatorUpdateListener
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Bitmap
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.activity.VideoEditorActivity
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.listener.OnDismissListener
import com.go.videoeditor.listener.SingleCallback
import com.go.videoeditor.model.ItemCmd
import com.go.videoeditor.timeLine.FrameTimeLineAdapter
import com.go.videoeditor.utils.*
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.activity_video_editor.*
import kotlinx.android.synthetic.main.fragment_trim_ve.*
import kotlinx.android.synthetic.main.fragment_trim_ve.buttonPause
import kotlinx.android.synthetic.main.fragment_trim_ve.recyclerView
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.textHeaderTitle
import java.io.File

class TrimFragment : BottomSheetDialogFragment() {
    private var aMiliSecondToPx: Float = 0F
    val TAG_NAME = this::class.simpleName
    var filePath: String? = null
    var trimData: String? = null
    var videoFile: File? = null
    var player: ExoPlayer? = null

    var mDuration: Long = 0
    var mThumbsTotalCount: Int = 0
    var isOverScaledTouchSlop = false
    var mRangeSeekBarView: RangeSeekBarView? = null
    var mLeftProgressPos: Long = 0
    var mRightProgressPos: Long = 0
    var mRedProgressBarPos: Long = 0
    var mRedProgressAnimator: ValueAnimator? = null
    var mAnimationHandler: Handler = Handler(Looper.getMainLooper())
    var lastScrollX = 0
    var VIDEO_MAX_TIME = 50
    var MAX_COUNT_RANGE = 10 //seekBar
    var MAX_SHOOT_DURATION = VIDEO_MAX_TIME * 1000L
    var scrollPos: Long = 0
    var mScaledTouchSlop = 0
    var isSeeking = false
    var mAverageMsPx: Float = 0f
    var averagePxMs: Float = 0f
    var mMaxWidth = VideoEditUtils.VIDEO_FRAMES_WIDTH
    var RECYCLER_VIEW_PADDING = UnitConverter.dpToPx(35)

    lateinit var adapter: FrameTimeLineAdapter
    var itemList: ArrayList<ItemFrame> = ArrayList()
    var onDismissListener: OnDismissListener? = null
    var isSuccess = false
    var cmd = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_trim_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog.behavior.skipCollapsed = true
            bottomSheetDialog.behavior.isDraggable = false
            bottomSheetDialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        initData()
        initControl()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }

    fun initData() {
        filePath = arguments?.getString(Constant.FILE_PATH, null)
        if (filePath.isNullOrEmpty()) {
            (activity as? BaseActivity)?.showToast("File not found")
            dismiss()
            return
        }
        videoFile = File(filePath!!)
        if (videoFile == null) {
            (activity as? BaseActivity)?.showToast("File not found")
            dismiss()
            return
        }
        trimData = arguments?.getString(Constant.DATA, null)
        textHeaderTitle.text = getString(R.string.item_trim)
        player = (activity as VideoEditorActivity).playerFragment?.player
        initAdapter()
        initRangeSeekBarView()
        if (!trimData.isNullOrEmpty()) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                val trimArr = trimData!!.split(Constant.KEY_SPLIT_TRIM)
                mLeftProgressPos = trimArr[0].toLong()
                mRightProgressPos = trimArr[1].toLong()
                mRangeSeekBarView!!.setSelectedMinValue(mLeftProgressPos)
                mRangeSeekBarView!!.setSelectedMaxValue(mRightProgressPos)
                mRangeSeekBarView!!.setStartEndTime(mLeftProgressPos, mRightProgressPos)
                player?.seekTo(mLeftProgressPos)
            }, 0)
        }
        val mSourceUri = Uri.fromFile(videoFile)
        startShootVideoThumbs(requireActivity(), mSourceUri, mThumbsTotalCount, 0, mDuration)
    }

    private fun startShootVideoThumbs(
        context: Context, videoUri: Uri,
        totalThumbsCount: Int, startPosition: Long, endPosition: Long
    ) {
        VideoEditUtils.shootVideoThumbInBackground(
            context,
            videoUri,
            totalThumbsCount,
            startPosition,
            endPosition,
            object : SingleCallback<Bitmap, Int> {
                override fun onSingleCallback(bitmap: Bitmap, interval: Int) {
                    UiThreadExecutor.runTask("", {
//                    mVideoThumbAdapter.addBitmaps(bitmap)
                        val position = itemList.size
                        Log.e(TAG_NAME, "position = $position _ interval = $interval")
                        itemList.add(ItemFrame(position, bitmap))
//                    adapter.notifyItemChanged(position)
                        adapter.notifyDataSetChanged()
                    }, 0L)
                }
            })
    }

    fun initAdapter() {
        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(activity, Uri.fromFile(videoFile))
        mDuration = OptiCommonMethods.getVideoDuration(requireActivity(), videoFile!!)
        val style = Constant.STYLE_HORIZONTAL
        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.setHasFixedSize(true)
        adapter = FrameTimeLineAdapter(requireActivity(), itemList, style, Constant.WIDTH_FRAME_DP)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    private fun initRangeSeekBarView() {
        if (mRangeSeekBarView != null) return
        mLeftProgressPos = 0
        mRightProgressPos = mDuration
        mThumbsTotalCount = VideoEditUtils.getThumbTotalCount(mDuration)
        val widthRangeSeekbar = mThumbsTotalCount * UnitConverter.dpToPx(Constant.WIDTH_FRAME_DP)
        aMiliSecondToPx = VideoEditUtils.getPixelAMilisSecond(mDuration)
        mRangeSeekBarView = RangeSeekBarView(requireActivity(), mLeftProgressPos, mRightProgressPos)
        val layoutParams =
            ViewGroup.LayoutParams(widthRangeSeekbar, ViewGroup.LayoutParams.MATCH_PARENT)
        mRangeSeekBarView!!.layoutParams = layoutParams
        mRangeSeekBarView!!.setSelectedMinValue(mLeftProgressPos)
        mRangeSeekBarView!!.setSelectedMaxValue(mRightProgressPos)
        mRangeSeekBarView!!.setStartEndTime(mLeftProgressPos, mRightProgressPos)
        mRangeSeekBarView!!.setNotifyWhileDragging(true)
        mRangeSeekBarView!!.setOnRangeSeekBarChangeListener(object :
            RangeSeekBarView.OnRangeSeekBarChangeListener {
            override fun onRangeSeekBarValuesChanged(
                bar: RangeSeekBarView?,
                minValue: Long,
                maxValue: Long,
                action: Int,
                isMin: Boolean,
                pressedThumb: RangeSeekBarView.Thumb?
            ) {
                Log.e(TAG_NAME, "-----minValue----->>>>>>$minValue")
                Log.e(TAG_NAME, "-----maxValue----->>>>>>$maxValue")
                mLeftProgressPos = minValue + scrollPos
                mRedProgressBarPos = mLeftProgressPos
                mRightProgressPos = maxValue + scrollPos
                Log.d(TAG_NAME, "-----mLeftProgressPos----->>>>>>$mLeftProgressPos")
                Log.d(
                    TAG_NAME,
                    "-----mRightProgressPos----->>>>>>$mRightProgressPos"
                )
                when (action) {
                    MotionEvent.ACTION_DOWN -> isSeeking = false
                    MotionEvent.ACTION_MOVE -> {
                        isSeeking = true
                        seekTo(if (pressedThumb === RangeSeekBarView.Thumb.MIN) mLeftProgressPos else mRightProgressPos)
                    }
                    MotionEvent.ACTION_UP -> {
                        isSeeking = false
                        seekTo(mLeftProgressPos)
                    }
                }
                mRangeSeekBarView!!.setStartEndTime(mLeftProgressPos, mRightProgressPos)
            }
        })
        seekBarLayout.addView(mRangeSeekBarView)
    }

    private fun seekTo(msec: Long) {
        player?.seekTo(msec)
        Log.e(TAG_NAME, "seekTo = $msec")
    }

    fun onVideoPause() {
        if (player!!.isPlaying()) {
            seekTo(mLeftProgressPos) //复位
            player!!.playWhenReady = false
            setPlayPauseViewIcon(false)
            mRedProgressIcon.visibility = View.GONE
        }
    }

    private fun playVideoOrPause() {
        mRedProgressBarPos = player!!.getCurrentPosition()
        if (player!!.isPlaying()) {
            player!!.playWhenReady = false
//            pauseRedProgressAnimation()
        } else {
            player!!.playWhenReady = true
//            playingRedProgressAnimation()
        }
        setPlayPauseViewIcon(player!!.isPlaying())
    }

    private fun setPlayPauseViewIcon(isPlaying: Boolean) {
        buttonPause.setImageResource(if (isPlaying) R.drawable.exo_icon_pause else R.drawable.exo_icon_play)
    }

    private val mOnScrollListener: RecyclerView.OnScrollListener =
        object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                Log.d(TAG_NAME, "newState = $newState")
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                isSeeking = false
                val scrollX: Int = calcScrollXDistance()
                //Can't reach the sliding distance
                if (Math.abs(lastScrollX - scrollX) < mScaledTouchSlop) {
                    isOverScaledTouchSlop = false
                    return
                }
                isOverScaledTouchSlop = true
                //Initial state, why? Because there is 35dp blank by default!
                if (scrollX == -RECYCLER_VIEW_PADDING) {
                    scrollPos = 0
                } else {
                    isSeeking = true
                    scrollPos =
                        (mAverageMsPx * (RECYCLER_VIEW_PADDING + scrollX) / VideoEditUtils.THUMB_WIDTH).toLong()
                    mLeftProgressPos = mRangeSeekBarView!!.selectedMinValue + scrollPos
                    mRightProgressPos = mRangeSeekBarView!!.selectedMaxValue + scrollPos
                    Log.d(
                        TAG_NAME,
                        "onScrolled >>>> mLeftProgressPos = $mLeftProgressPos"
                    )
                    mRedProgressBarPos = mLeftProgressPos
                    /*if (mVideoView.isPlaying()) {
                        mVideoView.pause()
                        setPlayPauseViewIcon(false)
                    }*/
                    mRedProgressIcon.setVisibility(View.GONE)
                    seekTo(mLeftProgressPos)
                    mRangeSeekBarView!!.setStartEndTime(mLeftProgressPos, mRightProgressPos)
                    mRangeSeekBarView!!.invalidate()
                }
                lastScrollX = scrollX
            }
        }

    private fun calcScrollXDistance(): Int {
        val layoutManager = recyclerView.getLayoutManager() as LinearLayoutManager
        val position = layoutManager.findFirstVisibleItemPosition()
        val firstVisibleChildView = layoutManager.findViewByPosition(position)
        val itemWidth = firstVisibleChildView!!.width
        return position * itemWidth - firstVisibleChildView.left
    }

    private fun playingRedProgressAnimation() {
        pauseRedProgressAnimation()
        playingAnimation()
        mAnimationHandler.post(mAnimationRunnable)
    }

    private fun playingAnimation() {
        if (isDetached) {
            return
        }
        if (mRedProgressIcon.visibility == View.GONE) {
            mRedProgressIcon.visibility = View.VISIBLE
        }
        val params = mRedProgressIcon.layoutParams as ConstraintLayout.LayoutParams
        val start = (RECYCLER_VIEW_PADDING + (mRedProgressBarPos - scrollPos) * averagePxMs).toInt()
        val end = (RECYCLER_VIEW_PADDING + (mRightProgressPos - scrollPos) * averagePxMs).toInt()
        mRedProgressAnimator =
            ValueAnimator.ofInt(start, end)
                .setDuration(mRightProgressPos - scrollPos - (mRedProgressBarPos - scrollPos))
        mRedProgressAnimator!!.setInterpolator(LinearInterpolator())
        mRedProgressAnimator!!.addUpdateListener(AnimatorUpdateListener { animation ->
            if (isDetached) {
                return@AnimatorUpdateListener
            }
            params.leftMargin = animation.animatedValue as Int
            mRedProgressIcon?.layoutParams = params
            Log.d(
                TAG_NAME,
                "----onAnimationUpdate--->>>>>>>$mRedProgressBarPos"
            )
        })
        mRedProgressAnimator!!.start()
    }

    private fun pauseRedProgressAnimation() {
        mRedProgressIcon.clearAnimation()
        if (mRedProgressAnimator != null && mRedProgressAnimator!!.isRunning) {
            mAnimationHandler.removeCallbacks(mAnimationRunnable)
            mRedProgressAnimator!!.cancel()
        }
    }

    private val mAnimationRunnable = Runnable { updateVideoProgress() }

    private fun updateVideoProgress() {
        val currentPosition: Long = player!!.getCurrentPosition()
        Log.d(TAG_NAME, "updateVideoProgress currentPosition = $currentPosition")
        if (currentPosition >= mRightProgressPos) {
            mRedProgressBarPos = mLeftProgressPos
            pauseRedProgressAnimation()
            onVideoPause()
        } else {
            mAnimationHandler.post(mAnimationRunnable)
        }
    }

    fun initControl() {
        buttonClose.setOnClickListener {
            isSuccess = false
            cmd = ""
            dismiss()
        }

        buttonDone.setOnClickListener {
            val newDuration = mRightProgressPos - mLeftProgressPos
            if (newDuration == mDuration) {
                dismiss()
                return@setOnClickListener
            }
            isSuccess = true
            cmd = "" + mLeftProgressPos + Constant.KEY_SPLIT_TRIM + mRightProgressPos
            if (activity is VideoEditorActivity) {
                val trimBar: RangeSeekBarView = (activity as VideoEditorActivity).trimRangeSeekBar
                trimBar.setSelectedMinValue(mLeftProgressPos)
                trimBar.setSelectedMaxValue(mRightProgressPos)
                trimBar.setStartEndTime(mLeftProgressPos, mRightProgressPos)
                Loggers.e(TAG_NAME, cmd)
                (activity as VideoEditorActivity).addCmd(ItemCmd(Constant.KEY_TRIM, cmd))
            }
            dismiss()
        }

        buttonPause.setOnClickListener {
            playVideoOrPause()
        }
    }

    override fun dismiss() {
        (activity as VideoEditorActivity).showButtonPlay(true)
        onDismissListener?.onDismiss(isSuccess, cmd, null)
        super.dismiss()
    }

    override fun onCancel(dialog: DialogInterface) {
        isSuccess = false
        cmd = ""
        onDismissListener?.onDismiss(isSuccess, cmd, null)
        super.onCancel(dialog)
    }

    override fun onDestroy() {
        BackgroundExecutor.cancelAll("", true)
        UiThreadExecutor.cancelAll("")
        super.onDestroy()
    }
}