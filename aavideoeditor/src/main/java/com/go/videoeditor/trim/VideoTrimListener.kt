package com.go.videoeditor.trim

interface VideoTrimListener {
    fun onStartTrim()
    fun onFinishTrim(url: String?)
    fun onCancel()
}