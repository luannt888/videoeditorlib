package com.go.videoeditor.trim

import android.graphics.Bitmap
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemFrame(var position: Int, var bitmap: Bitmap?) : Parcelable