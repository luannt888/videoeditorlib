package com.go.videoeditor.trim;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

import androidx.annotation.Nullable;

import com.go.videoeditor.R;
import com.go.videoeditor.app.Loggers;
import com.go.videoeditor.utils.UnitConverter;
import com.go.videoeditor.utils.VideoEditUtils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class RangeSeekBarView extends View {
    private static final String TAG = RangeSeekBarView.class.getSimpleName();
    public static final int INVALID_POINTER_ID = 255;
    public static final int ACTION_POINTER_INDEX_MASK = 0x0000ff00, ACTION_POINTER_INDEX_SHIFT = 8;
    private static final int TIME_TEXT_HEIGH_DP = 12;//7
    private static final int TITLE_TEXT_HEIGH_DP = 12;//7
    private static final int PADDING_TOP_DP = 18;//7

    private static final int TIME_TEXT_HEIGH_PX = UnitConverter.dpToPx(TIME_TEXT_HEIGH_DP);
    private static final int TITLE_TEXT_HEIGH_PX = UnitConverter.dpToPx(TITLE_TEXT_HEIGH_DP);
    private static final int paddingTop = UnitConverter.dpToPx(PADDING_TOP_DP);//10
    private static int TextPositionY;
    private int mActivePointerId = INVALID_POINTER_ID;

    private long mMinShootTime = 1000L;
    private long mMaxShootTime = VideoEditUtils.MAX_SHOOT_DURATION;
    private double absoluteMinValuePrim, absoluteMaxValuePrim;
    private double normalizedMinValue = 0d;//The ratio of point coordinates to the total length, ranging from 0-1
    private double normalizedMaxValue = 1d;//The ratio of point coordinates to the total length, ranging from 0-1
    private double normalizedMinValueTime = 0d;
    private double normalizedMaxValueTime = 1d;// normalized：the ratio of point coordinates to the total length, ranging from 0-1
    private int mScaledTouchSlop;
    private Bitmap thumbImageLeft;
    private Bitmap thumbImageRight;
    private Bitmap thumbPressedImage;
    private Paint paint;
    private Paint rectPaint;
    private Paint rectPaintBg;
    private final Paint mVideoTrimTimePaintL = new Paint();
    private final Paint mVideoTrimTimePaintR = new Paint();
    private final Paint mTextTitlePaintCenter = new Paint();
    private final Paint mShadow = new Paint();
    private int thumbWidth;
    private float thumbHalfWidth;
    private final float padding = 0;
    private long mStartPosition = 0;
    private long mEndPosition = 0;
    private float thumbPaddingTop = 0;
    private boolean isTouchDown;
    private float mDownMotionX;
    private boolean mIsDragging;
    private Thumb pressedThumb;
    private boolean isMin;
    private double min_width = 1;//最小裁剪距离
    private boolean notifyWhileDragging = false;
    private OnRangeSeekBarChangeListener mRangeSeekBarChangeListener;
    private int whiteColorRes = Color.WHITE;
    private int backgroundRangebar = Color.TRANSPARENT;
    private String title = "";

    public enum Thumb {
        MIN, MAX
    }

    public RangeSeekBarView(Context context) {
        this(context, null);
    }

    public RangeSeekBarView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RangeSeekBarView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFocusable(true);
        setFocusableInTouchMode(true);
        init(attrs, context);
    }

    public double getAbsoluteMaxValuePrim() {
        return absoluteMaxValuePrim;
    }

    public RangeSeekBarView(Context context, long absoluteMinValuePrim, long absoluteMaxValuePrim) {
        this(context);
        this.absoluteMinValuePrim = absoluteMinValuePrim;
        this.absoluteMaxValuePrim = absoluteMaxValuePrim;
    }

    public void setRangStart(long timeStart) {
        this.absoluteMinValuePrim = timeStart;
    }

    public void setRangEnd(long timeEnd) {
        this.absoluteMaxValuePrim = timeEnd;
    }

    private void init(AttributeSet attrs, Context context) {
//        TypedArray a = null;
//        a = context.obtainStyledAttributes(attrs, R.styleable.StickerView);
        mScaledTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();
        thumbImageLeft = BitmapFactory.decodeResource(getResources(), R.drawable.ic_video_thumb_handle);

//        int width = thumbImageLeft.getWidth();
//        int height = thumbImageLeft.getHeight();
//        int newWidth = UnitConverter.dpToPx(11);
////    int newHeight = UnitConverter.dpToPx(55);
//        int newHeight = getResources().getDimensionPixelSize(R.dimen.dimen_70);
//        float scaleWidth = newWidth * 1.0f / width;
//        float scaleHeight = newHeight * 1.0f / height;
//        Matrix matrix = new Matrix();
//        matrix.postScale(scaleWidth, scaleHeight);
////        Log.e(TAG,"init_ height = " + height + " __ y+height = " + (-15 + height));
////        thumbImageLeft = Bitmap.createBitmap(thumbImageLeft, 0, 0, width, height, matrix, true);
//        thumbImageLeft = Bitmap.createBitmap(thumbImageLeft, 0, 0, width, height, matrix, true);
//        thumbImageRight = thumbImageLeft;
//        thumbPressedImage = thumbImageLeft;
//        thumbWidth = newWidth;
//        thumbHalfWidth = thumbWidth / 2;
        int shadowColor = getContext().getResources().getColor(R.color.shadow_color);
        mShadow.setAntiAlias(true);
        mShadow.setColor(shadowColor);

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        rectPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        rectPaint.setStyle(Paint.Style.FILL);
        rectPaint.setColor(whiteColorRes);

        rectPaintBg = new Paint(Paint.ANTI_ALIAS_FLAG);
        rectPaintBg.setStyle(Paint.Style.FILL);
        rectPaintBg.setColor(backgroundRangebar);

        mVideoTrimTimePaintL.setStrokeWidth(1);
        mVideoTrimTimePaintL.setARGB(255, 51, 51, 51);
        mVideoTrimTimePaintL.setTextSize(TIME_TEXT_HEIGH_PX);
        mVideoTrimTimePaintL.setAntiAlias(true);
        mVideoTrimTimePaintL.setColor(whiteColorRes);
        mVideoTrimTimePaintL.setTextAlign(Paint.Align.LEFT);

        mVideoTrimTimePaintR.setStrokeWidth(1);
        mVideoTrimTimePaintR.setARGB(255, 51, 51, 51);
        mVideoTrimTimePaintR.setTextSize(TIME_TEXT_HEIGH_PX);
        mVideoTrimTimePaintR.setAntiAlias(true);
        mVideoTrimTimePaintR.setColor(whiteColorRes);
        mVideoTrimTimePaintR.setTextAlign(Paint.Align.RIGHT);
        TextPositionY = (int) (paddingTop / 2 - ((mVideoTrimTimePaintL.descent() + mVideoTrimTimePaintL.ascent()) / 2));

        mTextTitlePaintCenter.setStrokeWidth(1);
        mTextTitlePaintCenter.setARGB(255, 51, 51, 51);
        mTextTitlePaintCenter.setTextSize(TIME_TEXT_HEIGH_PX);
        mTextTitlePaintCenter.setAntiAlias(true);
        mTextTitlePaintCenter.setColor(whiteColorRes);
        mTextTitlePaintCenter.setTextAlign(Paint.Align.CENTER);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = 300;
        if (MeasureSpec.UNSPECIFIED != MeasureSpec.getMode(widthMeasureSpec)) {
            width = MeasureSpec.getSize(widthMeasureSpec);
        }
        int height = 120;
        if (MeasureSpec.UNSPECIFIED != MeasureSpec.getMode(heightMeasureSpec)) {
            height = MeasureSpec.getSize(heightMeasureSpec);
        }
        setMeasuredDimension(width, height);
//        thumbImageLeft = BitmapFactory.decodeResource(getResources(), R.drawable.ic_video_thumb_handle);

        int widthbm = thumbImageLeft.getWidth();
        int heightbm = thumbImageLeft.getHeight();
        int newWidth = UnitConverter.dpToPx(11);
//    int newHeight = UnitConverter.dpToPx(55);
        int newHeight = height - paddingTop;
        float scaleWidth = newWidth * 1.0f / widthbm;
        float scaleHeight = newHeight * 1.0f / heightbm;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
//        Log.e(TAG,"init_ height = " + height + " __ y+height = " + (-15 + height));
//        thumbImageLeft = Bitmap.createBitmap(thumbImageLeft, 0, 0, width, height, matrix, true);
        thumbImageLeft = Bitmap.createBitmap(thumbImageLeft, 0, 0, widthbm, heightbm, matrix, true);
        thumbImageRight = thumbImageLeft;
        thumbPressedImage = thumbImageLeft;
        thumbWidth = newWidth;
        thumbHalfWidth = thumbWidth / 2;
    }

    int bottomShadow = UnitConverter.dpToPx(20);

    /*public void setNormalizedMaxValue(long normalizedMaxValue) {
        this.normalizedMaxValue = normalizedMaxValue;
    }*/

    private Canvas canvas;

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.canvas = canvas;

        float bg_middle_left = 0;
        float bg_middle_right = getWidth() - getPaddingRight();
        float rangeL = normalizedToScreen(normalizedMinValue);
        float rangeR = normalizedToScreen(normalizedMaxValue);
//        Log.e(TAG, "onDraw = " + bg_middle_left + " _ " + getHeight() + " _ " + rangeL );
//        Rect leftRect = new Rect((int) bg_middle_left, getHeight(), (int) rangeL, 0);
        Rect leftRect = new Rect((int) bg_middle_left, getHeight(), (int) rangeL, bottomShadow);
        Rect rightRect = new Rect((int) rangeR, getHeight(), (int) bg_middle_right, bottomShadow);
        canvas.drawRect(leftRect, mShadow);
        canvas.drawRect(rightRect, mShadow);

//        int paddingTopTest = UnitConverter.dpToPx(18);
        canvas.drawRect(rangeL, thumbPaddingTop + paddingTop, rangeR, thumbPaddingTop + UnitConverter.dpToPx(2) + paddingTop, rectPaint);
        canvas.drawRect(rangeL, getHeight() - UnitConverter.dpToPx(2), rangeR, getHeight(), rectPaint);
        canvas.drawRect(rangeL, thumbPaddingTop + paddingTop + UnitConverter.dpToPx(2), rangeR, getHeight() - UnitConverter.dpToPx(2), rectPaintBg);

        canvas.drawText(title, normalizedToScreen(normalizedMinValue) + ((normalizedToScreen(normalizedMaxValue) - normalizedToScreen(normalizedMinValue)) / 2), paddingTop + ((getHeight() - paddingTop) / 2) - ((mTextTitlePaintCenter.descent() + mTextTitlePaintCenter.ascent()) / 2), mTextTitlePaintCenter);
        drawThumb(normalizedToScreen(normalizedMinValue), false, canvas, true);
        drawThumb(normalizedToScreen(normalizedMaxValue), false, canvas, false);
        drawVideoTrimTimeText(canvas);
    }

    private void drawThumb(float screenCoord, boolean pressed, Canvas canvas, boolean isLeft) {
//        int paddingTopTest = UnitConverter.dpToPx(18);
        canvas.drawBitmap(pressed ? thumbPressedImage : (isLeft ? thumbImageLeft : thumbImageRight), screenCoord - (isLeft ? 0 : thumbWidth), paddingTop,
                paint);
    }

    public void drawVideoTrimTimeText(Canvas canvas) {
        String leftThumbsTime = VideoEditUtils.convertSecondsToTime(mStartPosition);
        String rightThumbsTime = VideoEditUtils.convertSecondsToTime(mEndPosition);
//        int TextPositionYTest = UnitConverter.dpToPx(12);
        Log.e("drawVideoTrimTimeText", "TextPositionY = " + TextPositionY + " _ leftThumbsTime = " + leftThumbsTime + " (" + mStartPosition + ") _" + "rightThumbsTime = " + rightThumbsTime + " (" + mEndPosition + ")");
        canvas.drawText(leftThumbsTime, normalizedToScreen(normalizedMinValue), TextPositionY, mVideoTrimTimePaintL);
        canvas.drawText(rightThumbsTime, normalizedToScreen(normalizedMaxValue), TextPositionY, mVideoTrimTimePaintR);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isTouchDown) {
            return super.onTouchEvent(event);
        }
        if (event.getPointerCount() > 1) {
            return super.onTouchEvent(event);
        }

        if (!isEnabled()) return false;
        if (absoluteMaxValuePrim <= mMinShootTime) {
            return super.onTouchEvent(event);
        }
        int pointerIndex;
        final int action = event.getAction();
//        Loggers.e("Seekbar_AC2", "action = " + action);
        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                mActivePointerId = event.getPointerId(event.getPointerCount() - 1);
                pointerIndex = event.findPointerIndex(mActivePointerId);
                mDownMotionX = event.getX(pointerIndex);
                pressedThumb = evalPressedThumb(mDownMotionX);
                if (pressedThumb == null) {
                    if (mRangeSeekBarChangeListener != null) {
                        mRangeSeekBarChangeListener.onRangeSeekBarValuesChanged(this, getSelectedMinValue(), getSelectedMaxValue(), MotionEvent.ACTION_DOWN, isMin,
                                pressedThumb);
                        return true;
                    }
                    return super.onTouchEvent(event);
                }
                setPressed(true);
                onStartTrackingTouch();
                trackTouchEvent(event);
                attemptClaimDrag();
                if (mRangeSeekBarChangeListener != null) {
                    mRangeSeekBarChangeListener.onRangeSeekBarValuesChanged(this, getSelectedMinValue(), getSelectedMaxValue(), MotionEvent.ACTION_DOWN, isMin,
                            pressedThumb);
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (pressedThumb != null) {
                    if (mIsDragging) {
                        trackTouchEvent(event);
                    } else {
                        // Scroll to follow the motion event
                        pointerIndex = event.findPointerIndex(mActivePointerId);
                        final float x = event.getX(pointerIndex);
                        if (Math.abs(x - mDownMotionX) > mScaledTouchSlop) {
                            setPressed(true);
//                            Log.e(TAG, "没有拖住最大最小值");
                            invalidate();
                            onStartTrackingTouch();
                            trackTouchEvent(event);
                            attemptClaimDrag();
                        }
                    }
                    if (notifyWhileDragging && mRangeSeekBarChangeListener != null) {
                        mRangeSeekBarChangeListener.onRangeSeekBarValuesChanged(this, getSelectedMinValue(), getSelectedMaxValue(), MotionEvent.ACTION_MOVE,
                                isMin, pressedThumb);
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                if (mIsDragging) {
                    trackTouchEvent(event);
                    onStopTrackingTouch();
                    setPressed(false);
                } else {
                    onStartTrackingTouch();
                    trackTouchEvent(event);
                    onStopTrackingTouch();
                }

                invalidate();
                if (mRangeSeekBarChangeListener != null) {
                    mRangeSeekBarChangeListener.onRangeSeekBarValuesChanged(this, getSelectedMinValue(), getSelectedMaxValue(), MotionEvent.ACTION_UP, isMin,
                            pressedThumb);
                }
                pressedThumb = null;
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                final int index = event.getPointerCount() - 1;
                // final int index = ev.getActionIndex();
                mDownMotionX = event.getX(index);
                mActivePointerId = event.getPointerId(index);
                invalidate();
                break;
            case MotionEvent.ACTION_POINTER_UP:
                onSecondaryPointerUp(event);
                invalidate();
                break;
            case MotionEvent.ACTION_CANCEL:
                if (mIsDragging) {
                    onStopTrackingTouch();
                    setPressed(false);
                }
                invalidate(); // see above explanation
                break;
            default:
                break;
        }
        return true;
    }

    private void onSecondaryPointerUp(MotionEvent ev) {
        final int pointerIndex = (ev.getAction() & ACTION_POINTER_INDEX_MASK) >> ACTION_POINTER_INDEX_SHIFT;
        final int pointerId = ev.getPointerId(pointerIndex);
        if (pointerId == mActivePointerId) {
            final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
            mDownMotionX = ev.getX(newPointerIndex);
            mActivePointerId = ev.getPointerId(newPointerIndex);
        }
    }

    private void trackTouchEvent(MotionEvent event) {
        if (event.getPointerCount() > 1) return;
        Log.e(TAG, "trackTouchEvent: " + event.getAction() + " x: " + event.getX());
        final int pointerIndex = event.findPointerIndex(mActivePointerId);// 得到按下点的index
        float x = 0;
        try {
            x = event.getX(pointerIndex);
        } catch (Exception e) {
            return;
        }
        if (Thumb.MIN.equals(pressedThumb)) {
            setNormalizedMinValue(screenToNormalized(x, 0));
            Loggers.e("Seekbar_trackTouchEvent_A1", "normalizedMinValue = " + normalizedMinValue + " _ normalizedMinValueTime=" + normalizedMinValueTime);
        } else if (Thumb.MAX.equals(pressedThumb)) {
            setNormalizedMaxValue(screenToNormalized(x, 1));
            Loggers.e("Seekbar_trackTouchEvent_A2", "normalizedMaxValue = " + normalizedMaxValue + " _ normalizedMaxValueTime=" + normalizedMaxValueTime);
        }
    }

    private double screenToNormalized(float screenCoord, int position) {
        int width = getWidth();
        if (width <= 2 * padding) {
            // prevent division by zero, simply return 0.
            return 0d;
        } else {
            isMin = false;
            double current_width = screenCoord;
            float rangeL = normalizedToScreen(normalizedMinValue);
            float rangeR = normalizedToScreen(normalizedMaxValue);
            double min = mMinShootTime / (absoluteMaxValuePrim - absoluteMinValuePrim) * (width - thumbWidth * 2);

            if (absoluteMaxValuePrim > 5 * 60 * 1000) {
                DecimalFormat df = new DecimalFormat("0.0000");
                df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.ENGLISH));
                min_width = Double.parseDouble(df.format(min));
            } else {
                min_width = Math.round(min + 0.5d);
            }
            if (position == 0) {
                if (isInThumbRangeLeft(screenCoord, normalizedMinValue, 0.5)) {
                    return normalizedMinValue;
                }

                float rightPosition = (getWidth() - rangeR) >= 0 ? (getWidth() - rangeR) : 0;
                double left_length = getValueLength() - (rightPosition + min_width);

                if (current_width > rangeL) {
                    current_width = rangeL + (current_width - rangeL);
                } else if (current_width <= rangeL) {
                    current_width = rangeL - (rangeL - current_width);
                }

                if (current_width > left_length) {
                    isMin = true;
                    current_width = left_length;
                }

                if (current_width < thumbWidth * 2 / 3) {
                    current_width = 0;
                }

                double resultTime = (current_width - padding) / (width - 2 * thumbWidth);
                if (normalizedMaxValueTime - maxRange >= resultTime) {
                    return normalizedMinValueTime;
                }
                normalizedMinValueTime = Math.min(1d, Math.max(0d, resultTime));
                double result = (current_width - padding) / (width - 2 * padding);
                Loggers.e("Seekbar_screenToNormalized_AA4_" + maxRange, "MinValueTime = " + normalizedMinValueTime + " _MaxValueTime = " + normalizedMaxValueTime + " _resultTime = " + resultTime + " + result= " + result);
                return Math.min(1d, Math.max(0d, result));// 保证该该值为0-1之间，但是什么时候这个判断有用呢？
            } else {
                if (isInThumbRange(screenCoord, normalizedMaxValue, 0.5)) {
                    return normalizedMaxValue;
                }

                double right_length = getValueLength() - (rangeL + min_width);
                if (current_width > rangeR) {
                    current_width = rangeR + (current_width - rangeR);
                } else if (current_width <= rangeR) {
                    current_width = rangeR - (rangeR - current_width);
                }

                double paddingRight = getWidth() - current_width;

                if (paddingRight > right_length) {
                    isMin = true;
                    current_width = getWidth() - right_length;
                    paddingRight = right_length;
                }

                if (paddingRight < thumbWidth * 2 / 3) {
                    current_width = getWidth();
                    paddingRight = 0;
                }

                double resultTime = (paddingRight - padding) / (width - 2 * thumbWidth);
                resultTime = 1 - resultTime;
                if (resultTime - normalizedMinValueTime >= maxRange) {
                    return normalizedMaxValueTime;
                }
                normalizedMaxValueTime = Math.min(1d, Math.max(0d, resultTime));
                double result = (current_width - padding) / (width - 2 * padding);
                Loggers.e("Seekbar_screenToNormalized_AA5_" + maxRange, "MinValueTime = " + normalizedMinValueTime + " _MaxValueTime = " + normalizedMaxValueTime + " _resultTime = " + resultTime + " + result= " + result);
                return Math.min(1d, Math.max(0d, result));// 保证该该值为0-1之间，但是什么时候这个判断有用呢？
            }
        }
    }

    private int getValueLength() {
        return (getWidth() - 2 * thumbWidth);
    }

    /**
     * 计算位于哪个Thumb内
     *
     * @param touchX touchX
     * @return 被touch的是空还是最大值或最小值
     */
    private Thumb evalPressedThumb(float touchX) {
        Thumb result = null;
        boolean minThumbPressed = isInThumbRange(touchX, normalizedMinValue, 2);// 触摸点是否在最小值图片范围内
        boolean maxThumbPressed = isInThumbRange(touchX, normalizedMaxValue, 2);
        if (minThumbPressed && maxThumbPressed) {
            result = (touchX / getWidth() > 0.5f) ? Thumb.MIN : Thumb.MAX;
        } else if (minThumbPressed) {
            result = Thumb.MIN;
        } else if (maxThumbPressed) {
            result = Thumb.MAX;
        }
        return result;
    }

    private boolean isInThumbRange(float touchX, double normalizedThumbValue, double scale) {
        return Math.abs(touchX - normalizedToScreen(normalizedThumbValue)) <= thumbHalfWidth * scale;
    }

    private boolean isInThumbRangeLeft(float touchX, double normalizedThumbValue, double scale) {
        return Math.abs(touchX - normalizedToScreen(normalizedThumbValue) - thumbWidth) <= thumbHalfWidth * scale;
    }

    private void attemptClaimDrag() {
        if (getParent() != null) {
            getParent().requestDisallowInterceptTouchEvent(true);
        }
    }

    void onStartTrackingTouch() {
        mIsDragging = true;
    }

    void onStopTrackingTouch() {
        mIsDragging = false;
    }

    public void setMinShootTime(long minRang) {
        this.mMinShootTime = minRang;
    }

    private float normalizedToScreen(double normalizedCoord) {
        return (float) (getPaddingLeft() + normalizedCoord * (getWidth() - getPaddingLeft() - getPaddingRight()));
    }

    private double valueToNormalized(long value) {
        if (0 == absoluteMaxValuePrim - absoluteMinValuePrim) {
            return 0d;
        }
        return (value - absoluteMinValuePrim) / (absoluteMaxValuePrim - absoluteMinValuePrim);
    }

    public void setStartEndTime(long start, long end) {
        this.mStartPosition = start / 1000;
        this.mEndPosition = end / 1000;
        Loggers.e("Seekbar_setStartEndTime", "mStartPosition = " + mStartPosition + " _ mEndPosition = " + mEndPosition);
    }

    public void setStartTime(long start) {
        this.mStartPosition = start / 1000;
    }

    public void setEndTime(long end) {
        this.mEndPosition = end / 1000;
    }

    public void setSelectedMinValue(long value) {
        if (0 == (absoluteMaxValuePrim - absoluteMinValuePrim)) {
            setNormalizedMinValue(0d);
        } else {
            double ab = valueToNormalized(value);
            setNormalizedMinValue(ab);
        }
    }

    public void setSelectedMaxValue(long value) {
        if (0 == (absoluteMaxValuePrim - absoluteMinValuePrim)) {
            setNormalizedMaxValue(1d);
        } else {
            setNormalizedMaxValue(valueToNormalized(value));
        }
    }

    public void setNormalizedMinValue(double value) {
        normalizedMinValue = Math.max(0d, Math.min(1d, Math.min(value, normalizedMaxValue)));
        invalidate();
    }

    public void setNormalizedMaxValue(double value) {
        normalizedMaxValue = Math.max(0d, Math.min(1d, Math.max(value, normalizedMinValue)));
        Loggers.e("notifyRangSeek", "normalizedMaxValue = " + normalizedMaxValue + " _value= " + value);
        invalidate();
    }

    private double maxRange = 1;

    public void notifyRangeSeek() {
        mMaxShootTime = (long) Math.min(absoluteMaxValuePrim, mMaxShootTime);
        maxRange = mMaxShootTime / absoluteMaxValuePrim;
//        setNormalizedMaxValue(maxRange);
        if (canvas == null) {
            return;
        }
//        drawVideoTrimTimeText(canvas);
    }

    public long getSelectedMinValue() {
        return normalizedToValue(normalizedMinValue);
    }

    public long getSelectedMaxValue() {
        return normalizedToValue(normalizedMaxValue);
    }

    private long normalizedToValue(double normalized) {
        return (long) (absoluteMinValuePrim + normalized * (absoluteMaxValuePrim - absoluteMinValuePrim));
    }

    public boolean isNotifyWhileDragging() {
        return notifyWhileDragging;
    }

    public void setNotifyWhileDragging(boolean flag) {
        this.notifyWhileDragging = flag;
    }

    public void setTouchDown(boolean touchDown) {
        isTouchDown = touchDown;
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        final Bundle bundle = new Bundle();
        bundle.putParcelable("SUPER", super.onSaveInstanceState());
        bundle.putDouble("MIN", normalizedMinValue);
        bundle.putDouble("MAX", normalizedMaxValue);
        bundle.putDouble("MIN_TIME", normalizedMinValueTime);
        bundle.putDouble("MAX_TIME", normalizedMaxValueTime);
        return bundle;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable parcel) {
        final Bundle bundle = (Bundle) parcel;
        super.onRestoreInstanceState(bundle.getParcelable("SUPER"));
        normalizedMinValue = bundle.getDouble("MIN");
        normalizedMaxValue = bundle.getDouble("MAX");
        normalizedMinValueTime = bundle.getDouble("MIN_TIME");
        normalizedMaxValueTime = bundle.getDouble("MAX_TIME");
    }

    public interface OnRangeSeekBarChangeListener {
        void onRangeSeekBarValuesChanged(RangeSeekBarView bar, long minValue, long maxValue, int action, boolean isMin, Thumb pressedThumb);
    }

    public void setOnRangeSeekBarChangeListener(OnRangeSeekBarChangeListener listener) {
        this.mRangeSeekBarChangeListener = listener;
    }

    private Object obj;

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    public long getStartPosition() {
        return mStartPosition;
    }

    public void setStartPosition(long mStartPosition) {
        this.mStartPosition = mStartPosition;
    }

    public long getEndPosition() {
        return mEndPosition;
    }

    public void setEndPosition(long mEndPosition) {
        this.mEndPosition = mEndPosition;
    }

    public int getBackgroundRangebar() {
        return backgroundRangebar;
    }

    public void setBackgroundRangebar(int backgroundRangebar) {
        this.backgroundRangebar = backgroundRangebar;
        if (rectPaintBg != null) {
            rectPaintBg.setColor(backgroundRangebar);
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getMaxShootTime() {
        return mMaxShootTime;
    }

    public void setMaxShootTime(long mMaxShootTime) {
        this.mMaxShootTime = mMaxShootTime;
    }
}
