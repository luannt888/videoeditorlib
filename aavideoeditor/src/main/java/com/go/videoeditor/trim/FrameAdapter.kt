package com.go.videoeditor.trim

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.go.videoeditor.R
import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.app.Constant
import com.go.videoeditor.utils.ScreenSize
import kotlinx.android.synthetic.main.item_frame_ve.view.*
import java.util.*

class FrameAdapter(
    val activity: Context, val itemList: ArrayList<ItemFrame>, val style: Int, val
    numberColumn: Int
) : RecyclerView.Adapter<ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0

    override fun getItemViewType(position: Int): Int {
        var viewType = style
        return viewType
    }

    fun initViewSize(viewType: Int) {
        val screenSize = ScreenSize(activity)
//        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
//            screenSize.width
        val height: Int = activity.resources.getDimensionPixelSize(R.dimen.dimen_50)
        when (viewType) {
            Constant.STYLE_HORIZONTAL -> {
                imageWidth = height * 2 / 2
                imageHeight = height
            }
        }
//        Loggers.e("initViewSize: $viewType", "$imageWidth x $imageHeight")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initViewSize(viewType)
        when (viewType) {
            Constant.STYLE_HORIZONTAL -> {
                val view = LayoutInflater.from(parent.context).inflate(
                    R.layout.item_frame_ve,
                    parent, false
                )
                return FrameViewHolder(view)
            }
        }
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_frame_ve,
            parent, false
        )
        return FrameViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemObj: ItemFrame = itemList.get(position)
        if (holder is FrameViewHolder) {
            bindDataFrameView(holder, itemObj, position)
        }
    }

    fun bindDataFrameView(holder: FrameViewHolder, itemObj: ItemFrame, position: Int) {
//        holder.imageThumbnail.setImageBitmap(itemObj.bitmap)
        if (itemObj.bitmap != null) {
            MyApplication.getInstance()?.loadImage(activity, holder.imageThumbnail, itemObj.bitmap!!)
        } else {
            MyApplication.getInstance()?.loadImage(activity, holder.imageThumbnail, R.drawable.img_loading)
        }

        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position, holder)
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class FrameViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imageThumbnail = itemView.imageThumbnail
        val layoutImage = itemView.layoutImage

        init {
            val lp: LinearLayout.LayoutParams = layoutImage.layoutParams as LinearLayout
            .LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
//            Log.e("FrameViewHolder", "$imageWidth x $imageHeight")
            layoutImage.layoutParams = lp
        }
    }

    interface OnItemClickListener {
        fun onClick(itemObject: ItemFrame, position: Int, holder: ViewHolder)
    }
}