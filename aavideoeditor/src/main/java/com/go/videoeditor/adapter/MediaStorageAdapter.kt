package com.go.videoeditor.adapter

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.go.videoeditor.R
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.model.ItemFile
import com.go.videoeditor.utils.ScreenSize
import com.go.videoeditor.utils.VideoEditUtils
import kotlinx.android.synthetic.main.item_file_ve.view.*
import java.util.*

class MediaStorageAdapter(val activity: Activity, var itemList: ArrayList<ItemFile>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imageThumbnail = itemView.imageThumbnail
        val imagePlayThumbnail = itemView.imagePlayThumbnail
        val textTitle = itemView.textTitle
        val textDuration = itemView.textDuration
        val textDate = itemView.textDate
        val textSize = itemView.textSize
        val buttonMoreMenu = itemView.buttonMoreMenu
        val layoutImage = itemView.layoutImage

        init {
            val lp: LinearLayout.LayoutParams = layoutImage.layoutParams as LinearLayout
            .LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
            layoutImage.layoutParams = lp
        }
    }

    interface OnItemClickListener {
        fun onClick(itemObject: ItemFile, position: Int, view: View)
        fun onLongClick(itemObject: ItemFile, position: Int, view: View)
        fun onMoreMenuClick(itemObject: ItemFile, position: Int, view: View)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val screenSize = ScreenSize(activity)
        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
            screenSize.width
        imageWidth = screenWidth / 4
        imageHeight = imageWidth * 7 / 10

        val view = LayoutInflater.from(activity).inflate(
            R.layout.item_file_ve,
            parent, false
        )
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemObj: ItemFile = itemList.get(position)
        if (holder is ListViewHolder) {
            bindDataListView(holder, itemObj, position)
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    fun bindDataListView(holder: ListViewHolder, itemObj: ItemFile, position: Int) {
        holder.textTitle.text = itemObj.title
        holder.textDate.text = itemObj.date
        holder.textDuration.text = VideoEditUtils.convertMiliSecondsToTime(itemObj.duration)
        holder.textSize.text = convertFileSize(itemObj.size)
        /*if (itemObj.bitmap != null) {
            MyApplication.getInstance()
                ?.loadImage(activity, holder.imageThumbnail, itemObj.bitmap!!)
        } else {
            MyApplication.getInstance()
                ?.loadImage(activity, holder.imageThumbnail, R.drawable.img_loading)
        }*/
//        holder.imagePlayThumbnail.clearColorFilter()

        when (itemObj.type) {
            Constant.FILE_TYPE_VIDEO -> {
                holder.imagePlayThumbnail.setImageResource(R.mipmap.ic_play_thumbnail_ve)
                holder.imagePlayThumbnail.visibility = View.VISIBLE
                holder.textDuration.visibility = View.VISIBLE
                if (itemObj.bitmap != null) {
                    MyApplication.getInstance()
                        ?.loadImage(activity, holder.imageThumbnail, itemObj.bitmap!!)
                } else {
//                    MyApplication.getInstance()
//                        ?.loadImage(activity, holder.imageThumbnail, R.drawable.img_loading)
                    MyApplication.getInstance()
                        ?.loadImage(activity, holder.imageThumbnail, itemObj.filePath)
                }
            }
            Constant.FILE_TYPE_IMAGE -> {
                holder.imagePlayThumbnail.visibility = View.GONE
                holder.textDuration.visibility = View.GONE
                if (itemObj.bitmap != null) {
                    MyApplication.getInstance()
                        ?.loadImage(activity, holder.imageThumbnail, itemObj.bitmap!!)
                } else {
                    loadThumbnail(itemObj, holder.imageThumbnail)
                }
                /*MyApplication.getInstance()
                    ?.loadImage(activity, holder.imageThumbnail, itemObj.filePath)*/
            }
            Constant.FILE_TYPE_AUDIO -> {
                holder.imagePlayThumbnail.setImageResource(R.mipmap.ic_record_audio_ve)
                /*holder.imagePlayThumbnail.setColorFilter(
                    ContextCompat.getColor(activity, R.color.dark_text),
                    PorterDuff.Mode.SRC_ATOP
                )*/
                holder.imagePlayThumbnail.visibility = View.VISIBLE
                holder.textDuration.visibility = View.VISIBLE
                MyApplication.getInstance()
                    ?.loadImage(activity, holder.imageThumbnail, R.drawable.img_audio)
            }
            else -> {
                holder.imagePlayThumbnail.visibility = View.GONE
                holder.textDuration.visibility = View.GONE
                MyApplication.getInstance()
                    ?.loadImage(activity, holder.imageThumbnail, R.drawable.img_loading)
            }
        }

        holder.buttonMoreMenu.setOnClickListener {
            onItemClickListener?.onMoreMenuClick(itemObj, position, holder.buttonMoreMenu)
        }
        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position, holder.layoutRoot)
        }
        holder.layoutRoot.setOnLongClickListener {
            onItemClickListener?.onLongClick(itemObj, position, holder.layoutRoot)
            true
        }
    }

    fun loadThumbnail(itemObject: ItemFile, imageThumbnail: ImageView) {
        val requestOption = RequestOptions().override(200, 110)
        Glide.with(activity).asBitmap()
            .load(itemObject.filePath)
            .apply(requestOption)
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    itemObject.bitmap = resource
                    imageThumbnail.setImageBitmap(resource)
                }

                override fun onLoadCleared(placeholder: Drawable?) {
                    Loggers.e("loadThumbnail", "onLoadCleared")
                }
            })
    }

    fun convertFileSize(length: Long): String {
        if (length < 1024) {
            return "$length B"
        }
        val fileSizeInKB = length / 1024f
        if (fileSizeInKB < 1024) {
            return String.format(Locale.ENGLISH, "%.2f KB", fileSizeInKB)
        }
        val fileSizeInMB = fileSizeInKB / 1024f
        if (fileSizeInMB < 1024) {
            return String.format(Locale.ENGLISH, "%.2f MB", fileSizeInMB)
        }
        val fileSizeInGb = fileSizeInMB / 1024f
        return String.format(Locale.ENGLISH, "%.2f Gb", fileSizeInGb)
    }

}