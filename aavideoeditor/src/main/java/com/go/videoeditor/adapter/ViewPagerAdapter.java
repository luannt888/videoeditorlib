package com.go.videoeditor.adapter;

import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<String> mTabTitleList;
    private List<Fragment> mFragments;

    public ViewPagerAdapter(FragmentManager fm, List<Fragment> fragments, ArrayList<String> tabTitleList) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mFragments = fragments;
        mTabTitleList = tabTitleList;
    }

    @Override
    public Fragment getItem(int position) {
        if (mFragments != null && position < mFragments.size()) {
            return mFragments.get(position);
        }
        return null;
    }

    @Override
    public int getCount() {
        if (mFragments != null) {
            return mFragments.size();
        }
        return 0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (mTabTitleList == null || mTabTitleList.size() <= position) {
            return null;
        }
        return mTabTitleList.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
    }
}