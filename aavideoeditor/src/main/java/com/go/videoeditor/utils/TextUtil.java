package com.go.videoeditor.utils;

import android.text.Html;
import android.text.Spanned;
import android.webkit.WebView;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Currency;

public class TextUtil {
    public static Spanned getHtmlFormat(String text) {
        if (text == null) {
            text = "";
        }
        Spanned textSpanned;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            textSpanned = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            textSpanned = Html.fromHtml(text);
        }
        return textSpanned;
    }

    public static void setHtmlTextView(String text, TextView textView) {
        if (textView == null) {
            return;
        }
        textView.setText(getHtmlFormat(text + ""));
    }

    public static void setWebviewContent(String message, WebView webView) {
        webView.loadDataWithBaseURL(null, message, "text/html", "utf-8", null);
    }

    public static String formatMoney(int number) {
        NumberFormat format = NumberFormat.getCurrencyInstance();
        format.setMaximumFractionDigits(0);
        format.setCurrency(Currency.getInstance("VND"));
        return format.format(number);
    }
}