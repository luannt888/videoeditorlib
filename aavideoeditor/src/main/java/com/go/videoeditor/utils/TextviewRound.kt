package com.go.videoeditor.utils

import android.content.Context
import android.graphics.Outline
import android.util.AttributeSet
import android.view.View
import android.view.ViewOutlineProvider
import androidx.appcompat.widget.AppCompatTextView
import com.go.videoeditor.R

class TextviewRound : AppCompatTextView {
    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    fun init(
        context: Context,
        attrs: AttributeSet?
    ) {
        val typedArray =
            context.obtainStyledAttributes(attrs, R.styleable.TextviewBackgroundRoundView)
        val isBgRound = typedArray.getBoolean(R.styleable.TextviewBackgroundRoundView_isBgRound, true)
        val cornerRadius =
            typedArray.getDimensionPixelSize(R.styleable.TextviewBackgroundRoundView_bgRadius, 0)
        if (!isBgRound || cornerRadius <= 0) {
            return
        }

        setOutlineProvider(object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                val left = 0
                val top = 0;
                val right = view.width
                val bottom = view.height
                outline.setRoundRect(left, top, right, bottom, cornerRadius.toFloat())
            }
        })
        setClipToOutline(true)
    }
}