package com.go.videoeditor.utils

import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.downloadVideo.ItemCategory
import com.go.videoeditor.downloadVideo.ItemDownloadVideo
import com.go.videoeditor.model.ItemAppConfig
import com.go.videoeditor.model.ItemVersion
import com.go.videoeditor.user.ItemProvince
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import com.go.videoeditor.user.ItemUser

class JsonParser {
    companion object {
        fun parseItemAppConfig(jObject: JSONObject?): ItemAppConfig? {
            if (jObject == null) {
                return null
            }
            val logo = getString(jObject, "logo")
            val bgLaunch = getString(jObject, "bg_launch")
            val linkShareApp = getString(jObject, "link_share")
            val apiUrl = getString(jObject, "api_url")
            val maxStreamQuality = getString(jObject, "max_stream_quality")
            val itemAppConfig = ItemAppConfig(logo, bgLaunch, linkShareApp, maxStreamQuality)
            MyApplication.getInstance()?.getDataManager()?.itemAppConfig = itemAppConfig
            MyApplication.getInstance()?.setBaseApiUrl(apiUrl)
            return itemAppConfig
        }

        fun parseItemVersion(jObject: JSONObject?): ItemVersion? {
            val versionObj: JSONObject? = getJsonObject(jObject!!, "config")
            if (versionObj == null || versionObj.length() == 0) {
                return null
            }
            val versionCode: Int? = getInt(versionObj, "version")
            val title = getString(versionObj, "name")
            val message = getString(versionObj, "message")
            val url = getString(versionObj, "url")
            val isUpdate: Boolean? = getBoolean(versionObj, "is_update")
            return ItemVersion(versionCode, title!!, message!!, url!!, isUpdate)
        }

        fun parseItemUserLogin(jsonObject: JSONObject?): ItemUser? {
            if (jsonObject == null) {
                return null
            }
            val id = getInt(jsonObject, "id")
            val email = getString(jsonObject, "email")
            val avatar = getString(jsonObject, "avatar")
            val phone = getString(jsonObject, "phone")
            val fullname = getString(jsonObject, "fullname")
            val gender = getInt(jsonObject, "gender")
            val address = getString(jsonObject, "address")
            val birthday = getString(jsonObject, "birthday")
            val socialId = getString(jsonObject, "social_id")
            val token = getString(jsonObject, "access_token")
            val accessTokenSocial = getString(jsonObject, "fb_token")
            val affiliateId = getString(jsonObject, "affiliate_id")
            val isPassword = getBoolean(jsonObject, "is_password")
            val chip = getInt(jsonObject, "chip")
            if (token.isNullOrEmpty()) {
                return null
            }
            val itemUser = ItemUser(
                id, email, avatar, phone, fullname, gender, address,
                birthday, socialId, token, accessTokenSocial, affiliateId, isPassword, chip!!
            )
            MyApplication.getInstance()?.getDataManager()?.itemUser = itemUser
            return itemUser
        }

        fun getItemProvinceList(jsonArray: JSONArray?): ArrayList<ItemProvince>? {
            if (jsonArray == null) {
                return null
            }
            val itemList: ArrayList<ItemProvince> = ArrayList()
            for (i in 0 until jsonArray.length()) {
                try {
                    val jsonObject: JSONObject? = jsonArray.getJSONObject(i)
                    itemList.add(parseItemProvince(jsonObject)!!)
                } catch (e: JSONException) {
                }
            }
            return itemList
        }

        fun parseItemProvince(jObject: JSONObject?): ItemProvince? {
            val id: Int? = getInt(jObject, "id")
            val name = getString(jObject, "name")
            val app_id = getString(jObject, "app_id")
            return ItemProvince(id!!, name!!, app_id)
        }

        fun getItemVideoServerList(jsonArray: JSONArray?): ArrayList<ItemDownloadVideo>? {
            if (jsonArray == null) {
                return null
            }
            val itemDownloadList: ArrayList<ItemDownloadVideo> = ArrayList()
            for (i in 0 until jsonArray.length()) {
                try {
                    val jsonObject: JSONObject? = jsonArray.getJSONObject(i)
                    itemDownloadList.add(parseItemVideoServer(jsonObject)!!)
                } catch (e: JSONException) {
                }
            }
            return itemDownloadList
        }

        fun parseItemVideoServer(jObject: JSONObject?): ItemDownloadVideo? {
            val id: String? = getString(jObject, "id")
            val name = getString(jObject, "name")
            val url = getString(jObject, "file_link")
            val published_at = getString(jObject, "published_at")
            val image = getString(jObject, "image")
            val alias = getString(jObject, "alias")
            val content_type = getString(jObject, "content_type")
            return ItemDownloadVideo(
                id,
                name,
                url,
                published_at,
                image,
                alias,
                content_type,
                0L
            )
        }

        fun parseItemCategoryList(jsonArray: JSONArray?): ArrayList<ItemCategory>? {
            if (jsonArray == null) {
                return null
            }
            val itemList: ArrayList<ItemCategory> = ArrayList()
            for (i in 0 until jsonArray.length()) {
                try {
                    val jsonObject: JSONObject? = jsonArray.getJSONObject(i)
                    val id: Int? = getInt(jsonObject, "id")
                    val name = getString(jsonObject, "name")
                    val type = getString(jsonObject, "type")
                    val style = getInt(jsonObject, "style")
                    itemList.add(ItemCategory(id, name, type, style))
                } catch (e: JSONException) {
                }
            }
            return itemList
        }

//        fun parseItemPhotoList(jsonArray: JSONArray?): ArrayList<ItemPhoto>? {
//            if (jsonArray == null) {
//                return null
//            }
//            val itemList: ArrayList<ItemPhoto> = ArrayList()
//            for (i in 0 until jsonArray.length()) {
//                try {
//                    val jsonObject: JSONObject? = jsonArray.getJSONObject(i)
//                    val title = getString(jsonObject, "title")
//                    val image = getString(jsonObject, "image")
//                    itemList.add(ItemPhoto(title, image))
//                } catch (e: JSONException) {
//                }
//            }
//            return itemList
//        }

//        fun parseItemNewsList(activity: Activity, jsonArray: JSONArray?): ArrayList<ItemNews>? {
//            if (jsonArray == null) {
//                return null
//            }
//            val screenSize = ScreenSize(activity)
//            val imageWidth: Int = if (screenSize.width > screenSize.height) screenSize.height
//            else screenSize.width
//            val imageHeight = imageWidth * 9 / 16
//            val imageSize = imageWidth.toString() + "x" + imageHeight
//
//            val itemList: ArrayList<ItemNews> = ArrayList()
//            for (i in 0 until jsonArray.length()) {
//                try {
//                    val jsonObject: JSONObject? = jsonArray.getJSONObject(i)
//                    val itemObj = parseItemNews(activity, jsonObject, imageSize) ?: continue
//                    itemList.add(itemObj)
//                } catch (e: JSONException) {
//                }
//            }
//            return itemList
//        }

//        fun parseItemNews(activity: Activity, jsonObject: JSONObject?, imageSize: String?): ItemNews? {
//            if (jsonObject == null || jsonObject.length() == 0) {
//                return null
//            }
//            val id = getString(jsonObject, "id")
//            var name = getString(jsonObject, "title")
//            if (name == null) {
//                name = getString(jsonObject, "name")
//            }
//            var description = getString(jsonObject, "sapo")
//            if (description == null) {
//                description = getString(jsonObject, "description")
//            }
//            val info = getString(jsonObject, "info")
//            var image = getString(jsonObject, "src")
//            if (image == null) {
//                image = getString(jsonObject, "image")
//            }
//            if (imageSize.isNullOrEmpty()) {
//                val screenSize = ScreenSize(activity)
//                val imageWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else screenSize.width
//                val imageHeight = imageWidth * 9 / 16
//                val imgSize = imageWidth.toString() + "x" + imageHeight
//                image = image?.replace("100x100", imgSize)
//            }
//            var categoryId = getString(jsonObject, "catid")
//            var categoryName = getString(jsonObject, "catname")
//            if (categoryName == null) {
//                val cateObj = getJsonObject(jsonObject, "category")
//                categoryId = getString(cateObj, "id")
//                categoryName = getString(cateObj, "name")
//            }
//            val createdAt = getString(jsonObject, "created_at")
//            val timeAgo = getString(jsonObject, "timeago")
//            val file = getString(jsonObject, "file")
//            val youtubeUrl = getString(jsonObject, "youtube_url")
//            val url = getString(jsonObject, "url")
//            val webview = getString(jsonObject, "webview")
//            val view = getString(jsonObject, "views")
//            var comment = getString(jsonObject, "comments")
//            if (comment == null) {
//                comment = getString(jsonObject, "comment")
//            }
//            val basePrice: Int? = getInt(jsonObject, "base_price")
//            val price = getString(jsonObject, "price")
//            val type = getString(jsonObject, "type")
//            val contentType = getString(jsonObject, "content_type")
//            val rateStr = getString(jsonObject, "rate")
//            val rate: Float = if (rateStr.isNullOrEmpty()) 0f else rateStr.toFloat()
//            var reviews = getString(jsonObject, "reviews")
//            if (!reviews.isNullOrEmpty()) {
//                reviews = "$reviews"
//            }
//            val maxQuantity = getInt(jsonObject, "quantity")
//            val userReviewArr = getJsonArray(jsonObject, "user_reviews")
//            val itemUserReviewList: ArrayList<ItemUserReview> = ArrayList()
//            if (userReviewArr != null) {
//                for (i in 0 until userReviewArr.length()) {
//                    val itemJsonObj = userReviewArr.getJSONObject(i)
//                    val userReviewId = getInt(itemJsonObj, "id")
//                    val userReviewFullname = getString(itemJsonObj, "fullname")
//                    val userReviewAvatar = getString(itemJsonObj, "avatar")
//                    itemUserReviewList.add(ItemUserReview(userReviewId, userReviewFullname, userReviewAvatar))
//                }
//            }
//            val interactive = getString(jsonObject, "interactive")
//            val liveStream = getString(jsonObject, "livestream")
//            val isLive = getBoolean(jsonObject, "is_live")
//            val publishDate = getString(jsonObject, "publishDate")
//            val time = getString(jsonObject, "time")
//            val gallery = getString(jsonObject, "gallery")
//            val read = getBoolean(jsonObject, "read")
//            return ItemNews(id, name, description, info, image, categoryId, categoryName,
//                    createdAt, timeAgo, file, youtubeUrl, url, webview, view, comment, basePrice!!, price, type,
//                    contentType, rate, reviews, maxQuantity, itemUserReviewList, interactive, liveStream, isLive, 0, publishDate, time, gallery, read)
//        }

        fun getString(jObject: JSONObject?, param: String): String? {
            if (!existParam(jObject, param)) {
                return null
            }
            try {
                return jObject?.getString(param)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return null
        }

        fun getInt(jObject: JSONObject?, param: String): Int? {
            if (!existParam(jObject, param)) {
                return 0
            }
            try {
                return jObject?.getInt(param)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return 0
        }

        fun getLong(jObject: JSONObject?, param: String): Long? {
            if (!existParam(jObject, param)) {
                return 0
            }
            try {
                return jObject?.getLong(param)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return 0
        }

        fun getBoolean(jObject: JSONObject?, param: String): Boolean? {
            if (!existParam(jObject, param)) {
                return false
            }
            try {
                return jObject?.getBoolean(param)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return false
        }

        fun getJsonObject(jObject: JSONObject?, param: String): JSONObject? {
            if (!existParam(jObject, param)) {
                return null
            }
            try {
                return jObject?.getJSONObject(param)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return null
        }

        fun getJsonObject(json: String?): JSONObject? {
            if (json.isNullOrEmpty()) {
                return null
            }
            try {
                return JSONObject(json)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return null
        }

        fun getJsonArray(jObject: JSONObject?, param: String?): JSONArray? {
            if (!existParam(jObject, param)) {
                return null
            }
            try {
                return jObject?.getJSONArray(param)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return null
        }

        fun getJsonArray(json: String?): JSONArray? {
            try {
                return JSONArray(json)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return null
        }

        private fun existParam(jObject: JSONObject?, param: String?): Boolean {
            if (jObject == null || param == null) {
                return false
            }
            return if (!jObject.has(param)) {
                false
            } else !jObject.isNull(param)
        }
    }
}