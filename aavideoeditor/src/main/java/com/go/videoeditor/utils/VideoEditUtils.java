package com.go.videoeditor.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Size;

import com.go.videoeditor.app.Constant;
import com.go.videoeditor.listener.SingleCallback;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

public class VideoEditUtils {
    public static final long MIN_SHOOT_DURATION = 3000L;// Minimum editing time 3s
    public static final long MAX_SHOOT_DURATION = Long.MAX_VALUE;// Max editing time
    public static final int THUMB_WIDTH = UnitConverter.dpToPx(Constant.WIDTH_FRAME_DP);
    public static int THUMB_HEIGHT = UnitConverter.dpToPx(70);
    private static final int SCREEN_WIDTH_FULL = DeviceUtil.getDeviceWidth();
    public static final int RECYCLER_VIEW_PADDING = UnitConverter.dpToPx(35);
    public static final int VIDEO_FRAMES_WIDTH = SCREEN_WIDTH_FULL - RECYCLER_VIEW_PADDING * 2;

    public static void shootVideoThumbInBackground(final Context context, final Uri videoUri, final int totalThumbsCount, final long startPosition,
                                                   final long endPosition, final SingleCallback<Bitmap, Integer> callback) {
        BackgroundExecutor.execute(new BackgroundExecutor.Task("", 0L, "") {
            @Override
            public void execute() {
                try {
                    MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                    mediaMetadataRetriever.setDataSource(context, videoUri);
                    // Retrieve media data use microsecond
                    long interval = (endPosition - startPosition) / (totalThumbsCount - 1);
                    for (long i = 0; i < totalThumbsCount; ++i) {
                        long frameTime = startPosition + interval * i;
                        Bitmap bitmap = mediaMetadataRetriever.getFrameAtTime(frameTime * 1000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
                        if (bitmap == null) continue;
                        THUMB_HEIGHT = THUMB_WIDTH * bitmap.getHeight() / bitmap.getWidth();
                        try {
                            bitmap = Bitmap.createScaledBitmap(bitmap, THUMB_WIDTH, THUMB_HEIGHT, false);
                        } catch (final Throwable t) {
                            t.printStackTrace();
                        }
                        callback.onSingleCallback(bitmap, (int) interval);
                    }
                    mediaMetadataRetriever.release();
                } catch (final Throwable e) {
                    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
                }
            }
        });
    }

    /**
     * second to HH:MM:ss
     *
     * @param seconds
     * @return
     */
    public static String convertSecondsToTime(long seconds) {
        String timeStr = null;
        int hour = 0;
        int minute = 0;
        int second = 0;
        if (seconds <= 0)
            return "00:00";
        else {
            minute = (int) seconds / 60;
            if (minute < 60) {
                second = (int) seconds % 60;
                timeStr = unitFormat(minute) + ":" + unitFormat(second);
            } else {
                hour = minute / 60;
                if (hour > 99)
                    return "99:59:59";
                minute = minute % 60;
                second = (int) (seconds - hour * 3600 - minute * 60);
                timeStr = unitFormat(hour) + ":" + unitFormat(minute) + ":" + unitFormat(second);
            }
        }
        return timeStr;
    }

    public static String convertMiliSecondsToTime(long millisecond) {
        return convertSecondsToTime(millisecond / 1000);
    }

    public static String convertSecondsToFormat(long seconds, String format) {
        if (TextUtils.isEmpty(format))
            return "";

        Date date = new Date(seconds);
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        return sdf.format(date);
    }
    public static String convertTimestampToFormat(long timestamp) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(timestamp * 1000L);
        String date = DateFormat.format("dd-MM-yyyy hh:mm:ss", cal).toString();
        return date;
    }

    private static String unitFormat(int i) {
        String retStr = null;
        if (i >= 0 && i < 10)
            retStr = "0" + Integer.toString(i);
        else
            retStr = "" + i;
        return retStr;
    }

    public static int getThumbTotalCount(long durationMs) {
        if (durationMs <= Constant.DURATION_MIN_SECOND * 1000) {
            return Constant.FRAME_COUNT_MIN;
        } else {
            int count = (int) (durationMs / Constant.SECOND_A_FRAME);
            if (count > Constant.FRAME_COUNT_MAX) {
                count = Constant.FRAME_COUNT_MAX;
            }
            return count;
        }

    }

    public static float getPixelAMilisSecond(long durationMs) {
        int totalPixel = getThumbTotalCount(durationMs) * (UnitConverter.dpToPx(Constant.WIDTH_FRAME_DP));
        return (totalPixel / (float) (durationMs));

    }

    public static MediaMetadataRetriever getMediaMetadataRetriever(String url) {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(url, new HashMap<String, String>());
        return mediaMetadataRetriever;
    }

    public static MediaMetadataRetriever getMediaMetadataRetriever(Context context, String videoPath) {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(context, Uri.fromFile(new File(videoPath)));
        return mediaMetadataRetriever;
    }

    public static Bitmap getFrameBitmapAt(Context context, String videoPath, Long atTimeMs) {
        MediaMetadataRetriever mediaMetadataRetriever = getMediaMetadataRetriever(context, videoPath);
        Bitmap bitmap = mediaMetadataRetriever.getFrameAtTime(atTimeMs);
        mediaMetadataRetriever.release();
        return bitmap;
    }

    public static Bitmap getFrameBitmapAt(MediaMetadataRetriever mediaMetadataRetriever, Long atTimeMs) {
        Bitmap bitmap = mediaMetadataRetriever.getFrameAtTime(atTimeMs);
        return bitmap;
    }

    public static Bitmap getBitmapThumbnail(Context context, String videoPath) {
        MediaMetadataRetriever mediaMetadataRetriever = getMediaMetadataRetriever(context, videoPath);
        Bitmap bm = getBitmapThumbnail(mediaMetadataRetriever);
        mediaMetadataRetriever.release();
        return bm;
    }

    public static Bitmap getFrameBitmapAt(String url, Long atTimeMs) {
        MediaMetadataRetriever mediaMetadataRetriever = getMediaMetadataRetriever(url);
        Bitmap bitmap = mediaMetadataRetriever.getFrameAtTime(atTimeMs);
        mediaMetadataRetriever.release();
        return bitmap;
    }

    //    public static Bitmap getFrameBitmapAtIndex(String url, int index) {
//        MediaMetadataRetriever mediaMetadataRetriever = getMediaMetadataRetriever(url);
//        Bitmap bitmap = mediaMetadataRetriever.getFrameAtIndex(index,null);
//        mediaMetadataRetriever.release();
//        return bitmap;
//    }
    public static Long getDurationMsVideo(String url) {
        MediaMetadataRetriever mediaMetadataRetriever = getMediaMetadataRetriever(url);
        String time = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        mediaMetadataRetriever.release();
        return (time != null) ? Long.parseLong(time) : 0L;
    }

    public static Long getDurationMsVideo(MediaMetadataRetriever mediaMetadataRetriever) {
        String time = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        return (time != null) ? Long.parseLong(time) : 0L;
    }

    public static Long getDurationMsVideo(Context context, String videoPath) {
        MediaMetadataRetriever mediaMetadataRetriever = getMediaMetadataRetriever(context, videoPath);
        String time = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        mediaMetadataRetriever.release();
        return (time != null) ? Long.parseLong(time) : 0L;
    }

    public static Bitmap getBitmapThumbnail(String url) {
        Bitmap bm = getFrameBitmapAt(url, 1000L);
        bm = Bitmap.createScaledBitmap(
                bm,
                500,
                500 * bm.getHeight() / bm.getWidth(),
                false);
        return bm;
    }

    public static Bitmap getBitmapThumbnail(MediaMetadataRetriever mediaMetadataRetriever) {
        Bitmap bm;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
//            bm = mediaMetadataRetriever.getPrimaryImage();
//        } else {
        bm = getFrameBitmapAt(mediaMetadataRetriever, 1000L);
//        }
        bm = Bitmap.createScaledBitmap(
                bm,
                500,
                500 * bm.getHeight() / bm.getWidth(),
                false);
        return bm;
    }

    public static Size getSizeVideo(String url) {
        MediaMetadataRetriever mediaMetadataRetriever = getMediaMetadataRetriever(url);
        String heigh = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
        String widht = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
        mediaMetadataRetriever.close();
        return new Size(Integer.parseInt(widht), Integer.parseInt(heigh));
    }
    public static int getVideoRotation(String videoFilePath) {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(videoFilePath);
        String orientation = mediaMetadataRetriever.extractMetadata(
                MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION
        );
        return Integer.parseInt(orientation);
    }
    public static Size getVideoResolution(String path) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(path);
        int width = Integer.parseInt(
                Objects.requireNonNull(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH))
        );
        int height = Integer.parseInt(
                Objects.requireNonNull(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT))
        );
        retriever.release();
        int rotation = getVideoRotation(path);
        if (rotation == 90 || rotation == 270) {
            return new Size(height, width);
        }
        return new Size(width, height);
    }
}
