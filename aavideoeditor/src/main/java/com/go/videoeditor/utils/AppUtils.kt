package com.go.videoeditor.utils

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.ParcelFileDescriptor
import android.provider.MediaStore
import android.view.View
import android.view.WindowManager
import android.webkit.MimeTypeMap
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import androidx.fragment.app.FragmentActivity
import com.go.videoeditor.R
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.model.ItemFile
import com.go.videoeditor.stream.ItemLivestreamConfig
import com.skydoves.powermenu.PowerMenuItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

class AppUtils {
    companion object {
        val FOLDER_EDITED = "Edited"
        val FOLDER_DOWNLOAD = "Download"
        val FOLDER_STORAGE = "Storage"
        val DATE_FORMAT = "yyyy/MM/dd HH:mm"

        fun getVideoConfig(quality: String, itemObj: ItemLivestreamConfig?): ItemLivestreamConfig {
            var item = itemObj
            if (item == null) {
                item = ItemLivestreamConfig()
            }
            when (quality) {
                "4K" -> {
                    item.videoWidth = 3840
                    item.videoHeight = 2160
                    item.videoBitrate = 2500
                    item.fps = 60
                    item.audioBitrate = 320
                    item.sampleRate = 48000
                }
                "2K" -> {
                    item.videoWidth = 2560
                    item.videoHeight = 1440
                    item.videoBitrate = 2000
                    item.fps = 60
                    item.audioBitrate = 256
                    item.sampleRate = 48000
                }
                "1080P" -> {
                    item.videoWidth = 1920
                    item.videoHeight = 1080
                    item.videoBitrate = 1500
                    item.fps = 50
                    item.audioBitrate = 192
                    item.sampleRate = 48000
                }
                "720P" -> {
                    item.videoWidth = 1280
                    item.videoHeight = 720
                    item.videoBitrate = 1200
                    item.fps = 30
                    item.audioBitrate = 128
                    item.sampleRate = 44100
                }
                "480P" -> {
                    item.videoWidth = 854
                    item.videoHeight = 480
                    item.videoBitrate = 800
                    item.fps = 25
                    item.audioBitrate = 128
                    item.sampleRate = 44100
                }
                "360P" -> {
                    item.videoWidth = 640
                    item.videoHeight = 360
                    item.videoBitrate = 500
                    item.fps = 25
                    item.audioBitrate = 128
                    item.sampleRate = 44100
                }
                "240P" -> {
                    item.videoWidth = 426
                    item.videoHeight = 240
                    item.videoBitrate = 300
                    item.fps = 25
                    item.audioBitrate = 128
                    item.sampleRate = 44100
                }
            }
            return item
        }

        var itemQualityList: ArrayList<PowerMenuItem>? = null
        fun getQualityList(): ArrayList<PowerMenuItem> {
            if (itemQualityList != null) {
                return itemQualityList!!
            }
            itemQualityList = ArrayList()
//        itemList.add(PowerMenuItem("4K"))
//        itemList.add(PowerMenuItem("2K"))
            itemQualityList!!.add(PowerMenuItem("1080P"))
            itemQualityList!!.add(PowerMenuItem("720P"))
            itemQualityList!!.add(PowerMenuItem("480P"))
            itemQualityList!!.add(PowerMenuItem("360P"))
            itemQualityList!!.add(PowerMenuItem("240P"))
            val maxQuality =
                MyApplication.getInstance()?.getDataManager()?.itemAppConfig?.maxStreamQuality
            if (!maxQuality.isNullOrEmpty()) {
                var indexOf = -1
                for (i in 0 until itemQualityList!!.size) {
                    if (maxQuality.equals(itemQualityList!!.get(i).title.toString(), true)) {
                        indexOf = i
                        break
                    }
                }
                if (indexOf > 0) {
                    for (i in indexOf - 1 downTo 0) {
                        itemQualityList!!.removeAt(i)
                    }
                }
            }
            return itemQualityList!!
        }

        fun convertSecondsToTime(seconds: Long): String {
            val timeStr: String?
            var hour = 0
            var minute = 0
            var second = 0
            if (seconds <= 0) return "00:00"
            minute = (seconds / 60).toInt()
            if (minute < 60) {
                second = (seconds % 60).toInt()
                timeStr = unitFormat(minute) + ":" + unitFormat(second)
            } else {
                hour = minute / 60
                if (hour > 99) return "99:59:59"
                minute = minute % 60
                second = (seconds - hour * 3600 - minute * 60).toInt()
                timeStr = unitFormat(hour) + ":" + unitFormat(minute) + ":" + unitFormat(second)
            }
            return timeStr
        }

        fun unitFormat(i: Int): String {
            val retStr: String?
            retStr = if (i >= 0 && i < 10) "0" + Integer.toString(i) else "" + i
            return retStr
        }

        fun createVideoFilePath(context: Context, folderName: String): String {
            val folder = File(context.getExternalFilesDir(folderName)!!.absolutePath)
            if (!folder.exists()) {
                folder.mkdirs()
            }
            val sdf = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)
            var fileName = "VIDEO_${sdf.format(Date())}"
            fileName = folder.absolutePath + "/" + fileName + ".mp4"
            return fileName
        }

        fun createVideoFilePath(context: Context): String {
            return createVideoFilePath(context, "")
        }

        fun createVideoFileEditedPath(context: Context): String {
            return createVideoFilePath(context, FOLDER_EDITED)
        }

        fun getVideoFileEditedPath(context: Context): File {
            return getFolderPath(context, FOLDER_EDITED)
        }

        fun createPictureFilePath(context: Context): String {
//            val appName = activity.getString(R.string.app_name).replace(" ", "")
//            val folder =  File(activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!.absolutePath + "/" + appName)
            val folder =
                File(context.getExternalFilesDir("")!!.absolutePath)
            if (!folder.exists()) {
                folder.mkdirs()
            }
            val sdf = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)
            var fileName = "IMG_${sdf.format(Date())}"
            fileName = folder.absolutePath + "/" + fileName + ".jpg"
            return fileName
        }

        fun createAudioFilePath(context: Context): String {
//            val appName = activity.getString(R.string.app_name).replace(" ", "")
//            val folder = File(activity.getExternalFilesDir(Environment.DIRECTORY_MUSIC)!!.absolutePath + "/" + appName)
            val folder =
                File(context.getExternalFilesDir("")!!.absolutePath)
            if (!folder.exists()) {
                folder.mkdirs()
            }
            val sdf = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)
            var fileName = "AUDIO_${sdf.format(Date())}"
            fileName = folder.absolutePath + "/" + fileName + ".3gpp"
            return fileName
        }

        fun convertBitmapToFile(context: Context, bitmap: Bitmap?): String? {
            if (bitmap == null) {
                return null
            }
            val filePath = createPictureFilePath(context)
            val file = File(filePath)
            file.createNewFile()
            val fos = FileOutputStream(file)
            bitmap.compress(
                Bitmap.CompressFormat.JPEG,
                100,
                fos
            )
            return filePath
        }

        fun getFolderPath(context: Context, folderName: String): File {
            return context.getExternalFilesDir(folderName)!!
        }

        fun getFolderPath(context: Context): File {
            return getFolderPath(context, "")
        }

        fun getDownloadFolderPath(context: Context): File {
            return getFolderPath(context, FOLDER_DOWNLOAD)
        }

        fun checkVideoDownloadFileExist(context: Context, fileName: String?): Boolean {
            if (fileName.isNullOrEmpty()) {
                return false
            }
            /*val file = File(getDownloadFolderPath(context), fileName)
            return file.exists()*/

            val childFolder = context.getString(R.string.app_name).trim()
                .replace(" ", "-") + "/" + FOLDER_DOWNLOAD
            val filePath: String
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val folderPath = Environment.DIRECTORY_MOVIES + "/" + childFolder
                filePath = "/storage/emulated/0/" + folderPath + "/" + fileName
            } else {
                val directory = Environment.getExternalStorageDirectory()
                    .getAbsolutePath()
                val folderPath = directory + "/" + Environment.DIRECTORY_MOVIES + "/" + childFolder
                filePath = folderPath + "/" + fileName
            }
            val file = File(filePath)
            return file.exists()
        }

        fun getVideoDownloadFile(context: Context, fileName: String): File {
            return File(getDownloadFolderPath(context), fileName)
        }

        fun showFileManagerBrowseFileDownload(context: Context) {
            val file = getDownloadFolderPath(context)
            if (file.exists()) {
                val selectedUri = FileProvider.getUriForFile(
                    context,
                    context.getPackageName().toString() + ".provider",
                    file
                )
                val intent = Intent(Intent.ACTION_VIEW)
                intent.setDataAndType(selectedUri, "*/*")
                context.startActivity(intent)
            }
        }

        fun createImageTempFile(context: Context): File {
            val path = createPictureFilePath(context)
            return File(path)
        }

        fun removeTempDownloadFolderPath(context: Context) {
            val file = getDownloadFolderPath(context)
            if (file.exists() && file.isDirectory) {
                val list = file.listFiles()
                if (list != null) {
                    for (item in list) {
                        if (item.name.endsWith(".temp")) {
                            item.delete()
                        }
                    }
                }
            }
        }

        fun getMimeType(file: File): String? {
            var mimeType: String? = ""
            val extension: String = getExtension(file.name)
            if (MimeTypeMap.getSingleton().hasExtension(extension)) {
                mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
            }
            return mimeType
        }

        fun getExtension(fileName: String): String {
            val arrayOfFilename = fileName.toCharArray()
            for (i in arrayOfFilename.size - 1 downTo 1) {
                if (arrayOfFilename[i] == '.') {
                    return fileName.substring(i + 1, fileName.length)
                }
            }
            return ""
        }

        fun addToGallery(context: Context, filePath: String, folderType: String? = null) {
            val file = File(filePath)
            if (!file.exists()) {
                return
            }
            Loggers.e("addToGallery_path", filePath)
            Loggers.e("addToGallery_name", file.name)
            Loggers.e("addToGallery_mimeType", getMimeType(file))
            Loggers.e("addToGallery_length", "${file.length()}")

            val resolver = context.contentResolver
            val contentValues = ContentValues()
            val uriFileSaved: Uri?
            val mimeType = getMimeType(file)
            var childFolder = context.getString(R.string.app_name).trim().replace(" ", "-")
            if (!folderType.isNullOrEmpty()) {
                childFolder += "/" + folderType
            }
            val isVideoFile: Boolean
            val parentFolder: String
            if (mimeType!!.contains("video", true) || mimeType.contains("audio", true)) {
                isVideoFile = true
                parentFolder = Environment.DIRECTORY_MOVIES
            } else {
                isVideoFile = false
                parentFolder = Environment.DIRECTORY_PICTURES
            }
            childFolder = parentFolder + "/" + childFolder
            Loggers.e("addToGallery_path", "childFolder = $childFolder")

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                contentValues.put(MediaStore.Video.Media.TITLE, file.name)
                contentValues.put(MediaStore.Video.Media.DISPLAY_NAME, file.name)
                contentValues.put(MediaStore.Video.Media.MIME_TYPE, mimeType)
                contentValues.put(MediaStore.Video.Media.SIZE, file.length())
//                contentValues.put(MediaStore.Video.Media.DURATION, 0)
                contentValues.put(
                    MediaStore.Video.Media.DATE_ADDED,
                    System.currentTimeMillis() / 1000
                )
                contentValues.put(
                    MediaStore.Video.Media.RELATIVE_PATH,
                    childFolder
                )
                contentValues.put(MediaStore.Video.Media.DATE_TAKEN, System.currentTimeMillis());
                contentValues.put(MediaStore.Video.Media.IS_PENDING, 1)
                val collection =
                    if (isVideoFile) MediaStore.Video.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY) else MediaStore.Images.Media.getContentUri(
                        MediaStore.VOLUME_EXTERNAL_PRIMARY
                    )
                uriFileSaved = resolver.insert(collection, contentValues)
            } else {
                val directory = Environment.getExternalStorageDirectory()
                    .getAbsolutePath() + "/" + childFolder
                val fileDir = File(directory)
                if (!fileDir.exists()) {
                    Loggers.e("addToGallery_fileDir", "not exists: $directory")
                    fileDir.mkdirs()
                } else {
                    Loggers.e("addToGallery_fileDir", "exists: $directory")
                }
                Loggers.e(
                    "addToGallery_fileDir",
                    "check exists: ${fileDir.exists()} _ $directory"
                )

                val fileUri = File(directory, file.name)
//                val createdvideo = File(file.absolutePath)
                contentValues.put(MediaStore.Video.Media.TITLE, file.name)
                contentValues.put(MediaStore.Video.Media.DISPLAY_NAME, file.name)
                contentValues.put(MediaStore.Video.Media.MIME_TYPE, mimeType)
                contentValues.put(
                    MediaStore.Video.Media.DATE_ADDED,
                    System.currentTimeMillis() / 1000
                )
                contentValues.put(MediaStore.Video.Media.DATA, fileUri.getAbsolutePath())
                uriFileSaved =
                    resolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, contentValues)
            }
            if (uriFileSaved == null) {
                return
            }

            val pfd: ParcelFileDescriptor?
            try {
                pfd = resolver.openFileDescriptor(uriFileSaved, "w")
                val out = FileOutputStream(pfd!!.fileDescriptor)
                val ins = FileInputStream(file)
                val buf = ByteArray(8192)
                var len: Int
                while (ins.read(buf).also { len = it } > 0) {
                    out.write(buf, 0, len)
                }
                out.close()
                ins.close()
                pfd.close()
                if (Build.VERSION.SDK_INT >= 29) {
                    contentValues.clear()
                    contentValues.put(MediaStore.Video.Media.IS_PENDING, 0)
                    resolver.update(uriFileSaved, contentValues, null, null)
                }
                file.delete()//remove cache
                Loggers.e("addToGallery_path", "Done: ${uriFileSaved.toString()}")
            } catch (e: Exception) {
                e.printStackTrace()
            }
            refreshGallery(context, filePath)
        }

        fun addToGalleryRunBackground(
            context: Context,
            filePath: String,
            folderType: String? = null
        ) {
            GlobalScope.async(Dispatchers.IO) {
                addToGallery(context, filePath, folderType)
            }
        }

        fun refreshGallery(context: Context, filePath: String) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val values = ContentValues(2)
                val file = File(filePath)
                val mimeType = getMimeType(file) ?: return
                if (mimeType.contains("video")) {
                    values.put(MediaStore.Video.Media.MIME_TYPE, mimeType)
                    values.put(MediaStore.Video.Media.DATA, filePath)
                    context.contentResolver.insert(
                        MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                        values
                    )
                } else {
                    values.put(MediaStore.Images.Media.MIME_TYPE, mimeType)
                    values.put(MediaStore.Images.Media.DATA, filePath)
                    context.contentResolver.insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        values
                    )
                }
            } else {
                val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
                val f = File(filePath)
                val contentUri = Uri.fromFile(f)
                mediaScanIntent.data = contentUri
                context.sendBroadcast(mediaScanIntent)
            }
        }

        fun getRealPathFromURI(context: Context, contentUri: Uri): String? {
            var cursor: Cursor?
            try {
                val proj = null//arrayOf(MediaStore.Video.Media.DATA)
                cursor = context.contentResolver.query(contentUri, proj, null, null, null)
                if (cursor == null || cursor.count == 0) {
                    return null
                }
                val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                cursor.moveToFirst()
                val path = cursor.getString(column_index)
                cursor.close()
                return path
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
            }
            return null
        }

        fun getFileListFromFolder(context: Context, folderPath: String): ArrayList<ItemFile> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                return getFileListFromAPI29(context, folderPath)
            } else {
                return getFileListBelowAPI29(folderPath)
            }
        }

        @RequiresApi(Build.VERSION_CODES.Q)
        private fun getFileListFromAPI29(
            context: Context,
            folderPath: String
        ): ArrayList<ItemFile> {
            val itemList: ArrayList<ItemFile> = ArrayList()
            val selection = "${MediaStore.Video.Media.RELATIVE_PATH} like ? "
            val selectionargs = arrayOf("%" + folderPath + "%")
            val externalUri = if (folderPath.startsWith(
                    Environment.DIRECTORY_MOVIES,
                    true
                )
            ) MediaStore.Video.Media.EXTERNAL_CONTENT_URI else MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            val projection = arrayOf(
                MediaStore.Files.FileColumns._ID,
                MediaStore.Images.Media.DATE_TAKEN,
                MediaStore.Images.Media.DATE_ADDED,
                MediaStore.MediaColumns.TITLE,
                MediaStore.Images.Media.MIME_TYPE,
                MediaStore.Images.Media.SIZE,
                MediaStore.MediaColumns.RELATIVE_PATH
            )
            val resolver = context.contentResolver
            val cursor = resolver.query(
                externalUri,
                projection,
                selection,
                selectionargs,
                MediaStore.Images.Media.DATE_TAKEN + " DESC"
            )
//            https://stackoverflow.com/questions/60024543/android-q-how-to-get-a-list-of-images-from-a-specific-directory
            val idColumn = cursor!!.getColumnIndex(MediaStore.MediaColumns._ID)
            val titleColumn = cursor.getColumnIndex(MediaStore.MediaColumns.TITLE)
            val dateAddedColumn = cursor.getColumnIndex(MediaStore.MediaColumns.DATE_ADDED)
            val sizeColumn = cursor.getColumnIndex(MediaStore.MediaColumns.SIZE)
            val mimeTypeColumn = cursor.getColumnIndex(MediaStore.MediaColumns.MIME_TYPE)
            val relativePathColumn =
                cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.RELATIVE_PATH)

            while (cursor.moveToNext()) {
                val fileUri = Uri.withAppendedPath(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    cursor.getString(idColumn)
                )
                var filePath = getRealPathFromURI(context, fileUri)
                /*if (filePath.isNullOrEmpty()) {
                    continue
                }*/
                val id = cursor.getString(idColumn)
                var title = cursor.getString(titleColumn)
                val dateAdded = cursor.getString(dateAddedColumn).toLong() * 1000
                val size = cursor.getString(sizeColumn)?.toLong()
//                val relativePath = cursor.getString(relativePathColumn)
                val mimeType = cursor.getString(mimeTypeColumn)
                if (size == null || size == 0L) {
                    continue
                }

                val sdf = SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH)
                val date = Date(dateAdded)
                val modifiedDate = sdf.format(date)
                var fileType: Int
                if (mimeType!!.contains("video", true)) {
                    if (mimeType.contains("3gpp", true)) {
                        fileType = Constant.FILE_TYPE_AUDIO
                    } else {
                        fileType = Constant.FILE_TYPE_VIDEO
                    }
                } else if (mimeType.contains("image", true)) {
                    fileType = Constant.FILE_TYPE_IMAGE
                } else {
                    continue
                }
                val extensionArr = mimeType.split("/")
                if (extensionArr.size > 1) {
                    val extension = extensionArr.get(extensionArr.size - 1).replace("jpeg", "jpg")
                    if (!title.endsWith(extension)) {
                        title = title.plus(".$extension")
                    }
                }
                if (filePath.isNullOrEmpty()) {
                    filePath = "/storage/emulated/0/" + folderPath + "/" + title
                }
                itemList.add(
                    ItemFile(
                        id,
                        fileType,
                        title,
                        fileUri,
                        filePath,
                        dateAdded,
                        modifiedDate,
                        null,
                        0,
                        size
                    )
                )
                Loggers.e("A_getFileListFromAPI29_", "${fileUri.toString()}, $filePath __")
            }
            return itemList
        }

        private fun getFileListBelowAPI29(folderPath: String): ArrayList<ItemFile> {
            val itemList: ArrayList<ItemFile> = ArrayList()
            val folderFile = File(folderPath)
            if (!folderFile.exists()) {
                return itemList
            }
            val folder = folderFile.listFiles()
            for (i in folder.indices) {
                val file = folder.get(i)
                if (file.isDirectory) {
                    continue
                }
                val length = file.length()
                if (length == 0L) {
                    continue
                }
                val filePath = file.absolutePath
                val fileName = file.name
                val extension = file.extension
                val lastModified = file.lastModified()

                var fileType = Constant.FILE_TYPE_UNDEFINED

                when (extension.toLowerCase(Locale.ENGLISH)) {
                    "jpg", "jpeg", "png", "gif" -> fileType = Constant.FILE_TYPE_IMAGE
                    "mp4", "avi" -> fileType = Constant.FILE_TYPE_VIDEO
                    "3gpp", "amr" -> fileType = Constant.FILE_TYPE_AUDIO
                }
                val sdf = SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH)
                val date = Date(lastModified)
                val modifiedDate = sdf.format(date)
                itemList.add(
                    ItemFile(
                        null,
                        fileType,
                        fileName,
                        null,
                        filePath,
                        lastModified,
                        modifiedDate,
                        null,
                        0,
                        length
                    )
                )
                Loggers.e(
                    "getListFile_ $i",
                    "$fileName, $extension, $length, $modifiedDate, $filePath"
                )
            }
            return itemList
        }

        fun deleteFile(context: Context, itemObject: ItemFile) {
            val file = File(itemObject.filePath)
            if (file.exists()) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                    file.delete()
                    refreshGallery(context, itemObject.filePath)
                    return
                }
                if (!itemObject.fileId.isNullOrEmpty() && itemObject.fileUri != null) {
                    val mimeType = AppUtils.getMimeType(file)
                    if (mimeType == null) {
                        return
                    }
                    val contentUri: String
                    if (mimeType.contains("video", true) || mimeType.contains("audio", true)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI.toString()
                    } else {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI.toString()
                    }
                    val where = MediaStore.Images.Media._ID + "=?"
                    val selectionArgs = arrayOf(itemObject.fileId)
                    try {
                        context.contentResolver.delete(Uri.parse(contentUri), where, selectionArgs)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                file.delete()
                try {
                    refreshGallery(context, itemObject.filePath)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
        fun setFullStatusTransparent(activity: FragmentActivity, layout: View?) {
            try {
                activity.window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                activity.window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                activity.window.decorView.systemUiVisibility = (
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
                activity.window.statusBarColor = Color.TRANSPARENT
            } catch (e: Exception) {
                e.printStackTrace()
            }
//        val bgColor = "#00000000"
//        setBackgroudColor(layout, bgColor)
            if (layout == null) {
                return
            }
            val statusBarHeight: Int = getStatusBarHeight(activity)
            if (layout.layoutParams is RelativeLayout.LayoutParams) {
                val params = layout.layoutParams as RelativeLayout.LayoutParams
                params.topMargin = statusBarHeight
                layout.layoutParams = params
            } else if (layout.layoutParams is LinearLayout.LayoutParams) {
                val params = layout.layoutParams as LinearLayout.LayoutParams
                params.topMargin = statusBarHeight
                layout.layoutParams = params
            }
        }

        private fun getStatusBarHeight(activity: FragmentActivity): Int {
            val res = activity.resources.getIdentifier("status_bar_height", "dimen", "android")
            var statusBarHeight = 0
            if (res != 0) {
                statusBarHeight = activity.resources.getDimensionPixelSize(res)
            }
            return statusBarHeight
        }

        fun setStatusBarColor(activity: FragmentActivity, isTextLight: Boolean, bgColor: String?) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                activity.window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                if (!bgColor.isNullOrEmpty()) {
                    activity.window.statusBarColor = Color.parseColor(bgColor)
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    activity.window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                    try {
                        if (isTextLight) {
                            //                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                            activity.window.decorView.systemUiVisibility =
                                activity.window.decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv() //set status text  light
                        } else {
                            activity.window.decorView.systemUiVisibility =
                                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }
}