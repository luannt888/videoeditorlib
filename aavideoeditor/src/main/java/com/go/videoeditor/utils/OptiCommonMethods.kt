package com.go.videoeditor.utils

import android.app.Activity
import android.content.Context
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import com.go.videoeditor.app.Constant
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


object OptiCommonMethods {
    val tagName = this::class.java.simpleName

    fun getFileExtension(filePath: String): String {
        val extension = filePath.substring(filePath.lastIndexOf("."))
        Log.e(tagName, "extension = $extension")
        return extension
    }

    fun getVideoDuration(context: Context, filePath: String): Long {
        return getVideoDuration(context, File(filePath))
    }

    fun getVideoDuration(context: Context, file: File): Long {
        return getVideoDuration(context, Uri.fromFile(file))
    }

    fun getVideoDuration(context: Context, uri: Uri): Long {
        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(context, uri)
        val time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
        val timeInMillis = if (time != null) time.toLong() else 0
        retriever.release()
        return timeInMillis
    }

    //get video duration in minutes
    fun convertDurationInMin(duration: Long): Long {
        val minutes = TimeUnit.MILLISECONDS.toMinutes(duration)
        Log.e(tagName, "duration = $duration _ min: $minutes")
        return if (minutes > 0) {
            minutes
        } else {
            0
        }
    }

    fun createVideoFile(context: Context): File {
        val timeStamp: String = SimpleDateFormat(Constant.DATE_FORMAT, Locale.getDefault()).format(
            Date()
        )
        val imageFileName: String = Constant.APP_NAME + timeStamp + "_"
        val storageDir: File = context.getExternalFilesDir(Environment.DIRECTORY_MOVIES)!!
        if (!storageDir.exists()) storageDir.mkdirs()
        return File.createTempFile(imageFileName, Constant.VIDEO_FORMAT, storageDir)
    }

    fun getAllFrameVideo(file: File) {

    }

    fun decodeFile(file: String): Bitmap? {
        var fileInputStream: FileInputStream? = null
        try {
            fileInputStream = FileInputStream(file)
            return decodeInputStream(fileInputStream)
        } finally {
            fileInputStream?.run {
                close()
            }
        }
    }

    fun decodeInputStream(inputStream: InputStream): Bitmap? {
        val opt_decord = BitmapFactory.Options()
        opt_decord.inPurgeable = true
        opt_decord.inInputShareable = true
        var bitmap_ret: Bitmap? = null
        try {
            bitmap_ret = BitmapFactory.decodeStream(inputStream, null, opt_decord)
        } catch (e: Throwable) {
            // TODO: handle exception
            bitmap_ret = null
        }

        return bitmap_ret
    }

    fun getRealPathFromUri(context: Context, contentUri: Uri?): String? {
        var cursor: Cursor? = null
        try {
            val proj = arrayOf(MediaStore.Video.Media.DATA)
            cursor = context.contentResolver.query(contentUri!!, proj, null, null, null)
            if (cursor == null || cursor.count == 0) {
                return null
            }
            val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            val path = cursor.getString(column_index)
            cursor.close()
            return path
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }
        return null
    }

    fun getFileName(activity: Activity, uri: Uri): String? {
        var uri = uri
        var result: String?

        //if uri is content
        if (uri.scheme != null && uri.scheme == "content") {
            val cursor: Cursor = activity.contentResolver.query(uri, null, null, null, null)!!
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    //local filesystem
                    var index = cursor.getColumnIndex("_data")
                    if (index == -1) //google drive
                        index = cursor.getColumnIndex("_display_name")
                    result = cursor.getString(index)
                    uri = if (result != null) Uri.parse(result) else return null
                }
            } finally {
                cursor.close()
            }
        }
        result = uri.path

        //get filename + ext of path
        val cut = result!!.lastIndexOf('/')
        if (cut != -1) result = result.substring(cut + 1)
        return result
    }

    fun getVideoFilePath(activity: Activity): String? {
        return FileUltis.createVideoFile(activity).absolutePath
    }

    fun getVideoDurationBySharedContentUri(activity: Activity, uri: Uri): Long {
        val realPath = getRealPathFromUri(activity, uri)
        if (realPath.isNullOrEmpty()) {
            return 0
        }
        val file = File(realPath)
        val timeInMillis =
            OptiCommonMethods.getVideoDuration(activity, file)
        Log.e("TrimVideo", "timeInMillis: $timeInMillis")
        val duration = convertDurationInMin(timeInMillis)
        Log.e("TrimVideo", "duration: $duration")
        val second = TimeUnit.MILLISECONDS.toSeconds(timeInMillis)
        return second
    }
}

fun writeToFile(bitmap: Bitmap?, outBitmap: String, quality: Int = 50): Boolean {
    var success = false
    if (bitmap == null) {
        return success
    }
    var out: FileOutputStream? = null
    try {
        out = FileOutputStream(outBitmap)
        success = bitmap.compress(Bitmap.CompressFormat.JPEG, quality, out)
        out.close()
    } catch (e: IOException) {
        // success is already false
    } finally {
        try {
            if (out != null) {
                out.close()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }
    return success
}