package com.go.videoeditor.utils

import android.app.Activity
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

class MyRequestPermissions() : Fragment() {
    var permissionArray: Array<String>? = null
    var onMyRequestPermissionListener: OnMyRequestPermissionListener? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        permissionArray = requireArguments().getStringArray("data")
        if (permissionArray.isNullOrEmpty()) {
            onMyRequestPermissionListener?.onError()

            Handler(Looper.getMainLooper()).postDelayed(
                {
                    requireActivity().supportFragmentManager.beginTransaction()
                        .remove(this@MyRequestPermissions)
                        .commit()
                }, 1000
            )
            return
        }
        val permReqLauncher =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
                permissions.size
                if (hasPermissions(activity, permissionArray)) {
                    onMyRequestPermissionListener?.onPermissionGranted()
                } else {
                    onMyRequestPermissionListener?.onPermissionDenied()
                }
                requireActivity().runOnUiThread {
                    requireActivity().supportFragmentManager.beginTransaction().remove(this)
                        .commit()
                }
            }
        permReqLauncher.launch(permissionArray)
    }


    interface OnMyRequestPermissionListener {
        fun onPermissionGranted()
        fun onPermissionDenied()
        fun onError() {}
    }

    companion object {
        fun requestPermission(
            activity: Activity?,
            permissions: Array<String>?,
            onMyRequestPermissionListener: OnMyRequestPermissionListener?
        ) {
            if (activity == null || activity !is FragmentActivity || activity.isDestroyed || activity.isFinishing || permissions.isNullOrEmpty()) {
                onMyRequestPermissionListener?.onError()
                return
            }
            val myRequestPermissions = MyRequestPermissions()
            val bundle = Bundle()
            bundle.putStringArray("data", permissions)
            myRequestPermissions.arguments = bundle
            myRequestPermissions.onMyRequestPermissionListener = onMyRequestPermissionListener
            activity.supportFragmentManager.beginTransaction()
                .add(myRequestPermissions, "REQUEST_PERMISSION").commit()
        }

        fun requestPermission(
            activity: Activity?,
            permission: String?,
            onMyRequestPermissionListener: OnMyRequestPermissionListener?
        ) {
            if (permission.isNullOrEmpty()) {
                return
            }
            requestPermission(activity, arrayOf(permission), onMyRequestPermissionListener)
        }

        fun hasPermissions(activity: Activity?, permissions: Array<String>?): Boolean {
            if (activity == null || permissions == null || permissions.size == 0) {
                return false
            }
            return permissions.all {
                ActivityCompat.checkSelfPermission(
                    activity,
                    it
                ) == PackageManager.PERMISSION_GRANTED
            }
        }

        fun hasPermissions(activity: Activity?, permission: String?): Boolean {
            if (activity == null || permission.isNullOrEmpty()) {
                return false
            }
            return hasPermissions(activity, arrayOf(permission))
        }
    }
}