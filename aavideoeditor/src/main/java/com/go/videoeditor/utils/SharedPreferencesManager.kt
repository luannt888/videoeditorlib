package com.go.videoeditor.utils

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.go.videoeditor.app.Constant

class SharedPreferencesManager {
    companion object {
        val KEY_VIDEO_DATA = "KEY_VIDEO_DATA"

        fun saveVideo(context: Context, projectKey: String, data: String) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            val editor = prefs.edit()
            val list = getVideoList(context)
            if (list == null || list.size == 0) {
                val item = "$projectKey __ $data"
                editor.putString(KEY_VIDEO_DATA, item)
                editor.apply()
                return
            }
            for (i in 0 until list.size) {
                var item = list.get(i)
                val arr = item.split(Constant.KEY_SPLIT_DRAFT_VIDEO_ITEM_CONTENT)
                if (arr[0].equals(projectKey)) {
                    list.removeAt(i)
                    item = "$projectKey __ $data"
                    list.add(0, item)
                    val newListStr = list.joinToString(Constant.KEY_SPLIT_DRAFT_VIDEO_ITEM)
                    editor.putString(KEY_VIDEO_DATA, newListStr)
                    editor.apply()
                    return
                }
            }

            val item = "$projectKey __ $data"
            list.add(0, item)
            val newListStr = list.joinToString(Constant.KEY_SPLIT_DRAFT_VIDEO_ITEM)
            editor.putString(KEY_VIDEO_DATA, newListStr)
            editor.apply()
        }

        fun deleteVideo(context: Context, projectKey: String) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            val editor = prefs.edit()
            val list = getVideoList(context)
            if (list == null || list.size == 0) {
                return
            }
            for (i in 0 until list.size) {
                val item = list.get(i)
                val arr = item.split(Constant.KEY_SPLIT_DRAFT_VIDEO_ITEM_CONTENT)
                if (arr[0].equals(projectKey)) {
                    list.removeAt(i)
                    val newListStr = list.joinToString(Constant.KEY_SPLIT_DRAFT_VIDEO_ITEM)
                    editor.putString(KEY_VIDEO_DATA, newListStr)
                    editor.apply()
                    return
                }
            }
        }

        fun getVideoList(context: Context): ArrayList<String>? {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            val listJson = prefs.getString(KEY_VIDEO_DATA, null)
            if (listJson.isNullOrEmpty()) {
                return null
            }
            val arr = listJson.split(Constant.KEY_SPLIT_DRAFT_VIDEO_ITEM)
            val list: ArrayList<String> = ArrayList()
            for (i in 0 until arr.size) {
                list.add(arr[i])
            }
            return list
        }

        private val KEY_FONT_SIZE = "KEY_FONT_SIZE"

        fun saveUser(context: Context, username: String?, token: String?) {
            val sharedPreferences = context.getSharedPreferences(
                Constant.CONFIG,
                Context.MODE_PRIVATE
            )
            val editor = sharedPreferences.edit()
            editor.putString(Constant.USERNAME, username)
            editor.putString(Constant.ACCESS_TOKEN, token)
            editor.apply()
        }

        fun getToken(context: Context): String? {
            val sharedPreferences: SharedPreferences = context.getSharedPreferences(
                Constant.CONFIG,
                Context.MODE_PRIVATE
            )
            return sharedPreferences.getString(Constant.ACCESS_TOKEN, null)
        }

        fun getUserName(context: Context): String {
            val sharedPreferences: SharedPreferences = context.getSharedPreferences(
                Constant.CONFIG,
                Context.MODE_PRIVATE
            )
            return sharedPreferences.getString(Constant.USERNAME, null)!!
        }

        fun getDefaultWebviewFontSize(context: Context?): Int {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context!!)
            return prefs.getInt(KEY_FONT_SIZE, 100)
        }

        fun saveDefaultWebviewFontSize(context: Context?, percent: Int) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context!!)
            val editor = prefs.edit()
            editor.putInt(KEY_FONT_SIZE, percent)
            editor.apply()
        }

        //        TODO: LIVE STREAM
        val KEY_VIDEO_QUALITY = "KEY_VIDEO_QUALITY"
        val KEY_AUDIO_QUALITY = "KEY_AUDIO_QUALITY"
        val KEY_LANDSCAPE = "KEY_LANDSCAPE"
        val KEY_NOISE_SUPPRESSOR = "KEY_NOISE_SUPPRESSOR"
        val KEY_ECHO_CANCELER = "KEY_ECHO_CANCELER"
        val KEY_STEREO = "KEY_STEREO"
        val KEY_AUDIO_MUTE = "KEY_AUDIO_MUTE"
        val KEY_ANTI_ALIASING = "KEY_ANTI_ALIASING"

        fun getVideoQuality(context: Context): String {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            return prefs.getString(KEY_VIDEO_QUALITY, Constant.VIDEO_QUALITY_DEFAULT)!!
        }

        fun setVideoQuality(context: Context, quality: String) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            val editor = prefs.edit()
            editor.putString(KEY_VIDEO_QUALITY, quality)
            editor.apply()
        }

        fun getStringValue(context: Context, key: String): String {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            return prefs.getString(key, "")!!
        }

        fun setStringValue(context: Context, key: String, value: String) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            val editor = prefs.edit()
            editor.putString(key, value)
            editor.apply()
        }

        fun getBooleanValue(context: Context, key: String): Boolean {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            val defaultValue =
                !(key.equals(KEY_LANDSCAPE) || key.equals(KEY_AUDIO_MUTE) || key.equals(
                    KEY_ANTI_ALIASING
                ))
            return prefs.getBoolean(key, defaultValue)
        }

        fun setBooleanValue(context: Context, key: String, value: Boolean) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            val editor = prefs.edit()
            editor.putBoolean(key, value)
            editor.apply()
        }

        fun getAudioQuality(context: Context): String {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            return prefs.getString(KEY_VIDEO_QUALITY, Constant.AUDIO_QUALITY_DEFAULT)!!
        }

        fun setAudioQuality(context: Context, quality: String) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            val editor = prefs.edit()
            editor.putString(KEY_AUDIO_QUALITY, quality)
            editor.apply()
        }
    }

}