package com.go.videoeditor.utils

import android.os.Handler
import android.os.Looper
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.listener.PositionListener
import com.google.android.exoplayer2.Player

class ProgressTracker : Runnable {
    lateinit var player: Player
    var positionListener: PositionListener? = null
    lateinit var handler: Handler
    var timeDelay: Long = 1000L

    constructor(player: Player, timeDelay: Long, positionListener: PositionListener?) {
        this.player = player
        this.positionListener = positionListener
        this.timeDelay = timeDelay
        handler = Handler(Looper.getMainLooper())
    }

    override fun run() {
        val position = player.currentPosition
        Loggers.e("onPlaybackStateChanged_A", "ProgressTracker run: position = $position")
        positionListener?.progress(position)
        handler.postDelayed(this, timeDelay)
    }

    fun start() {
        handler.post(this)
//        handler.postDelayed(this, timeDelay)
    }

    fun cancel() {
        handler.removeCallbacks(this)
    }

}