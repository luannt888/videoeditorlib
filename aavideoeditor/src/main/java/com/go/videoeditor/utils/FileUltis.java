package com.go.videoeditor.utils;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.StatFs;
import android.provider.MediaStore;
import android.util.Log;

import androidx.core.content.FileProvider;

import com.go.videoeditor.R;
import com.go.videoeditor.app.Loggers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


public class FileUltis {

    private static final String HISTORY_PROJECT_THUMB_NAME = "VideoEditorHistoryProjectThumb";
    private static final String FOLDER_HISTORY_PROJECT = "VideoEditorHistoryProject";
    public static final String OLD_PROJECT_CONTENT_NAME = "VIDEOEDITOR_JSONCONTENT_FILENAME";
    public static final String PROJECT_FOLDER = "VIDEOEDITOR";
    public static final String DOWNLOAD_FOLDER = "Downloads";
    public static final String THUMBNAIL_FOLDER = "Thumbnail";
    public static final String EDITED_FOLDER = "Edited";
    public static final String RECORD_VIDEO_FOLDER = "RecorderVideo";
    public static final String RECORD_AUDIO_FOLDER = "RecorderAudio";
    public static final String FILE_AUTHORITY = "com.go.videoeditor.provider";

    //create File
    public static File createRandomnameTempImageFile(Context context) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File imageFile = File.createTempFile(imageFileName, ".jpg", storageDir);
        return imageFile;
    }

    //create File
    public static File createFile(Context context, String name) throws IOException {
        File file = new File(getEditedFolderPath(context), name);
        return file;
    }

    public static File createFilePath(Context context) throws IOException {
        String appName = context.getString(R.string.app_name).trim().replace(" ", "-");
        File root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        File projectFile = new File(root, appName);
        projectFile.mkdirs();
        return projectFile;
    }

    public static File getDownloadFolderPath(Context context) throws IOException {
        File file = new File(createFilePath(context), DOWNLOAD_FOLDER);
        file.mkdirs();
        return file;
    }

    public static File getRecordVideoFolderPath(Context context) throws IOException {
        File file = new File(createFilePath(context), RECORD_VIDEO_FOLDER);
        file.mkdirs();
        return file;
    }

    public static File getRecordAudioFolderPath(Context context) throws IOException {
        File file = new File(createFilePath(context), RECORD_AUDIO_FOLDER);
        file.mkdirs();
        return file;
    }

    public static File getEditedFolderPath(Context context) throws IOException {
        File file = new File(createFilePath(context), EDITED_FOLDER);
        file.mkdirs();
        return file;
    }

    public static void removeTempDownloadFolderPath(Context context) throws IOException {
        File file = getDownloadFolderPath(context);
        if (file.exists() && file.isDirectory()) {
            File[] list = file.listFiles();
            if (list != null) {
                for (File item : list) {
                    if (item.getName().endsWith(".temp")) {
                        item.delete();
                    }
                }
            }
        }
    }

    public static void removeFilePath(String filePath) throws IOException {
        File file = new File(filePath);
        if (file.exists()) {
            file.delete();
        }
    }

    public static File getThumbnailFolderPath(Context context) throws IOException {
        File file = new File(createFilePath(context), THUMBNAIL_FOLDER);
        file.mkdirs();
        return file;
    }

    public static File getVideoDownloadFile(Context context, String name) throws IOException {
        File file = new File(getDownloadFolderPath(context), name);
        return file;
    }

    public static boolean checkVideoDownloadFileExist(Context context, String name) throws IOException {
        File file = new File(getDownloadFolderPath(context), name);
        return file.exists();
    }

    public static boolean checkVideoDownloadFileUrlExist(Context context, String urlVideo) throws IOException {
        return checkVideoDownloadFileExist(context, getVideoNameFileUrlOrPath(urlVideo));
    }

    public static File getThumbnailVideoFile(Context context, String name) throws IOException {
        File file = new File(getThumbnailFolderPath(context), name);
        return file;
    }

    public static boolean checkThumbnailVideoFileExist(Context context, String name) throws IOException {
        File file = new File(getThumbnailFolderPath(context), name);
        return file.exists();
    }

    public static File createVideoFile(Context context) throws IOException {
        String nameFile = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date()) + ".mp4";
        nameFile = "VDE_" + nameFile;
        return createFile(context, nameFile);
    }

    public static File createVideoRecordFile(Context context) throws IOException {
        String nameFile = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date()) + ".mp4";
        nameFile = "VDE_" + nameFile;
        return new File(getRecordVideoFolderPath(context), nameFile);
    }

    public static File createAudioRecordFile(Context context) throws IOException {
        String nameFile = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date()) + ".mp3";
        nameFile = "VDE_" + nameFile;
        return new File(getRecordAudioFolderPath(context), nameFile);
    }

    public static Bitmap getBitmapDecodeFileDescriptor(Uri uri, Context ct) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = ct.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = null;
        if (parcelFileDescriptor != null) {
            fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        }
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        if (parcelFileDescriptor != null) {
            parcelFileDescriptor.close();
        }
        return image;
    }

    //getBitmap(c2)
    public static Bitmap getBitmapDecodeStream(Context ct, Uri uri, BitmapFactory.Options o) {
        Bitmap resultBm = null;
        try {
            InputStream inputStream = ct.getApplicationContext().getContentResolver().openInputStream(uri);
            resultBm = BitmapFactory.decodeStream(inputStream, new Rect(), o);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return resultBm;
    }

    //getBitmap(c3)
    public static Bitmap getBitmapDecodeFile(Uri uri, int thumnailSize) {
        File image = new File(uri.getPath());
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(image.getPath(), bounds);
        if ((bounds.outWidth == -1) || (bounds.outHeight == -1))
            return null;

        int originalSize = (bounds.outHeight > bounds.outWidth) ? bounds.outHeight : bounds.outWidth;

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = originalSize / thumnailSize;
        return BitmapFactory.decodeFile(image.getPath(), opts);
    }
    //</editor-fold>


    //Bitmap ->Uri(c3)
    public static File saveThumbnail(Bitmap bm, String projectName, Context context) throws IOException {
        File root = new File(context.getFilesDir(), FOLDER_HISTORY_PROJECT);
        if (!root.exists()) {
            root.mkdir();
        }
        File project = new File(root, projectName);
        // Make sure the path directory exists.
        if (!project.exists()) {
            project.mkdirs();
        }


        File thumbailFile = new File(project, HISTORY_PROJECT_THUMB_NAME);
//        if(thumbailFile.exists()) thumbailFile.delete();
        FileOutputStream fOut = new FileOutputStream(thumbailFile);
        bm.compress(Bitmap.CompressFormat.PNG, 90, fOut);
        fOut.flush();
        fOut.close();
        return thumbailFile;
    }

    public static void refreshGalery(Activity activity, File f) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        activity.sendBroadcast(mediaScanIntent);
    }

    public static void refreshGalery(Activity activity, String filePath) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ContentValues values = new ContentValues(2);
            values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
            values.put(MediaStore.Video.Media.DATA, filePath);
            activity.getContentResolver().insert(
                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                    values
            );
        } else {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(new File(filePath));
            mediaScanIntent.setData(contentUri);
            activity.sendBroadcast(mediaScanIntent);
        }
    }

    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }

    public static List<File> getListFileOldProject(File rootFile) {

        File parentFile = new File(rootFile, FOLDER_HISTORY_PROJECT);
        List<File> pathStringList = new ArrayList<>();
        File[] listFile;
        if (parentFile.isDirectory()) {
            //1. lay danh sach
            listFile = parentFile.listFiles();

            //2 xep theo thu tu thoi gian
            if (listFile != null && listFile.length > 1) {
                Arrays.sort(listFile, new Comparator<File>() {
                    @Override
                    public int compare(File object1, File object2) {
                        return (int) ((object1.lastModified() <= object2.lastModified()) ? object1.lastModified() : object2.lastModified());
                    }
                });
            }

            if (listFile != null) {
                List<File> temp = Arrays.asList(listFile);
                pathStringList.clear();
                for (int i = temp.size() - 1; i >= 0; i--) {
                    pathStringList.add(temp.get(i));
                }
            }


        }
        return pathStringList;
    }

    public static File writeToFile(String projecJSOnString, String projectname, Context context) {

        File root = new File(context.getFilesDir(), FOLDER_HISTORY_PROJECT);
        if (!root.exists()) {
            root.mkdir();
        }
        File project = new File(root, projectname);
        // Make sure the path directory exists.
        if (!project.exists()) {
            // Make it, if it doesn't exit
            project.mkdirs();
        }
        File data = new File(project, OLD_PROJECT_CONTENT_NAME);

        // Save your stream, don't forget to flush() it before closing it.

        try {
            data.createNewFile();
            FileOutputStream fOut = new FileOutputStream(data);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(projecJSOnString);
            myOutWriter.close();

            fOut.flush();
            fOut.close();
            return data;
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
            return null;
        }
    }

    public static String readFromFile(File proJectFile) {
        //Read text from file
        if (proJectFile != null) {
            StringBuilder JSONProJectStr = new StringBuilder();
            try {
                BufferedReader br = new BufferedReader(new FileReader(proJectFile));
                String line;

                while ((line = br.readLine()) != null) {
                    JSONProJectStr.append(line);
                    JSONProJectStr.append('\n');
                }
                br.close();

                return JSONProJectStr.toString();
            } catch (IOException e) {
                //
            }

        }
        return null;
    }

    public static void saveBitmapToFile(Bitmap pageBm, File pageFile) {
        try {
            OutputStream os = new FileOutputStream(pageFile);
            pageBm.compress(Bitmap.CompressFormat.JPEG, 70, os);
            os.close();
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static File reSaveBitmap(final Context context, File file, Bitmap bitmap) {
        OutputStream out = null;
        try {
            out = new FileOutputStream(file);
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        }
        OutputStream op = out;
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, op);
        new Thread(new Runnable() {
            @Override
            public void run() {
                context.sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.fromFile(file)));
            }
        }).start();
        try {
            op.close();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        bitmap.recycle();
        return file;
    }

    public static File getThumbnailFile(Context context, String videoPath) throws IOException {
        String name = getImageNameFileUrl(videoPath);
        File imageFile = getThumbnailVideoFile(context, name);
        if (!checkThumbnailVideoFileExist(context, name)) {
            reSaveBitmap(context, imageFile, VideoEditUtils.getBitmapThumbnail(context, videoPath));
        }
        return imageFile;
    }

    public static String getNameFileUrl(String url) {
        String name;
        if (url.contains("/")) {
            name = url.substring(url.lastIndexOf("/") + 1);
        } else {
            name = url;
        }
        return name;
    }

    public static String getVideoNameFileUrlOrPath(String url) {
        String name = getNameFileUrl(url);
        if (!name.endsWith(".mp4")) {
            name = name + ".mp4";
        }
        return name;
    }
    public static String getVideoNameFileFromTitle(String title) {
        String s = title.replace(":", "_");
        String name = getVideoNameFileUrlOrPath(s);
        return name;
    }
    public static String getImageNameFileUrl(String url) {
        String name = getNameFileUrl(url);
        if (!name.endsWith(".jpg")) {
            if (name.lastIndexOf(".") == -1) {
                name = name + ".jpg";
            } else {
                name = name.replace(name.substring(name.lastIndexOf(".") - 1), ".jpg");
            }
        }
        return name;
    }

    public static float convertByteToMegaByte(long byteLong, int count0) {
        int count10 = 1;
        for (int i = 0; i < count0; i++) {
            count10 = count10 * 10;
        }
        float megaByte = (float) byteLong / (1024 * 1024);
        if (count0 == 0) {
            int a = (int) megaByte;
            String s = String.valueOf(a);

            float c = Float.parseFloat(s);
            Loggers.e("MB", a + "/" + c);
            return a;
        }
        megaByte = (float) ((int) (megaByte * count10)) / count10;
        return megaByte;
    }

    public static long getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize, availableBlocks;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            blockSize = stat.getBlockSizeLong();
            availableBlocks = stat.getAvailableBlocksLong();
        } else {
            blockSize = stat.getBlockSize();
            availableBlocks = stat.getAvailableBlocks();
        }
        return availableBlocks * blockSize;
    }

    public static void showFileManagerBrowseFileDownload(Activity activity, String url) throws IOException {
//        File file = getVideoDownloadFile(getVideoNameFileUrl(url));
        File file = getDownloadFolderPath(activity);
        if (file.exists()) {
//            Uri selectedUri = Uri.parse(file.getAbsolutePath());
            Uri selectedUri = FileProvider.getUriForFile(
                    activity, activity.getApplicationContext().getPackageName().toString() + ".provider",
                    file
            );
            Intent intent = new Intent(Intent.ACTION_VIEW);
//            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(selectedUri, "*/*");
//            intent.setData(selectedUri);
//
//            if (intent.resolveActivityInfo(activity.getPackageManager(), 0) != null) {
////                activity.startActivity(Intent.createChooser(intent, "Open folder"));
//                activity.startActivity(intent);
//            } else {
//                // if you reach this place, it means there is no any file
//                // explorer app installed on your device
//            }
//            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//            intent.setData(selectedUri, "text/csv");
            activity.startActivity(intent);
        }
    }

    public static void refreshGalery(Context context, String filePath) {
        MediaScannerConnection.scanFile(context.getApplicationContext(), new String[]{filePath}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    @Override
                    public void onScanCompleted(String path, Uri uri) {
                    }
                });
    }

}
