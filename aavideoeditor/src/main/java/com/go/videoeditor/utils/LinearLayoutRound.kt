package com.go.videoeditor.utils

import android.content.Context
import android.graphics.Outline
import android.util.AttributeSet
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.LinearLayout
import com.go.videoeditor.R

class LinearLayoutRound : LinearLayout {
    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context, attrs)
    }

    fun init(
        context: Context?,
        attrs: AttributeSet?
    ) {
        if (context == null) {
            return
        }
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.LinearLayoutRoundView)
        val isRound = typedArray.getBoolean(R.styleable.LinearLayoutRoundView_isRound, true)
        val cornerRadius =
            typedArray.getDimensionPixelSize(R.styleable.LinearLayoutRoundView_radius, 0)
        if (!isRound || cornerRadius <= 0) {
            return
        }
        /*val topLeftRadius =  typedArray.getDimensionPixelSize(R.styleable.LinearLayoutRoundView_topLeftRadius, 0)
        val topRightRadius =
            typedArray.getDimensionPixelSize(R.styleable.LinearLayoutRoundView_topRightRadius, 0)
        val bottomLeftRadius =
            typedArray.getDimensionPixelSize(R.styleable.LinearLayoutRoundView_bottomLeftRadius, 0)
        val bottomRightRadius =
            typedArray.getDimensionPixelSize(R.styleable.LinearLayoutRoundView_bottomRightRadius, 0)*/

        setOutlineProvider(object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                /*val cornerRadiusDP = 7f
                val cornerRadius = TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    cornerRadiusDP, context.resources.getDisplayMetrics()
                )*/
                val left = 0
                val top = 0;
                val right = view.width
                val bottom = view.height
//                val cornerRadiusDP = 7f
                /*val cornerRadius = TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    radius.toFloat(),
                    resources.displayMetrics
                ).toInt()*/

                // all corners
                outline.setRoundRect(left, top, right, bottom, cornerRadius.toFloat())

                /* top corners
                outline.setRoundRect(left, top, right, bottom+cornerRadius, cornerRadius.toFloat())*/

                /* bottom corners
                outline.setRoundRect(left, top - cornerRadius, right, bottom, cornerRadius.toFloat())*/

                /* left corners
                outline.setRoundRect(left, top, right + cornerRadius, bottom, cornerRadius.toFloat())*/

                /* right corners
                outline.setRoundRect(left - cornerRadius, top, right, bottom, cornerRadius.toFloat())*/

                /* top left corner
                outline.setRoundRect(left , top, right+ cornerRadius, bottom + cornerRadius, cornerRadius.toFloat())*/

                /* top right corner
                outline.setRoundRect(left - cornerRadius , top, right, bottom + cornerRadius, cornerRadius.toFloat())*/

                /* bottom left corner
                outline.setRoundRect(left, top - cornerRadius, right + cornerRadius, bottom, cornerRadius.toFloat())*/

                /* bottom right corner
                outline.setRoundRect(left - cornerRadius, top - cornerRadius, right, bottom, cornerRadius.toFloat())*/

//                if (isRoundBottomStyleListBox) {
//                    outline.setRoundRect(0, 0, view.width, (view.height + cornerRadius).toInt()
//                    , cornerRadius)
//                } else {
//                outline.setRoundRect(0, 0, view.width, view.height, cornerRadius)
//                }
            }
        })
        setClipToOutline(true)
    }
}