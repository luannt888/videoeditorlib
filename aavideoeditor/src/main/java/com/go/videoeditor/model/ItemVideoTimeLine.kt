package com.go.videoeditor.model

import com.go.videoeditor.app.Constant
import com.google.android.exoplayer2.ExoPlayer

data class ItemVideoTimeLine(
    var key: String,
    var filePath: String,
    var startPosition: Long,
    var endPosition: Long
) {
    var player: ExoPlayer? = null
    fun isPlayAtTime(timeMs: Long): Boolean {
        return (timeMs in startPosition..endPosition)
    }

    var originStartPosition: Long = 0;
    var originEndPosition: Long = Long.MAX_VALUE;
    var type: Int = Constant.TYPE_VIDEO
    var duration:Long = Long.MAX_VALUE
    var maxRange:Long = 0

}