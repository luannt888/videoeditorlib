package com.go.videoeditor.model

data class ItemSimpleParam(val key: String, var value: String)