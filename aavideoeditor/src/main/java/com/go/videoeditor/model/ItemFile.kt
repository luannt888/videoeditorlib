package com.go.videoeditor.model

import android.graphics.Bitmap
import android.net.Uri
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemFile(
    var fileId: String?,
    var type: Int,
    var title: String,
    var fileUri: Uri?,
    var filePath: String,
    var dateLong: Long,
    var date: String,
    var bitmap: Bitmap?,
    var duration: Long,
    var size: Long
) : Parcelable