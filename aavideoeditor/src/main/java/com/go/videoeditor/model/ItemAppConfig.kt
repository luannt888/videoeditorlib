package com.go.videoeditor.model

data class ItemAppConfig(val logo: String?, val bgLaunch: String?, val linkShareApp: String?, val maxStreamQuality: String?)