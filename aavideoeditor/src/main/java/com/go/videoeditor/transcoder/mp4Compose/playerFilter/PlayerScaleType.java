package com.go.videoeditor.transcoder.mp4Compose.playerFilter;

/**
 * Created by sudamasayuki on 2017/05/16.
 */

public enum PlayerScaleType {
    RESIZE_FIT_WIDTH,  //
    RESIZE_FIT_HEIGHT, //
    RESIZE_NONE,   // The specified aspect ratio is ignored.
    RESIZE_FIT_AUTO,
    ;

}
