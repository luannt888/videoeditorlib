package com.go.videoeditor.transcoder.mp4Compose.playerFilter;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Size;

import com.go.videoeditor.transcoder.mp4Compose.filter.GlFilter;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.video.VideoSize;

/**
 *
 */
public class EPlayerView extends GLSurfaceView implements Player.Listener {

    private final static String TAG = EPlayerView.class.getSimpleName();
    private PlayerView playerView;
    private final EPlayerRenderer renderer;
    private ExoPlayer player;
    private float videoAspect = 1f;
    private PlayerScaleType playerScaleType = PlayerScaleType.RESIZE_FIT_AUTO;

    public EPlayerView(Context context) {
        this(context, null);
    }

    public EPlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setEGLContextFactory(new EContextFactory());
        setEGLConfigChooser(new EConfigChooser());

        renderer = new EPlayerRenderer(this);
        setRenderer(renderer);

    }

    public EPlayerRenderer getRenderer() {
        return renderer;
    }

    public EPlayerView setExoPlayer(ExoPlayer player) {
        if (this.player != null) {
            this.player.release();
            this.player = null;
        }
        if (player != null) {
            this.player = player;
            this.player.addListener(this);
            this.renderer.setExoPlayer(player);
        }
        return this;
    }

    public void setGlFilter(GlFilter glFilter) {
        renderer.setGlFilter(glFilter);
    }

    public void setPlayerScaleType(PlayerScaleType playerScaleType) {
        this.playerScaleType = playerScaleType;
        requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        Log.d("onMeasure", "widthMeasureSpec = " + widthMeasureSpec + " heightMeasureSpec = " + heightMeasureSpec + "measuredWidth =" + measuredWidth + " measuredHeight= " + measuredHeight);

        int viewWidth = measuredWidth;
        int viewHeight = measuredHeight;
        float frameAspect = (float) measuredWidth / (float) measuredHeight;
        switch (playerScaleType) {
            case RESIZE_FIT_WIDTH:
                viewHeight = (int) (measuredWidth / videoAspect);
                break;
            case RESIZE_FIT_HEIGHT:
                viewWidth = (int) (measuredHeight * videoAspect);
                break;
            case RESIZE_FIT_AUTO:
                if (frameAspect > videoAspect) {
                    viewWidth = (int) (measuredHeight * videoAspect);
                } else {
                    viewHeight = (int) (measuredWidth / videoAspect);
                }
                break;
        }

        // Log.d(TAG, "onMeasure viewWidth = " + viewWidth + " viewHeight = " + viewHeight);
        setMeasuredDimension(viewWidth, viewHeight);

    }

    @Override
    public void onPause() {
        super.onPause();
        renderer.release();
    }

    //////////////////////////////////////////////////////////////////////////
    // ExoPlayer.VideoListener


    @Override
    public void onVideoSizeChanged(VideoSize videoSize) {
        int width = videoSize.width;
        int height = videoSize.height;
        Log.d(TAG, "width = " + width + " height = " + height + " unappliedRotationDegrees = " + videoSize.unappliedRotationDegrees + " pixelWidthHeightRatio = " + videoSize.pixelWidthHeightRatio);
        if (size == null) {
            size = new Size(width, height);
        }
        videoAspect = ((float) width / height) * videoSize.pixelWidthHeightRatio;
        // Log.d(TAG, "videoAspect = " + videoAspect);
        requestLayout();
    }

    private Size size;

    public Size getActureSize() {
        return size;
    }
}
