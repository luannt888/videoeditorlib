package com.go.videoeditor.transcoder.mp4Compose.filter;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by sudamasayuki on 2018/01/07.
 */
public class GlBitmapOverlaySampleFilter extends GlOverlayFilter {
    private Bitmap bitmap;

    public GlBitmapOverlaySampleFilter(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    @Override
    protected void drawCanvas(Canvas canvas) {

        canvas.drawBitmap(bitmap, 0, 0, null);

    }
}