package com.go.videoeditor.transcoder;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.go.videoeditor.transcoder.engine.TrackType;
import com.go.videoeditor.transcoder.mp4Compose.filter.GlFilter;
import com.go.videoeditor.transcoder.mp4Compose.tranform.FillMode;
import com.go.videoeditor.transcoder.mp4Compose.tranform.FillModeCustomItem;
import com.go.videoeditor.transcoder.resample.AudioResampler;
import com.go.videoeditor.transcoder.resample.DefaultAudioResampler;
import com.go.videoeditor.transcoder.sink.DataSink;
import com.go.videoeditor.transcoder.sink.DefaultDataSink;
import com.go.videoeditor.transcoder.source.BlankAudioDataSource;
import com.go.videoeditor.transcoder.source.DataSource;
import com.go.videoeditor.transcoder.source.FileDescriptorDataSource;
import com.go.videoeditor.transcoder.source.FilePathDataSource;
import com.go.videoeditor.transcoder.source.UriDataSource;
import com.go.videoeditor.transcoder.strategy.DefaultAudioStrategy;
import com.go.videoeditor.transcoder.strategy.DefaultVideoStrategies;
import com.go.videoeditor.transcoder.strategy.DefaultVideoStrategy;
import com.go.videoeditor.transcoder.strategy.TrackStrategy;
import com.go.videoeditor.transcoder.strategy.size.PassThroughResizer;
import com.go.videoeditor.transcoder.stretch.AudioStretcher;
import com.go.videoeditor.transcoder.stretch.DefaultAudioStretcher;
import com.go.videoeditor.transcoder.time.DefaultTimeInterpolator;
import com.go.videoeditor.transcoder.time.SpeedTimeInterpolator;
import com.go.videoeditor.transcoder.time.TimeInterpolator;
import com.go.videoeditor.transcoder.validator.DefaultValidator;
import com.go.videoeditor.transcoder.validator.Validator;

import java.io.FileDescriptor;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

/**
 * Collects transcoding options consumed by {@link Transcoder}.
 */
public class TranscoderOptions {

    private TranscoderOptions() {
    }

    private DataSink dataSink;
    private List<DataSource> videoDataSources;
    private List<DataSource> audioDataSources;
    private TrackStrategy audioTrackStrategy;
    private TrackStrategy videoTrackStrategy;
    private Validator validator;
    private int rotation;
    private TimeInterpolator timeInterpolator;
    private AudioStretcher audioStretcher;
    private AudioResampler audioResampler;

    private ArrayList<GlFilter> listFilter;
    private FillMode fillMode;
    private FillModeCustomItem fillModeCustomItem;
    TranscoderListener listener;
    Handler listenerHandler;

    @NonNull
    public DataSink getDataSink() {
        return dataSink;
    }

    @NonNull
    public List<DataSource> getAudioDataSources() {
        return audioDataSources;
    }

    @NonNull
    public List<DataSource> getVideoDataSources() {
        return videoDataSources;
    }

    @NonNull
    public TrackStrategy getAudioTrackStrategy() {
        return audioTrackStrategy;
    }

    @NonNull
    public TrackStrategy getVideoTrackStrategy() {
        return videoTrackStrategy;
    }

    @NonNull
    public Validator getValidator() {
        return validator;
    }

    public int getVideoRotation() {
        return rotation;
    }

    @NonNull
    public TimeInterpolator getTimeInterpolator() {
        return timeInterpolator;
    }

    @NonNull
    public AudioStretcher getAudioStretcher() {
        return audioStretcher;
    }

    @NonNull
    public AudioResampler getAudioResampler() {
        return audioResampler;
    }

    public ArrayList<GlFilter> getListFilter() {
        return listFilter;
    }

    public FillMode getFillMode() {
        return fillMode;
    }

    public FillModeCustomItem getFillModeCustomItem() {
        return fillModeCustomItem;
    }

    public static class Builder {
        private DataSink dataSink;
        private final List<DataSource> audioDataSources = new ArrayList<>();
        private final List<DataSource> videoDataSources = new ArrayList<>();
        private TranscoderListener listener;
        private Handler listenerHandler;
        private TrackStrategy audioTrackStrategy;
        private TrackStrategy videoTrackStrategy;
        private Validator validator;
        private int rotation;
        private TimeInterpolator timeInterpolator;
        private AudioStretcher audioStretcher;
        private AudioResampler audioResampler;
        private ArrayList<GlFilter> listFilter;
        private FillMode fillMode;
        private FillModeCustomItem fillModeCustomItem;


        Builder(@NonNull String outPath) {
            this.dataSink = new DefaultDataSink(outPath);
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        Builder(@NonNull FileDescriptor fileDescriptor) {
            this.dataSink = new DefaultDataSink(fileDescriptor);
        }

        Builder(@NonNull DataSink dataSink) {
            this.dataSink = dataSink;
        }

        @NonNull
        @SuppressWarnings("WeakerAccess")
        public Builder addDataSource(@NonNull DataSource dataSource) {
            audioDataSources.add(dataSource);
            videoDataSources.add(dataSource);
            return this;
        }

        @NonNull
        @SuppressWarnings("WeakerAccess")
        public Builder addDataSource(@NonNull TrackType type, @NonNull DataSource dataSource) {
            if (type == TrackType.AUDIO) {
                audioDataSources.add(dataSource);
            } else if (type == TrackType.VIDEO) {
                videoDataSources.add(dataSource);
            }
            return this;
        }

        @NonNull
        @SuppressWarnings("unused")
        public Builder addDataSource(@NonNull FileDescriptor fileDescriptor) {
            return addDataSource(new FileDescriptorDataSource(fileDescriptor));
        }

        @NonNull
        @SuppressWarnings("unused")
        public Builder addDataSource(@NonNull TrackType type, @NonNull FileDescriptor fileDescriptor) {
            return addDataSource(type, new FileDescriptorDataSource(fileDescriptor));
        }

        @NonNull
        @SuppressWarnings("unused")
        public Builder addDataSource(@NonNull String inPath) {
            return addDataSource(new FilePathDataSource(inPath));
        }

        @NonNull
        @SuppressWarnings("unused")
        public Builder addDataSource(@NonNull TrackType type, @NonNull String inPath) {
            return addDataSource(type, new FilePathDataSource(inPath));
        }

        @NonNull
        @SuppressWarnings({"unused", "UnusedReturnValue"})
        public Builder addDataSource(@NonNull Context context, @NonNull Uri uri) {
            return addDataSource(new UriDataSource(context, uri));
        }

        @NonNull
        @SuppressWarnings({"unused", "UnusedReturnValue"})
        public Builder addDataSource(@NonNull TrackType type, @NonNull Context context, @NonNull Uri uri) {
            return addDataSource(type, new UriDataSource(context, uri));
        }

        /**
         * Sets the audio output strategy. If absent, this defaults to
         * {@link DefaultAudioStrategy}.
         *
         * @param trackStrategy the desired strategy
         * @return this for chaining
         */
        @NonNull
        @SuppressWarnings("unused")
        public Builder setAudioTrackStrategy(@Nullable TrackStrategy trackStrategy) {
            this.audioTrackStrategy = trackStrategy;
            return this;
        }

        /**
         * Sets the video output strategy. If absent, this defaults to the 16:9
         * strategy returned by {@link DefaultVideoStrategies#for720x1280()}.
         *
         * @param trackStrategy the desired strategy
         * @return this for chaining
         */
        @NonNull
        @SuppressWarnings("unused")
        public Builder setVideoTrackStrategy(@Nullable TrackStrategy trackStrategy) {
            this.videoTrackStrategy = trackStrategy;
            return this;
        }

        @NonNull
        public Builder setListener(@NonNull TranscoderListener listener) {
            this.listener = listener;
            return this;
        }

        /**
         * Sets an handler for {@link TranscoderListener} callbacks.
         * If null, this will default to the thread that starts the transcoding, if it
         * has a looper, or the UI thread otherwise.
         *
         * @param listenerHandler the thread to receive callbacks
         * @return this for chaining
         */
        @NonNull
        @SuppressWarnings("WeakerAccess")
        public Builder setListenerHandler(@Nullable Handler listenerHandler) {
            this.listenerHandler = listenerHandler;
            return this;
        }

        /**
         * Sets a validator to understand whether the transcoding process should
         * stop before being started, based on the tracks status. Will default to
         * {@link DefaultValidator}.
         *
         * @param validator the validator
         * @return this for chaining
         */
        @NonNull
        @SuppressWarnings("unused")
        public Builder setValidator(@Nullable Validator validator) {
            this.validator = validator;
            return this;
        }

        /**
         * The clockwise rotation to be applied to the input video frames.
         * Defaults to 0, which leaves the input rotation unchanged.
         *
         * @param rotation either 0, 90, 180 or 270
         * @return this for chaining
         */
        @NonNull
        @SuppressWarnings("unused")
        public Builder setVideoRotation(int rotation) {
            this.rotation = rotation;
            return this;
        }

        /**
         * Sets a {@link TimeInterpolator} to change the frames timestamps - either video or
         * audio or both - before they are written into the output file.
         * Defaults to {@link DefaultTimeInterpolator}.
         *
         * @param timeInterpolator a time interpolator
         * @return this for chaining
         */
        @NonNull
        @SuppressWarnings("WeakerAccess")
        public Builder setTimeInterpolator(@NonNull TimeInterpolator timeInterpolator) {
            this.timeInterpolator = timeInterpolator;
            return this;
        }

        /**
         * Shorthand for calling {@link #setTimeInterpolator(TimeInterpolator)}
         * and passing a {@link SpeedTimeInterpolator}.
         * This interpolator can modify the video speed by the given factor.
         *
         * @param speedFactor a factor, greather than 0
         * @return this for chaining
         */
        @NonNull
        @SuppressWarnings("unused")
        public Builder setSpeed(float speedFactor) {
            return setTimeInterpolator(new SpeedTimeInterpolator(speedFactor));
        }

        /**
         * Sets an {@link AudioStretcher} to perform stretching of audio samples
         * as a consequence of speed and time interpolator changes.
         * Defaults to {@link DefaultAudioStretcher}.
         *
         * @param audioStretcher an audio stretcher
         * @return this for chaining
         */
        @NonNull
        @SuppressWarnings("unused")
        public Builder setAudioStretcher(@NonNull AudioStretcher audioStretcher) {
            this.audioStretcher = audioStretcher;
            return this;
        }

        /**
         * Sets an {@link AudioResampler} to change the sample rate of audio
         * frames when sample rate conversion is needed. Upsampling is discouraged.
         * Defaults to {@link DefaultAudioResampler}.
         *
         * @param audioResampler an audio resampler
         * @return this for chaining
         */
        @NonNull
        @SuppressWarnings("unused")
        public Builder setAudioResampler(@NonNull AudioResampler audioResampler) {
            this.audioResampler = audioResampler;
            return this;
        }

        public Builder setListFilter(ArrayList<GlFilter> listFilter) {
            this.listFilter = listFilter;
            return this;
        }

        public Builder setFilter(GlFilter filter) {
            if (filter != null) {
                this.listFilter = new ArrayList<>();
                this.listFilter.add(filter);
            }
            return this;
        }

        public Builder setFillMode(FillMode fillMode) {
            this.fillMode = fillMode;
            return this;
        }

        public Builder setFillModeCustomItem(FillModeCustomItem fillModeCustomItem) {
            this.fillModeCustomItem = fillModeCustomItem;
            this.fillMode = FillMode.CUSTOM;
            return this;
        }

        /**
         * Generates muted audio data sources if needed
         *
         * @return The list of audio data sources including the muted sources
         */
        private List<DataSource> buildAudioDataSources() {
            // Check if we have a mix of empty and non-empty data sources
            // This would cause an error in Engine::computeTrackStatus
            boolean hasMissingAudioDataSources = false;
            boolean hasAudioDataSources = false;
            boolean hasValidAudioDataSources = true;
            for (DataSource dataSource : audioDataSources) {
                if (dataSource.getTrackFormat(TrackType.AUDIO) == null) {
                    hasMissingAudioDataSources = true;
                } else {
                    hasAudioDataSources = true;
                }
                if (hasAudioDataSources && hasMissingAudioDataSources) {
                    hasValidAudioDataSources = false;
                    break;
                }
            }
            if (hasValidAudioDataSources) {
                return audioDataSources;
            }
            // Fix the audioDataSources by replacing the empty data source by muted data source
            List<DataSource> result = new ArrayList<>();
            for (DataSource dataSource : audioDataSources) {
                if (dataSource.getTrackFormat(TrackType.AUDIO) != null) {
                    result.add(dataSource);
                } else {
                    result.add(new BlankAudioDataSource(dataSource.getDurationUs()));
                }
            }
            return result;
        }

        @NonNull
        public TranscoderOptions build() {
            if (listener == null) {
                throw new IllegalStateException("listener can't be null");
            }
            if (audioDataSources.isEmpty() && videoDataSources.isEmpty()) {
                throw new IllegalStateException("we need at least one data source");
            }
            if (rotation != 0 && rotation != 90 && rotation != 180 && rotation != 270) {
                throw new IllegalArgumentException("Accepted values for rotation are 0, 90, 180, 270");
            }
            if (listenerHandler == null) {
                Looper looper = Looper.myLooper();
                if (looper == null) looper = Looper.getMainLooper();
                listenerHandler = new Handler(looper);
            }
            if (audioTrackStrategy == null) {
                audioTrackStrategy = DefaultAudioStrategy.builder().build();
            }
            if (videoTrackStrategy == null) {
//                videoTrackStrategy = DefaultVideoStrategies.for720x1280();
                videoTrackStrategy = new DefaultVideoStrategy.Builder().addResizer(new PassThroughResizer())
                        .build();
            }
            if (validator == null) {
                validator = new DefaultValidator();
            }
            if (timeInterpolator == null) {
                timeInterpolator = new DefaultTimeInterpolator();
            }
            if (audioStretcher == null) {
                audioStretcher = new DefaultAudioStretcher();
            }
            if (audioResampler == null) {
                audioResampler = new DefaultAudioResampler();
            }
            if (fillMode == null) {
                fillMode = FillMode.PRESERVE_ASPECT_FIT;
            }
            if (fillMode == FillMode.CUSTOM && fillModeCustomItem == null) {
                fillMode = FillMode.PRESERVE_ASPECT_FIT;
            }
            if (fillModeCustomItem != null) {
                fillMode = FillMode.CUSTOM;
            }
//            if (listFilter == null) {
//                this.listFilter = new ArrayList<>();
//                this.listFilter.add(new GlFilter());
//            }
            TranscoderOptions options = new TranscoderOptions();
            options.listener = listener;
            options.audioDataSources = buildAudioDataSources();
            options.videoDataSources = videoDataSources;
            options.dataSink = dataSink;
            options.listenerHandler = listenerHandler;
            options.audioTrackStrategy = audioTrackStrategy;
            options.videoTrackStrategy = videoTrackStrategy;
            options.validator = validator;
            options.rotation = rotation;
            options.timeInterpolator = timeInterpolator;
            options.audioStretcher = audioStretcher;
            options.audioResampler = audioResampler;
            options.fillMode = fillMode;
            options.fillModeCustomItem = fillModeCustomItem;
            options.listFilter = listFilter;
            return options;
        }

        @NonNull
        public Future<Void> transcode() {
            return Transcoder.getInstance().transcode(build());
        }
    }
}
