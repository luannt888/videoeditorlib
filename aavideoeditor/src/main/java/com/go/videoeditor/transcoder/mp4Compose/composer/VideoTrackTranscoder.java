/*
 * Copyright (C) 2014 Yuya Tanaka
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.go.videoeditor.transcoder.mp4Compose.composer;

import android.media.MediaCodec;
import android.media.MediaFormat;
import android.opengl.EGL14;
import android.util.Log;
import android.util.Size;

import androidx.annotation.NonNull;

import com.go.videoeditor.transcoder.TranscoderOptions;
import com.go.videoeditor.transcoder.engine.TrackType;
import com.go.videoeditor.transcoder.internal.Logger;
import com.go.videoeditor.transcoder.internal.MediaCodecBuffers;
import com.go.videoeditor.transcoder.internal.MediaFormatConstants;
import com.go.videoeditor.transcoder.mp4Compose.tranform.FillMode;
import com.go.videoeditor.transcoder.mp4Compose.tranform.Rotation;
import com.go.videoeditor.transcoder.sink.DataSink;
import com.go.videoeditor.transcoder.source.DataSource;
import com.go.videoeditor.transcoder.time.TimeInterpolator;
import com.go.videoeditor.transcoder.transcode.BaseTrackTranscoder;
import com.go.videoeditor.transcoder.transcode.internal.VideoDecoderOutput;
import com.go.videoeditor.transcoder.transcode.internal.VideoEncoderInput;
import com.go.videoeditor.transcoder.transcode.internal.VideoFrameDropper;

import java.nio.ByteBuffer;

public class VideoTrackTranscoder extends BaseTrackTranscoder {

    private static final String TAG = VideoTrackTranscoder.class.getSimpleName();
    @SuppressWarnings("unused")
    private static final Logger LOG = new Logger(TAG);

    private VideoDecoderOutput mDecoderOutputSurface;
    private VideoEncoderInput mEncoderInputSurface;
    private MediaCodec mEncoder; // Keep this since we want to signal EOS on it.
    private VideoFrameDropper mFrameDropper;
    private final TimeInterpolator mTimeInterpolator;
    private final int mSourceRotation;
    private final int mExtraRotation;
    private DecoderSurface decoderSurface;
    private EncoderSurface encoderSurface;
    private TranscoderOptions options;
    private boolean isUseMp4ComposerDecodeSurface = true;
    private float inputRatio, outputRatio;

    public void setOptions(TranscoderOptions options) {
        this.options = options;
        isUseMp4ComposerDecodeSurface = options.getListFilter() != null || options.getFillMode() != FillMode.PRESERVE_ASPECT_FIT;
//        isUseMp4ComposerDecodeSurface = true;

    }

    public VideoTrackTranscoder(
            @NonNull DataSource dataSource,
            @NonNull DataSink dataSink,
            @NonNull TimeInterpolator timeInterpolator,
            int rotation) {
        super(dataSource, dataSink, TrackType.VIDEO);
        mTimeInterpolator = timeInterpolator;
        mSourceRotation = dataSource.getOrientation();
        inputRatio = getVideoRatio(dataSource.getTrackFormat(TrackType.VIDEO));
        mExtraRotation = rotation;
    }


    @Override
    protected void onConfigureEncoder(@NonNull MediaFormat format, @NonNull MediaCodec encoder) {
        // Flip the width and height as needed. This means rotating the VideoStrategy rotation
        // by the amount that was set in the TranscoderOptions.
        // It is possible that the format has its own KEY_ROTATION, but we don't care, that will
        // be respected at playback time.
        int width = format.getInteger(MediaFormat.KEY_WIDTH);
        int height = format.getInteger(MediaFormat.KEY_HEIGHT);
        boolean flip = (mExtraRotation % 180) != 0;
        format.setInteger(MediaFormat.KEY_WIDTH, flip ? height : width);
        format.setInteger(MediaFormat.KEY_HEIGHT, flip ? width : height);
        outputRatio = getVideoRatio(format);
        setUseDecodeSurFace(inputRatio, outputRatio);
        Log.e("DecodeSurface", "" + isUseMp4ComposerDecodeSurface);
        super.onConfigureEncoder(format, encoder);
    }

    @Override
    protected void onStartEncoder(@NonNull MediaFormat format, @NonNull MediaCodec encoder) {
        if (!isUseMp4ComposerDecodeSurface) {
            mEncoderInputSurface = new VideoEncoderInput(encoder.createInputSurface());
        } else {
            encoderSurface = new EncoderSurface(encoder.createInputSurface(), EGL14.EGL_NO_CONTEXT);
            encoderSurface.makeCurrent();
        }
        super.onStartEncoder(format, encoder);
    }

    @Override
    protected void onConfigureDecoder(@NonNull MediaFormat format, @NonNull MediaCodec decoder) {
        // Just a sanity check that the rotation coming from DataSource is not different from
        // the one found in the DataSource's MediaFormat for video.
        int sourceRotation = 0;
        if (format.containsKey(MediaFormatConstants.KEY_ROTATION_DEGREES)) {
            sourceRotation = format.getInteger(MediaFormatConstants.KEY_ROTATION_DEGREES);
        }
        if (sourceRotation != mSourceRotation) {
            throw new RuntimeException("Unexpected difference in rotation." +
                    " DataSource:" + mSourceRotation +
                    " MediaFormat:" + sourceRotation);
        }

        // Decoded video is rotated automatically in Android 5.0 lollipop. Turn off here because we don't want to encode rotated one.
        // refer: https://android.googlesource.com/platform/frameworks/av/+blame/lollipop-release/media/libstagefright/Utils.cpp
        format.setInteger(MediaFormatConstants.KEY_ROTATION_DEGREES, 0);

        // The rotation we should apply is the intrinsic source rotation, plus any extra
        // rotation that was set into the TranscoderOptions.
        if (!isUseMp4ComposerDecodeSurface) {
            mDecoderOutputSurface = new VideoDecoderOutput();
            mDecoderOutputSurface.setRotation((mSourceRotation + mExtraRotation) % 360);
            decoder.configure(format, mDecoderOutputSurface.getSurface(), null, 0);
        } else {
            decoderSurface = new DecoderSurface(options.getListFilter(), null);
            decoderSurface.setRotation(Rotation.fromInt((mSourceRotation + mExtraRotation) % 360));
            decoder.configure(format, decoderSurface.getSurface(), null, 0);
        }
    }

    @Override
    protected void onCodecsStarted(@NonNull MediaFormat inputFormat, @NonNull MediaFormat outputFormat, @NonNull MediaCodec decoder, @NonNull MediaCodec encoder) {
        super.onCodecsStarted(inputFormat, outputFormat, decoder, encoder);
        mFrameDropper = VideoFrameDropper.newDropper(
                inputFormat.getInteger(MediaFormat.KEY_FRAME_RATE),
                outputFormat.getInteger(MediaFormat.KEY_FRAME_RATE));
        mEncoder = encoder;

        // Cropping support.
        // Ignoring any outputFormat KEY_ROTATION (which is applied at playback time), the rotation
        // difference between input and output is mSourceRotation + mExtraRotation.

        if (!isUseMp4ComposerDecodeSurface) {
            int rotation = (mSourceRotation + mExtraRotation) % 360;
            boolean flip = (rotation % 180) != 0;
            float inputWidth = inputFormat.getInteger(MediaFormat.KEY_WIDTH);
            float inputHeight = inputFormat.getInteger(MediaFormat.KEY_HEIGHT);
            float inputRatio = inputWidth / inputHeight;
            float outputWidth = flip ? outputFormat.getInteger(MediaFormat.KEY_HEIGHT) : outputFormat.getInteger(MediaFormat.KEY_WIDTH);
            float outputHeight = flip ? outputFormat.getInteger(MediaFormat.KEY_WIDTH) : outputFormat.getInteger(MediaFormat.KEY_HEIGHT);
            float outputRatio = outputWidth / outputHeight;
            float scaleX = 1, scaleY = 1;
            if (inputRatio > outputRatio) { // Input wider. We have a scaleX.
                scaleX = inputRatio / outputRatio;
            } else if (inputRatio < outputRatio) { // Input taller. We have a scaleY.
                scaleY = outputRatio / inputRatio;
            }
            mDecoderOutputSurface.setScale(scaleX, scaleY);

        } else {
            decoderSurface.setOutputResolution(getSizeOfMediaFormat(outputFormat));
            decoderSurface.setInputResolution(getSizeOfMediaFormat(inputFormat));
            decoderSurface.setFillMode(options.getFillMode());
            decoderSurface.setFillModeCustomItem(options.getFillModeCustomItem());
//        decoderSurface.setFlipHorizontal(flipHorizontal);
//        decoderSurface.setFlipVertical(flipVertical);
            decoderSurface.completeParams();
        }
    }

    @Override
    public void release() {
        if (mDecoderOutputSurface != null) {
            mDecoderOutputSurface.release();
            mDecoderOutputSurface = null;
        }
        if (mEncoderInputSurface != null) {
            mEncoderInputSurface.release();
            mEncoderInputSurface = null;
        }
        if (decoderSurface != null) {
            decoderSurface.release();
            decoderSurface = null;
        }
        if (encoderSurface != null) {
            encoderSurface.release();
            encoderSurface = null;
        }
        super.release();
        mEncoder = null;
    }

    @Override
    protected boolean onFeedEncoder(@NonNull MediaCodec encoder, @NonNull MediaCodecBuffers encoderBuffers, long timeoutUs) {
        // We do not feed the encoder, instead we wait for the encoder surface onFrameAvailable callback.
        return false;
    }

    @Override
    protected void onDrainDecoder(@NonNull MediaCodec decoder, int bufferIndex, @NonNull ByteBuffer bufferData, long presentationTimeUs, boolean endOfStream) {
        if (endOfStream) {
            mEncoder.signalEndOfInputStream();
            decoder.releaseOutputBuffer(bufferIndex, false);
        } else {
            long interpolatedTimeUs = mTimeInterpolator.interpolate(TrackType.VIDEO, presentationTimeUs);
            if (mFrameDropper.shouldRenderFrame(interpolatedTimeUs)) {
                decoder.releaseOutputBuffer(bufferIndex, true);
                if (!isUseMp4ComposerDecodeSurface) {
                    mDecoderOutputSurface.drawFrame();
                    mEncoderInputSurface.onFrame(interpolatedTimeUs);
                } else {
                    decoderSurface.drawFrame(interpolatedTimeUs);
                    encoderSurface.setPresentationTime(interpolatedTimeUs * 1000);
                    encoderSurface.swapBuffers();
                }
            } else {
                decoder.releaseOutputBuffer(bufferIndex, false);
            }
        }
    }

    private Size getSizeOfMediaFormat(MediaFormat format) {
        int width = format.getInteger(MediaFormat.KEY_WIDTH);
        int height = format.getInteger(MediaFormat.KEY_HEIGHT);
        return new Size(width, height);
    }

    private float getVideoRatio(MediaFormat format) {
        int width = format.getInteger(MediaFormat.KEY_WIDTH);
        int height = format.getInteger(MediaFormat.KEY_HEIGHT);
        if (height == 0) {
            return 0;
        }
        return (float) width / (float) height;
    }

    private enum TYPE {
        HORIZONTAL, VERTICAL, SQUARE
    }

    private void setUseDecodeSurFace(float inRatio, float outRatio) {
        TYPE inputType, outputType;
        if (inRatio > 1) {
            inputType = TYPE.HORIZONTAL;
        } else if (inRatio == 1) {
            inputType = TYPE.SQUARE;
        } else {
            inputType = TYPE.VERTICAL;
        }

        if (outRatio > 1) {
            outputType = TYPE.HORIZONTAL;
        } else if (outRatio == 1) {
            outputType = TYPE.SQUARE;
        } else {
            outputType = TYPE.VERTICAL;
        }
        if (inputType != outputType) {
            isUseMp4ComposerDecodeSurface = true;
        }
    }
}
