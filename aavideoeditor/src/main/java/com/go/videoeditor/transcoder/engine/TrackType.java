package com.go.videoeditor.transcoder.engine;

/**
 * Represent the types of a track (either video or audio).
 */
public enum TrackType {
    VIDEO, AUDIO
}
