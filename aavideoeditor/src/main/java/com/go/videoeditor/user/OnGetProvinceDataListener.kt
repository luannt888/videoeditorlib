package com.go.videoeditor.user

interface OnGetProvinceDataListener {
    fun onResponse(hasData: Boolean)
}