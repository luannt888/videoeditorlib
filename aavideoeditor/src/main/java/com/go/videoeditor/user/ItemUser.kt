package com.go.videoeditor.user

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemUser(val id: Int? = 0, var email: String? = "", var avatar: String? = "", val phone: String? = "", var fullname: String? = "", var gender: Int? = -1, var address: String? = "", var birthday: String? = "", var socialId: String? = "", var accessToken: String? = "", var accessTokenSocial: String? = "", val affiliateId: String? = "", val isPassword: Boolean? = false, val chip: Int = 0) : Parcelable


/*
@Parcelize
class ItemUser() : Parcelable {
    var id: Int? = 0
    var email: String? = null
    var avatar: String? = null
    var phone: String? = null
    var fullname: String? = null
    var gender: String? = null
    var address: String? = null
    var birthday: String? = null
    var socialId: String? = null
    var accessToken: String? = null
    var accessTokenSocial: String? = null
    var affiliateId: String? = null
    var isPassword: Boolean? = false;


    constructor(id: Int?, email: String?, avatar: String?, phone: String?, fullname: String?, gender: String?, address: String?, birthday: String?, socialId: String?, accessToken: String?, accessTokenSocial: String?, affiliateId: String?, isPassword: Boolean?) : this() {
        this.id = id
        this.email = email
        this.avatar = avatar
        this.phone = phone
        this.fullname = fullname
        this.gender = gender
        this.address = address
        this.birthday = birthday
        this.socialId = socialId
        this.accessToken = accessToken
        this.accessTokenSocial = accessTokenSocial
        this.affiliateId = affiliateId
        this.isPassword = isPassword
    }
}*/
