package com.go.videoeditor.user

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemProvince (val id:Int, val name: String, val appId: String?): Parcelable