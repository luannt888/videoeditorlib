package com.go.videoeditor.user

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemUserReview(val id: Int?, val fullname: String?, val avatar: String?) : Parcelable