package com.go.videoeditor.user

interface OnLoginFormListener {
    fun onLogin(hasLogin: Boolean)
}