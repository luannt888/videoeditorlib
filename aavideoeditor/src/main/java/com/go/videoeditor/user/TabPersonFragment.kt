package com.go.videoeditor.user

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.provider.Settings
import android.text.method.KeyListener
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.activity.SplashActivity
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.listener.OnBaseRequestListener
import com.go.videoeditor.listener.OnCancelListener
import com.go.videoeditor.listener.OnDialogButtonListener
import com.go.videoeditor.listener.OnSimpleListviewListener
import com.go.videoeditor.service.MyHttpRequest
import com.go.videoeditor.service.RequestParams
import com.go.videoeditor.service.ServiceUtil
import com.go.videoeditor.utils.FileCompressor
import com.go.videoeditor.utils.JsonParser
import com.go.videoeditor.utils.ScreenSize
import com.go.videoeditor.utils.SharedPreferencesManager
import com.skydoves.powermenu.*
import kotlinx.android.synthetic.main.fragment_tab_person_ve.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class TabPersonFragment : Fragment() {
    var isTabSelected: Boolean = false
    var isViewCreated: Boolean = false
    var popupMenuChangeAvatar: PowerMenu? = null
    var popupMenuChangeGender: PowerMenu? = null
    var currentKeyListenerFullName: KeyListener? = null
    var currentKeyListenerAddress: KeyListener? = null
    var myHttpRequestUploadImage: MyHttpRequest? = null
    var myHttpRequestUpdateProfile: MyHttpRequest? = null
    var itemUser: ItemUser? = null
    lateinit var fileCompressor: FileCompressor

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tab_person_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isViewCreated = true
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 1000)
            return
        }
        init()
    }

    fun init() {
        if (isTabSelected) {
            return
        }
        isTabSelected = true
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
    }

    fun initData() {
        val pInfo = activity!!.packageManager.getPackageInfo(activity!!.packageName, 0)
        textVersion.text = pInfo.versionName ?: ""

        fileCompressor = FileCompressor(activity!!)
        val lp: RelativeLayout.LayoutParams = buttonBack.layoutParams as RelativeLayout.LayoutParams
        lp.topMargin = (activity as BaseActivity).getStatusBarHeight()
        currentKeyListenerAddress = textPersonFullName.keyListener
        currentKeyListenerFullName = textPersonAddress.keyListener
        textPersonFullName.keyListener = null//disable edittable
        textPersonAddress.keyListener = null//disable edittable
        textPersonGender.isEnabled = false
        textPersonBirthday.isEnabled = false
        textPersonBirthday.text = BIRTH_DAY_FORMAT
        initLogin()
    }

    fun initLogin() {
        if (!isViewCreated) {
            return
        }
        itemUser = MyApplication.getInstance()?.getDataManager()?.itemUser
        textPhone.visibility = View.GONE
        textLogout.visibility = View.GONE
        if (itemUser == null) {
            layoutPersonInfo.visibility = View.GONE
            buttonEdit.visibility = View.GONE
            textLogin.text = getString(R.string.login)
            imageAvatar.setImageResource(R.mipmap.ic_avatar_ve)
        } else {
            textLogin.text = itemUser!!.fullname
            if (!itemUser!!.phone.isNullOrEmpty()) {
                textPhone.visibility = View.VISIBLE
                textPhone.text = itemUser!!.phone
            }
//            layoutPersonInfo.visibility = View.VISIBLE
            textLogout.visibility = View.VISIBLE
//            buttonEdit.visibility = View.VISIBLE
            MyApplication.getInstance()
                ?.loadImageCircle(activity, imageAvatar, itemUser!!.avatar, R.mipmap.ic_avatar_ve)
            initPersonInfo()
        }
    }

    fun initPersonInfo() {
        textPersonUserName.text = itemUser!!.phone
        textPersonEmail.text = itemUser!!.email
        textPersonFullName.setText(if (itemUser!!.fullname.isNullOrEmpty()) getString(R.string.no_info) else itemUser!!.fullname)
        textPersonAddress.setText(if (itemUser!!.address.isNullOrEmpty()) getString(R.string.no_info) else itemUser!!.address)
        textPersonBirthday.text =
            if (itemUser!!.birthday.isNullOrEmpty()) getString(R.string.no_info) else itemUser!!.birthday
        textPersonChip.text = "${itemUser!!.chip} chip"
        val genderId = itemUser!!.gender
        var gender = getString(R.string.no_info)
        if (genderId != null) {
            when (genderId) {
                1 -> gender = getString(R.string.male)
                2 -> gender = getString(R.string.female)
                3 -> gender = getString(R.string.other)
            }

        }
        textPersonGender.text = gender

        /*var gender = itemUser!!.gender
        when (gender) {
            getString(R.string.male) -> genderId = 1
            getString(R.string.female) -> genderId = 2
            getString(R.string.other) -> genderId = 3
        }*/
    }

    fun initControl() {
        /*buttonBack.setOnClickListener {
            (activity as? MainActivity)?.removePersonFragment()
        }*/
        buttonShare.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                (activity as? BaseActivity)?.shareLinkApp()
            }, Constant.DELAY_HANDLE)
        }
        textLogin.setOnClickListener {
            if (itemUser != null) {
                return@setOnClickListener
            }
//            (activity as BaseActivity?)?.gotoActivityForResult(UserLoginActivity::class.java, Constant.CODE_LOGIN)
            (activity as? BaseActivity)?.gotoActivity(UserLoginActivity::class.java)
        }
        textLogout.setOnClickListener {
            (activity as? BaseActivity)?.showDialog(
                true,
                R.string.notification,
                R.string.logout_msg,
                R.string.logout,
                R.string.close,
                object :
                    OnDialogButtonListener {
                    override fun onLeftButtonClick() {
                        itemUser = null
                        MyApplication.getInstance()?.getDataManager()?.itemUser = null
                        SharedPreferencesManager.saveUser(activity!!, null, null)
//                        initLogin()
//                    Handler(Looper.getMainLooper()).postDelayed(Runnable {
//                        FirebaseAuth.getInstance().signOut()
//                        if (AccessToken.getCurrentAccessToken() == null) {
//                            return@Runnable
//                        }
//                        GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE) {
//                            LoginManager.getInstance().logOut()
//                        }.executeAsync()
//                    }, 200)
                        activity!!.finish()
                        (activity as BaseActivity).gotoActivity(SplashActivity::class.java)
                    }

                    override fun onRightButtonClick() {}

                })
        }
//        layoutPersonChangePassword.setOnClickListener {
//            Handler(Looper.getMainLooper()).postDelayed(Runnable {
//                val bundle = Bundle()
//                bundle.putBoolean(Constant.IS_CHANGE_PASSWORD_SCREEN, true)
//                (activity as? BaseActivity)?.gotoActivity(UserEnterNewPasswordActivity::class.java, bundle)
//            }, 200)
//        }
        textPersonBirthday.setOnClickListener {
            (activity as? BaseActivity)?.hideKeyboard(textPersonFullName)
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                showBirthdayPicker()
            }, 200)
        }
        textPersonGender.setOnClickListener {
            (activity as? BaseActivity)?.hideKeyboard(textPersonFullName)
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                showChangeGenderPopup()
            }, 200)
        }
        imageAvatar.setOnClickListener {
            /*if (itemUser == null) {
                return@setOnClickListener
            }
            showChangeAvatarPopup()*/
        }
        buttonCancelEdit.setOnClickListener {
            initPersonInfo()
            initItemInfoEdittable(false, R.color.gray)
            buttonCancelEdit.visibility = View.GONE
            if (itemUser!!.birthday.isNullOrEmpty()) {
                textPersonBirthday.text = BIRTH_DAY_FORMAT
            }
            buttonEdit.text = getString(R.string.edit_profile)
        }
        buttonEdit.setOnClickListener {
            val title = buttonEdit.text.toString()
            val enable = title.equals(getString(R.string.edit_profile))
            val bgEditText =
                if (title.equals(getString(R.string.edit_profile))) R.color.yellow else R.color.gray
            initItemInfoEdittable(enable, bgEditText)
            if (title.equals(getString(R.string.update))) {
                (activity as? BaseActivity)?.hideKeyboard(textPersonFullName)
                updateProfile()
                return@setOnClickListener
            }
            buttonEdit.text = getString(R.string.update)
//            buttonCancelEdit.visibility = View.VISIBLE
        }
    }

    fun initItemInfoEdittable(enable: Boolean, bgEditText: Int) {
        textPersonFullName.setTextColor(ContextCompat.getColor(activity!!, bgEditText))
        textPersonBirthday.setTextColor(ContextCompat.getColor(activity!!, bgEditText))
        textPersonGender.setTextColor(ContextCompat.getColor(activity!!, bgEditText))
        textPersonAddress.setTextColor(ContextCompat.getColor(activity!!, bgEditText))
        textPersonAddress.setHintTextColor(ContextCompat.getColor(activity!!, bgEditText))

        if (enable) {
            textPersonFullName.keyListener = currentKeyListenerFullName
            textPersonAddress.keyListener = currentKeyListenerFullName
        } else {
            textPersonFullName.keyListener = null
            textPersonAddress.keyListener = null
        }
        textPersonBirthday.isEnabled = enable
        textPersonGender.isEnabled = enable
    }

    fun updateProfile() {
        if (itemUser == null) {
            initLogin()
            return
        }

        val fullName = textPersonFullName.text.toString().trim()
        if (fullName.isEmpty()) {
            textPersonFullName.requestFocus()
            (activity as? BaseActivity)?.showToast(R.string.msg_fullname_empty)
            return
        }
        val address = textPersonAddress.text.toString().trim()
        val gender = textPersonGender.text.toString()
        var birthday = textPersonBirthday.text.toString()
        birthday = if (birthday.equals(BIRTH_DAY_FORMAT)) "" else birthday

        var genderId = -1
        if (!gender.isEmpty()) {
            when (gender) {
                getString(R.string.male) -> genderId = 1
                getString(R.string.female) -> genderId = 2
                getString(R.string.other) -> genderId = 3
            }
        }

        (activity as? BaseActivity)?.showLoadingDialog(true, object : OnCancelListener {
            override fun onCancel(isCancel: Boolean) {
                myHttpRequestUpdateProfile?.cancel()
            }
        })
        if (myHttpRequestUpdateProfile == null) {
            myHttpRequestUpdateProfile = MyHttpRequest(activity!!)
        } else {
            myHttpRequestUpdateProfile!!.cancel()
        }
        val requestParams = RequestParams()
        requestParams.put("fullname", fullName)
        requestParams.put("birthday", birthday)
        requestParams.put("address", address)
        requestParams.put("access_token", itemUser!!.accessToken)
        if (genderId != -1) {
            requestParams.put("gender", "$genderId")
        }
        ServiceUtil.updateUserProfile(activity!!, requestParams, object : OnBaseRequestListener {
            override fun onResponse(isSuccess: Boolean, message: String?, response: String?) {
                (activity as? BaseActivity)?.hideLoadingDialog()
                activity!!.runOnUiThread { (activity as? BaseActivity)?.showToast(message!!) }
                if (isSuccess) {
                    itemUser!!.fullname = fullName
                    itemUser!!.address = address
                    itemUser!!.birthday = birthday
                    itemUser!!.gender = genderId
                    activity!!.runOnUiThread {
                        buttonCancelEdit.visibility = View.GONE
                        buttonEdit.text = getString(R.string.edit_profile)
                        textLogin.text = fullName
                    }
                }
            }
        })
    }

    var datePickerDialog: DatePickerDialog? = null
    val BIRTH_DAY_FORMAT = "YYYY-MM-dd"
    fun showBirthdayPicker() {
        if (datePickerDialog != null) {
            datePickerDialog!!.show()
            return
        }
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val date = calendar.get(Calendar.DAY_OF_MONTH)
        datePickerDialog =
            DatePickerDialog(activity!!, object : DatePickerDialog.OnDateSetListener {
                override fun onDateSet(
                    datePicker: DatePicker?,
                    year: Int,
                    monthOfYear: Int,
                    dayOfMonth: Int
                ) {
                    if (datePicker != null) {
                        if (!datePicker.isShown) {
                            return
                        }
                    }
                    val calendarSelected = Calendar.getInstance()
                    calendarSelected[Calendar.YEAR] = year
                    calendarSelected[Calendar.MONTH] = monthOfYear
                    calendarSelected.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    val sdf = SimpleDateFormat(BIRTH_DAY_FORMAT, Locale.ENGLISH)
                    val dateDisplay = sdf.format(calendarSelected.time)
                    textPersonBirthday.text = dateDisplay
                }

            }, year, month, date)
        val distance = year - 13//from 13 years old and up
        val calendarMaxDate = Calendar.getInstance()
        calendarMaxDate.set(Calendar.YEAR, distance)
        calendarMaxDate.set(Calendar.MONTH, 11)
        calendarMaxDate.set(Calendar.DAY_OF_MONTH, 31)
//        calendarMaxDate.add(Calendar.YEAR, -12)
        val calendarMinDate = Calendar.getInstance()
        calendarMinDate.set(Calendar.YEAR, (distance - 100))// distance from min to max = 100 year
        calendarMinDate.set(Calendar.MONTH, 0)
        calendarMinDate.set(Calendar.DAY_OF_MONTH, 1)
        datePickerDialog!!.datePicker.maxDate = calendarMaxDate.timeInMillis
        datePickerDialog!!.datePicker.minDate = calendarMinDate.timeInMillis
        datePickerDialog!!.show()
    }

    fun showChangeAvatarPopup() {
        val itemList: ArrayList<PowerMenuItem> = ArrayList()
        itemList.add(PowerMenuItem(getString(R.string.select_from_gallery)))
        itemList.add(PowerMenuItem(getString(R.string.select_from_camera)))

        configPopup(
            POPUP_TYPE_AVATAR,
            popupMenuChangeAvatar,
            itemList,
            getString(R.string.change_avatar),
            imageAvatar,
            MenuAnimation.SHOWUP_TOP_LEFT,
            object :
                OnSimpleListviewListener {
                override fun onItemClick(position: Int, title: String?) {
                    if (title.equals(getString(R.string.select_from_gallery))) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(
                                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                                Constant.REQUEST_CODE_GALLERY
                            )
                            return
                        }
                        openGallery()
                        return
                    }
                    if (title.equals(getString(R.string.select_from_camera))) {
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                            requestPermissions(
                                arrayOf(Manifest.permission.CAMERA),
                                Constant.REQUEST_CODE_CAMERA
                            )
                            return
                        }
                        openCamera()
                    }
                }
            })
    }

    fun isPopupMenuChangeAvatarShowing(): Boolean {
        if (popupMenuChangeAvatar == null || !popupMenuChangeAvatar!!.isShowing()) {
            return false
        }
        popupMenuChangeAvatar!!.dismiss()
        return true
    }

    fun showChangeGenderPopup() {
        val itemList: ArrayList<PowerMenuItem> = ArrayList()
        itemList.add(PowerMenuItem(getString(R.string.male)))
        itemList.add(PowerMenuItem(getString(R.string.female)))
        itemList.add(PowerMenuItem(getString(R.string.other)))
        configPopup(
            POPUP_TYPE_GENDER,
            popupMenuChangeGender,
            itemList,
            getString(R.string.please_select),
            textPersonGender,
            MenuAnimation.SHOWUP_TOP_RIGHT,
            object : OnSimpleListviewListener {
                override fun onItemClick(position: Int, title: String?) {
                    textPersonGender.text = title
                }
            })
    }

    fun isPopupMenuChangeGenderShowing(): Boolean {
        if (popupMenuChangeGender == null || !popupMenuChangeGender!!.isShowing()) {
            return false
        }
        popupMenuChangeGender!!.dismiss()
        return true
    }

    val POPUP_TYPE_AVATAR = 1
    val POPUP_TYPE_GENDER = 2
    fun configPopup(
        popupType: Int,
        _popupMenu: PowerMenu?,
        itemList: ArrayList<PowerMenuItem>,
        titlePopup: String?,
        viewAnchor: View,
        menuAnimation: MenuAnimation,
        onSimpleListviewListener: OnSimpleListviewListener?
    ) {
        var popupMenu = _popupMenu
        if (popupMenu != null) {
            popupMenu.showAsDropDown(viewAnchor)
            return
        }
        val textView = TextView(activity)
        if (!titlePopup.isNullOrEmpty()) {
            textView.textSize = 17f
            textView.setTextColor(ContextCompat.getColor(activity!!, R.color.color_primary_dark))
            textView.text = titlePopup
            textView.setPadding(25, 15, 15, 15)
            textView.setBackgroundColor(Color.parseColor("#f8f8f8"))
            textView.setTypeface(Typeface.create("sans-serif-medium", Typeface.BOLD))
        } else {
            textView.visibility = View.GONE
        }

        val screenSize = ScreenSize(activity!!)
        val popupWidth = screenSize.width * 2 / 3
//        val popupHeight = screenSize.height / 2
        popupMenu = PowerMenu.Builder(activity!!)
            .addItemList(itemList)
            .setAnimation(menuAnimation)
            .setShowBackground(true)
            .setWidth(popupWidth)
//                .setHeight(popupHeight)
            .setMenuRadius(10f)
            .setMenuShadow(10f)
            .setDividerHeight(1)
            .setDivider(ColorDrawable(ContextCompat.getColor(activity!!, R.color.dark_text)))
            .setCircularEffect(CircularEffect.BODY)
            .setTextSize(16)
            .setTextGravity(Gravity.START)
            .setTextColor(ContextCompat.getColor(activity!!, R.color.dark_text))
            .setTextTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL))
            .setSelectedTextColor(ContextCompat.getColor(activity!!, R.color.dark_text))
            .setMenuColor(Color.parseColor("#f8f8f8"))
            .setSelectedMenuColor(Color.parseColor("#f8f8f8"))
            .setOnMenuItemClickListener(object : OnMenuItemClickListener<PowerMenuItem> {
                override fun onItemClick(position: Int, item: PowerMenuItem?) {
                    onSimpleListviewListener?.onItemClick(position, item?.title.toString())
                    popupMenu!!.dismiss()
                }

            })
            /*.setOnMenuItemClickListener(object : OnMenuItemClickListener<PowerMenuItem> {
                override fun onItemClick(position: Int, item: PowerMenuItem?) {
                    onSimpleListviewListener?.onItemClick(position, item?.title)
                    popupMenu!!.dismiss()
                }
            })*/
            .setHeaderView(textView)
            .build()
        popupMenu!!.showAsDropDown(viewAnchor)
        if (popupType == POPUP_TYPE_AVATAR) {
            popupMenuChangeAvatar = popupMenu
        } else if (popupType == POPUP_TYPE_GENDER) {
            popupMenuChangeGender = popupMenu
        }
    }

    fun openGallery() {
//        val intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "image/*"
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
//        intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
        startActivityForResult(intent, Constant.REQUEST_CODE_GALLERY)
    }

    private fun createImageFile(): File? {
        val fileName = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = activity!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        if (storageDir == null) {
            return null
        }
        return File.createTempFile(fileName, ".jpg", storageDir)
    }

    var photoFile: File? = null
    fun openCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(activity!!.packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            if (photoFile != null) {
                val photoURI: Uri = FileProvider.getUriForFile(
                    activity!!, activity!!.packageName + ".provider",
                    photoFile
                )
                this.photoFile = photoFile
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(intent, Constant.REQUEST_CODE_CAMERA)
            }
        }
    }

    fun callPermissionSettings(requestCode: Int) {
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.fromParts("package", activity!!.packageName, null)
        intent.data = uri
        startActivityForResult(intent, requestCode)
    }

    fun uploadImageToServer(filePath: String) {
        if (myHttpRequestUploadImage == null) {
            myHttpRequestUploadImage = MyHttpRequest(activity!!)
        } else {
            myHttpRequestUploadImage!!.cancel()
        }

        val itemUser = MyApplication.getInstance()?.getDataManager()?.itemUser
        if (itemUser == null) {
            return
        }
        (activity as? BaseActivity)?.showLoadingDialog(false, null)
        val token = itemUser.accessToken
        val requestParams = RequestParams()
        requestParams.put("access_token", token)
        requestParams.put("image_file", filePath)
        myHttpRequestUploadImage!!.request(
            true,
            ServiceUtil.validAPI(Constant.API_USER_CHANGE_AVATAR),
            requestParams,
            object : MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    if (isDetached) {
                        return
                    }
                    activity!!.runOnUiThread {
                        (activity as? BaseActivity)?.hideLoadingDialog()
                    }
                }

                override fun onSuccess(statusCode: Int, responseStr: String?) {
                    if (isDetached) {
                        return
                    }
                    activity!!.runOnUiThread {
                        (activity as? BaseActivity)?.hideLoadingDialog()
                    }
                    if (responseStr.isNullOrEmpty()) {
                        showDialogUI(getString(R.string.msg_empty_data))
                        return
                    }
                    val responseString = responseStr.trim()
                    val jsonObject = JsonParser.getJsonObject(responseString);
                    if (jsonObject == null) {
                        showDialogUI(getString(R.string.msg_empty_data))
                        return
                    }
                    val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
                    val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
                    if (errorCode != Constant.SUCCESS) {
                        showDialogUI(message)
                        return
                    }
                    val avatar = JsonParser.getString(jsonObject, "result")
                    itemUser.avatar = avatar
                    message?.let {
                        activity!!.runOnUiThread {
//                    MyApplication.getInstance()?.loadImage(activity!!, imageAvatar, avatar, true)
                            (activity as? BaseActivity)?.showToast(it)
                        }
                    }
                }

            })
    }

    fun showDialogUI(message: String?) {
        activity!!.runOnUiThread { (activity as? BaseActivity)?.showDialog(message) }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            Constant.REQUEST_CODE_GALLERY, Constant.REQUEST_CODE_CAMERA -> {
                for (permission in permissions) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            activity!!,
                            permission
                        )
                    ) {
                        (activity as? BaseActivity)?.showDialog(getString(R.string.msg_accept_permission))
                        break
                    } else {
                        if (ActivityCompat.checkSelfPermission(
                                activity!!,
                                permission
                            ) == PackageManager.PERMISSION_GRANTED
                        ) {
                            if (requestCode == Constant.REQUEST_CODE_GALLERY) {
                                openGallery()
                            } else if (requestCode == Constant.REQUEST_CODE_CAMERA) {
                                openCamera()
                            }
                        } else {
                            callPermissionSettings(requestCode)
                        }
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        when (requestCode) {
            Constant.CODE_LOGIN -> {
                MyApplication.getInstance()?.getDataManager()?.isNeedRefreshLeftMenu = true
                initData()
                MyApplication.getInstance()?.getDataManager()?.isNeedRefreshLeftMenu = false
            }
            Constant.REQUEST_CODE_GALLERY -> {
                data?.let {
                    val selectedImage = data.data
                    if (selectedImage == null) {
                        return
                    }
                    /*val filePathColumn = arrayOf(MediaStore.MediaColumns.DATA)
                    val cursor = activity!!.contentResolver
                            .query(selectedImage!!, filePathColumn, null, null, null)
                    if (cursor != null) {
                        cursor.moveToFirst()
                        val columnIndex = cursor
                                .getColumnIndex(filePathColumn[0])
                        val filePath = cursor.getString(columnIndex)
                        cursor.close()
                        uploadImageToServer(filePath)
                    }*/
                    val realPath = fileCompressor.getRealPathFromUri(activity!!, selectedImage)
                    if (realPath.isNullOrEmpty()) {
                        return
                    }
                    photoFile = fileCompressor.compressToFile(File(realPath))
                    MyApplication.getInstance()?.loadImage(activity!!, imageAvatar, photoFile, true)
                    uploadImageToServer(photoFile.toString())
                    photoFile = null
                }
            }
            Constant.REQUEST_CODE_CAMERA -> {
                if (photoFile == null) {
                    return
                }
                photoFile = fileCompressor.compressToFile(photoFile)
                MyApplication.getInstance()?.loadImage(activity!!, imageAvatar, photoFile, true)
                uploadImageToServer(photoFile.toString())
                photoFile = null
            }
        }
    }

    /*fun getRealPathFromUri(contentUri: Uri): String? {
        var cursor: Cursor? = null
        return try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = activity!!.getContentResolver().query(contentUri, proj, null, null, null)
            assert(cursor != null)
            val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            cursor.getString(column_index)
        } finally {
            cursor?.close()
        }
    }*/

    override fun onDestroy() {
        myHttpRequestUploadImage?.cancel()
        myHttpRequestUpdateProfile?.cancel()
        super.onDestroy()
    }
}