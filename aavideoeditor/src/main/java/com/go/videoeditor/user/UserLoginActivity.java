package com.go.videoeditor.user;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.go.videoeditor.R;
import com.go.videoeditor.activity.BaseActivity;
import com.go.videoeditor.activity.MainVideoEditorActivity;
import com.go.videoeditor.app.Constant;
import com.go.videoeditor.app.MyApplication;
import com.go.videoeditor.listener.OnCancelListener;
import com.go.videoeditor.listener.OnDialogButtonListener;
import com.go.videoeditor.service.MyHttpRequest;
import com.go.videoeditor.service.RequestParams;
import com.go.videoeditor.service.ServiceUtil;
import com.go.videoeditor.utils.JsonParser;
import com.go.videoeditor.utils.ScreenSize;
import com.go.videoeditor.utils.SharedPreferencesManager;
import com.go.videoeditor.utils.TextUtil;
import com.skydoves.powermenu.CircularEffect;
import com.skydoves.powermenu.MenuAnimation;
import com.skydoves.powermenu.OnMenuItemClickListener;
import com.skydoves.powermenu.PowerMenu;
import com.skydoves.powermenu.PowerMenuItem;

import org.json.JSONObject;

import java.util.ArrayList;


public class UserLoginActivity extends BaseActivity {
    private ImageView buttonBack;
    private TextView buttonRegisterAccount, buttonLoginFacebook, buttonConfirm,
            buttonForgotPassword;
    private EditText editPhone, editPassword, editProvince;
    private MyHttpRequest myHttpRequest;
    //    private CallbackManager callbackManager;
//    private FirebaseAuth mAuth;
    private String loginType = Constant.TYPE_REGISTER_FORM;
    private String accessTokenSocial = "";
    private String userIdSocial = "";
    private String emailSocial = "";
    private ItemUser itemUserTemp = new ItemUser();
    private boolean resultOk = false;
    private String groupId = "-1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login_ve);
        MyApplication.Companion.getInstance().getDataManager().setItemUserTemp(null);
        initUI();
        initData();
        initControl();

    }

    private void initUI() {
        setFullStatusTransparent();
        buttonBack = findViewById(R.id.buttonBack);
        editPhone = findViewById(R.id.editPhone);
        editPassword = findViewById(R.id.editPassword);
        editProvince = findViewById(R.id.editProvince);
        buttonConfirm = findViewById(R.id.buttonConfirm);
        buttonRegisterAccount = findViewById(R.id.buttonRegisterAccount);
        buttonForgotPassword = findViewById(R.id.buttonForgotPassword);
        buttonLoginFacebook = findViewById(R.id.buttonLoginFacebook);
        TextUtil.setHtmlTextView(getString(R.string.user_form_register), buttonRegisterAccount);
//        setBackgroudActionbarFragment(rootView);
    }

    private void initData() {
//        Bundle bundle = getIntent().getExtras();
//        if (bundle == null) {
//            return;
//        }
//        boolean isRegisterScreen = bundle.getBoolean(Constant.IS_REGISTER, false);
//        if (isRegisterScreen) {
////            goRegisterScreen();
//        }
        editPhone.setText("thanhluan@hungyentv.vn");
        editPassword.setText("123123");
        validButtonConfirm();
        getData();
    }

    private void validForm() {
        String username = editPhone.getText().toString().trim();
        if (username.isEmpty()) {
            editPhone.requestFocus();
            showToast(R.string.msg_username_empty);
            return;
        }
        String password = editPassword.getText().toString().trim();
        if (password.isEmpty()) {
            editPassword.requestFocus();
            showToast(R.string.msg_password_empty);
            return;
        }
        if (groupId.equals("-1")) {
            editProvince.requestFocus();
            showToast(R.string.msg_province_empty);
            return;
        }
        loginType = Constant.TYPE_REGISTER_FORM;
        login(username, password);
    }

    private void login(String username, String password) {
        if (!MyApplication.Companion.getInstance().isNetworkConnect()) {
            showDialogUser(getString(R.string.msg_network_error));
            return;
        }
        showLoadingDialog(true, null, new OnCancelListener() {
            @Override
            public void onCancel(boolean isCancel) {
                if (myHttpRequest != null) {
                    myHttpRequest.cancel();
                }
            }
        });
        if (myHttpRequest == null) {
            myHttpRequest = new MyHttpRequest(this);
        } else {
            myHttpRequest.cancel();
        }
        RequestParams requestParams = new RequestParams();
        String api = Constant.API_USER_LOGIN;
        boolean isPostMethod = true;
        if (loginType.equals(Constant.TYPE_REGISTER_FORM)) {
            api = Constant.API_USER_LOGIN;
//            requestParams.put("phone", username);
//            requestParams.put("password", password);
//            requestParams.put("type", "editor");
//            requestParams.put("email", username);
//            requestParams.put("password", password);

//            requestParams.put("email", username);
//            requestParams.put("password", password);
//            requestParams.put("group_id", groupId);
            requestParams.put("email", "thanhluan@hungyentv.vn");
            requestParams.put("password", "123123");
            requestParams.put("group_id", "2");
        } else if (loginType.equals(Constant.TYPE_REGISTER_FACEBOOK)) {
            api = Constant.API_USER_LOGIN_SOCIAL;
            requestParams.put("social_id", userIdSocial);
            requestParams.put("email", emailSocial);
            if (itemUserTemp != null) {
                requestParams.put("fullname", itemUserTemp.getFullname());
            }
        } else if (loginType.equals(Constant.TYPE_REGISTER_GOOGLE)) {
            api = Constant.API_USER_LOGIN_SOCIAL;
            requestParams.put("social_id", userIdSocial);
            requestParams.put("email", emailSocial);
        }
//        requestParams.put("type", loginType);
        myHttpRequest.request(isPostMethod, ServiceUtil.validAPI(api), requestParams,
                new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                        if (isFinishing() || isDestroyed()) {
                            return;
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoadingDialog();
                                showDialogUser(getString(R.string.msg_network_error));
                            }
                        });
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                        if (isFinishing() || isDestroyed()) {
                            return;
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoadingDialog();
                                handleDataLogin(responseString, username, password);
                            }
                        });
                    }
                });
    }

    private void handleDataLogin(String responseString, String username, String password) {
        if (MyApplication.Companion.getInstance().isEmpty(responseString)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        responseString = responseString.trim();
//        responseString = responseString.substring(responseString.indexOf("{"), responseString.lastIndexOf("}") +1 );
        JSONObject jsonObject = JsonParser.Companion.getJsonObject(responseString);
        if (jsonObject == null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        int errorCode = JsonParser.Companion.getInt(jsonObject, Constant.CODE);
        if (errorCode == Constant.CODE_ENTER_PHONE) {
            Bundle bundle = new Bundle();
            bundle.putString(Constant.TYPE, loginType);
//            gotoActivityForResult(UserEnterPhoneActivity.class, bundle, Constant.CODE_LOGIN);
            return;
        }
        if (errorCode == Constant.CODE_VERIFY_OTP) {
            JSONObject resultObj = JsonParser.Companion.getJsonObject(jsonObject, Constant.DATA);
            int expire = JsonParser.Companion.getInt(resultObj, "expire");
            String phone = JsonParser.Companion.getString(resultObj, "phone");
            Bundle bundle = new Bundle();
            bundle.putString(Constant.TYPE, loginType);
            bundle.putInt(Constant.DATA, expire);
//            bundle.putString(Constant.PHONE, phone);
//            gotoActivityForResult(UserOTPConfirmActivity.class, bundle, Constant.CODE_LOGIN);
            return;
        }
        if (errorCode == Constant.CODE_REGISTER_FORM) {
            Bundle bundle = new Bundle();
            bundle.putString(Constant.TYPE, loginType);
//            gotoActivityForResult(UserRegisterActivity.class, bundle, Constant.CODE_LOGIN);
            return;
        }
        String message = JsonParser.Companion.getString(jsonObject, Constant.MESSAGE);
        if (errorCode != Constant.SUCCESS) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(message);
                }
            });
            return;
        }
        JSONObject resultObj = JsonParser.Companion.getJsonObject(jsonObject, "result");
        ItemUser itemUser = JsonParser.Companion.parseItemUserLogin(resultObj);
        if (itemUser == null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        resultOk = true;
//        SharedPreferencesManager.Companion.saveUser(this, itemUser.getPhone(), itemUser.getAccessToken());
        SharedPreferencesManager.Companion.saveUser(this, itemUser.getEmail(), itemUser.getAccessToken());
       runOnUiThread(() -> {
           startActivity(new Intent(this, MainVideoEditorActivity.class));
       });
//        finish();
    }

    private void validButtonConfirm() {
        String phone = editPhone.getText().toString().trim();
        if (phone.length() < 5) {
            buttonConfirm.setEnabled(false);
            return;
        }
        String password = editPassword.getText().toString().trim();
        if (password.length() < 6) {
            buttonConfirm.setEnabled(false);
            return;
        }

        if (groupId.trim().equals(-1)) {
            buttonConfirm.setEnabled(false);
            return;
        }
        buttonConfirm.setEnabled(true);
    }

    private void initControl() {
        editPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validButtonConfirm();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validButtonConfirm();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        editProvince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenuPopup();
            }
        });
        editProvince.setOnFocusChangeListener((v, hasFocus) -> {
                    if (hasFocus) {
                        showMenuPopup();
                    }
                }
        );
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPhone);
                finish();
            }
        });
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPhone);
                validForm();
            }
        });
        buttonRegisterAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                goRegisterScreen();
            }
        });
        setOnKeyboardVisibilityListener(visible -> {
            if (visible) {
                findViewById(R.id.textScreenUserDescription).setVisibility(View.GONE);
            } else {
                findViewById(R.id.textScreenUserDescription).setVisibility(View.VISIBLE);

            }
        });
        buttonLoginFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                hideKeyboard(editPhone);
//                loginFacebook();
            }
        });
        buttonForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                hideKeyboard(editPhone);
//                Bundle bundle = new Bundle();
//                bundle.putString(Constant.TYPE, Constant.TYPE_FORGOT_PASSWORD);
//                bundle.putString(Constant.PHONE, editPhone.getText().toString().trim());
//                gotoActivityForResult(UserEnterPhoneActivity.class, bundle, Constant.CODE_LOGIN);
            }
        });
    }


    @Override
    public void onDestroy() {
        if (myHttpRequest != null) {
            myHttpRequest.cancel();
        }
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == Constant.REQUEST_CODE_LOGIN_GOOGLE) {
//            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
//            try {
//                GoogleSignInAccount account = task.getResult(ApiException.class);
//                firebaseAuthWithGoogle(account);
//            } catch (ApiException e) {
//                e.printStackTrace();
//            }
//            return;
//        }
//        if (callbackManager != null) {
//            callbackManager.onActivityResult(requestCode, resultCode, data);
//        }
//        if (resultCode == RESULT_OK && requestCode == Constant.CODE_LOGIN) {
//            MyApplication.getInstance()?.getDataManager().setItemUserTemp(null);
//            if (data != null) {
//                Bundle bundle = data.getExtras();
//                if (bundle != null) {
//                    int type = bundle.getInt(Constant.TYPE, Constant.RESULT_CODE_LOGIN_SCREEN);
//                    if (type == Constant.RESULT_CODE_LOGIN_SUCCESS) {
//                        resultOk = true;
//                        finish();
//                    }
//                }
//            }
////            finish();
//        }
    }

    @Override
    public void finish() {
        if (resultOk) {
            Intent intent = new Intent();
//        Bundle bundle = new Bundle();
//        intent.putExtras(bundle);
            setResult(RESULT_OK, intent);
        }
        super.finish();
    }

    private PowerMenu powerMenu;
    private ArrayList<ItemProvince> provinceArrayList;

    private void showMenuPopup() {
        hideKeyboard(editPassword);
        if (powerMenu == null) {
            initPowermenu();
        }
        powerMenu.showAsDropDown(editProvince, 0, 0);
    }

    private void initPowermenu() {
        if (provinceArrayList == null) {
            provinceArrayList = MyApplication.Companion.getInstance().getDataManager().getItemProvinceList();
        }
        if (provinceArrayList == null || provinceArrayList.size() == 0) {
            return;
        }
        ArrayList<PowerMenuItem> itemList = new ArrayList();
        for (int i = 0; i < provinceArrayList.size(); i++) {
            ItemProvince item = provinceArrayList.get(i);
            itemList.add(new PowerMenuItem(item.getName()));
        }
        ScreenSize screenSize = new ScreenSize(this);
        int popupWidth = screenSize.getWidth() * 8 / 10;
        powerMenu = new PowerMenu.Builder(this)
                .addItemList(itemList)
                .setAnimation(MenuAnimation.SHOWUP_BOTTOM_LEFT)
                .setShowBackground(true)
                .setWidth(popupWidth)
//            .setHeight(popupHeight)
                .setMenuRadius(10f)
                .setMenuShadow(10f)
                .setDividerHeight(1)
                .setDivider(new ColorDrawable(ContextCompat.getColor(this, R.color.gray2)))
                .setCircularEffect(CircularEffect.BODY)
                .setTextSize(16)
                .setTextGravity(Gravity.START)
                .setTextColor(ContextCompat.getColor(this, R.color.black))
                .setTextTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL))
                .setSelectedTextColor(ContextCompat.getColor(this, R.color.black))
                .setMenuColor(ContextCompat.getColor(this, R.color.white))
                .setSelectedMenuColor(Color.parseColor("#000000"))
                .setOnMenuItemClickListener(new OnMenuItemClickListener<PowerMenuItem>() {
                                                @Override
                                                public void onItemClick(int position, PowerMenuItem item) {
                                                    powerMenu.dismiss();
                                                    editProvince.setText(provinceArrayList.get(position).getName());
                                                    groupId = provinceArrayList.get(position).getId() + "";
                                                    validButtonConfirm();
                                                }
                                            }
                ).build();
    }

    private void getProvinceData() {
        showLoadingDialog(false, null);
        ServiceUtil.getDataProvince(this, hasData -> {
            if (hasData) {
                runOnUiThread(this::hideLoadingDialog);
            } else {
                runOnUiThread(() -> {
                    hideLoadingDialog();
                    showErrorNetwork();
                });
            }
        });
    }

    private void showErrorNetwork() {
        showDialog(
                false,
                R.string.notification,
                R.string.msg_network_error,
                R.string.close,
                R.string.try_again, new OnDialogButtonListener() {
                    @Override
                    public void onLeftButtonClick() {
                        finish();
                    }

                    @Override
                    public void onRightButtonClick() {
                        getProvinceData();
                    }
                }

        );
    }

    private void getData() {
        if (provinceArrayList == null) {
            provinceArrayList = MyApplication.Companion.getInstance().getDataManager().getItemProvinceList();
        }
        if (provinceArrayList == null || provinceArrayList.size() == 0) {
            getProvinceData();
        } else {
            initPowermenu();
        }
    }

    @Override
    public void onBackPressed() {
        checkExitApp();
        super.onBackPressed();
    }



    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            checkExitApp();
        }
        return super.onKeyUp(keyCode, event);
    }
}