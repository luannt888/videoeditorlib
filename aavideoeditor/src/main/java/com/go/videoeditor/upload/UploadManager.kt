package com.go.videoeditor.upload

import okhttp3.internal.and
import java.io.File
import java.io.UnsupportedEncodingException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.concurrent.ConcurrentHashMap

class UploadManager {

    var currentRequestMap: ConcurrentHashMap<Int, ItemUploadVideo>? = null

    companion object {
        private var instance: UploadManager? = null
        fun getInstance(): UploadManager? {
            if (instance == null) {
                instance = UploadManager()
            }
            return instance
        }
    }

    init {
        currentRequestMap = ConcurrentHashMap()
    }

    fun putItemUpload(itemUpload: ItemUploadVideo?) {
        if (itemUpload == null) {
            return
        }
        cancelUpload(itemUpload.id)
        currentRequestMap?.put(itemUpload.id, itemUpload)
    }

    fun getItemUpload(id: Int): ItemUploadVideo? {
        if (currentRequestMap != null) {
            return currentRequestMap?.get(id)
        } else return null
    }

    fun getUniqueId(filePath: String, title: String): Int {
        val string = filePath + File.separator + title
        val hash: ByteArray
        hash = try {
            MessageDigest.getInstance("MD5").digest(string.toByteArray(charset("UTF-8")))
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException("NoSuchAlgorithmException", e)
        } catch (e: UnsupportedEncodingException) {
            throw RuntimeException("UnsupportedEncodingException", e)
        }
        val hex = StringBuilder(hash.size * 2)
        for (b in hash) {
            if (b and 0xFF < 0x10) hex.append("0")
            hex.append(Integer.toHexString(b and 0xFF))
        }
        return hex.toString().hashCode()
    }

    fun getListItemUpload(): ArrayList<ItemUploadVideo>? {
        var list: ArrayList<ItemUploadVideo> = ArrayList()
        for ((_, item) in currentRequestMap!!) {
            list.add(item)
        }
        return list
    }

    fun cancelAll() {
        for ((id, item) in currentRequestMap!!) {
            item.myHttpRequest?.cancel()
            cancelAndRemove(id)
        }
    }

    fun cancelUpload(uploadId: Int) {
        if (currentRequestMap!!.containsKey(uploadId) && currentRequestMap!!.get(uploadId) != null) {
            var item = currentRequestMap!!.get(uploadId)
            item?.myHttpRequest?.cancel()
        }
    }
    fun cancelAndRemove(uploadId: Int) {
        if (currentRequestMap!!.containsKey(uploadId) && currentRequestMap!!.get(uploadId) != null) {
            var item = currentRequestMap!!.get(uploadId)
            item?.myHttpRequest?.cancel()
        }
        currentRequestMap?.remove(uploadId)
    }
}