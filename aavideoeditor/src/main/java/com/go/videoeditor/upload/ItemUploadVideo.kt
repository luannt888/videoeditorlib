package com.go.videoeditor.upload

import com.go.videoeditor.service.FileRequestBody
import com.go.videoeditor.service.MyHttpRequest

class ItemUploadVideo(var id: Int, var filePath: String?){
    var title: String? = null
     var name: String? = null
    var content: String? = null
    var progressListener: FileRequestBody.FileRequestBodyHttpListener? = null
    var listener: MyHttpRequest.ResponseListener? = null
    var thumbnail: String? = null
    var myHttpRequest:MyHttpRequest? = null
}
