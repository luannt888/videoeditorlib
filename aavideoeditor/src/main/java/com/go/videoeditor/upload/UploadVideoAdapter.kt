package com.go.videoeditor.upload

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.go.videoeditor.R
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.MyApplication
import com.go.videoeditor.fragment.PlayPreviewFragment
import com.go.videoeditor.listener.OnNotifyDataChangeListener
import com.go.videoeditor.service.FileRequestBody
import com.go.videoeditor.service.MyHttpRequest
import com.go.videoeditor.utils.FileUltis
import kotlinx.android.synthetic.main.item_upload_video_ve.view.*
import java.util.*

class UploadVideoAdapter(
    val activity: Context, val itemList: ArrayList<ItemUploadVideo>, val style: Int,
    val
    numberColumn: Int,
) : RecyclerView.Adapter<ViewHolder>() {
    private var isPlaying: Boolean = false
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0
    var onNotifyDataChangeListener:OnNotifyDataChangeListener? = null

    override fun getItemViewType(position: Int): Int {
        val viewType = style
        return viewType
    }

    fun initViewSize(viewType: Int) {
//        val screenSize = ScreenSize(activity)
//        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
//            screenSize.width
        val height: Int = activity.resources.getDimensionPixelSize(R.dimen.dimen_150)
        when (viewType) {
            Constant.STYLE_HORIZONTAL -> {
                imageWidth = height * 2 / 3
                imageHeight = height
            }
        }
//        Loggers.e("initViewSize: $viewType", "$imageWidth x $imageHeight")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initViewSize(viewType)
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_upload_video_ve,
            parent, false
        )
        return VideoViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemObj: ItemUploadVideo = itemList.get(position)
        if (holder is VideoViewHolder) {
            bindDataFrameView(holder, itemObj, position)
        }
    }

    fun bindDataFrameView(holder: VideoViewHolder, itemObj: ItemUploadVideo, position: Int) {
        if (itemObj.thumbnail != null) {
            MyApplication.getInstance()
                ?.loadImage(activity, holder.imageThumbnail, itemObj.thumbnail!!)
        } else {
            MyApplication.getInstance()
                ?.loadImage(activity, holder.imageThumbnail, R.drawable.img_loading)
        }
        holder.textTitle.text = itemObj.title
//        holder.textDuration.text = VideoEditUtils.convertMiliSecondsToTime(itemObj.duration!!)
        holder.layoutRoot.setOnClickListener {
            if (!isPlaying) {
                playPreview(itemObj)
            }
//            onItemClickListener?.onClick(itemObj, position, holder)
        }
        holder.buttonCancel.setOnClickListener {
            resetUpload(itemObj)
            onNotifyDataChangeListener?.onChange()
            onItemClickListener?.onCancelClick()
        }
        setListenerUpload(itemObj, holder)
    }


    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class VideoViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imageThumbnail = itemView.imageThumbnail
        val layoutImage = itemView.layoutImage
        val textTitle = itemView.textTitle
        val textDuration = itemView.textDuration
        val buttonPause = itemView.buttonPause
        val buttonCancel = itemView.buttonDownload
        val layoutDowload = itemView.layoutDowload
        val buttonFolder = itemView.buttonFolder
        val tvProgress = itemView.tvProgress
        val progress = itemView.progressBar
        val layoutDownloaded = itemView.layoutDownloaded
        val tvMsg = itemView.tvMsg
        init {
            buttonCancel.setImageResource(R.drawable.ic_sticker_del)
        }
        /*init {
            val lp: LinearLayout.LayoutParams = layoutImage.layoutParams as LinearLayout
            .LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
//            Log.e("FrameViewHolder", "$imageWidth x $imageHeight")
            layoutImage.layoutParams = lp
        }*/
    }

    interface OnItemClickListener {
        //        fun onClick(itemObject: ItemVideoServer, position: Int, holder: ViewHolder)
        fun onCancelClick()
//        fun onPlayClick(itemObject: ItemVideoServer, position: Int, holder: ViewHolder)
    }

    private fun setListenerUpload(itemObj: ItemUploadVideo, holder: VideoViewHolder) {

        itemObj.progressListener = object : FileRequestBody.FileRequestBodyHttpListener {
            override fun onProgress(readSize: Long, totalSixe: Long) {
                holder.tvMsg.text = "Uploading"
                holder.tvProgress.text = "" + FileUltis.convertByteToMegaByte(
                    readSize,
                    2
                ) + "MB/ " + FileUltis.convertByteToMegaByte(totalSixe, 2) + "MB"
                val progress = (readSize * 100 / (totalSixe.toFloat())).toInt()
                holder.progress.progress = progress
            }
        }
        itemObj.listener = object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                onNotifyDataChangeListener?.onChange()
                resetUpload(itemObj)
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                onNotifyDataChangeListener?.onChange()
                resetUpload(itemObj)
            }
        }
    }

    private fun resetUpload(itemObj: ItemUploadVideo) {
        UploadManager.getInstance()?.cancelAndRemove(itemObj.id)
        itemList.clear()
        itemList.addAll(UploadManager.getInstance()?.getListItemUpload()!!)
        notifyDataSetChanged()
    }

    private fun playPreview(
        itemUploadVideo: ItemUploadVideo
    ) {
        var playPreviewFragment = PlayPreviewFragment()
        val bundle = Bundle()
        bundle.putString(Constant.FILE_PATH, itemUploadVideo.filePath)
        playPreviewFragment!!.arguments = bundle

        var act = activity as FragmentActivity?
        if (act == null || act.isDestroyed || act.isFinishing) {
            return
        }
        playPreviewFragment.show(act.supportFragmentManager, "PlayerView")
        playPreviewFragment.setButtonName("")
        playPreviewFragment.onClickButtonActionListenner = object : View.OnClickListener {
            override fun onClick(v: View?) {
                playPreviewFragment.dismiss()
            }
        }
        playPreviewFragment?.onDiaLogOnDismissedListener = object : DialogInterface {
            override fun cancel() {
            }

            override fun dismiss() {
                isPlaying = false
            }
        }
    }
}