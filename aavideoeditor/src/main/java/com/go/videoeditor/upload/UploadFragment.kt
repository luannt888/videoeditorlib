package com.go.videoeditor.upload

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.downloadVideo.UploadedVideoFragment
import com.go.videoeditor.service.ServiceUtil
import com.go.videoeditor.utils.FileUltis
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_upload_ve.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*
import java.util.*

class UploadFragment : BottomSheetDialogFragment() {

    private var filePath: String? = null
    var url: String? = null
    lateinit var adapter: UploadVideoAdapter
    var itemList: ArrayList<ItemUploadVideo> = ArrayList()
    var accessToken: String? = null

    companion object {
        fun newInstance(accessToken: String, path:String) = UploadFragment().apply {
            arguments = Bundle().apply {
                putString(Constant.ACCESS_TOKEN, accessToken)
                putString(Constant.FILE_PATH, path)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_upload_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog.behavior.skipCollapsed = true
            bottomSheetDialog.behavior.isDraggable = true
            bottomSheetDialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        initView()
        initData()
        initControl()
    }

    private fun initView() {
        textHeaderTitle.text = getString(R.string.upload)
        buttonDone.visibility = View.GONE
        initAdapter()
    }


    var onClickDownloadListenner: View.OnClickListener? = null
    private fun initControl() {
        buttonClose.setOnClickListener {
            dismiss()
        }
        buttonConfirm.setOnClickListener {
            (activity as? BaseActivity)?.hideKeyboard(editContent)
            (activity as? BaseActivity)?.hideKeyboard(editTitle)
            Loggers.e("Upload", "click")
            val name = FileUltis.getVideoNameFileUrlOrPath(filePath)
            val nameFile = if (editNameFile.text.toString().trim()
                    .isNullOrEmpty()
            ) name else editNameFile.text.toString().trim()
            val title = if (editTitle.text.toString().trim().isNullOrEmpty()) name.replace(
                ".mp4",
                ""
            ) else editTitle.text.toString().trim()
            val content = editContent.text.toString().trim()
            val id = UploadManager.getInstance()!!.getUniqueId(filePath!!, title)
            val item = ItemUploadVideo(id, filePath)
            item.name = nameFile
            item.title = title
            item.content = content
            UploadManager.getInstance()?.putItemUpload(item)
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                uploadVideo(item)
                notifyDataChange()
            }, 200)
        }

        buttonUploaded.setOnClickListener {
            uploadedClick()
        }
    }

    fun initData() {
        arguments?.let {
            filePath = it.getString(Constant.FILE_PATH, null)
            accessToken= it.getString(Constant.ACCESS_TOKEN, null)

        }
        if (filePath.isNullOrEmpty()) {
            (activity as? BaseActivity)?.showToast("Không tìm thấy file")
            dismiss()
            return
        }
        val name = FileUltis.getVideoNameFileUrlOrPath(filePath)
        editNameFile.setText(name)
        editTitle.setText(name.replace(".mp4", ""))
        notifyDataChange()
    }

    private fun uploadVideo(item: ItemUploadVideo) {
        ServiceUtil.uploadVideo(item, activity!!,accessToken)
        dismiss()
    }

    fun initAdapter() {
        val style = Constant.STYLE_HORIZONTAL
        adapter = UploadVideoAdapter(activity!!, itemList, style, 1)
        val layoutManager = StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL)
        adapter.onItemClickListener = object : UploadVideoAdapter.OnItemClickListener {
            override fun onCancelClick() {
//                notifyDataChange()
            }
        }
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    fun notifyDataChange() {
        itemList.clear()
        itemList.addAll(UploadManager.getInstance()?.getListItemUpload()!!)
        adapter?.notifyDataSetChanged()
    }

    var uploadedVideoFragment: UploadedVideoFragment? = null
    fun uploadedClick() {
        if (uploadedVideoFragment == null) {
            uploadedVideoFragment = UploadedVideoFragment()
        }
        Handler(Looper.getMainLooper()).postDelayed(object : Runnable {
            override fun run() {
                uploadedVideoFragment?.show(
                    (activity as FragmentActivity)!!.supportFragmentManager,
                    uploadedVideoFragment?.tag
                )

            }
        }, 200)
    }
}