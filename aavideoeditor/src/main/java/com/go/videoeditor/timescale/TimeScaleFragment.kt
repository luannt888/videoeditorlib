package com.go.videoeditor.timescale

import android.app.Dialog
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.go.videoeditor.R
import com.go.videoeditor.activity.BaseActivity
import com.go.videoeditor.activity.VideoEditorActivity
import com.go.videoeditor.app.Constant
import com.go.videoeditor.app.Loggers
import com.go.videoeditor.model.ItemCmd
import com.google.android.exoplayer2.PlaybackParameters
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_time_scale_ve.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_header_ve.*

class TimeScaleFragment : BottomSheetDialogFragment() {
    var player: ExoPlayer? = null
    var currentTimeScale = 1f

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_time_scale_ve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog.behavior.skipCollapsed = true
            bottomSheetDialog.behavior.isDraggable = false
            bottomSheetDialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        initData()
        initControl()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }

    fun initData() {
        val filePath = arguments?.getString(Constant.FILE_PATH, null)
        if (filePath.isNullOrEmpty()) {
            (activity as? BaseActivity)?.showToast("Không tìm thấy file")
            dismiss()
            return
        }
        textHeaderTitle.text = getString(R.string.item_time_scale)
        player = (activity as VideoEditorActivity).playerFragment?.player
        seekBar.setIndicatorTextFormat("\${PROGRESS}x")
        seekBar.setDecimalScale(2)
        val tickSeekbarTitle = arrayOf("0x", "1x", "2x", "3x", "4x", "5x", "6x", "7x", "8x")
        seekBar.customTickTexts(tickSeekbarTitle)
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            currentTimeScale = player!!.playbackParameters.speed
            seekBar.setProgress(currentTimeScale)
        }, 700)
    }

    fun initControl() {
        buttonClose.setOnClickListener {
            setTimeScalePlayer(currentTimeScale)
            dismiss()
        }

        buttonDone.setOnClickListener {
            player!!.playWhenReady = false
            var currentProgress = seekBar.progressFloat
            if (currentProgress < minValue) {
                currentProgress = minValue
            }
            setTimeScalePlayer(currentProgress)
            val cmd = "$currentProgress"
            (activity as VideoEditorActivity).addCmd(ItemCmd(Constant.KEY_TIME_SCALE, cmd))
            dismiss()
        }

        buttonPause.setOnClickListener {
            playVideoOrPause()
        }
    }

    fun playVideoOrPause() {
        val currentProgress = seekBar.progressFloat
//        Log.e("playVideoOrPause", "currentProgress = $currentProgress")
        setTimeScalePlayer(currentProgress)
        player!!.playWhenReady = !player!!.playWhenReady
        setPlayPauseViewIcon(player!!.isPlaying())
    }

    val minValue = 0.125f
    fun setTimeScalePlayer(value: Float) {
        var newValue = value
        if (newValue < minValue) {
            newValue = minValue
        }
        Loggers.e("setTimeScalePlayer", "$newValue")
        val param = PlaybackParameters(newValue)
        player!!.setPlaybackParameters(param)
    }

    private fun setPlayPauseViewIcon(isPlaying: Boolean) {
        buttonPause.setImageResource(if (isPlaying) R.drawable.exo_icon_pause else R.drawable.exo_icon_play)
    }

    override fun dismiss() {
        player!!.playWhenReady = false
        (activity as VideoEditorActivity).showButtonPlay(true)
        super.dismiss()
    }
}